-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2017 at 04:13 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nsim2`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_config`
--

CREATE TABLE IF NOT EXISTS `app_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_config`
--

INSERT INTO `app_config` (`key`, `value`) VALUES
('address', 'Jl. Dimana mana'),
('barcode_content', 'id'),
('barcode_first_row', 'category'),
('barcode_font', 'Arial'),
('barcode_font_size', '10'),
('barcode_generate_if_empty', '0'),
('barcode_height', '50'),
('barcode_num_in_row', '2'),
('barcode_page_cellspacing', '20'),
('barcode_page_width', '100'),
('barcode_quality', '100'),
('barcode_second_row', 'item_code'),
('barcode_third_row', 'unit_price'),
('barcode_type', 'Code39'),
('barcode_width', '250'),
('company', 'Asih Growing Guna'),
('company_logo', ''),
('currency_decimals', '2'),
('currency_side', '0'),
('currency_symbol', 'Rp. '),
('dateformat', 'd/m/Y'),
('decimal_point', '.'),
('default_sales_discount', '0'),
('default_tax_rate', '8'),
('email', 'inoreds@gmail.com'),
('fax', ''),
('invoice_default_comments', 'This is a default comment'),
('invoice_email_message', 'Dear $CU, In attachment the receipt for sale $INV'),
('invoice_enable', '1'),
('language', 'id'),
('lines_per_page', '25'),
('msg_msg', ''),
('msg_pwd', ''),
('msg_src', ''),
('msg_uid', ''),
('phone', '555-555-5555'),
('print_bottom_margin', '0'),
('print_footer', '0'),
('print_header', '0'),
('print_left_margin', '0'),
('print_right_margin', '0'),
('print_silently', '1'),
('print_top_margin', '0'),
('quantity_decimals', '0'),
('receipt_show_description', '1'),
('receipt_show_serialnumber', '1'),
('receipt_show_taxes', '0'),
('receipt_show_total_discount', '1'),
('recv_invoice_format', '$CO'),
('return_policy', 'KebijakanRetur'),
('sales_invoice_format', '$CO'),
('tax_decimals', '2'),
('tax_included', '0'),
('thousands_separator', ''),
('timeformat', 'H:i:s'),
('timezone', 'Asia/Bangkok'),
('use_invoice_template', '1'),
('website', '');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id_city` int(11) NOT NULL AUTO_INCREMENT,
  `name_city` varchar(50) NOT NULL,
  `status_deleted` int(11) NOT NULL,
  PRIMARY KEY (`id_city`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id_city`, `name_city`, `status_deleted`) VALUES
(1, 'Surabaya', 0),
(2, 'Malang', 0),
(3, 'Banyuwangi', 1),
(4, 'Pasuruan', 0);

-- --------------------------------------------------------

--
-- Table structure for table `coa`
--

CREATE TABLE IF NOT EXISTS `coa` (
  `id_coa` int(11) NOT NULL AUTO_INCREMENT,
  `number_coa1` varchar(20) NOT NULL,
  `number_coa2` varchar(20) NOT NULL,
  `number_coa3` varchar(20) NOT NULL,
  `number_coa4` varchar(20) NOT NULL,
  `coa_name` varchar(100) NOT NULL,
  `balance` double NOT NULL,
  `note` varchar(50) NOT NULL,
  `lower_stage` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(30) NOT NULL,
  `custom2` varchar(30) NOT NULL,
  PRIMARY KEY (`id_coa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `coa`
--

INSERT INTO `coa` (`id_coa`, `number_coa1`, `number_coa2`, `number_coa3`, `number_coa4`, `coa_name`, `balance`, `note`, `lower_stage`, `deleted`, `custom1`, `custom2`) VALUES
(1, '01-000-000.000', '', '', '', 'ASET LANCAR', 0, '', 0, 0, '2', '2'),
(2, '01-000-000.000', '01-001-000.000', '', '', 'KAS', 0, '', 0, 0, '12', '2'),
(4, '01-000-000.000', '01-001-001.000', '', '', 'Kas Operasional', 0, '', 0, 0, '12', '2'),
(5, '01-000-000.000', '01-001-002.000', '', '', 'Kas Agro', 0, '', 0, 0, '2', '1'),
(6, '01-000-000.000', '01-001-003.000', '', '', 'Kas Kecil Lahan', 0, '', 0, 0, '2', '1'),
(7, '01-000-000.000', '01-001-003.000', '01-001-003.001', '', 'Kas Kecil Lahan Sucipto', 0, '', 0, 0, '2', '1'),
(8, '01-000-000.000', '01-001-003.000', '01-001-003.002', '', 'Kas Kecil Lahan Toni Setiawan', 0, '', 0, 0, '2', '1'),
(9, '01-000-000.000', '01-001-003.00', '01-001-003.003', '', 'Kas Kecil Lahan Pamuji', 0, '', 0, 0, '2', '1'),
(10, '01-000-000.000', '01-001-003.000', '01-001-003.004', '', 'Kas Kecil Lahan Rudi B W', 0, '', 0, 0, '2', '1'),
(11, '01-000-000.000', '01-001-003.000', '01-001-003.005', '', 'Kas Kecil Lahan M Rofian', 0, '', 0, 0, '2', '1'),
(12, '01-000-000.000', '01-015-000.000', '', '', 'BANK', 0, '', 0, 0, '2', '1'),
(13, '01-000-000.000', '01-001-005.000', '01-001-005.001', '', 'Bank Danamon 3587082060', 0, '', 0, 1, '2', '1'),
(14, '01-000-000.000', '01-015-000.000', '01-015-001.000', '', 'Bank Danamon 3587082060', 0, '', 0, 0, '1', '2'),
(15, '01-000-000.000', '01-015-000.000', '01-015-002.000', '', 'Bank Mandiri 1780019612999', 0, '', 0, 0, '1', '2'),
(16, '01-000-000.000', '01-002-000.000', '', '', 'DEPOSITO BERJANGKA', 0, '', 0, 0, '2', '1'),
(17, '01-000-000.000', '01-002-000.000', '01-002-001.000', '', 'Bank Danamon', 0, '', 0, 0, '1', '2'),
(18, '01-000-000.000', '01-002-000.000', '01-002-002.000', '', 'Bank Mandiri', 0, '', 0, 0, '1', '2'),
(19, '01-000-000.000', '01-003-000.000', '', '', 'PIUTANG USAHA', 0, '', 0, 0, '2', '1'),
(20, '01-000-000.000', '01-003-000.000', '01-003-001.000', '', 'Piutang Usaha', 0, '', 0, 0, '2', '1'),
(21, '01-000-000.000', '01-003-000.000', '01-003-002.000', '', 'Penjualan Tunai', 0, '', 0, 0, '2', '1'),
(22, '01-000-000.000', '01-003-000.000', '01-003-005.000', '', 'Cadangan Piutang Usaha Tak Tertagih', 0, '', 0, 0, '2', '1'),
(23, '01-000-000.000', '01-004-000.000', '', '', 'PIUTANG LAIN-LAIN', 0, '', 0, 0, '2', '1'),
(24, '01-000-000.000', '01-004-000.000', '01-004-001.000', '', 'Piutang Karyawan', 0, '', 0, 0, '2', '1'),
(25, '01-000-000.000', '01-004-000.000', '01-004-002.000', '', 'Piutang Direksi', 0, '', 0, 0, '2', '1'),
(26, '01-000-000.000', '01-004-000.000', '01-004-003.000', '', 'Piutang Pihak Ketiga', 0, '', 0, 0, '2', '1'),
(27, '01-000-000.000', '01-004-000.000', '01-004-004.000', '', 'Piutang Giro', 0, '', 0, 0, '2', '1'),
(28, '01-000-000.000', '01-004-000.000', '01-004-005.000', '', 'Piutang Giro Tolakan', 0, '', 0, 0, '2', '1'),
(29, '01-000-000.000', '01-004-000.000', '01-004-006.000', '', 'Piutang Jamsostek', 0, '', 0, 0, '2', '1'),
(30, '01-000-000.000', '01-004-000.000', '01-004-009.000', '', 'Piutang lain-lain', 0, '', 0, 0, '2', '1'),
(31, '01-000-000.000', '01-005-000.000', '', '', 'PERSEDIAAN', 0, '', 0, 0, '2', '1'),
(32, '01-000-000.000', '01-005-000.000', '01-005-001.000', '', 'Persediaan Agro', 0, '', 0, 0, '2', '1'),
(33, '01-000-000.000', '01-005-000.000', '01-005-001.001', '', 'Persediaan Benih', 0, '', 0, 0, '2', '1'),
(34, '01-000-000.000', '01-005-000.000', '01-005-001.002', '', 'Persediaan Bibit', 0, '', 0, 0, '2', '1'),
(35, '01-000-000.000', '01-005-000.000', '01-005-001.003', '', 'Persediaan Tanaman Semusim', 0, '', 0, 0, '2', '1'),
(36, '01-000-000.000', '01-005-000.000', '01-005-001.004', '', 'Persediaan Hasil Perkebunan', 0, '', 0, 0, '2', '1'),
(37, '01-000-000.000', '01-005-000.000', '01-005-001.005', '', 'Persediaan Tanaman Display', 0, '', 0, 0, '2', '1'),
(38, '01-000-000.000', '01-005-000.000', '01-005-002.000', '', 'Persediaan Pupuk', 0, '', 0, 0, '2', '1'),
(39, '01-000-000.000', '01-005-000.000', '01-005-002.001', '', 'Persediaan Bahan Baku Pupuk', 0, '', 0, 0, '2', '1'),
(40, '01-000-000.000', '01-005-000.000', '01-005-002.002', '', 'Persediaan Bahan Pembantu Pupuk', 0, '', 0, 0, '2', '1'),
(41, '01-000-000.000', '01-005-000.000', '01-005-002.003', '', 'Persediaan Barang Setengah Jadi Pupuk', 0, '', 0, 0, '2', '1'),
(42, '01-000-000.000', '01-005-000.000', '01-005-002.004', '', 'Persediaan Barang Jadi Pupuk', 0, '', 0, 0, '2', '1'),
(43, '01-000-000.000', '01-005-000.000', '01-005-002.004', '01-005-002.004.001', 'Persediaan Barang Jadi Pupuk Organic', 0, '', 0, 0, '2', '1'),
(44, '01-000-000.000', '01-005-000.000', '01-005-002.004', '01-005-002.004.002', 'Persediaan Barang Jadi Pupuk Non Organic', 0, '', 0, 0, '2', '1'),
(45, '01-000-000.000', '01-005-000.000', '01-005-003.000', '', 'Persediaan Lain-Lain', 0, '', 0, 0, '2', '1'),
(46, '01-000-000.000', '01-005-000.000', '01-005-003.001', '', 'Persediaan Konsinyasi', 0, '', 0, 0, '2', '1'),
(47, '01-000-000.000', '01-005-000.000', '01-005-003.002', '', 'Persediaan Dagang', 0, '', 0, 0, '2', '1'),
(48, '01-000-000.000', '01-006-000.000', '', '', 'UANG MUKA PEMBELIAN', 0, '', 0, 0, '2', '1'),
(49, '01-000-000.000', '01-006-000.000', '01-006-001.000', '', 'Uang Muka Pembelian Pupuk', 0, '', 0, 0, '2', '1'),
(50, '01-000-000.000', '01-006-000.000', '01-006-002.000', '', 'Uang Muka Pembelian Bahan Baku', 0, '', 0, 0, '2', '1');

-- --------------------------------------------------------

--
-- Table structure for table `coa_classification`
--

CREATE TABLE IF NOT EXISTS `coa_classification` (
  `id_coa_classification` int(11) NOT NULL AUTO_INCREMENT,
  `name_classification` varchar(100) NOT NULL,
  PRIMARY KEY (`id_coa_classification`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `coa_classification`
--

INSERT INTO `coa_classification` (`id_coa_classification`, `name_classification`) VALUES
(1, 'BANK'),
(2, 'BIAYA DIBAYAR DI MUKA'),
(3, 'BIAYA LAIN'),
(4, 'BIAYA OPERASIONAL'),
(5, 'BIAYA PRODUKSI'),
(6, 'HARTA LAINNYA'),
(7, 'HARTA TETAP BERWUJUD'),
(8, 'HUTANG JANGKA PANJANG'),
(9, 'HUTANG LAINNYA'),
(10, 'HUTANG PAJAK'),
(11, 'INVESTASI JANGKA PANJANG'),
(12, 'KAS'),
(13, 'LABA'),
(14, 'MODAL'),
(15, 'PENDAPATAN DI TERIMA DI MUKA'),
(16, 'PENDAPATAN LUAR USAHA'),
(17, 'PENDAPATAN USAHA'),
(18, 'PENGELUARAN LUAR USAHA'),
(19, 'PERSEDIAAN'),
(20, 'PIUTANG LAINNYA'),
(21, 'PIUTANG NON USAHA'),
(22, 'PIUTANG PAJAK'),
(23, 'PIUTANG USAHA'),
(24, 'PRIVE');

-- --------------------------------------------------------

--
-- Table structure for table `coa_type`
--

CREATE TABLE IF NOT EXISTS `coa_type` (
  `id_coa_type` int(11) NOT NULL AUTO_INCREMENT,
  `name_coa_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id_coa_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `coa_type`
--

INSERT INTO `coa_type` (`id_coa_type`, `name_coa_type`) VALUES
(1, 'BIAYA ATAS PENDAPATAN'),
(2, 'HARTA'),
(3, 'KEWAJIBAN'),
(4, 'MODAL'),
(5, 'PENDAPATAN'),
(6, 'PENDAPATAN LAIN'),
(7, 'PENGELUARAN LAIN'),
(8, 'PENGELUARAN OPERASIONAL');

-- --------------------------------------------------------

--
-- Table structure for table `count_paper`
--

CREATE TABLE IF NOT EXISTS `count_paper` (
  `id_count_paper` int(11) NOT NULL AUTO_INCREMENT,
  `paper_type` varchar(30) NOT NULL,
  `date_paper` date NOT NULL,
  `paper_count` int(11) NOT NULL,
  PRIMARY KEY (`id_count_paper`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `count_paper`
--

INSERT INTO `count_paper` (`id_count_paper`, `paper_type`, `date_paper`, `paper_count`) VALUES
(1, 'BKM', '2017-01-26', 13),
(2, 'BBM', '2017-01-26', 8),
(3, 'BBK', '2017-01-26', 6),
(4, 'BBK', '2017-02-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id_currency` int(11) NOT NULL AUTO_INCREMENT,
  `name_currency` varchar(30) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  `status_deleted` int(11) NOT NULL,
  PRIMARY KEY (`id_currency`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id_currency`, `name_currency`, `symbol`, `status_deleted`) VALUES
(1, 'Rupiah', 'Rp', 0),
(2, 'Dollar', 'USD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(10) NOT NULL,
  `company_code` varchar(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(50) NOT NULL,
  `company_phone` varchar(20) NOT NULL,
  `company_email` varchar(30) NOT NULL,
  `agency_name` varchar(30) NOT NULL,
  `ppn` varchar(20) NOT NULL,
  `no_npwp` varchar(20) NOT NULL,
  `name_npwp` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `date_in` date NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `person_id`, `company_code`, `company_name`, `company_address`, `company_phone`, `company_email`, `agency_name`, `ppn`, `no_npwp`, `name_npwp`, `note`, `date_in`, `account_number`, `taxable`, `discount_percent`, `deleted`) VALUES
(1, 8, '', 'Nusa Indo', '', '', '', '', '', '', '', '', '0000-00-00', 'C-001', 1, 0.00, 0),
(2, 9, 'C-1001', 'PT. Island Globalindo', 'Gresik', '031898939', 'info@globalindo.com', 'Mamat', '', '310390939', 'Joko', '-', '2017-01-05', NULL, 1, 0.00, 0),
(3, 10, 'C-1002', 'PT. Makmur Sejahtera', 'Sidoarjo', '0315849938', 'info@makmur.com', 'Joko', '', '', '', '', '2017-01-05', NULL, 1, 0.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_lpb`
--

CREATE TABLE IF NOT EXISTS `detail_lpb` (
  `id_lpb` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `bruto` double NOT NULL,
  `weight_empty` double NOT NULL,
  `netto` double NOT NULL,
  `unit_price` double NOT NULL,
  `unit_qty` double NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(50) NOT NULL,
  `custom2` varchar(50) NOT NULL,
  `custom3` varchar(50) NOT NULL,
  `custom4` varchar(50) NOT NULL,
  `custom5` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_lpb`
--

INSERT INTO `detail_lpb` (`id_lpb`, `item_id`, `bruto`, `weight_empty`, `netto`, `unit_price`, `unit_qty`, `note`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`) VALUES
('BPB/00001', 11, 222, 34, 200, 3000, 0, '', 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `detail_mr`
--

CREATE TABLE IF NOT EXISTS `detail_mr` (
  `id_mr` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `qty` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_po`
--

CREATE TABLE IF NOT EXISTS `detail_po` (
  `id_po` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_request` double NOT NULL,
  `qty_accept` double NOT NULL,
  `unit_price` double NOT NULL,
  `balance` double NOT NULL,
  `discount` double NOT NULL,
  `tax` double NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_po`
--

INSERT INTO `detail_po` (`id_po`, `item_id`, `qty_request`, `qty_accept`, `unit_price`, `balance`, `discount`, `tax`, `note`, `deleted`, `custom1`, `custom2`) VALUES
('PO/00001', 11, 69, 0, 3000, 207000, 0, 0, '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `detail_so`
--

CREATE TABLE IF NOT EXISTS `detail_so` (
  `id_so` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_request` double NOT NULL,
  `qty_accept` double NOT NULL,
  `unit_price` double NOT NULL,
  `balance` double NOT NULL,
  `discount` double NOT NULL,
  `tax` double NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_so`
--

INSERT INTO `detail_so` (`id_so`, `item_id`, `qty_request`, `qty_accept`, `unit_price`, `balance`, `discount`, `tax`, `note`, `deleted`, `custom1`, `custom2`) VALUES
('SO/00002', 9, 50, 50, 11409, 570450, 0, 0, '', 0, '', ''),
('SO/00003', 9, 2000, 2000, 7545.4545, 15090000, 500, 0, '', 0, '', ''),
('SO/00003', 10, 100000, 100000, 2000, 200000000, 0, 0, '', 0, '', ''),
('SO/00003', 11, 275, 275, 2000, 550000, 0, 0, '', 0, '', ''),
('SO/00003', 12, 176, 176, 2000, 352000, 0, 0, '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `detail_spp`
--

CREATE TABLE IF NOT EXISTS `detail_spp` (
  `id_spp` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_request` double NOT NULL,
  `qty_accept` double NOT NULL,
  `qty_purchased` double NOT NULL,
  `date_accept` date NOT NULL,
  `balance` double NOT NULL,
  `discount` double NOT NULL,
  `tax` varchar(10) NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `cs1` varchar(100) NOT NULL,
  `cs2` varchar(100) NOT NULL,
  `cs3` varchar(100) NOT NULL,
  `cs4` varchar(100) NOT NULL,
  `cs5` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_spp`
--

INSERT INTO `detail_spp` (`id_spp`, `item_id`, `qty_request`, `qty_accept`, `qty_purchased`, `date_accept`, `balance`, `discount`, `tax`, `note`, `deleted`, `cs1`, `cs2`, `cs3`, `cs4`, `cs5`) VALUES
('PR/00001', 9, 30000, 30000, 0, '2017-01-05', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00001', 10, 5000, 5000, 0, '2017-01-05', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00002', 9, 50, 50, 0, '2017-01-09', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00003', 11, 69, 69, 0, '2017-01-18', 0, 0, '', '', 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `detail_suppliers`
--

CREATE TABLE IF NOT EXISTS `detail_suppliers` (
  `id_detail_suppliers` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `unit_price_acc` double NOT NULL,
  `unit_discount` double NOT NULL,
  `provision` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_suppliers`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `detail_suppliers`
--

INSERT INTO `detail_suppliers` (`id_detail_suppliers`, `supplier_id`, `item_id`, `unit_price`, `unit_price_acc`, `unit_discount`, `provision`, `acc`, `deleted`) VALUES
(1, 4, 9, 7000, 0, 0, '', 0, 0),
(2, 4, 10, 6500, 6000, 0, '', 1, 0),
(3, 6, 11, 3500, 3000, 0, '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_ttf`
--

CREATE TABLE IF NOT EXISTS `detail_ttf` (
  `id_ttf` varchar(20) NOT NULL,
  `id_po` varchar(20) NOT NULL,
  `no_invoice` varchar(20) NOT NULL,
  `due_date` date NOT NULL,
  `balance` double NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dn`
--

CREATE TABLE IF NOT EXISTS `dn` (
  `id_dn` varchar(20) NOT NULL,
  `id_mr` varchar(20) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `note` text NOT NULL,
  `person_id` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  PRIMARY KEY (`id_dn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `type_user` int(11) NOT NULL,
  `blocked` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `username` (`username`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`username`, `password`, `person_id`, `type_user`, `blocked`, `deleted`) VALUES
('admin', '0192023a7bbd73250516f069df18b500', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `giftcards`
--

CREATE TABLE IF NOT EXISTS `giftcards` (
  `record_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `giftcard_id` int(11) NOT NULL AUTO_INCREMENT,
  `giftcard_number` int(10) NOT NULL,
  `value` decimal(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `person_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_number` (`giftcard_number`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `grants`
--

CREATE TABLE IF NOT EXISTS `grants` (
  `permission_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  PRIMARY KEY (`permission_id`,`person_id`),
  KEY `grants_ibfk_2` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grants`
--

INSERT INTO `grants` (`permission_id`, `person_id`) VALUES
('config', 1),
('customers', 1),
('employees', 1),
('items', 1),
('items_stock', 1),
('reports', 1),
('reports_categories', 1),
('reports_customers', 1),
('reports_discounts', 1),
('reports_employees', 1),
('reports_inventory', 1),
('reports_items', 1),
('reports_payments', 1),
('reports_receivings', 1),
('reports_sales', 1),
('reports_suppliers', 1),
('reports_taxes', 1),
('sales', 1),
('sales_stock', 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE IF NOT EXISTS `inventory` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_location` int(11) NOT NULL,
  `trans_inventory` int(11) NOT NULL DEFAULT '0',
  `trans_type` int(11) NOT NULL,
  PRIMARY KEY (`trans_id`),
  KEY `trans_items` (`trans_items`),
  KEY `trans_user` (`trans_user`),
  KEY `trans_location` (`trans_location`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_location`, `trans_inventory`, `trans_type`) VALUES
(1, 9, 1, '2017-01-04 17:00:00', 'Input Stok Awal', 1, 0, 0),
(2, 10, 1, '2017-01-05 02:43:44', 'Perubahan jumlah Stok secara manual', 1, 50, 0),
(3, 9, 1, '2017-01-09 03:20:29', 'POS 1', 1, -1, 0),
(4, 10, 1, '2017-01-09 03:20:29', 'POS 1', 1, -1, 0),
(5, 11, 1, '2017-01-17 17:00:00', 'Input Stok Awal', 1, 0, 0),
(6, 12, 1, '2017-01-25 17:00:00', 'Input Stok Awal', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `reorder_level` decimal(15,3) NOT NULL DEFAULT '0.000',
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `pic_id` int(10) DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `custom1` varchar(25) NOT NULL,
  `custom2` varchar(25) NOT NULL,
  `custom3` varchar(25) NOT NULL,
  `custom4` double NOT NULL,
  `custom5` varchar(25) NOT NULL,
  `custom6` varchar(25) NOT NULL,
  `custom7` varchar(25) NOT NULL,
  `custom8` varchar(25) NOT NULL,
  `custom9` varchar(25) NOT NULL,
  `custom10` varchar(25) NOT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `item_number` (`item_number`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `reorder_level`, `receiving_quantity`, `item_id`, `pic_id`, `allow_alt_description`, `is_serialized`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `custom8`, `custom9`, `custom10`) VALUES
('Kawat Seng BWG 8', 'Besi', NULL, 'BG-101', '', 0.00, 7545.45, 0.000, 1.000, 9, NULL, 0, 0, 0, 'Raw Material', '', 'Kg', 8300, '', '', '', '', '', ''),
('Afalan Besi Tua ', 'Besi', NULL, 'BG-102', '', 50000.00, 2000.00, 10.000, 50.000, 10, NULL, 0, 0, 0, 'Raw Material', '', 'Roll', 2200, '', '', '', '', '', ''),
('Fiber Glass tbl 1mm panjang : 2,60 mtr', '', NULL, 'fads', '', 0.00, 0.00, 0.000, 1.000, 11, NULL, 0, 0, 0, 'Raw Material', '', '', 0, '', '', '', '', '', ''),
('afal', '', NULL, 'BG102939', '', 0.00, 2000.00, 0.000, 1.000, 12, NULL, 0, 0, 0, 'Raw Material', '', '', 0, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `items_taxes`
--

CREATE TABLE IF NOT EXISTS `items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  PRIMARY KEY (`item_id`,`name`,`percent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items_taxes`
--

INSERT INTO `items_taxes` (`item_id`, `name`, `percent`) VALUES
(9, '', 0.000),
(11, '', 0.000),
(12, '', 0.000);

-- --------------------------------------------------------

--
-- Table structure for table `item_kits`
--

CREATE TABLE IF NOT EXISTS `item_kits` (
  `item_kit_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`item_kit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_kit_items`
--

CREATE TABLE IF NOT EXISTS `item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL,
  PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  KEY `item_kit_items_ibfk_2` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `item_quantities`
--

CREATE TABLE IF NOT EXISTS `item_quantities` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`item_id`,`location_id`),
  KEY `item_id` (`item_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_quantities`
--

INSERT INTO `item_quantities` (`item_id`, `location_id`, `quantity`) VALUES
(9, 1, 19.000),
(10, 1, 49.000),
(11, 1, 20.000),
(12, 1, 20.000);

-- --------------------------------------------------------

--
-- Table structure for table `lpb`
--

CREATE TABLE IF NOT EXISTS `lpb` (
  `id_lpb` varchar(20) NOT NULL,
  `id_po` varchar(20) NOT NULL,
  `no_sj` varchar(20) NOT NULL,
  `number_plate` varchar(20) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `note` text NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `custom1` varchar(30) NOT NULL,
  `custom2` varchar(30) NOT NULL,
  `custom3` varchar(30) NOT NULL,
  `custom4` varchar(30) NOT NULL,
  PRIMARY KEY (`id_lpb`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lpb`
--

INSERT INTO `lpb` (`id_lpb`, `id_po`, `no_sj`, `number_plate`, `date_transaction`, `date_input`, `note`, `acc`, `deleted`, `person_id`, `custom1`, `custom2`, `custom3`, `custom4`) VALUES
('BPB/00001', 'PO/00001', '123t566', 'L123KL', '2017-01-18', '2017-01-18', 'oki', 1, 0, 0, '1', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  UNIQUE KEY `name_lang_key` (`name_lang_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES
('module_config', 'module_config_desc', 110, 'config'),
('module_customers', 'module_customers_desc', 10, 'customers'),
('module_employees', 'module_employees_desc', 80, 'employees'),
('module_items', 'module_items_desc', 20, 'items'),
('module_reports', 'module_reports_desc', 50, 'reports'),
('module_sales', 'module_sales_desc', 70, 'sales');

-- --------------------------------------------------------

--
-- Table structure for table `mr`
--

CREATE TABLE IF NOT EXISTS `mr` (
  `id_mr` varchar(20) NOT NULL,
  `id_lpb` varchar(20) NOT NULL,
  `note` text NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `person_id` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  PRIMARY KEY (`id_mr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE IF NOT EXISTS `people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `type_people` int(11) NOT NULL,
  `person_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`first_name`, `last_name`, `gender`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `type_people`, `person_id`) VALUES
('Mamat', 'Suhimat', NULL, '555-555-5555', 'inoreds@gmail.com', 'Address 1', '', '', '', '', '', '', 0, 1),
('Andy ', 'Tour', NULL, '', '', '', '', '', '', '', '', '', 0, 2),
('ony', 'prabowo', 1, '', '', '', '', '', '', '', '', '', 0, 3),
('fifi', 'faristia', 1, '321', 'adi@ymail.com', 'a', 'b', 'v', 'b', 'b', 'b', 'b', 0, 4),
('jon', '', NULL, '', '', '', '', '', '', '', '', '', 1, 5),
('', '', NULL, '', '', '', '', '', '', '', '', '', 1, 6),
('', '', NULL, '', '', '', '', '', '', '', '', '', 1, 7),
('Ony', 'Prabowo', 1, '087852461990', 'onyprabowo@gmail.com', 'Jl. Dimana Mana', 'Penjaringan', 'Kedung Baruk', 'Jawa Timur', '60297', 'Indonesia', 'Ok Baru', 0, 8),
('PT. Island Globalindo', '', NULL, '', '', '', '', '', '', '', '', '', 1, 9),
('PT. Makmur Sejahtera', '', NULL, '', '', '', '', '', '', '', '', '', 1, 10),
('', '', NULL, '', '', '', '', '', '', '', '', '', 1, 11),
('PT. Panca Jaya Manunggal', '', NULL, '', '', '', '', '', '', '', '', '', 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `permission_id` varchar(255) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `location_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  KEY `module_id` (`module_id`),
  KEY `permissions_ibfk_2` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`permission_id`, `module_id`, `location_id`) VALUES
('config', 'config', NULL),
('customers', 'customers', NULL),
('employees', 'employees', NULL),
('items', 'items', NULL),
('items_stock', 'items', 1),
('reports', 'reports', NULL),
('reports_categories', 'reports', NULL),
('reports_customers', 'reports', NULL),
('reports_discounts', 'reports', NULL),
('reports_employees', 'reports', NULL),
('reports_inventory', 'reports', NULL),
('reports_items', 'reports', NULL),
('reports_payments', 'reports', NULL),
('reports_receivings', 'reports', NULL),
('reports_sales', 'reports', NULL),
('reports_suppliers', 'reports', NULL),
('reports_taxes', 'reports', NULL),
('sales', 'sales', NULL),
('sales_stock', 'sales', 1);

-- --------------------------------------------------------

--
-- Table structure for table `po`
--

CREATE TABLE IF NOT EXISTS `po` (
  `id_po` varchar(20) NOT NULL,
  `id_spp` varchar(20) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `type_op` varchar(30) NOT NULL,
  `consignment` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `total` double NOT NULL,
  `dp` double NOT NULL,
  `tax` double NOT NULL,
  `shipping_cost` double NOT NULL,
  `payment_type` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(50) NOT NULL,
  `custom2` date NOT NULL,
  `custom3` varchar(50) NOT NULL,
  `custom4` varchar(50) NOT NULL,
  `custom5` varchar(50) NOT NULL,
  PRIMARY KEY (`id_po`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `po`
--

INSERT INTO `po` (`id_po`, `id_spp`, `supplier_id`, `date_transaction`, `date_input`, `type_op`, `consignment`, `note`, `total`, `dp`, `tax`, `shipping_cost`, `payment_type`, `acc`, `person_id`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`) VALUES
('PO/00001', 'PR/00003', 6, '2017-01-18', '2017-01-18', 'Lokal', '', 'oje', 207000, 0, 0, 0, '', 1, 0, 0, 'Barang', '2017-01-25', 'oke', '', ''),
('PO/00002', 'PR/00001', 4, '2017-01-26', '2017-01-26', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 1, 0, 'Barang', '0000-00-00', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `receivings`
--

CREATE TABLE IF NOT EXISTS `receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `receiving_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(20) DEFAULT NULL,
  `invoice_number` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`receiving_id`),
  UNIQUE KEY `invoice_number` (`invoice_number`),
  KEY `supplier_id` (`supplier_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `receivings_items`
--

CREATE TABLE IF NOT EXISTS `receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE IF NOT EXISTS `sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `invoice_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `invoice_number` (`invoice_number`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `sale_time` (`sale_time`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`sale_time`, `customer_id`, `employee_id`, `comment`, `invoice_number`, `sale_id`) VALUES
('2017-01-09 03:20:29', NULL, 1, '', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales_items`
--

CREATE TABLE IF NOT EXISTS `sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `sale_id` (`sale_id`),
  KEY `item_id` (`item_id`),
  KEY `item_location` (`item_location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_items`
--

INSERT INTO `sales_items` (`sale_id`, `item_id`, `description`, `serialnumber`, `line`, `quantity_purchased`, `item_cost_price`, `item_unit_price`, `discount_percent`, `item_location`) VALUES
(1, 9, '', '', 1, 1.000, 0.00, 30000.00, 0.00, 1),
(1, 10, '', '', 2, 1.000, 50000.00, 57000.00, 0.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales_items_taxes`
--

CREATE TABLE IF NOT EXISTS `sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `sale_id` (`sale_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_items_taxes`
--

INSERT INTO `sales_items_taxes` (`sale_id`, `item_id`, `line`, `name`, `percent`) VALUES
(1, 9, 1, '', 0.000);

-- --------------------------------------------------------

--
-- Table structure for table `sales_payments`
--

CREATE TABLE IF NOT EXISTS `sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  KEY `sale_id` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_payments`
--

INSERT INTO `sales_payments` (`sale_id`, `payment_type`, `payment_amount`) VALUES
(1, 'Tunai', 87000.00);

-- --------------------------------------------------------

--
-- Table structure for table `sales_suspended`
--

CREATE TABLE IF NOT EXISTS `sales_suspended` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `invoice_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sale_id`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sales_suspended_items`
--

CREATE TABLE IF NOT EXISTS `sales_suspended_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `sale_id` (`sale_id`),
  KEY `item_id` (`item_id`),
  KEY `sales_suspended_items_ibfk_3` (`item_location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_suspended_items_taxes`
--

CREATE TABLE IF NOT EXISTS `sales_suspended_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_suspended_payments`
--

CREATE TABLE IF NOT EXISTS `sales_suspended_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `ip_address`, `data`, `timestamp`) VALUES
('0d5e5ee53b00ee1dea75f014e07bfed653150718', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333538343032303b, 1483584020),
('0fd3d78be553416226dbaa7ee2409f0a3daf4095', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333933313237363b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483931293),
('10c82c8f6600c476b44962898a68aca70be5a585', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333532353438353b, 1483525485),
('1c534eb1f2eaab7ed7d5bc2efb832b02b1c3d83c', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333538353338383b, 1483585388),
('240d5954b8b2e0873a18658bdab892bef5a8e4ce', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333532333132373b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483523154),
('2a7e3146d183a58d42b266021c6d9c4a501952a7', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433313730333b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483431206),
('337703467e9ebb495384e436795933076896b4a1', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438363730353737323b, 1486705772),
('485bf38df6ff46f13f4bb693c33c951ae941fd72', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343732303035343b, 1484720054),
('5df439b5ff38d36b8dee44f93885e3884233f9f9', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353231323632373b, 1485212628),
('5f3b4b30fad762eb4ff5e34f4622497ab0030959', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433303439303b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483430682),
('6e3e36e76c0ca99b3c9ce965c0748e3f1076cfd7', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433303132323b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483430305),
('95918bb47c692dab51a8e9e377719790d6f12ef3', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333538353938363b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b636172747c613a303a7b7d73616c655f6d6f64657c733a343a2273616c65223b73616c655f6c6f636174696f6e7c733a313a2231223b637573746f6d65727c693a2d313b7061796d656e74737c613a303a7b7d73616c65735f696e766f6963655f6e756d6265727c733a313a2230223b, 1483584436),
('963964551cc00a5eab859f99eaea03412b720c47', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433323230363b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b73616c655f6c6f636174696f6e7c733a313a2231223b, 1483429704),
('a03a648ccaa4ec642da38060dbe63ad1fb668e68', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353231323130373b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b636172747c613a313a7b693a313b613a31363a7b733a373a226974656d5f6964223b733a323a223130223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a343a22546f6b6f223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a383a22576972656d657368223b733a31313a226974656d5f6e756d626572223b733a363a2242472d313032223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a363a2234392e303030223b733a353a227072696365223b733a383a2235373030302e3030223b733a353a22746f74616c223b733a383a2235373030302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a31313a2235373030302e3030303030223b7d7d73616c655f6d6f64657c733a343a2273616c65223b73616c655f6c6f636174696f6e7c733a313a2231223b637573746f6d65727c693a2d313b7061796d656e74737c613a303a7b7d73616c65735f696e766f6963655f6e756d6265727c733a313a2230223b, 1485212159),
('b959043f5c7e4b7772aac34c755a8974f86bdb59', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333933313731343b, 1483931714),
('e6ea95b21099d69b3a77aabd53187ed050a454d1', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433313134333b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483431441);

-- --------------------------------------------------------

--
-- Table structure for table `so`
--

CREATE TABLE IF NOT EXISTS `so` (
  `id_so` varchar(30) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `note` text NOT NULL,
  `total` double NOT NULL,
  `discount` double NOT NULL,
  `dpp` double NOT NULL,
  `dp` double NOT NULL,
  `tax` double NOT NULL,
  `shipping_cost` double NOT NULL,
  `grand_total` double NOT NULL,
  `shipping_type` varchar(30) NOT NULL,
  `provision_payment` varchar(30) NOT NULL,
  `payment_type` varchar(30) NOT NULL,
  `person_request` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL,
  `custom4` varchar(100) NOT NULL,
  `custom5` varchar(100) NOT NULL,
  PRIMARY KEY (`id_so`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `so`
--

INSERT INTO `so` (`id_so`, `customer_id`, `sales_id`, `date_transaction`, `date_input`, `note`, `total`, `discount`, `dpp`, `dp`, `tax`, `shipping_cost`, `grand_total`, `shipping_type`, `provision_payment`, `payment_type`, `person_request`, `person_id`, `acc`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`) VALUES
('SO/00001', 2, 0, '2017-01-18', '2017-01-18', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 1, 0, 0, '', '', '', '', ''),
('SO/00002', 1, 0, '2017-01-24', '2017-01-24', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 1, 0, 0, '', '', '', '', ''),
('SO/00003', 2, 0, '2017-01-26', '2017-01-26', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 1, 0, 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `spp`
--

CREATE TABLE IF NOT EXISTS `spp` (
  `id_spp` varchar(20) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `type_spp` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `tax` varchar(10) NOT NULL,
  `person_request` varchar(50) NOT NULL,
  `person_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL,
  `custom4` varchar(100) NOT NULL,
  `custom5` varchar(100) NOT NULL,
  PRIMARY KEY (`id_spp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp`
--

INSERT INTO `spp` (`id_spp`, `date_transaction`, `date_input`, `type_spp`, `note`, `tax`, `person_request`, `person_id`, `deleted`, `acc`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`) VALUES
('PR/00001', '2017-01-05', '2017-01-05', 'Lokal', 'okee', '', 'Andy', 1, 0, 1, 'Barang', '', '', '', ''),
('PR/00002', '2017-01-09', '2017-01-09', 'Lokal', '-p', '', 'wwee', 1, 0, 1, 'Barang', '', '', '', ''),
('PR/00003', '2017-01-18', '2017-01-18', 'Lokal', 'ok', '', 'iko', 1, 0, 1, 'Barang', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `stock_locations`
--

CREATE TABLE IF NOT EXISTS `stock_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `stock_locations`
--

INSERT INTO `stock_locations` (`location_id`, `location_name`, `deleted`) VALUES
(1, 'Toko', 0);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(10) NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_address` varchar(100) DEFAULT NULL,
  `company_phone` varchar(30) DEFAULT NULL,
  `company_email` varchar(30) DEFAULT NULL,
  `agency_name` varchar(100) NOT NULL,
  `account_number` varchar(100) DEFAULT NULL,
  `provision` varchar(50) DEFAULT NULL,
  `ppn` varchar(30) DEFAULT NULL,
  `npwp_name` varchar(30) NOT NULL,
  `pkp` varchar(30) NOT NULL,
  `date_pkp` date NOT NULL,
  `date_in` date NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`supplier_id`),
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `person_id`, `company_code`, `company_name`, `company_address`, `company_phone`, `company_email`, `agency_name`, `account_number`, `provision`, `ppn`, `npwp_name`, `pkp`, `date_pkp`, `date_in`, `acc`, `deleted`) VALUES
(4, 1, '', 'PT. Artha Buana', 'Surabaya', '031678898998', 'info@artha.om', 'Ana', NULL, 'Kredit', NULL, 'Ana', '3567', '2017-01-01', '2017-01-05', 1, 0),
(5, 11, '', 'Andri', '-', '-', '-', '-', NULL, '-', NULL, '-', '-', '2017-01-05', '2017-01-05', 0, 0),
(6, 12, '', 'PT. Panca Jaya Manunggal', '', '', '', '', NULL, '', NULL, '', '', '1970-01-01', '2017-01-18', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `trf`
--

CREATE TABLE IF NOT EXISTS `trf` (
  `id_trf` varchar(30) NOT NULL,
  `id_references` varchar(30) NOT NULL,
  `type_trf` varchar(50) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `received_by` varchar(100) NOT NULL,
  `paid_to` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `acc` int(11) NOT NULL,
  `posted` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL,
  `custom4` varchar(100) NOT NULL,
  PRIMARY KEY (`id_trf`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trf_journal`
--

CREATE TABLE IF NOT EXISTS `trf_journal` (
  `id_trf_journal` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_transaction_trf` varchar(30) NOT NULL,
  `id_coa` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `type_trf_journal` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `debit` double NOT NULL,
  `kredit` double NOT NULL,
  `kurs` double NOT NULL,
  `acc` int(11) NOT NULL,
  `posted` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL,
  PRIMARY KEY (`id_trf_journal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `No_User` int(11) NOT NULL AUTO_INCREMENT,
  `id_office` int(11) NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `hak_akses` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `bagian` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `id_session` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`No_User`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`No_User`, `id_office`, `username`, `password`, `nama_lengkap`, `email`, `hak_akses`, `level`, `bagian`, `blokir`, `id_session`) VALUES
(1, 0, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Ony Prabowo', 'ony.prabowo@jtanzilco.com', '1', 'admin', '1', 'N', 'u3haekgiqhltr61p6ifdmlbll1');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `people` (`person_id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `people` (`person_id`);

--
-- Constraints for table `giftcards`
--
ALTER TABLE `giftcards`
  ADD CONSTRAINT `giftcards_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `people` (`person_id`);

--
-- Constraints for table `grants`
--
ALTER TABLE `grants`
  ADD CONSTRAINT `grants_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`permission_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `grants_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `employees` (`person_id`) ON DELETE CASCADE;

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `employees` (`person_id`),
  ADD CONSTRAINT `inventory_ibfk_3` FOREIGN KEY (`trans_location`) REFERENCES `stock_locations` (`location_id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`person_id`);

--
-- Constraints for table `items_taxes`
--
ALTER TABLE `items_taxes`
  ADD CONSTRAINT `items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON DELETE CASCADE;

--
-- Constraints for table `item_kit_items`
--
ALTER TABLE `item_kit_items`
  ADD CONSTRAINT `item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `item_kits` (`item_kit_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON DELETE CASCADE;

--
-- Constraints for table `item_quantities`
--
ALTER TABLE `item_quantities`
  ADD CONSTRAINT `item_quantities_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `item_quantities_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `stock_locations` (`location_id`);

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permissions_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `stock_locations` (`location_id`) ON DELETE CASCADE;

--
-- Constraints for table `receivings`
--
ALTER TABLE `receivings`
  ADD CONSTRAINT `receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`person_id`),
  ADD CONSTRAINT `receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`person_id`);

--
-- Constraints for table `receivings_items`
--
ALTER TABLE `receivings_items`
  ADD CONSTRAINT `receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `receivings` (`receiving_id`);

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`person_id`),
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`person_id`);

--
-- Constraints for table `sales_items`
--
ALTER TABLE `sales_items`
  ADD CONSTRAINT `sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`sale_id`),
  ADD CONSTRAINT `sales_items_ibfk_3` FOREIGN KEY (`item_location`) REFERENCES `stock_locations` (`location_id`);

--
-- Constraints for table `sales_items_taxes`
--
ALTER TABLE `sales_items_taxes`
  ADD CONSTRAINT `sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `sales_items` (`sale_id`),
  ADD CONSTRAINT `sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`);

--
-- Constraints for table `sales_payments`
--
ALTER TABLE `sales_payments`
  ADD CONSTRAINT `sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`sale_id`);

--
-- Constraints for table `sales_suspended`
--
ALTER TABLE `sales_suspended`
  ADD CONSTRAINT `sales_suspended_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`person_id`),
  ADD CONSTRAINT `sales_suspended_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`person_id`);

--
-- Constraints for table `sales_suspended_items`
--
ALTER TABLE `sales_suspended_items`
  ADD CONSTRAINT `sales_suspended_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `sales_suspended_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `sales_suspended` (`sale_id`),
  ADD CONSTRAINT `sales_suspended_items_ibfk_3` FOREIGN KEY (`item_location`) REFERENCES `stock_locations` (`location_id`);

--
-- Constraints for table `sales_suspended_items_taxes`
--
ALTER TABLE `sales_suspended_items_taxes`
  ADD CONSTRAINT `sales_suspended_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `sales_suspended_items` (`sale_id`),
  ADD CONSTRAINT `sales_suspended_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`);

--
-- Constraints for table `sales_suspended_payments`
--
ALTER TABLE `sales_suspended_payments`
  ADD CONSTRAINT `sales_suspended_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `sales_suspended` (`sale_id`);

--
-- Constraints for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD CONSTRAINT `suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `people` (`person_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
