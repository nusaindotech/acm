-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 24, 2017 at 04:00 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nsim`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_config`
--

CREATE TABLE `app_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_config`
--

INSERT INTO `app_config` (`key`, `value`) VALUES
('address', 'Jl. Dimana mana'),
('barcode_content', 'id'),
('barcode_first_row', 'category'),
('barcode_font', 'Arial'),
('barcode_font_size', '10'),
('barcode_generate_if_empty', '0'),
('barcode_height', '50'),
('barcode_num_in_row', '2'),
('barcode_page_cellspacing', '20'),
('barcode_page_width', '100'),
('barcode_quality', '100'),
('barcode_second_row', 'item_code'),
('barcode_third_row', 'unit_price'),
('barcode_type', 'Code39'),
('barcode_width', '250'),
('company', 'Asih Growing Guna'),
('company_logo', ''),
('currency_decimals', '2'),
('currency_side', '0'),
('currency_symbol', 'Rp. '),
('dateformat', 'd/m/Y'),
('decimal_point', '.'),
('default_sales_discount', '0'),
('default_tax_rate', '8'),
('email', 'inoreds@gmail.com'),
('fax', ''),
('invoice_default_comments', 'This is a default comment'),
('invoice_email_message', 'Dear $CU, In attachment the receipt for sale $INV'),
('invoice_enable', '1'),
('language', 'id'),
('lines_per_page', '25'),
('msg_msg', ''),
('msg_pwd', ''),
('msg_src', ''),
('msg_uid', ''),
('phone', '555-555-5555'),
('print_bottom_margin', '0'),
('print_footer', '0'),
('print_header', '0'),
('print_left_margin', '0'),
('print_right_margin', '0'),
('print_silently', '1'),
('print_top_margin', '0'),
('quantity_decimals', '0'),
('receipt_show_description', '1'),
('receipt_show_serialnumber', '1'),
('receipt_show_taxes', '0'),
('receipt_show_total_discount', '1'),
('recv_invoice_format', '$CO'),
('return_policy', 'KebijakanRetur'),
('sales_invoice_format', '$CO'),
('tax_decimals', '2'),
('tax_included', '0'),
('thousands_separator', ''),
('timeformat', 'H:i:s'),
('timezone', 'Asia/Bangkok'),
('use_invoice_template', '1'),
('website', '');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id_city` int(11) NOT NULL,
  `name_city` varchar(50) NOT NULL,
  `status_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id_city`, `name_city`, `status_deleted`) VALUES
(1, 'jakarta', 0),
(2, 'Malang', 0),
(4, 'Pasuruan', 0),
(6, 'surabaya', 0);

-- --------------------------------------------------------

--
-- Table structure for table `coa`
--

CREATE TABLE `coa` (
  `id_coa` int(11) NOT NULL,
  `number_coa1` varchar(20) NOT NULL,
  `number_coa2` varchar(20) NOT NULL,
  `number_coa3` varchar(20) NOT NULL,
  `number_coa4` varchar(20) NOT NULL,
  `coa_name` varchar(100) NOT NULL,
  `balance` double NOT NULL,
  `note` varchar(50) NOT NULL,
  `lower_stage` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(30) NOT NULL,
  `custom2` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa`
--

INSERT INTO `coa` (`id_coa`, `number_coa1`, `number_coa2`, `number_coa3`, `number_coa4`, `coa_name`, `balance`, `note`, `lower_stage`, `deleted`, `custom1`, `custom2`) VALUES
(1, '01-000-000.000', '', '', '', 'ASET LANCAR', 0, '', 0, 0, '2', '2'),
(2, '01-000-000.000', '01-001-000.000', '', '', 'KAS', 0, '', 0, 0, '12', '2'),
(4, '01-000-000.000', '01-001-001.000', '', '', 'Kas Operasional', 0, '', 0, 0, '12', '2'),
(5, '01-000-000.000', '01-001-002.000', '', '', 'Kas Agro', 0, '', 0, 0, '2', '1'),
(6, '01-000-000.000', '01-001-003.000', '', '', 'Kas Kecil Lahan', 0, '', 0, 0, '2', '1'),
(7, '01-000-000.000', '01-001-003.000', '01-001-003.001', '', 'Kas Kecil Lahan Sucipto', 0, '', 0, 0, '2', '1'),
(8, '01-000-000.000', '01-001-003.000', '01-001-003.002', '', 'Kas Kecil Lahan Toni Setiawan', 0, '', 0, 0, '2', '1'),
(9, '01-000-000.000', '01-001-003.00', '01-001-003.003', '', 'Kas Kecil Lahan Pamuji', 0, '', 0, 0, '2', '1'),
(10, '01-000-000.000', '01-001-003.000', '01-001-003.004', '', 'Kas Kecil Lahan Rudi B W', 0, '', 0, 0, '2', '1'),
(11, '01-000-000.000', '01-001-003.000', '01-001-003.005', '', 'Kas Kecil Lahan M Rofian', 0, '', 0, 0, '2', '1'),
(12, '01-000-000.000', '01-015-000.000', '', '', 'BANK', 0, '', 0, 0, '2', '1'),
(13, '01-000-000.000', '01-001-005.000', '01-001-005.001', '', 'Bank Danamon 3587082060', 0, '', 0, 1, '2', '1'),
(14, '01-000-000.000', '01-015-000.000', '01-015-001.000', '', 'Bank Danamon 3587082060', 0, '', 0, 0, '1', '2'),
(15, '01-000-000.000', '01-015-000.000', '01-015-002.000', '', 'Bank Mandiri 1780019612999', 0, '', 0, 0, '1', '2'),
(16, '01-000-000.000', '01-002-000.000', '', '', 'DEPOSITO BERJANGKA', 0, '', 0, 0, '2', '1'),
(17, '01-000-000.000', '01-002-000.000', '01-002-001.000', '', 'Bank Danamon', 0, '', 0, 0, '1', '2'),
(18, '01-000-000.000', '01-002-000.000', '01-002-002.000', '', 'Bank Mandiri', 0, '', 0, 0, '1', '2'),
(19, '01-000-000.000', '01-003-000.000', '', '', 'PIUTANG USAHA', 0, '', 0, 0, '2', '1'),
(20, '01-000-000.000', '01-003-000.000', '01-003-001.000', '', 'Piutang Usaha', 0, '', 0, 0, '2', '1'),
(21, '01-000-000.000', '01-003-000.000', '01-003-002.000', '', 'Penjualan Tunai', 0, '', 0, 0, '2', '1'),
(22, '01-000-000.000', '01-003-000.000', '01-003-005.000', '', 'Cadangan Piutang Usaha Tak Tertagih', 0, '', 0, 0, '2', '1'),
(23, '01-000-000.000', '01-004-000.000', '', '', 'PIUTANG LAIN-LAIN', 0, '', 0, 0, '2', '1'),
(24, '01-000-000.000', '01-004-000.000', '01-004-001.000', '', 'Piutang Karyawan', 0, '', 0, 0, '2', '1'),
(25, '01-000-000.000', '01-004-000.000', '01-004-002.000', '', 'Piutang Direksi', 0, '', 0, 0, '2', '1'),
(26, '01-000-000.000', '01-004-000.000', '01-004-003.000', '', 'Piutang Pihak Ketiga', 0, '', 0, 0, '2', '1'),
(27, '01-000-000.000', '01-004-000.000', '01-004-004.000', '', 'Piutang Giro', 0, '', 0, 0, '2', '1'),
(28, '01-000-000.000', '01-004-000.000', '01-004-005.000', '', 'Piutang Giro Tolakan', 0, '', 0, 0, '2', '1'),
(29, '01-000-000.000', '01-004-000.000', '01-004-006.000', '', 'Piutang Jamsostek', 0, '', 0, 0, '2', '1'),
(30, '01-000-000.000', '01-004-000.000', '01-004-009.000', '', 'Piutang lain-lain', 0, '', 0, 0, '2', '1'),
(31, '01-000-000.000', '01-005-000.000', '', '', 'PERSEDIAAN', 0, '', 0, 0, '2', '1'),
(32, '01-000-000.000', '01-005-000.000', '01-005-001.000', '', 'Persediaan Agro', 0, '', 0, 0, '2', '1'),
(33, '01-000-000.000', '01-005-000.000', '01-005-001.001', '', 'Persediaan Benih', 0, '', 0, 0, '2', '1'),
(34, '01-000-000.000', '01-005-000.000', '01-005-001.002', '', 'Persediaan Bibit', 0, '', 0, 0, '2', '1'),
(35, '01-000-000.000', '01-005-000.000', '01-005-001.003', '', 'Persediaan Tanaman Semusim', 0, '', 0, 0, '2', '1'),
(36, '01-000-000.000', '01-005-000.000', '01-005-001.004', '', 'Persediaan Hasil Perkebunan', 0, '', 0, 0, '2', '1'),
(37, '01-000-000.000', '01-005-000.000', '01-005-001.005', '', 'Persediaan Tanaman Display', 0, '', 0, 0, '2', '1'),
(38, '01-000-000.000', '01-005-000.000', '01-005-002.000', '', 'Persediaan Pupuk', 0, '', 0, 0, '2', '1'),
(39, '01-000-000.000', '01-005-000.000', '01-005-002.001', '', 'Persediaan Bahan Baku Pupuk', 0, '', 0, 0, '2', '1'),
(40, '01-000-000.000', '01-005-000.000', '01-005-002.002', '', 'Persediaan Bahan Pembantu Pupuk', 0, '', 0, 0, '2', '1'),
(41, '01-000-000.000', '01-005-000.000', '01-005-002.003', '', 'Persediaan Barang Setengah Jadi Pupuk', 0, '', 0, 0, '2', '1'),
(42, '01-000-000.000', '01-005-000.000', '01-005-002.004', '', 'Persediaan Barang Jadi Pupuk', 0, '', 0, 0, '2', '1'),
(43, '01-000-000.000', '01-005-000.000', '01-005-002.004', '01-005-002.004.001', 'Persediaan Barang Jadi Pupuk Organic', 0, '', 0, 0, '2', '1'),
(44, '01-000-000.000', '01-005-000.000', '01-005-002.004', '01-005-002.004.002', 'Persediaan Barang Jadi Pupuk Non Organic', 0, '', 0, 0, '2', '1'),
(45, '01-000-000.000', '01-005-000.000', '01-005-003.000', '', 'Persediaan Lain-Lain', 0, '', 0, 0, '2', '1'),
(46, '01-000-000.000', '01-005-000.000', '01-005-003.001', '', 'Persediaan Konsinyasi', 0, '', 0, 0, '2', '1'),
(47, '01-000-000.000', '01-005-000.000', '01-005-003.002', '', 'Persediaan Dagang', 0, '', 0, 0, '2', '1'),
(48, '01-000-000.000', '01-006-000.000', '', '', 'UANG MUKA PEMBELIAN', 0, '', 0, 0, '2', '1'),
(49, '01-000-000.000', '01-006-000.000', '01-006-001.000', '', 'Uang Muka Pembelian Pupuk', 0, '', 0, 0, '2', '1'),
(50, '01-000-000.000', '01-006-000.000', '01-006-002.000', '', 'Uang Muka Pembelian Bahan Baku', 0, '', 0, 0, '2', '1');

-- --------------------------------------------------------

--
-- Table structure for table `coa_classification`
--

CREATE TABLE `coa_classification` (
  `id_coa_classification` int(11) NOT NULL,
  `name_classification` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa_classification`
--

INSERT INTO `coa_classification` (`id_coa_classification`, `name_classification`) VALUES
(1, 'BANK'),
(2, 'BIAYA DIBAYAR DI MUKA'),
(3, 'BIAYA LAIN'),
(4, 'BIAYA OPERASIONAL'),
(5, 'BIAYA PRODUKSI'),
(6, 'HARTA LAINNYA'),
(7, 'HARTA TETAP BERWUJUD'),
(8, 'HUTANG JANGKA PANJANG'),
(9, 'HUTANG LAINNYA'),
(10, 'HUTANG PAJAK'),
(11, 'INVESTASI JANGKA PANJANG'),
(12, 'KAS'),
(13, 'LABA'),
(14, 'MODAL'),
(15, 'PENDAPATAN DI TERIMA DI MUKA'),
(16, 'PENDAPATAN LUAR USAHA'),
(17, 'PENDAPATAN USAHA'),
(18, 'PENGELUARAN LUAR USAHA'),
(19, 'PERSEDIAAN'),
(20, 'PIUTANG LAINNYA'),
(21, 'PIUTANG NON USAHA'),
(22, 'PIUTANG PAJAK'),
(23, 'PIUTANG USAHA'),
(24, 'PRIVE');

-- --------------------------------------------------------

--
-- Table structure for table `coa_type`
--

CREATE TABLE `coa_type` (
  `id_coa_type` int(11) NOT NULL,
  `name_coa_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa_type`
--

INSERT INTO `coa_type` (`id_coa_type`, `name_coa_type`) VALUES
(1, 'BIAYA ATAS PENDAPATAN'),
(2, 'HARTA'),
(3, 'KEWAJIBAN'),
(4, 'MODAL'),
(5, 'PENDAPATAN'),
(6, 'PENDAPATAN LAIN'),
(7, 'PENGELUARAN LAIN'),
(8, 'PENGELUARAN OPERASIONAL');

-- --------------------------------------------------------

--
-- Table structure for table `count_paper`
--

CREATE TABLE `count_paper` (
  `id_count_paper` int(11) NOT NULL,
  `paper_type` varchar(30) NOT NULL,
  `date_paper` date NOT NULL,
  `paper_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `count_paper`
--

INSERT INTO `count_paper` (`id_count_paper`, `paper_type`, `date_paper`, `paper_count`) VALUES
(1, 'BKM', '2017-01-26', 13),
(2, 'BBM', '2017-01-26', 8),
(3, 'BBK', '2017-01-26', 6),
(4, 'BBK', '2017-02-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id_currency` int(11) NOT NULL,
  `name_currency` varchar(30) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  `status_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id_currency`, `name_currency`, `symbol`, `status_deleted`) VALUES
(1, 'Rupiah', 'Rp', 0),
(2, 'Dollar', 'USD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_code` varchar(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(50) NOT NULL,
  `company_phone` varchar(20) NOT NULL,
  `company_email` varchar(30) NOT NULL,
  `agency_name` varchar(30) NOT NULL,
  `ppn` varchar(20) NOT NULL,
  `no_npwp` varchar(20) NOT NULL,
  `name_npwp` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `date_in` date NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `person_id`, `company_code`, `company_name`, `company_address`, `company_phone`, `company_email`, `agency_name`, `ppn`, `no_npwp`, `name_npwp`, `note`, `date_in`, `account_number`, `taxable`, `discount_percent`, `deleted`) VALUES
(1, 8, '', 'Nusa Indo', '', '', '', '', '', '', '', '', '0000-00-00', 'C-001', 1, '0.00', 0),
(2, 9, 'C-1001', 'PT. Island Globalindo', 'Gresik', '031898939', 'info@globalindo.com', 'Mamat', '', '310390939', 'Joko', '-', '2017-01-05', NULL, 1, '0.00', 0),
(3, 10, 'C-1002', 'PT. Makmur Sejahtera', 'Sidoarjo', '0315849938', 'info@makmur.com', 'Joko', '', '', '', '', '2017-01-05', NULL, 1, '0.00', 0),
(4, 14, 'c-0012', 'sdf', 'sdf', 'df', 'sdf', 'df', '', 'sdf', 'dsf', 'dsf', '2017-03-15', NULL, 1, '0.00', 0),
(5, 15, 'c-003', 'asd', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0),
(6, 16, 'c-006', 'fdsdf', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0),
(7, 17, 'c-00777', 'sdfsdf', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0),
(8, 18, 'c-0066', 'fg', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0),
(9, 19, 'c-0055', 'g', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0),
(10, 20, 'c-0003333', 'f', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0),
(11, 21, 'c-00393', 'adsad', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_lpb`
--

CREATE TABLE `detail_lpb` (
  `id_lpb` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `bruto` double NOT NULL,
  `weight_empty` double NOT NULL,
  `netto` double NOT NULL,
  `unit_price` double NOT NULL,
  `unit_qty` double NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(50) NOT NULL,
  `custom2` varchar(50) NOT NULL,
  `custom3` varchar(50) NOT NULL,
  `custom4` varchar(50) NOT NULL,
  `custom5` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_lpb`
--

INSERT INTO `detail_lpb` (`id_lpb`, `item_id`, `bruto`, `weight_empty`, `netto`, `unit_price`, `unit_qty`, `note`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`) VALUES
('BPB/00001', 11, 222, 34, 200, 3000, 0, '', 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `detail_mr`
--

CREATE TABLE `detail_mr` (
  `id_mr` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `qty` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_ph`
--

CREATE TABLE `detail_ph` (
  `id_ph` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga_qty` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_po`
--

CREATE TABLE `detail_po` (
  `id_po` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_request` double NOT NULL,
  `qty_accept` double NOT NULL,
  `unit_price` double NOT NULL,
  `balance` double NOT NULL,
  `discount` double NOT NULL,
  `tax` double NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_po`
--

INSERT INTO `detail_po` (`id_po`, `item_id`, `qty_request`, `qty_accept`, `unit_price`, `balance`, `discount`, `tax`, `note`, `deleted`, `custom1`, `custom2`) VALUES
('PO/00001', 11, 69, 0, 3000, 207000, 0, 0, '', 0, '', ''),
('PO/00005', 9, 5, 0, 1000, 5000, 0, 0, '', 0, '', ''),
('PO/00007', 10, 5, 0, 6000, 30000, 0, 0, '', 0, '', ''),
('PO/00013', 9, 400, 0, 0, 0, 0, 0, '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `detail_so`
--

CREATE TABLE `detail_so` (
  `id_so` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_request` double NOT NULL,
  `qty_accept` double NOT NULL,
  `unit_price` double NOT NULL,
  `balance` double NOT NULL,
  `discount` double NOT NULL,
  `tax` double NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_so`
--

INSERT INTO `detail_so` (`id_so`, `item_id`, `qty_request`, `qty_accept`, `unit_price`, `balance`, `discount`, `tax`, `note`, `deleted`, `custom1`, `custom2`) VALUES
('SO/00002', 9, 50, 50, 11409, 570450, 0, 0, '', 0, '', ''),
('SO/00003', 9, 2000, 2000, 7545.4545, 15090000, 500, 0, '', 0, '', ''),
('SO/00003', 10, 100000, 100000, 2000, 200000000, 0, 0, '', 0, '', ''),
('SO/00003', 11, 275, 275, 2000, 550000, 0, 0, '', 0, '', ''),
('SO/00003', 12, 176, 176, 2000, 352000, 0, 0, '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `detail_spp`
--

CREATE TABLE `detail_spp` (
  `id_spp` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_request` double NOT NULL,
  `qty_accept` double NOT NULL,
  `qty_purchased` double NOT NULL,
  `date_accept` date NOT NULL,
  `balance` double NOT NULL,
  `discount` double NOT NULL,
  `tax` varchar(10) NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `cs1` varchar(100) NOT NULL,
  `cs2` varchar(100) NOT NULL,
  `cs3` varchar(100) NOT NULL,
  `cs4` varchar(100) NOT NULL,
  `cs5` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_spp`
--

INSERT INTO `detail_spp` (`id_spp`, `item_id`, `qty_request`, `qty_accept`, `qty_purchased`, `date_accept`, `balance`, `discount`, `tax`, `note`, `deleted`, `cs1`, `cs2`, `cs3`, `cs4`, `cs5`) VALUES
('PR/00001', 9, 30000, 30000, 0, '2017-01-05', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00001', 10, 5000, 5000, 0, '2017-01-05', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00002', 9, 50, 50, 0, '2017-01-09', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00003', 11, 69, 69, 0, '2017-01-18', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00004', 9, 5, 5, 0, '2017-03-15', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00005', 10, 5, 5, 0, '2017-03-15', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00006', 10, 44, 44, 0, '2017-03-15', 0, 0, '', '', 0, '', '', '', '', ''),
('PR/00007', 10, 500, 500, 0, '2017-03-21', 0, 0, '', '', 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `detail_suppliers`
--

CREATE TABLE `detail_suppliers` (
  `id_detail_suppliers` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `unit_price_acc` double NOT NULL,
  `unit_discount` double NOT NULL,
  `provision` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_suppliers`
--

INSERT INTO `detail_suppliers` (`id_detail_suppliers`, `supplier_id`, `item_id`, `unit_price`, `unit_price_acc`, `unit_discount`, `provision`, `acc`, `deleted`) VALUES
(1, 4, 9, 7000, 0, 0, '', 0, 0),
(2, 4, 10, 6500, 6000, 0, '', 1, 0),
(3, 6, 11, 3500, 3000, 0, '', 1, 0),
(4, 7, 9, 1000, 1000, 0, '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_ttf`
--

CREATE TABLE `detail_ttf` (
  `id_ttf` varchar(20) NOT NULL,
  `id_po` varchar(20) NOT NULL,
  `no_invoice` varchar(20) NOT NULL,
  `due_date` date NOT NULL,
  `balance` double NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dn`
--

CREATE TABLE `dn` (
  `id_dn` varchar(20) NOT NULL,
  `id_mr` varchar(20) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `note` text NOT NULL,
  `person_id` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `type_user` int(11) NOT NULL,
  `blocked` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`username`, `password`, `person_id`, `type_user`, `blocked`, `deleted`) VALUES
('admin', '0192023a7bbd73250516f069df18b500', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `giftcards`
--

CREATE TABLE `giftcards` (
  `record_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `giftcard_id` int(11) NOT NULL,
  `giftcard_number` int(10) NOT NULL,
  `value` decimal(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `person_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grants`
--

CREATE TABLE `grants` (
  `permission_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grants`
--

INSERT INTO `grants` (`permission_id`, `person_id`) VALUES
('config', 1),
('customers', 1),
('employees', 1),
('items', 1),
('items_stock', 1),
('reports', 1),
('reports_categories', 1),
('reports_customers', 1),
('reports_discounts', 1),
('reports_employees', 1),
('reports_inventory', 1),
('reports_items', 1),
('reports_payments', 1),
('reports_receivings', 1),
('reports_sales', 1),
('reports_suppliers', 1),
('reports_taxes', 1),
('sales', 1),
('sales_stock', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id_home` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `fax` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `no_npwp` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id_home`, `nama`, `alamat`, `fax`, `email`, `no_npwp`, `deleted`) VALUES
('K001', 'PT. ACM', 'Margomulyo', '031394848', 'acm@gmail.com', 1039847744, 0),
('K002', 'PT. ACS', 'Sidoarjo', '031838477', 'acs@gmail.com', 394049944, 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `trans_id` int(11) NOT NULL,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_location` int(11) NOT NULL,
  `trans_inventory` int(11) NOT NULL DEFAULT '0',
  `trans_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_location`, `trans_inventory`, `trans_type`) VALUES
(1, 9, 1, '2017-01-04 17:00:00', 'Input Stok Awal', 1, 0, 0),
(2, 10, 1, '2017-01-05 02:43:44', 'Perubahan jumlah Stok secara manual', 1, 50, 0),
(3, 9, 1, '2017-01-09 03:20:29', 'POS 1', 1, -1, 0),
(4, 10, 1, '2017-01-09 03:20:29', 'POS 1', 1, -1, 0),
(5, 11, 1, '2017-01-17 17:00:00', 'Input Stok Awal', 1, 0, 0),
(6, 12, 1, '2017-01-25 17:00:00', 'Input Stok Awal', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `reorder_level` decimal(15,3) NOT NULL DEFAULT '0.000',
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  `item_id` int(10) NOT NULL,
  `pic_id` int(10) DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `custom1` varchar(25) NOT NULL,
  `custom2` varchar(25) NOT NULL,
  `custom3` varchar(25) NOT NULL,
  `custom4` double NOT NULL,
  `custom5` varchar(25) NOT NULL,
  `custom6` varchar(25) NOT NULL,
  `custom7` varchar(25) NOT NULL,
  `custom8` varchar(25) NOT NULL,
  `custom9` varchar(25) NOT NULL,
  `custom10` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `reorder_level`, `receiving_quantity`, `item_id`, `pic_id`, `allow_alt_description`, `is_serialized`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `custom8`, `custom9`, `custom10`) VALUES
('COKLAT', '', 1, '1', 'COKLAT', '1000.00', '1000.00', '0.000', '1.000', 1, NULL, 0, 0, 0, '', '', '', 0, '', '', '', '', '', ''),
('Kawat Seng BWG 8', 'Besi', NULL, 'BG-101', '', '0.00', '7545.45', '0.000', '1.000', 9, NULL, 0, 0, 0, 'Raw Material', '', 'Kg', 8300, '', '', '', '', '', ''),
('Afalan Besi Tua ', 'Besi', NULL, 'BG-102', '', '50000.00', '2000.00', '10.000', '50.000', 10, NULL, 0, 0, 0, 'Raw Material', '', 'Roll', 2200, '', '', '', '', '', ''),
('Fiber Glass tbl 1mm panjang : 2,60 mtr', '', NULL, 'fads', '', '0.00', '0.00', '0.000', '1.000', 11, NULL, 0, 0, 0, 'Raw Material', '', '', 0, '', '', '', '', '', ''),
('afal', '', NULL, 'BG102939', '', '0.00', '2000.00', '0.000', '1.000', 12, NULL, 0, 0, 0, 'Raw Material', '', '', 0, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `items_taxes`
--

CREATE TABLE `items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items_taxes`
--

INSERT INTO `items_taxes` (`item_id`, `name`, `percent`) VALUES
(9, '', '0.000'),
(11, '', '0.000'),
(12, '', '0.000');

-- --------------------------------------------------------

--
-- Table structure for table `item_kits`
--

CREATE TABLE `item_kits` (
  `item_kit_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `item_kit_items`
--

CREATE TABLE `item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `item_quantities`
--

CREATE TABLE `item_quantities` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL DEFAULT '0.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_quantities`
--

INSERT INTO `item_quantities` (`item_id`, `location_id`, `quantity`) VALUES
(9, 1, '19.000'),
(10, 1, '49.000'),
(11, 1, '20.000'),
(12, 1, '20.000');

-- --------------------------------------------------------

--
-- Table structure for table `konsumen`
--

CREATE TABLE `konsumen` (
  `cust_id` varchar(10) NOT NULL,
  `cust_name` varchar(30) NOT NULL,
  `No_KTP` int(11) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `nohp` varchar(11) NOT NULL,
  `fax` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `pembayaran` int(20) NOT NULL,
  `ppn` int(11) NOT NULL,
  `npwp` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konsumen`
--

INSERT INTO `konsumen` (`cust_id`, `cust_name`, `No_KTP`, `alamat`, `nohp`, `fax`, `email`, `pembayaran`, `ppn`, `npwp`, `acc`, `deleted`) VALUES
('', '', 0, '', '', '', '', 0, 0, '', 0, 0),
('asd', 'asd', 0, 'adf', '453', '345', 'asdas', 0, 0, '', 0, 1),
('C001', 'PT.Samporna', 0, 'Jl. Wiyung Raya', '318838822', '318838822', 'sampoerna@gmail.com', 0, 0, '', 0, 0),
('C002', 'Mafia', 0, 'rungkut', '031992992', '31992992', 'wijaya@gmail.com', 0, 0, '1029383333', 0, 0),
('C003', 'kodel', 0, 'asdl', '0303', '9393', 'a@gmail.com', 0, 0, '234235', 0, 0),
('c004', 'sdf', 0, 'fs', '039', '39', 'asd', 0, 1, '5345', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `lpb`
--

CREATE TABLE `lpb` (
  `id_lpb` varchar(20) NOT NULL,
  `id_po` varchar(20) NOT NULL,
  `no_sj` varchar(20) NOT NULL,
  `number_plate` varchar(20) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `note` text NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `custom1` varchar(30) NOT NULL,
  `custom2` varchar(30) NOT NULL,
  `custom3` varchar(30) NOT NULL,
  `custom4` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lpb`
--

INSERT INTO `lpb` (`id_lpb`, `id_po`, `no_sj`, `number_plate`, `date_transaction`, `date_input`, `note`, `acc`, `deleted`, `person_id`, `custom1`, `custom2`, `custom3`, `custom4`) VALUES
('BPB/00001', 'PO/00001', '123t566', 'L123KL', '2017-01-18', '2017-01-18', 'oki', 1, 0, 0, '1', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES
('module_config', 'module_config_desc', 110, 'config'),
('module_customers', 'module_customers_desc', 10, 'customers'),
('module_employees', 'module_employees_desc', 80, 'employees'),
('module_items', 'module_items_desc', 20, 'items'),
('module_reports', 'module_reports_desc', 50, 'reports'),
('module_sales', 'module_sales_desc', 70, 'sales');

-- --------------------------------------------------------

--
-- Table structure for table `mr`
--

CREATE TABLE `mr` (
  `id_mr` varchar(20) NOT NULL,
  `id_lpb` varchar(20) NOT NULL,
  `note` text NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `person_id` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `type_people` int(11) NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`first_name`, `last_name`, `gender`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `type_people`, `person_id`) VALUES
('Mamat', 'Suhimat', NULL, '555-555-5555', 'inoreds@gmail.com', 'Address 1', '', '', '', '', '', '', 0, 1),
('Andy ', 'Tour', NULL, '', '', '', '', '', '', '', '', '', 0, 2),
('ony', 'prabowo', 1, '', '', '', '', '', '', '', '', '', 0, 3),
('fifi', 'faristia', 1, '321', 'adi@ymail.com', 'a', 'b', 'v', 'b', 'b', 'b', 'b', 0, 4),
('jon', '', NULL, '', '', '', '', '', '', '', '', '', 1, 5),
('', '', NULL, '', '', '', '', '', '', '', '', '', 1, 6),
('', '', NULL, '', '', '', '', '', '', '', '', '', 1, 7),
('Ony', 'Prabowo', 1, '087852461990', 'onyprabowo@gmail.com', 'Jl. Dimana Mana', 'Penjaringan', 'Kedung Baruk', 'Jawa Timur', '60297', 'Indonesia', 'Ok Baru', 0, 8),
('PT. Island Globalindo', '', NULL, '', '', '', '', '', '', '', '', '', 1, 9),
('PT. Makmur Sejahtera', '', NULL, '', '', '', '', '', '', '', '', '', 1, 10),
('', '', NULL, '', '', '', '', '', '', '', '', '', 1, 11),
('PT. Panca Jaya Manunggal', '', NULL, '', '', '', '', '', '', '', '', '', 1, 12),
('Agus', '', NULL, '', '', '', '', '', '', '', '', '', 1, 13),
('sdf', '', NULL, '', '', '', '', '', '', '', '', '', 1, 14),
('asd', '', NULL, '', '', '', '', '', '', '', '', '', 1, 15),
('fdsdf', '', NULL, '', '', '', '', '', '', '', '', '', 1, 16),
('sdfsdf', '', NULL, '', '', '', '', '', '', '', '', '', 1, 17),
('fg', '', NULL, '', '', '', '', '', '', '', '', '', 1, 18),
('g', '', NULL, '', '', '', '', '', '', '', '', '', 1, 19),
('f', '', NULL, '', '', '', '', '', '', '', '', '', 1, 20),
('adsad', '', NULL, '', '', '', '', '', '', '', '', '', 1, 21),
('joko', '', NULL, '', '', '', '', '', '', '', '', '', 1, 22);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `permission_id` varchar(255) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `location_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`permission_id`, `module_id`, `location_id`) VALUES
('config', 'config', NULL),
('customers', 'customers', NULL),
('employees', 'employees', NULL),
('items', 'items', NULL),
('items_stock', 'items', 1),
('reports', 'reports', NULL),
('reports_categories', 'reports', NULL),
('reports_customers', 'reports', NULL),
('reports_discounts', 'reports', NULL),
('reports_employees', 'reports', NULL),
('reports_inventory', 'reports', NULL),
('reports_items', 'reports', NULL),
('reports_payments', 'reports', NULL),
('reports_receivings', 'reports', NULL),
('reports_sales', 'reports', NULL),
('reports_suppliers', 'reports', NULL),
('reports_taxes', 'reports', NULL),
('sales', 'sales', NULL),
('sales_stock', 'sales', 1);

-- --------------------------------------------------------

--
-- Table structure for table `po`
--

CREATE TABLE `po` (
  `id_po` varchar(20) NOT NULL,
  `id_spp` varchar(20) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `type_op` varchar(30) NOT NULL,
  `consignment` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `total` double NOT NULL,
  `dp` double NOT NULL,
  `tax` double NOT NULL,
  `shipping_cost` double NOT NULL,
  `payment_type` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(50) NOT NULL,
  `custom2` date NOT NULL,
  `custom3` varchar(50) NOT NULL,
  `custom4` varchar(50) NOT NULL,
  `custom5` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `po`
--

INSERT INTO `po` (`id_po`, `id_spp`, `supplier_id`, `date_transaction`, `date_input`, `type_op`, `consignment`, `note`, `total`, `dp`, `tax`, `shipping_cost`, `payment_type`, `acc`, `person_id`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`) VALUES
('PO/00001', 'PR/00003', 6, '2017-01-18', '2017-01-18', 'Lokal', '', 'oje', 207000, 0, 0, 0, '', 1, 0, 0, 'Barang', '2017-01-25', 'oke', '', ''),
('PO/00002', 'PR/00001', 4, '2017-01-26', '2017-01-26', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 1, 0, 'Barang', '0000-00-00', '', '', ''),
('PO/00003', 'PR/00004', 7, '2017-03-15', '2017-03-15', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 0, 0, 'Barang', '0000-00-00', '', '', ''),
('PO/00004', 'PR/00004', 7, '2017-03-15', '2017-03-15', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 0, 0, 'Barang', '0000-00-00', '', '', ''),
('PO/00005', 'PR/00004', 7, '2017-03-15', '2017-03-15', 'Lokal', '', 'kk', 5000, 0, 0, 0, '', 1, 0, 0, 'Barang', '2017-03-15', 'kk', '', ''),
('PO/00006', 'PR/00005', 4, '2017-03-15', '2017-03-15', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 0, 0, 'Barang', '0000-00-00', '', '', ''),
('PO/00007', 'PR/00005', 4, '2017-03-15', '2017-03-15', 'Lokal', '', 'f', 30000, 0, 0, 0, '', 1, 0, 0, 'Barang', '2017-03-15', 'f', '', ''),
('PO/00008', 'PR/00006', 4, '2017-03-15', '2017-03-15', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 0, 0, 'Barang', '0000-00-00', '', '', ''),
('PO/00009', 'PR/00007', 4, '2017-03-17', '2017-03-17', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 0, 0, 'Barang', '0000-00-00', '', '', ''),
('PO/00010', 'PR/00006', 4, '2017-03-17', '2017-03-17', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 0, 0, 'Barang', '0000-00-00', '', '', ''),
('PO/00011', 'PR/00001', 4, '2017-03-17', '2017-03-17', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 0, 0, 'Barang', '0000-00-00', '', '', ''),
('PO/00012', 'PR/00002', 4, '2017-03-20', '2017-03-20', 'Lokal', '', '', 0, 0, 0, 0, '', 0, 0, 0, 'Barang', '0000-00-00', '', '', ''),
('PO/00013', 'PR/00001', 4, '2017-03-20', '2017-03-20', 'Lokal', '', 'ss', 0, 0, 0, 0, '', 1, 1, 0, 'Barang', '2017-03-21', 'dd', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `receivings`
--

CREATE TABLE `receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `receiving_id` int(10) NOT NULL,
  `payment_type` varchar(20) DEFAULT NULL,
  `invoice_number` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `receivings_items`
--

CREATE TABLE `receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `invoice_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`sale_time`, `customer_id`, `employee_id`, `comment`, `invoice_number`, `sale_id`) VALUES
('2017-01-09 03:20:29', NULL, 1, '', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales_items`
--

CREATE TABLE `sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_items`
--

INSERT INTO `sales_items` (`sale_id`, `item_id`, `description`, `serialnumber`, `line`, `quantity_purchased`, `item_cost_price`, `item_unit_price`, `discount_percent`, `item_location`) VALUES
(1, 9, '', '', 1, '1.000', '0.00', '30000.00', '0.00', 1),
(1, 10, '', '', 2, '1.000', '50000.00', '57000.00', '0.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales_items_taxes`
--

CREATE TABLE `sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_items_taxes`
--

INSERT INTO `sales_items_taxes` (`sale_id`, `item_id`, `line`, `name`, `percent`) VALUES
(1, 9, 1, '', '0.000');

-- --------------------------------------------------------

--
-- Table structure for table `sales_payments`
--

CREATE TABLE `sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales_payments`
--

INSERT INTO `sales_payments` (`sale_id`, `payment_type`, `payment_amount`) VALUES
(1, 'Tunai', '87000.00');

-- --------------------------------------------------------

--
-- Table structure for table `sales_suspended`
--

CREATE TABLE `sales_suspended` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `invoice_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_suspended_items`
--

CREATE TABLE `sales_suspended_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_suspended_items_taxes`
--

CREATE TABLE `sales_suspended_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_suspended_payments`
--

CREATE TABLE `sales_suspended_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `ip_address`, `data`, `timestamp`) VALUES
('0d5e5ee53b00ee1dea75f014e07bfed653150718', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333538343032303b, 1483584020),
('0fd3d78be553416226dbaa7ee2409f0a3daf4095', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333933313237363b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483931293),
('10c82c8f6600c476b44962898a68aca70be5a585', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333532353438353b, 1483525485),
('1c534eb1f2eaab7ed7d5bc2efb832b02b1c3d83c', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333538353338383b, 1483585388),
('240d5954b8b2e0873a18658bdab892bef5a8e4ce', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333532333132373b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483523154),
('2a7e3146d183a58d42b266021c6d9c4a501952a7', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433313730333b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483431206),
('337703467e9ebb495384e436795933076896b4a1', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438363730353737323b, 1486705772),
('485bf38df6ff46f13f4bb693c33c951ae941fd72', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343732303035343b, 1484720054),
('5df439b5ff38d36b8dee44f93885e3884233f9f9', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353231323632373b, 1485212628),
('5f3b4b30fad762eb4ff5e34f4622497ab0030959', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433303439303b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483430682),
('6e3e36e76c0ca99b3c9ce965c0748e3f1076cfd7', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433303132323b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483430305),
('95918bb47c692dab51a8e9e377719790d6f12ef3', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333538353938363b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b636172747c613a303a7b7d73616c655f6d6f64657c733a343a2273616c65223b73616c655f6c6f636174696f6e7c733a313a2231223b637573746f6d65727c693a2d313b7061796d656e74737c613a303a7b7d73616c65735f696e766f6963655f6e756d6265727c733a313a2230223b, 1483584436),
('963964551cc00a5eab859f99eaea03412b720c47', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433323230363b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b73616c655f6c6f636174696f6e7c733a313a2231223b, 1483429704),
('a03a648ccaa4ec642da38060dbe63ad1fb668e68', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353231323130373b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b636172747c613a313a7b693a313b613a31363a7b733a373a226974656d5f6964223b733a323a223130223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a343a22546f6b6f223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a383a22576972656d657368223b733a31313a226974656d5f6e756d626572223b733a363a2242472d313032223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a363a2234392e303030223b733a353a227072696365223b733a383a2235373030302e3030223b733a353a22746f74616c223b733a383a2235373030302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a31313a2235373030302e3030303030223b7d7d73616c655f6d6f64657c733a343a2273616c65223b73616c655f6c6f636174696f6e7c733a313a2231223b637573746f6d65727c693a2d313b7061796d656e74737c613a303a7b7d73616c65735f696e766f6963655f6e756d6265727c733a313a2230223b, 1485212159),
('b959043f5c7e4b7772aac34c755a8974f86bdb59', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333933313731343b, 1483931714),
('e6ea95b21099d69b3a77aabd53187ed050a454d1', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433313134333b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483431441);

-- --------------------------------------------------------

--
-- Table structure for table `so`
--

CREATE TABLE `so` (
  `id_so` varchar(30) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `note` text NOT NULL,
  `total` double NOT NULL,
  `discount` double NOT NULL,
  `dpp` double NOT NULL,
  `dp` double NOT NULL,
  `tax` double NOT NULL,
  `shipping_cost` double NOT NULL,
  `grand_total` double NOT NULL,
  `shipping_type` varchar(30) NOT NULL,
  `provision_payment` varchar(30) NOT NULL,
  `payment_type` varchar(30) NOT NULL,
  `person_request` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL,
  `custom4` varchar(100) NOT NULL,
  `custom5` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `so`
--

INSERT INTO `so` (`id_so`, `customer_id`, `sales_id`, `date_transaction`, `date_input`, `note`, `total`, `discount`, `dpp`, `dp`, `tax`, `shipping_cost`, `grand_total`, `shipping_type`, `provision_payment`, `payment_type`, `person_request`, `person_id`, `acc`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`) VALUES
('SO/00001', 2, 0, '2017-01-18', '2017-01-18', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 1, 0, 0, '', '', '', '', ''),
('SO/00002', 1, 0, '2017-01-24', '2017-01-24', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 1, 0, 0, '', '', '', '', ''),
('SO/00003', 2, 0, '2017-01-26', '2017-01-26', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 1, 0, 0, '', '', '', '', ''),
('SO/00004', 0, 0, '2017-03-21', '2017-03-21', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 1, 0, 0, '', '', '', '', ''),
('SO/00005', 0, 0, '2017-03-21', '2017-03-21', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 1, 0, 0, '', '', '', '', ''),
('SO/00006', 0, 0, '2017-03-21', '2017-03-21', '', 0, 0, 0, 0, 0, 0, 0, '', '', '', 0, 1, 0, 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `spp`
--

CREATE TABLE `spp` (
  `id_spp` varchar(20) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `type_spp` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `tax` varchar(10) NOT NULL,
  `person_request` varchar(50) NOT NULL,
  `person_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL,
  `custom4` varchar(100) NOT NULL,
  `custom5` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spp`
--

INSERT INTO `spp` (`id_spp`, `date_transaction`, `date_input`, `type_spp`, `note`, `tax`, `person_request`, `person_id`, `deleted`, `acc`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`) VALUES
('PR/00001', '2017-01-05', '2017-01-05', 'Lokal', 'okee', '', 'Andy', 1, 0, 1, 'Barang', '', '', '', ''),
('PR/00002', '2017-01-09', '2017-01-09', 'Lokal', '-p', '', 'wwee', 1, 0, 1, 'Barang', '', '', '', ''),
('PR/00003', '2017-01-18', '2017-01-18', 'Lokal', 'ok', '', 'iko', 1, 0, 1, 'Barang', '', '', '', ''),
('PR/00004', '2017-03-15', '2017-03-15', 'Lokal', '', '', 'agus', 1, 0, 1, 'Barang', '', '', '', ''),
('PR/00005', '2017-03-15', '2017-03-15', 'Lokal', 'ee', '', 'Dedik', 1, 0, 1, 'Barang', '', '', '', ''),
('PR/00006', '2017-03-15', '2017-03-15', 'Lokal', '', '', 'JOKO', 1, 0, 1, 'Barang', '', '', '', ''),
('PR/00007', '2017-03-16', '2017-03-16', 'Lokal', '', '', 'aa', 1, 0, 1, 'Barang', '', '', '', ''),
('PR/00008', '2017-03-17', '2017-03-17', 'Lokal', '', '', 'ddd', 1, 0, 0, 'Barang', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `stock_locations`
--

CREATE TABLE `stock_locations` (
  `location_id` int(11) NOT NULL,
  `location_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stock_locations`
--

INSERT INTO `stock_locations` (`location_id`, `location_name`, `deleted`) VALUES
(1, 'Toko', 0);

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `supplier_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_address` varchar(100) DEFAULT NULL,
  `company_phone` varchar(30) DEFAULT NULL,
  `company_email` varchar(30) DEFAULT NULL,
  `agency_name` varchar(100) NOT NULL,
  `account_number` varchar(100) DEFAULT NULL,
  `provision` varchar(50) DEFAULT NULL,
  `ppn` varchar(30) DEFAULT NULL,
  `npwp_name` varchar(30) NOT NULL,
  `pkp` varchar(30) NOT NULL,
  `date_pkp` date NOT NULL,
  `date_in` date NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `person_id`, `company_code`, `company_name`, `company_address`, `company_phone`, `company_email`, `agency_name`, `account_number`, `provision`, `ppn`, `npwp_name`, `pkp`, `date_pkp`, `date_in`, `acc`, `deleted`) VALUES
(4, 1, 's001', 'PT. Artha Buana', 'Surabaya', '031678898998', 'info@artha.om', 'Ana', NULL, 'Kredit', NULL, 'Ana', '3567', '2017-01-01', '2017-01-05', 1, 0),
(5, 11, '', 'Andri', '-', '-', '-', '-', NULL, '-', NULL, '-', '-', '2017-01-05', '2017-01-05', 0, 0),
(6, 12, '', 'PT. Panca Jaya Manunggal', '', '', '', '', NULL, '', NULL, '', '', '1970-01-01', '2017-01-18', 1, 0),
(7, 13, '', 'Agus', 'sby', '08384484848', 'ag', 'asd', NULL, 'tunai', NULL, 'sdf', 'df', '2017-03-16', '2017-03-15', 1, 0),
(8, 22, '', 'joko', 'sido', '039', 'a@gmail.com', 'pppc', NULL, 'tunai', NULL, 'aa', '3', '2017-03-15', '2017-03-15', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbph`
--

CREATE TABLE `tbph` (
  `id_ph` varchar(10) NOT NULL,
  `cust_id` varchar(10) NOT NULL,
  `id_home` varchar(10) NOT NULL,
  `tgl_trx` date NOT NULL,
  `lampiran` int(11) NOT NULL,
  `validity` date NOT NULL,
  `hari` int(11) NOT NULL,
  `persen` double NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbph`
--

INSERT INTO `tbph` (`id_ph`, `cust_id`, `id_home`, `tgl_trx`, `lampiran`, `validity`, `hari`, `persen`, `status`, `deleted`) VALUES
('SP00001', 'C001', 'K001', '0000-00-00', 0, '0000-00-00', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_phj`
--

CREATE TABLE `tb_phj` (
  `id_phj` int(11) NOT NULL,
  `item_id` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `harga_jual` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_phj`
--

INSERT INTO `tb_phj` (`id_phj`, `item_id`, `tanggal`, `keterangan`, `harga_jual`, `deleted`) VALUES
(1, 9, '2017-03-23', 'ok', 1000, 0),
(2, 10, '2017-03-23', 'ss', 1111111111111, 0),
(3, 12, '2017-03-23', 'sd', 3000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id_produk` varchar(10) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga_satuan` double NOT NULL,
  `harga_jual` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`id_produk`, `nama_produk`, `satuan`, `harga_satuan`, `harga_jual`, `deleted`) VALUES
('P001', 'Cokleat', 'Ball', 1000, 0, 1),
('P001', 'Cokelat', 'Ball', 1000, 0, 1),
('P002', 'Cokleat', 'Not', 1000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_umum`
--

CREATE TABLE `tb_umum` (
  `id_umum` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `ukuran` varchar(10) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `kategori` varchar(10) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tms_kemasan`
--

CREATE TABLE `tms_kemasan` (
  `id_kemasan` int(11) NOT NULL,
  `kemasan` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tms_kemasan`
--

INSERT INTO `tms_kemasan` (`id_kemasan`, `kemasan`) VALUES
(2, 'Goni'),
(3, 'Plastik Besar'),
(4, 'Plastik Kecil'),
(5, 'Double Plastik'),
(6, 'Pallet'),
(7, 'Goni Plastik');

-- --------------------------------------------------------

--
-- Table structure for table `trf`
--

CREATE TABLE `trf` (
  `id_trf` varchar(30) NOT NULL,
  `id_references` varchar(30) NOT NULL,
  `type_trf` varchar(50) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `received_by` varchar(100) NOT NULL,
  `paid_to` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `acc` int(11) NOT NULL,
  `posted` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL,
  `custom4` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trf_journal`
--

CREATE TABLE `trf_journal` (
  `id_trf_journal` bigint(20) NOT NULL,
  `id_transaction_trf` varchar(30) NOT NULL,
  `id_coa` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `type_trf_journal` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `debit` double NOT NULL,
  `kredit` double NOT NULL,
  `kurs` double NOT NULL,
  `acc` int(11) NOT NULL,
  `posted` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `truck`
--

CREATE TABLE `truck` (
  `id_truck` varchar(10) NOT NULL,
  `nama_truck` varchar(20) NOT NULL,
  `nopol` varchar(10) NOT NULL,
  `tahun` varchar(5) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `truck`
--

INSERT INTO `truck` (`id_truck`, `nama_truck`, `nopol`, `tahun`, `deleted`) VALUES
('T001', 'Jason Trcuk', 'L 10001 RR', '2016', 0),
('T002', 'Ngedrak', 'L 7719 WK', '2017', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ttr_kemasan`
--

CREATE TABLE `ttr_kemasan` (
  `id_kemas` int(11) NOT NULL,
  `id_timbang` varchar(30) NOT NULL,
  `id_kemasan` int(11) NOT NULL,
  `jumlah_kemasan` int(11) NOT NULL,
  `total_berat` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ttr_kemasan`
--

INSERT INTO `ttr_kemasan` (`id_kemas`, `id_timbang`, `id_kemasan`, `jumlah_kemasan`, `total_berat`) VALUES
(1, 'NT/00014', 4, 3, 0.2),
(4, 'NT/00016', 5, 6, 0.3),
(3, 'NT/00016', 6, 8, 0.6);

-- --------------------------------------------------------

--
-- Table structure for table `ttr_main_scale`
--

CREATE TABLE `ttr_main_scale` (
  `id_timbang` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `id_jt` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `no_polisi` varchar(50) NOT NULL,
  `no_kontainer` varchar(50) NOT NULL,
  `seal1` int(50) NOT NULL,
  `seal2` int(50) NOT NULL,
  `catatan` text NOT NULL,
  `total_colly` double NOT NULL,
  `total_bruto` double NOT NULL,
  `total_tarra` double NOT NULL,
  `total_netto` double NOT NULL,
  `rataka` double NOT NULL,
  `No_User` int(11) NOT NULL,
  `acc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ttr_main_scale`
--

INSERT INTO `ttr_main_scale` (`id_timbang`, `item_id`, `location_id`, `id_jt`, `tanggal`, `supplier_id`, `no_polisi`, `no_kontainer`, `seal1`, `seal2`, `catatan`, `total_colly`, `total_bruto`, `total_tarra`, `total_netto`, `rataka`, `No_User`, `acc`) VALUES
('NT/00001', 1, 1, 1, '2017-03-24', 5, 'L99900M', 'CO001', 12, 21, 'ok', 22, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ttr_scale`
--

CREATE TABLE `ttr_scale` (
  `id_scale` int(11) NOT NULL,
  `id_timbang` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `jenis_timbang` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `berat` varchar(10) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `no_polisi` varchar(10) NOT NULL,
  `no_kontainer` varchar(10) NOT NULL,
  `seal1` varchar(50) NOT NULL,
  `seal2` varchar(50) NOT NULL,
  `catatan` text NOT NULL,
  `ka1` varchar(10) NOT NULL,
  `ka2` varchar(10) NOT NULL,
  `ka3` varchar(10) NOT NULL,
  `No_User` int(11) NOT NULL,
  `acc` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ttr_scale`
--

INSERT INTO `ttr_scale` (`id_scale`, `id_timbang`, `item_id`, `location_id`, `jenis_timbang`, `tanggal`, `berat`, `id_supplier`, `no_polisi`, `no_kontainer`, `seal1`, `seal2`, `catatan`, `ka1`, `ka2`, `ka3`, `No_User`, `acc`) VALUES
(1, '1', 1, 1, '', '2017-01-01', '90', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(2, '1', 1, 1, '', '2017-01-01', '90', 2, 'l13254', 'eeqwew', '132', '123', '231', '123', '123', '132', 0, 0),
(13, 'NT/00012', 1, 1, '', '2017-03-12', '7.1\r\n', 0, 'L 1876 KL', 'BNHJ1234', '132', '123', 'OK', '123', '123', '123', 0, 0),
(18, 'NT/00014', 1, 1, '', '2017-03-12', '88', 0, '', '', '132', '123', '', '123', '123', '132', 0, 0),
(5, 'NT/00010', 1, 1, '', '2017-03-12', '90', 0, 'l13254', 'abcdefg', '132', '123', '231', '4234', '234', '234', 0, 0),
(6, 'NT/00010', 1, 1, '', '2017-03-12', '90', 0, 'l13254', 'abcdefg', '132', '123', '231', '4324', '432432', '23432', 0, 0),
(7, 'NT/00010', 1, 1, '', '2017-03-12', '90', 0, 'l13254', 'abcdefg', '132', '123', '231', '11', '11', '33', 0, 0),
(8, 'NT/00010', 1, 1, '', '2017-03-12', '200', 0, 'l13254', 'abcdefg', '132', '123', '231', '4234', '56554', '4354', 0, 0),
(17, 'NT/00014', 1, 1, '', '2017-03-12', '88', 0, '', '', '132', '123', '', '123', '123', '132', 0, 0),
(10, 'NT/00010', 1, 1, '', '2017-03-12', '200', 0, 'l13254', 'abcdefg', '132', '123', '231', '5432', '4324', '543534', 0, 0),
(11, 'NT/00010', 1, 1, '', '2017-03-12', '200', 0, 'l13254', 'abcdefg', '132', '123', '231', '20', '20', '20', 0, 0),
(15, 'NT/00012', 1, 1, '', '2017-03-12', '7.1\r\n', 0, 'L 1876 KL', 'BNHJ1234', '132', '123', 'OK', '423', '654', '645', 0, 0),
(16, 'NT/00012', 1, 1, '', '2017-03-12', '7.0\r\n', 0, 'L 1876 KL', 'BNHJ1234', '132', '123', 'OK', '2432', '2322', '23432', 0, 0),
(19, 'NT/00016', 1, 1, '', '2017-03-12', '88', 0, '', '', '132', '123', '', '123', '123', '132', 0, 0),
(20, 'NT/00016', 1, 1, '', '2017-03-12', '88', 0, '', '', '132', '123', '', '123', '234', '132', 0, 0),
(21, 'NT/00017', 1, 1, '', '2017-03-15', '8.4\r\n', 0, '', '', '132', '123', '', '89', '45', '23', 0, 0),
(22, 'NT/00021', 1, 1, '', '2017-03-21', '-0.5\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(23, 'NT/00021', 1, 1, '', '2017-03-21', '3.5\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_timbang`
--

CREATE TABLE `t_jenis_timbang` (
  `id_jt` int(11) NOT NULL,
  `nama_jenis` varchar(20) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenis_timbang`
--

INSERT INTO `t_jenis_timbang` (`id_jt`, `nama_jenis`, `deleted`) VALUES
(1, 'Pembelian', 0),
(2, 'Titipan', 0),
(3, 'Titip Ayak', 0),
(4, 'Titip Jemur', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_qc`
--

CREATE TABLE `t_qc` (
  `id_qc` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `id_timbang` varchar(30) NOT NULL,
  `kadar_air` double DEFAULT NULL,
  `mouldy` double DEFAULT NULL,
  `insect` double DEFAULT NULL,
  `biji_pipih` double DEFAULT NULL,
  `triple_bean` double DEFAULT NULL,
  `broken_b` double DEFAULT NULL,
  `broken_k` double DEFAULT NULL,
  `sampah` double DEFAULT NULL,
  `slety` double DEFAULT NULL,
  `fm` double DEFAULT NULL,
  `total_kotor` double DEFAULT NULL,
  `bc1` double DEFAULT NULL,
  `bc2` double DEFAULT NULL,
  `bc3` double DEFAULT NULL,
  `bc4` double DEFAULT NULL,
  `bc5` double DEFAULT NULL,
  `bc6` double DEFAULT NULL,
  `total_bc` double DEFAULT NULL,
  `bc_rata` double DEFAULT NULL,
  `tester_ka_testsby` double DEFAULT NULL,
  `tester_ka_testdaerah` double DEFAULT NULL,
  `tester_ka_testbeli` double DEFAULT NULL,
  `abu_testsby` double DEFAULT NULL,
  `abu_testdaerah` double DEFAULT NULL,
  `abu_testbeli` double DEFAULT NULL,
  `ak_testsby` double DEFAULT NULL,
  `ak_testdaerah` double DEFAULT NULL,
  `ak_testbeli` double DEFAULT NULL,
  `bm_testsby` double DEFAULT NULL,
  `bm_testdaerah` double DEFAULT NULL,
  `bm_testbeli` double DEFAULT NULL,
  `pl_testsby` double DEFAULT NULL,
  `pl_testdaerah` double DEFAULT NULL,
  `pl_testbeli` double DEFAULT NULL,
  `gg_testsby` double DEFAULT NULL,
  `gg_testdaerah` double DEFAULT NULL,
  `gg_testbeli` double DEFAULT NULL,
  `daun` double DEFAULT NULL,
  `polong` double DEFAULT NULL,
  `bean_count` double DEFAULT NULL,
  `biji_bagus` double DEFAULT NULL,
  `triase` double DEFAULT NULL,
  `biji_rusak` double NOT NULL,
  `tongkol` double DEFAULT NULL,
  `biji_mati` double DEFAULT NULL,
  `wringkle` double DEFAULT NULL,
  `spotted` double DEFAULT NULL,
  `brown` double DEFAULT NULL,
  `oily` double DEFAULT NULL,
  `void` double DEFAULT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_qc`
--

INSERT INTO `t_qc` (`id_qc`, `item_id`, `id_timbang`, `kadar_air`, `mouldy`, `insect`, `biji_pipih`, `triple_bean`, `broken_b`, `broken_k`, `sampah`, `slety`, `fm`, `total_kotor`, `bc1`, `bc2`, `bc3`, `bc4`, `bc5`, `bc6`, `total_bc`, `bc_rata`, `tester_ka_testsby`, `tester_ka_testdaerah`, `tester_ka_testbeli`, `abu_testsby`, `abu_testdaerah`, `abu_testbeli`, `ak_testsby`, `ak_testdaerah`, `ak_testbeli`, `bm_testsby`, `bm_testdaerah`, `bm_testbeli`, `pl_testsby`, `pl_testdaerah`, `pl_testbeli`, `gg_testsby`, `gg_testdaerah`, `gg_testbeli`, `daun`, `polong`, `bean_count`, `biji_bagus`, `triase`, `biji_rusak`, `tongkol`, `biji_mati`, `wringkle`, `spotted`, `brown`, `oily`, `void`, `deleted`) VALUES
('QC/00001', 10, 'NT/00001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `No_User` int(11) NOT NULL,
  `id_office` int(11) NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `hak_akses` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `bagian` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `id_session` varchar(100) COLLATE latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`No_User`, `id_office`, `username`, `password`, `nama_lengkap`, `email`, `hak_akses`, `level`, `bagian`, `blokir`, `id_session`) VALUES
(1, 0, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Ony Prabowo', 'ony.prabowo@jtanzilco.com', '1', 'admin', '1', 'N', 'e6t63c9d2a26id7l39tq58ivb4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_config`
--
ALTER TABLE `app_config`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id_city`);

--
-- Indexes for table `coa`
--
ALTER TABLE `coa`
  ADD PRIMARY KEY (`id_coa`);

--
-- Indexes for table `coa_classification`
--
ALTER TABLE `coa_classification`
  ADD PRIMARY KEY (`id_coa_classification`);

--
-- Indexes for table `coa_type`
--
ALTER TABLE `coa_type`
  ADD PRIMARY KEY (`id_coa_type`);

--
-- Indexes for table `count_paper`
--
ALTER TABLE `count_paper`
  ADD PRIMARY KEY (`id_count_paper`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id_currency`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `detail_suppliers`
--
ALTER TABLE `detail_suppliers`
  ADD PRIMARY KEY (`id_detail_suppliers`);

--
-- Indexes for table `dn`
--
ALTER TABLE `dn`
  ADD PRIMARY KEY (`id_dn`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `giftcards`
--
ALTER TABLE `giftcards`
  ADD PRIMARY KEY (`giftcard_id`),
  ADD UNIQUE KEY `giftcard_number` (`giftcard_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `grants`
--
ALTER TABLE `grants`
  ADD PRIMARY KEY (`permission_id`,`person_id`),
  ADD KEY `grants_ibfk_2` (`person_id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id_home`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `trans_items` (`trans_items`),
  ADD KEY `trans_user` (`trans_user`),
  ADD KEY `trans_location` (`trans_location`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `item_number` (`item_number`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `items_taxes`
--
ALTER TABLE `items_taxes`
  ADD PRIMARY KEY (`item_id`,`name`,`percent`);

--
-- Indexes for table `item_kits`
--
ALTER TABLE `item_kits`
  ADD PRIMARY KEY (`item_kit_id`);

--
-- Indexes for table `item_kit_items`
--
ALTER TABLE `item_kit_items`
  ADD PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  ADD KEY `item_kit_items_ibfk_2` (`item_id`);

--
-- Indexes for table `item_quantities`
--
ALTER TABLE `item_quantities`
  ADD PRIMARY KEY (`item_id`,`location_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `konsumen`
--
ALTER TABLE `konsumen`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `lpb`
--
ALTER TABLE `lpb`
  ADD PRIMARY KEY (`id_lpb`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`),
  ADD UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  ADD UNIQUE KEY `name_lang_key` (`name_lang_key`);

--
-- Indexes for table `mr`
--
ALTER TABLE `mr`
  ADD PRIMARY KEY (`id_mr`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `permissions_ibfk_2` (`location_id`);

--
-- Indexes for table `po`
--
ALTER TABLE `po`
  ADD PRIMARY KEY (`id_po`);

--
-- Indexes for table `receivings`
--
ALTER TABLE `receivings`
  ADD PRIMARY KEY (`receiving_id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `receivings_items`
--
ALTER TABLE `receivings_items`
  ADD PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`sale_id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `sale_time` (`sale_time`);

--
-- Indexes for table `sales_items`
--
ALTER TABLE `sales_items`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `item_location` (`item_location`);

--
-- Indexes for table `sales_items_taxes`
--
ALTER TABLE `sales_items_taxes`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `sales_payments`
--
ALTER TABLE `sales_payments`
  ADD PRIMARY KEY (`sale_id`,`payment_type`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indexes for table `sales_suspended`
--
ALTER TABLE `sales_suspended`
  ADD PRIMARY KEY (`sale_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `sales_suspended_items`
--
ALTER TABLE `sales_suspended_items`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `sales_suspended_items_ibfk_3` (`item_location`);

--
-- Indexes for table `sales_suspended_items_taxes`
--
ALTER TABLE `sales_suspended_items_taxes`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `sales_suspended_payments`
--
ALTER TABLE `sales_suspended_payments`
  ADD PRIMARY KEY (`sale_id`,`payment_type`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `so`
--
ALTER TABLE `so`
  ADD PRIMARY KEY (`id_so`);

--
-- Indexes for table `spp`
--
ALTER TABLE `spp`
  ADD PRIMARY KEY (`id_spp`);

--
-- Indexes for table `stock_locations`
--
ALTER TABLE `stock_locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`supplier_id`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `tbph`
--
ALTER TABLE `tbph`
  ADD PRIMARY KEY (`id_ph`),
  ADD KEY `cust_id` (`cust_id`),
  ADD KEY `id_home` (`id_home`);

--
-- Indexes for table `tb_phj`
--
ALTER TABLE `tb_phj`
  ADD PRIMARY KEY (`id_phj`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `tms_kemasan`
--
ALTER TABLE `tms_kemasan`
  ADD PRIMARY KEY (`id_kemasan`);

--
-- Indexes for table `trf`
--
ALTER TABLE `trf`
  ADD PRIMARY KEY (`id_trf`);

--
-- Indexes for table `trf_journal`
--
ALTER TABLE `trf_journal`
  ADD PRIMARY KEY (`id_trf_journal`);

--
-- Indexes for table `ttr_kemasan`
--
ALTER TABLE `ttr_kemasan`
  ADD PRIMARY KEY (`id_kemas`);

--
-- Indexes for table `ttr_main_scale`
--
ALTER TABLE `ttr_main_scale`
  ADD PRIMARY KEY (`id_timbang`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `id_jt` (`id_jt`);

--
-- Indexes for table `ttr_scale`
--
ALTER TABLE `ttr_scale`
  ADD PRIMARY KEY (`id_scale`),
  ADD KEY `id_timbang` (`id_timbang`);

--
-- Indexes for table `t_jenis_timbang`
--
ALTER TABLE `t_jenis_timbang`
  ADD PRIMARY KEY (`id_jt`);

--
-- Indexes for table `t_qc`
--
ALTER TABLE `t_qc`
  ADD PRIMARY KEY (`id_qc`),
  ADD KEY `item_fk` (`item_id`),
  ADD KEY `notatimbang_fk` (`id_timbang`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`No_User`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id_city` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `coa`
--
ALTER TABLE `coa`
  MODIFY `id_coa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `coa_classification`
--
ALTER TABLE `coa_classification`
  MODIFY `id_coa_classification` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `coa_type`
--
ALTER TABLE `coa_type`
  MODIFY `id_coa_type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `count_paper`
--
ALTER TABLE `count_paper`
  MODIFY `id_count_paper` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id_currency` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `detail_suppliers`
--
ALTER TABLE `detail_suppliers`
  MODIFY `id_detail_suppliers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `giftcards`
--
ALTER TABLE `giftcards`
  MODIFY `giftcard_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `item_kits`
--
ALTER TABLE `item_kits`
  MODIFY `item_kit_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `person_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `receivings`
--
ALTER TABLE `receivings`
  MODIFY `receiving_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `sale_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sales_suspended`
--
ALTER TABLE `sales_suspended`
  MODIFY `sale_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stock_locations`
--
ALTER TABLE `stock_locations`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_phj`
--
ALTER TABLE `tb_phj`
  MODIFY `id_phj` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tms_kemasan`
--
ALTER TABLE `tms_kemasan`
  MODIFY `id_kemasan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `trf_journal`
--
ALTER TABLE `trf_journal`
  MODIFY `id_trf_journal` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ttr_kemasan`
--
ALTER TABLE `ttr_kemasan`
  MODIFY `id_kemas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ttr_scale`
--
ALTER TABLE `ttr_scale`
  MODIFY `id_scale` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `No_User` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `people` (`person_id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `people` (`person_id`);

--
-- Constraints for table `giftcards`
--
ALTER TABLE `giftcards`
  ADD CONSTRAINT `giftcards_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `people` (`person_id`);

--
-- Constraints for table `grants`
--
ALTER TABLE `grants`
  ADD CONSTRAINT `grants_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`permission_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `grants_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `employees` (`person_id`) ON DELETE CASCADE;

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `employees` (`person_id`),
  ADD CONSTRAINT `inventory_ibfk_3` FOREIGN KEY (`trans_location`) REFERENCES `stock_locations` (`location_id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`person_id`);

--
-- Constraints for table `items_taxes`
--
ALTER TABLE `items_taxes`
  ADD CONSTRAINT `items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON DELETE CASCADE;

--
-- Constraints for table `item_kit_items`
--
ALTER TABLE `item_kit_items`
  ADD CONSTRAINT `item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `item_kits` (`item_kit_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`) ON DELETE CASCADE;

--
-- Constraints for table `item_quantities`
--
ALTER TABLE `item_quantities`
  ADD CONSTRAINT `item_quantities_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `item_quantities_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `stock_locations` (`location_id`);

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permissions_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `stock_locations` (`location_id`) ON DELETE CASCADE;

--
-- Constraints for table `receivings`
--
ALTER TABLE `receivings`
  ADD CONSTRAINT `receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`person_id`),
  ADD CONSTRAINT `receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`person_id`);

--
-- Constraints for table `receivings_items`
--
ALTER TABLE `receivings_items`
  ADD CONSTRAINT `receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `receivings` (`receiving_id`);

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`person_id`),
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`person_id`);

--
-- Constraints for table `sales_items`
--
ALTER TABLE `sales_items`
  ADD CONSTRAINT `sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`sale_id`),
  ADD CONSTRAINT `sales_items_ibfk_3` FOREIGN KEY (`item_location`) REFERENCES `stock_locations` (`location_id`);

--
-- Constraints for table `sales_items_taxes`
--
ALTER TABLE `sales_items_taxes`
  ADD CONSTRAINT `sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `sales_items` (`sale_id`),
  ADD CONSTRAINT `sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`);

--
-- Constraints for table `sales_payments`
--
ALTER TABLE `sales_payments`
  ADD CONSTRAINT `sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `sales` (`sale_id`);

--
-- Constraints for table `sales_suspended`
--
ALTER TABLE `sales_suspended`
  ADD CONSTRAINT `sales_suspended_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`person_id`),
  ADD CONSTRAINT `sales_suspended_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`person_id`);

--
-- Constraints for table `sales_suspended_items`
--
ALTER TABLE `sales_suspended_items`
  ADD CONSTRAINT `sales_suspended_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `sales_suspended_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `sales_suspended` (`sale_id`),
  ADD CONSTRAINT `sales_suspended_items_ibfk_3` FOREIGN KEY (`item_location`) REFERENCES `stock_locations` (`location_id`);

--
-- Constraints for table `sales_suspended_items_taxes`
--
ALTER TABLE `sales_suspended_items_taxes`
  ADD CONSTRAINT `sales_suspended_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `sales_suspended_items` (`sale_id`),
  ADD CONSTRAINT `sales_suspended_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`);

--
-- Constraints for table `sales_suspended_payments`
--
ALTER TABLE `sales_suspended_payments`
  ADD CONSTRAINT `sales_suspended_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `sales_suspended` (`sale_id`);

--
-- Constraints for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD CONSTRAINT `suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `people` (`person_id`);

--
-- Constraints for table `tbph`
--
ALTER TABLE `tbph`
  ADD CONSTRAINT `penawaran_h1` FOREIGN KEY (`cust_id`) REFERENCES `konsumen` (`cust_id`),
  ADD CONSTRAINT `penawaran_h2` FOREIGN KEY (`id_home`) REFERENCES `home` (`id_home`);

--
-- Constraints for table `tb_phj`
--
ALTER TABLE `tb_phj`
  ADD CONSTRAINT `penetapan` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`);

--
-- Constraints for table `ttr_main_scale`
--
ALTER TABLE `ttr_main_scale`
  ADD CONSTRAINT `items_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `jenistimbang_fk` FOREIGN KEY (`id_jt`) REFERENCES `t_jenis_timbang` (`id_jt`),
  ADD CONSTRAINT `supliers_fk` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`supplier_id`);

--
-- Constraints for table `t_qc`
--
ALTER TABLE `t_qc`
  ADD CONSTRAINT `item_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `notatimbang_fk` FOREIGN KEY (`id_timbang`) REFERENCES `ttr_main_scale` (`id_timbang`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
