-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19 Jul 2017 pada 11.52
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acm_new`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `app_config`
--

CREATE TABLE `app_config` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `app_config`
--

INSERT INTO `app_config` (`key`, `value`) VALUES
('address', 'Jl. Dimana mana'),
('barcode_content', 'id'),
('barcode_first_row', 'category'),
('barcode_font', 'Arial'),
('barcode_font_size', '10'),
('barcode_generate_if_empty', '0'),
('barcode_height', '50'),
('barcode_num_in_row', '2'),
('barcode_page_cellspacing', '20'),
('barcode_page_width', '100'),
('barcode_quality', '100'),
('barcode_second_row', 'item_code'),
('barcode_third_row', 'unit_price'),
('barcode_type', 'Code39'),
('barcode_width', '250'),
('company', 'Asih Growing Guna'),
('company_logo', ''),
('currency_decimals', '2'),
('currency_side', '0'),
('currency_symbol', 'Rp. '),
('dateformat', 'd/m/Y'),
('decimal_point', '.'),
('default_sales_discount', '0'),
('default_tax_rate', '8'),
('email', 'inoreds@gmail.com'),
('fax', ''),
('invoice_default_comments', 'This is a default comment'),
('invoice_email_message', 'Dear $CU, In attachment the receipt for sale $INV'),
('invoice_enable', '1'),
('language', 'id'),
('lines_per_page', '25'),
('msg_msg', ''),
('msg_pwd', ''),
('msg_src', ''),
('msg_uid', ''),
('phone', '555-555-5555'),
('print_bottom_margin', '0'),
('print_footer', '0'),
('print_header', '0'),
('print_left_margin', '0'),
('print_right_margin', '0'),
('print_silently', '1'),
('print_top_margin', '0'),
('quantity_decimals', '0'),
('receipt_show_description', '1'),
('receipt_show_serialnumber', '1'),
('receipt_show_taxes', '0'),
('receipt_show_total_discount', '1'),
('recv_invoice_format', '$CO'),
('return_policy', 'KebijakanRetur'),
('sales_invoice_format', '$CO'),
('tax_decimals', '2'),
('tax_included', '0'),
('thousands_separator', ''),
('timeformat', 'H:i:s'),
('timezone', 'Asia/Bangkok'),
('use_invoice_template', '1'),
('website', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bagian`
--

CREATE TABLE `bagian` (
  `id_bagian` varchar(30) CHARACTER SET utf32 NOT NULL,
  `nama` varchar(30) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bagian`
--

INSERT INTO `bagian` (`id_bagian`, `nama`, `deleted`) VALUES
('1', 'super', 0),
('2', 'Dir', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `city`
--

CREATE TABLE `city` (
  `id_city` int(11) NOT NULL,
  `name_city` varchar(50) NOT NULL,
  `status_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `city`
--

INSERT INTO `city` (`id_city`, `name_city`, `status_deleted`) VALUES
(1, 'jakarta', 0),
(2, 'Malang', 0),
(4, 'Pasuruan', 0),
(6, 'surabaya', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `coa`
--

CREATE TABLE `coa` (
  `id_coa` int(11) NOT NULL,
  `number_coa1` varchar(20) NOT NULL,
  `number_coa2` varchar(20) NOT NULL,
  `number_coa3` varchar(20) NOT NULL,
  `number_coa4` varchar(20) NOT NULL,
  `coa_name` varchar(100) NOT NULL,
  `balance` double NOT NULL,
  `note` varchar(50) NOT NULL,
  `lower_stage` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(30) NOT NULL,
  `custom2` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `coa`
--

INSERT INTO `coa` (`id_coa`, `number_coa1`, `number_coa2`, `number_coa3`, `number_coa4`, `coa_name`, `balance`, `note`, `lower_stage`, `deleted`, `custom1`, `custom2`) VALUES
(1, '01-000-000.000', '', '', '', 'ASET LANCAR', 0, '', 0, 0, '2', '2'),
(2, '01-000-000.000', '01-001-000.000', '', '', 'KAS', 0, '', 0, 0, '12', '2'),
(4, '01-000-000.000', '01-001-001.000', '', '', 'Kas Operasional', 0, '', 0, 0, '12', '2'),
(5, '01-000-000.000', '01-001-002.000', '', '', 'Kas Agro', 0, '', 0, 0, '2', '1'),
(6, '01-000-000.000', '01-001-003.000', '', '', 'Kas Kecil Lahan', 0, '', 0, 0, '2', '1'),
(7, '01-000-000.000', '01-001-003.000', '01-001-003.001', '', 'Kas Kecil Lahan Sucipto', 0, '', 0, 0, '2', '1'),
(8, '01-000-000.000', '01-001-003.000', '01-001-003.002', '', 'Kas Kecil Lahan Toni Setiawan', 0, '', 0, 0, '2', '1'),
(9, '01-000-000.000', '01-001-003.00', '01-001-003.003', '', 'Kas Kecil Lahan Pamuji', 0, '', 0, 0, '2', '1'),
(10, '01-000-000.000', '01-001-003.000', '01-001-003.004', '', 'Kas Kecil Lahan Rudi B W', 0, '', 0, 0, '2', '1'),
(11, '01-000-000.000', '01-001-003.000', '01-001-003.005', '', 'Kas Kecil Lahan M Rofian', 0, '', 0, 0, '2', '1'),
(12, '01-000-000.000', '01-015-000.000', '', '', 'BANK', 0, '', 0, 0, '2', '1'),
(13, '01-000-000.000', '01-001-005.000', '01-001-005.001', '', 'Bank Danamon 3587082060', 0, '', 0, 1, '2', '1'),
(14, '01-000-000.000', '01-015-000.000', '01-015-001.000', '', 'Bank Danamon 3587082060', 0, '', 0, 0, '1', '2'),
(15, '01-000-000.000', '01-015-000.000', '01-015-002.000', '', 'Bank Mandiri 1780019612999', 0, '', 0, 0, '1', '2'),
(16, '01-000-000.000', '01-002-000.000', '', '', 'DEPOSITO BERJANGKA', 0, '', 0, 0, '2', '1'),
(17, '01-000-000.000', '01-002-000.000', '01-002-001.000', '', 'Bank Danamon', 0, '', 0, 0, '1', '2'),
(18, '01-000-000.000', '01-002-000.000', '01-002-002.000', '', 'Bank Mandiri', 0, '', 0, 0, '1', '2'),
(19, '01-000-000.000', '01-003-000.000', '', '', 'PIUTANG USAHA', 0, '', 0, 0, '2', '1'),
(20, '01-000-000.000', '01-003-000.000', '01-003-001.000', '', 'Piutang Usaha', 0, '', 0, 0, '2', '1'),
(21, '01-000-000.000', '01-003-000.000', '01-003-002.000', '', 'Penjualan Tunai', 0, '', 0, 0, '2', '1'),
(22, '01-000-000.000', '01-003-000.000', '01-003-005.000', '', 'Cadangan Piutang Usaha Tak Tertagih', 0, '', 0, 0, '2', '1'),
(23, '01-000-000.000', '01-004-000.000', '', '', 'PIUTANG LAIN-LAIN', 0, '', 0, 0, '2', '1'),
(24, '01-000-000.000', '01-004-000.000', '01-004-001.000', '', 'Piutang Karyawan', 0, '', 0, 0, '2', '1'),
(25, '01-000-000.000', '01-004-000.000', '01-004-002.000', '', 'Piutang Direksi', 0, '', 0, 0, '2', '1'),
(26, '01-000-000.000', '01-004-000.000', '01-004-003.000', '', 'Piutang Pihak Ketiga', 0, '', 0, 0, '2', '1'),
(27, '01-000-000.000', '01-004-000.000', '01-004-004.000', '', 'Piutang Giro', 0, '', 0, 0, '2', '1'),
(28, '01-000-000.000', '01-004-000.000', '01-004-005.000', '', 'Piutang Giro Tolakan', 0, '', 0, 0, '2', '1'),
(29, '01-000-000.000', '01-004-000.000', '01-004-006.000', '', 'Piutang Jamsostek', 0, '', 0, 0, '2', '1'),
(30, '01-000-000.000', '01-004-000.000', '01-004-009.000', '', 'Piutang lain-lain', 0, '', 0, 0, '2', '1'),
(31, '01-000-000.000', '01-005-000.000', '', '', 'PERSEDIAAN', 0, '', 0, 0, '2', '1'),
(32, '01-000-000.000', '01-005-000.000', '01-005-001.000', '', 'Persediaan Agro', 0, '', 0, 0, '2', '1'),
(33, '01-000-000.000', '01-005-000.000', '01-005-001.001', '', 'Persediaan Benih', 0, '', 0, 0, '2', '1'),
(34, '01-000-000.000', '01-005-000.000', '01-005-001.002', '', 'Persediaan Bibit', 0, '', 0, 0, '2', '1'),
(35, '01-000-000.000', '01-005-000.000', '01-005-001.003', '', 'Persediaan Tanaman Semusim', 0, '', 0, 0, '2', '1'),
(36, '01-000-000.000', '01-005-000.000', '01-005-001.004', '', 'Persediaan Hasil Perkebunan', 0, '', 0, 0, '2', '1'),
(37, '01-000-000.000', '01-005-000.000', '01-005-001.005', '', 'Persediaan Tanaman Display', 0, '', 0, 0, '2', '1'),
(38, '01-000-000.000', '01-005-000.000', '01-005-002.000', '', 'Persediaan Pupuk', 0, '', 0, 0, '2', '1'),
(39, '01-000-000.000', '01-005-000.000', '01-005-002.001', '', 'Persediaan Bahan Baku Pupuk', 0, '', 0, 0, '2', '1'),
(40, '01-000-000.000', '01-005-000.000', '01-005-002.002', '', 'Persediaan Bahan Pembantu Pupuk', 0, '', 0, 0, '2', '1'),
(41, '01-000-000.000', '01-005-000.000', '01-005-002.003', '', 'Persediaan Barang Setengah Jadi Pupuk', 0, '', 0, 0, '2', '1'),
(42, '01-000-000.000', '01-005-000.000', '01-005-002.004', '', 'Persediaan Barang Jadi Pupuk', 0, '', 0, 0, '2', '1'),
(43, '01-000-000.000', '01-005-000.000', '01-005-002.004', '01-005-002.004.001', 'Persediaan Barang Jadi Pupuk Organic', 0, '', 0, 0, '2', '1'),
(44, '01-000-000.000', '01-005-000.000', '01-005-002.004', '01-005-002.004.002', 'Persediaan Barang Jadi Pupuk Non Organic', 0, '', 0, 0, '2', '1'),
(45, '01-000-000.000', '01-005-000.000', '01-005-003.000', '', 'Persediaan Lain-Lain', 0, '', 0, 0, '2', '1'),
(46, '01-000-000.000', '01-005-000.000', '01-005-003.001', '', 'Persediaan Konsinyasi', 0, '', 0, 0, '2', '1'),
(47, '01-000-000.000', '01-005-000.000', '01-005-003.002', '', 'Persediaan Dagang', 0, '', 0, 0, '2', '1'),
(48, '01-000-000.000', '01-006-000.000', '', '', 'UANG MUKA PEMBELIAN', 0, '', 0, 0, '2', '1'),
(49, '01-000-000.000', '01-006-000.000', '01-006-001.000', '', 'Uang Muka Pembelian Pupuk', 0, '', 0, 0, '2', '1'),
(50, '01-000-000.000', '01-006-000.000', '01-006-002.000', '', 'Uang Muka Pembelian Bahan Baku', 0, '', 0, 0, '2', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `coa_classification`
--

CREATE TABLE `coa_classification` (
  `id_coa_classification` int(11) NOT NULL,
  `name_classification` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `coa_classification`
--

INSERT INTO `coa_classification` (`id_coa_classification`, `name_classification`) VALUES
(1, 'BANK'),
(2, 'BIAYA DIBAYAR DI MUKA'),
(3, 'BIAYA LAIN'),
(4, 'BIAYA OPERASIONAL'),
(5, 'BIAYA PRODUKSI'),
(6, 'HARTA LAINNYA'),
(7, 'HARTA TETAP BERWUJUD'),
(8, 'HUTANG JANGKA PANJANG'),
(9, 'HUTANG LAINNYA'),
(10, 'HUTANG PAJAK'),
(11, 'INVESTASI JANGKA PANJANG'),
(12, 'KAS'),
(13, 'LABA'),
(14, 'MODAL'),
(15, 'PENDAPATAN DI TERIMA DI MUKA'),
(16, 'PENDAPATAN LUAR USAHA'),
(17, 'PENDAPATAN USAHA'),
(18, 'PENGELUARAN LUAR USAHA'),
(19, 'PERSEDIAAN'),
(20, 'PIUTANG LAINNYA'),
(21, 'PIUTANG NON USAHA'),
(22, 'PIUTANG PAJAK'),
(23, 'PIUTANG USAHA'),
(24, 'PRIVE');

-- --------------------------------------------------------

--
-- Struktur dari tabel `coa_type`
--

CREATE TABLE `coa_type` (
  `id_coa_type` int(11) NOT NULL,
  `name_coa_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `coa_type`
--

INSERT INTO `coa_type` (`id_coa_type`, `name_coa_type`) VALUES
(1, 'BIAYA ATAS PENDAPATAN'),
(2, 'HARTA'),
(3, 'KEWAJIBAN'),
(4, 'MODAL'),
(5, 'PENDAPATAN'),
(6, 'PENDAPATAN LAIN'),
(7, 'PENGELUARAN LAIN'),
(8, 'PENGELUARAN OPERASIONAL');

-- --------------------------------------------------------

--
-- Struktur dari tabel `count_paper`
--

CREATE TABLE `count_paper` (
  `id_count_paper` int(11) NOT NULL,
  `paper_type` varchar(30) NOT NULL,
  `date_paper` date NOT NULL,
  `paper_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `count_paper`
--

INSERT INTO `count_paper` (`id_count_paper`, `paper_type`, `date_paper`, `paper_count`) VALUES
(1, 'BKM', '2017-01-26', 13),
(2, 'BBM', '2017-01-26', 8),
(3, 'BBK', '2017-01-26', 6),
(4, 'BBK', '2017-02-09', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `currency`
--

CREATE TABLE `currency` (
  `id_currency` int(11) NOT NULL,
  `name_currency` varchar(30) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  `status_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `currency`
--

INSERT INTO `currency` (`id_currency`, `name_currency`, `symbol`, `status_deleted`) VALUES
(1, 'Rupiah', 'Rp', 0),
(2, 'Dollar', 'USD', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_code` varchar(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(50) NOT NULL,
  `company_phone` varchar(20) NOT NULL,
  `company_email` varchar(30) NOT NULL,
  `company_fax` varchar(15) NOT NULL,
  `agency_name` varchar(30) NOT NULL,
  `ppn` varchar(20) NOT NULL,
  `no_npwp` varchar(20) NOT NULL,
  `name_npwp` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `pembayaran` varchar(15) NOT NULL,
  `date_in` date NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `acc` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `noktp` int(11) NOT NULL,
  `sppkp` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `customers`
--

INSERT INTO `customers` (`customer_id`, `person_id`, `company_code`, `company_name`, `company_address`, `company_phone`, `company_email`, `company_fax`, `agency_name`, `ppn`, `no_npwp`, `name_npwp`, `note`, `pembayaran`, `date_in`, `account_number`, `taxable`, `discount_percent`, `acc`, `deleted`, `noktp`, `sppkp`) VALUES
(1, 0, '', 'Hardanto', 'Jolotundo', '09212121221', 'dantoking@gmail.com', '23232323232323', '', '1', '12121jkjkj121212', '', '', 'n/30', '2017-06-07', '121212', 1, '0.00', 1, 0, 0, 'kjkjkj1k2j1k2j1k2jk1'),
(2, 9, 'C-1001', 'PT. Island Globalindo', 'Gresik', '031898939', 'info@globalindo.com', '', 'Mamat', '', '310390939', 'Joko', '-', '', '2017-01-05', NULL, 1, '0.00', 1, 0, 0, ''),
(3, 10, 'C-1002', 'PT. Makmur Sejahtera', 'Sidoarjo', '0315849938', 'info@makmur.com', '', 'Joko', '', '', '', '', '', '2017-01-05', NULL, 1, '0.00', 0, 0, 0, ''),
(4, 14, 'c-0012', 'sdf', 'sdf', 'df', 'sdf', '', 'df', '', 'sdf', 'dsf', 'dsf', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(5, 15, 'c-003', 'asd', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(6, 16, 'c-006', 'fdsdf', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(7, 17, 'c-00777', 'sdfsdf', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(8, 18, 'c-0066', 'fg', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(9, 19, 'c-0055', 'Gilang Saputra', 'Jl.Melati 20 Sby', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(10, 20, 'c-0003333', 'f', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(11, 21, 'c-00393', 'adsad', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(12, 1, 'C001', 'joko', 'hdh', '7373', 'hdh', '73', '', '1', '', '', '', '0', '2017-03-24', '8387', 1, '0.00', 0, 0, 0, ''),
(13, 8, '', 'Nusa Indo', '', '', '', '', '', '', '', '', '', '', '0000-00-00', 'C-001', 1, '0.00', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customers_copy`
--

CREATE TABLE `customers_copy` (
  `customer_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_code` varchar(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(50) NOT NULL,
  `company_phone` varchar(20) NOT NULL,
  `company_email` varchar(30) NOT NULL,
  `company_fax` varchar(15) NOT NULL,
  `agency_name` varchar(30) NOT NULL,
  `ppn` varchar(20) NOT NULL,
  `no_npwp` varchar(20) NOT NULL,
  `name_npwp` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `pembayaran` varchar(15) NOT NULL,
  `date_in` date NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `acc` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `noktp` int(11) NOT NULL,
  `sppkp` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `customers_copy`
--

INSERT INTO `customers_copy` (`customer_id`, `person_id`, `company_code`, `company_name`, `company_address`, `company_phone`, `company_email`, `company_fax`, `agency_name`, `ppn`, `no_npwp`, `name_npwp`, `note`, `pembayaran`, `date_in`, `account_number`, `taxable`, `discount_percent`, `acc`, `deleted`, `noktp`, `sppkp`) VALUES
(0, 0, '', 'Hardanto', 'Jolotundo', '09212121221', 'dantoking@gmail.com', '23232323232323', '', '1', '12121jkjkj121212', '', '', '', '2017-06-07', '121212', 1, '0.00', 1, 0, 0, 'kjkjkj1k2j1k2j1k2jk1'),
(1, 8, '', 'Nusa Indo', '', '', '', '', '', '', '', '', '', '', '0000-00-00', 'C-001', 1, '0.00', 0, 0, 0, ''),
(2, 9, 'C-1001', 'PT. Island Globalindo', 'Gresik', '031898939', 'info@globalindo.com', '', 'Mamat', '', '310390939', 'Joko', '-', '', '2017-01-05', NULL, 1, '0.00', 1, 0, 0, ''),
(3, 10, 'C-1002', 'PT. Makmur Sejahtera', 'Sidoarjo', '0315849938', 'info@makmur.com', '', 'Joko', '', '', '', '', '', '2017-01-05', NULL, 1, '0.00', 0, 0, 0, ''),
(4, 14, 'c-0012', 'sdf', 'sdf', 'df', 'sdf', '', 'df', '', 'sdf', 'dsf', 'dsf', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(5, 15, 'c-003', 'asd', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(6, 16, 'c-006', 'fdsdf', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(7, 17, 'c-00777', 'sdfsdf', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(8, 18, 'c-0066', 'fg', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(9, 19, 'c-0055', 'Gilang Saputra', 'Jl.Melati 20 Sby', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(10, 20, 'c-0003333', 'f', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(11, 21, 'c-00393', 'adsad', '', '', '', '', '', '', '', '', '', '', '2017-03-15', NULL, 1, '0.00', 0, 0, 0, ''),
(12, 1, 'C001', 'joko', 'hdh', '7373', 'hdh', '73', '', '1', '', '', '', '0', '2017-03-24', '8387', 1, '0.00', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_bapj`
--

CREATE TABLE `detail_bapj` (
  `id_detail_bapj` int(11) NOT NULL,
  `id_bapj` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `harga` double NOT NULL,
  `diskon` double NOT NULL,
  `jumlah` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `detail_bapj`
--

INSERT INTO `detail_bapj` (`id_detail_bapj`, `id_bapj`, `item_id`, `harga`, `diskon`, `jumlah`, `deleted`) VALUES
(1, 'BAPJ/00002', 0, 8, 0, 16, 0),
(5, 'BAPJ/00001', 19, 15000, 200, 0, 0),
(8, 'BAPJ/00002', 0, 8, 0, 16, 0),
(9, 'BAPJ/00002', 0, 8, 0, 16, 0),
(10, 'BAPJ/00003', 0, 10000, 0, 0, 0),
(11, 'BAPJ/00004', 0, 0, 15000, -15000, 0),
(12, 'BAPJ/00008', 0, 10000000000000, 100, 9999999999900, 0),
(13, 'BAPJ/00009', 0, 0, 0, 0, 1),
(14, 'BAPJ/00010', 0, 12, 5, 7, 0),
(15, 'BAPJ/00011', 0, 0, 0, 0, 0),
(16, 'BAPJ/00012', 0, 15000, 100, 14900, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_bpb`
--

CREATE TABLE `detail_bpb` (
  `id_detail_bpb` int(11) NOT NULL,
  `id_bpb` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `satuan` varchar(25) NOT NULL,
  `keperluan` varchar(30) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_bpb`
--

INSERT INTO `detail_bpb` (`id_detail_bpb`, `id_bpb`, `item_id`, `jumlah`, `satuan`, `keperluan`, `keterangan`, `deleted`) VALUES
(14, 'BPB00001', 1, 4, '', 'Keperluan', 'Keterangan', 0),
(15, 'BPB00001', 1, 0, '', 'Keperluan', 'Keterangan', 1),
(16, 'BPB00006', 18, 3, '', 'Keperluan', 'Keterangan', 0),
(17, 'BPB00007', 6, 900, '', 'Keperluan', 'Keterangan', 0),
(18, 'BPB00009', 1, 1000000, '', 'untuk di jual', 'ok', 0),
(19, 'BPB00010', 1, 1000000, '', 'ok', 'ok', 0),
(20, 'BPB00010', 6, 1000000, '', 'ok', 'ok', 0),
(21, 'BPB00011', 13, 2, '', 'asd', 'asdasd', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_cde`
--

CREATE TABLE `detail_cde` (
  `id_detail_cde` int(11) NOT NULL,
  `no_kontrak` varchar(30) NOT NULL,
  `no_dok` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_dn`
--

CREATE TABLE `detail_dn` (
  `id_dn` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `spek` varchar(30) NOT NULL,
  `qty` double NOT NULL,
  `satuan` varchar(30) NOT NULL,
  `harga` double NOT NULL,
  `diskon` double NOT NULL,
  `jumlah` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_dn`
--

INSERT INTO `detail_dn` (`id_dn`, `item_id`, `spek`, `qty`, `satuan`, `harga`, `diskon`, `jumlah`, `deleted`) VALUES
('DN/00001', 13, 'Hitam', 10, 'satuan', 15, 2, 13, 0),
('DN/00001', 16, 'Hitam', 212, 'satuan', 15, 2, 13, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_do`
--

CREATE TABLE `detail_do` (
  `id_detail_do` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `deleted` int(11) NOT NULL,
  `id_do` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_do`
--

INSERT INTO `detail_do` (`id_detail_do`, `item_id`, `jumlah`, `satuan`, `deleted`, `id_do`) VALUES
(1, 1, 120000, 'Kg', 0, 'do/00001'),
(2, 6, 1000000, 'Kg', 0, 'do/00002'),
(3, 1, 10000000, 'Kg', 0, 'do/00003'),
(4, 1, 1000, 'Kg', 0, 'do/00004');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_dpp`
--

CREATE TABLE `detail_dpp` (
  `id_dpp` varchar(30) NOT NULL,
  `id_np` varchar(30) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `tgljt` date NOT NULL,
  `hasil_fu` varchar(255) NOT NULL,
  `tgl_fu` date NOT NULL,
  `jumlah` double NOT NULL,
  `tranfer` varchar(255) NOT NULL,
  `tunai` double NOT NULL,
  `ch_gb` varchar(20) NOT NULL,
  `bank` varchar(35) NOT NULL,
  `kode_bank` varchar(20) NOT NULL,
  `jumlah_chgb` int(50) NOT NULL,
  `id_bbm` varchar(30) NOT NULL,
  `diketahui` varchar(50) NOT NULL,
  `deleted` int(11) NOT NULL,
  `id_detail_dpp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_dpp`
--

INSERT INTO `detail_dpp` (`id_dpp`, `id_np`, `customer_id`, `tgljt`, `hasil_fu`, `tgl_fu`, `jumlah`, `tranfer`, `tunai`, `ch_gb`, `bank`, `kode_bank`, `jumlah_chgb`, `id_bbm`, `diketahui`, `deleted`, `id_detail_dpp`) VALUES
('DPPH/00011', 'LPB/00013', 14, '2017-05-20', '', '0000-00-00', 1000000000, '', 0, '', '1000', '', 0, '2323``', '', 0, 1),
('DPPH/00001', 'LPB/00001', 10, '0000-00-00', '', '0000-00-00', 1000000, '', 1500, 'CH', 'BNI', '091', 0, 'BKK0101', 'Yes', 0, 4),
('DPPH/00003', 'LPB/00001', 10, '0000-00-00', '', '0000-00-00', 1000000, '', 0, '', '', '', 0, '', '', 0, 5),
('DPPH/00005', 'LPB/00001', 10, '2017-04-28', '', '0000-00-00', 1000000, '', 0, '', '', '', 0, '', '', 0, 6),
('DPPH/00015', 'LPB/00013', 14, '0000-00-00', '', '0000-00-00', 0, '', 1212, ' sasa', 'wqqw', 'w', 0, 'qwqwq', 'qsasasasas', 0, 10),
('DPPH/00015', 'LPB/00013', 14, '0000-00-00', '', '0000-00-00', 0, '', 1, 'asas', 'bc', 'njhj', 0, 'asas', 'assasas', 0, 11),
('DPPH/00014', 'LPB/00012', 14, '2017-05-20', '', '0000-00-00', 0, '', 0, '', '100000', '', 0, '', '', 0, 12),
('DPPH/00014', 'LPB/00013', 14, '2017-05-20', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 13),
('DPPH/00014', 'LPB/00022', 14, '2017-06-08', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 14),
('DPPH/00014', 'LPB/00023', 14, '2017-06-08', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 15),
('DPPH/00014', 'LPB/00023', 14, '2017-06-15', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 16),
('DPPH/00014', 'LPB/00024', 14, '2017-06-15', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 17),
('DPPH/00013', 'LPB/00012', 14, '0000-00-00', '', '0000-00-00', 0, '', 1000, '100', '10', '1212', 0, 'gfhfgfh', 'asasa', 0, 18),
('DPPH/00013', 'LPB/00013', 14, '0000-00-00', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 19),
('DPPH/00013', 'LPB/00022', 14, '0000-00-00', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 20),
('DPPH/00013', 'LPB/00023', 14, '0000-00-00', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 21),
('DPPH/00013', 'LPB/00023', 14, '0000-00-00', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 22),
('DPPH/00013', 'LPB/00024', 14, '0000-00-00', '', '0000-00-00', 0, '', 0, '', '', '', 0, '', '', 0, 23),
('DPPH/00018', 'LPB/00012', 14, '2017-05-20', '', '0000-00-00', 0, '', 10000, '', '', '', 0, '', '', 0, 24),
('DPPH/00018', 'LPB/00013', 14, '2017-05-20', '', '0000-00-00', 0, '', 10000, '', '', '', 0, '', '', 0, 25),
('DPPH/00018', 'LPB/00022', 14, '2017-06-08', '', '0000-00-00', 0, '', 10000, '', '', '', 0, '', '', 0, 26),
('DPPH/00018', 'LPB/00022', 14, '2017-06-14', '', '0000-00-00', 0, '', 10000, '', '', '', 0, '', '', 0, 27),
('DPPH/00018', 'LPB/00023', 14, '2017-06-15', '', '0000-00-00', 0, '', 10000, '', '', '', 0, '', '', 0, 28),
('DPPH/00018', 'LPB/00023', 14, '2017-06-08', '', '0000-00-00', 0, '', 10000, '', '', '', 0, '', '', 0, 29),
('DPPH/00018', 'LPB/00024', 14, '2017-06-15', '', '0000-00-00', 0, '', 10000, '', '', '', 0, '', '', 0, 30),
('DPPH/00018', 'LPB/00025', 14, '2017-06-14', '', '0000-00-00', 0, '', 10000, '', '', '', 0, '', '', 0, 31),
('DPPH/00017', 'LPB/00022', 14, '2017-06-14', '', '0000-00-00', 3000, '', 21111, '', '', '', 0, '', '', 0, 32),
('DPPH/00017', 'LPB/00022', 14, '2017-06-08', '', '0000-00-00', 10000, '', 0, '', '', '', 0, '', '', 0, 33),
('DPPH/00017', 'LPB/00023', 14, '2017-06-15', '', '0000-00-00', 10000, '', 0, '', '', '', 0, '', '', 0, 34),
('DPPH/00017', 'LPB/00023', 14, '2017-06-08', '', '0000-00-00', 10000, '', 0, '', '', '', 0, '', '', 0, 35),
('DPPH/00017', 'LPB/00024', 14, '2017-06-15', '', '0000-00-00', 10000, '', 0, '', '', '', 0, '', '', 0, 36),
('DPPH/00017', 'LPB/00025', 14, '2017-06-14', '', '0000-00-00', 2000, '', 0, '', '', '', 0, '', '', 0, 37),
('DPPH/00019', 'LPB/00012', 14, '0000-00-00', '', '0000-00-00', 10000000000, '', 400, '', '', '', 0, '', '', 0, 38),
('DPPH/00019', 'LPB/00013', 14, '0000-00-00', '', '0000-00-00', 1000000000, '', 400, '', '', '', 0, '', '', 0, 39),
('DPPH/00019', 'LPB/00022', 14, '0000-00-00', '', '0000-00-00', 3000, '', 400, '', '', '', 0, '', '', 0, 40),
('DPPH/00019', 'LPB/00022', 14, '0000-00-00', '', '0000-00-00', 10000, '', 400, '', '', '', 0, '', '', 0, 41),
('DPPH/00019', 'LPB/00023', 14, '0000-00-00', '', '0000-00-00', 10000, '', 400, '', '', '', 0, '', '', 0, 42),
('DPPH/00019', 'LPB/00023', 14, '0000-00-00', '', '0000-00-00', 10000, '', 400, '', '', '', 0, '', '', 0, 43),
('DPPH/00019', 'LPB/00024', 14, '0000-00-00', '', '0000-00-00', 10000, '', 400, '', '', '', 0, '', '', 0, 44),
('DPPH/00019', 'LPB/00025', 14, '0000-00-00', '', '0000-00-00', 2000, '', 400, '', '', '', 0, '', '', 0, 45);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_dpph`
--

CREATE TABLE `detail_dpph` (
  `id_dpph` varchar(30) NOT NULL,
  `id_lpb` varchar(30) NOT NULL,
  `id_ttf` varchar(30) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `tgljt` date NOT NULL,
  `jumlah` double NOT NULL,
  `tunai` double NOT NULL,
  `ch_gb` varchar(20) NOT NULL,
  `bank` varchar(35) NOT NULL,
  `kode_bank` varchar(20) NOT NULL,
  `id_bkk` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `id_detail_dpph` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_dpph`
--

INSERT INTO `detail_dpph` (`id_dpph`, `id_lpb`, `id_ttf`, `supplier_id`, `tgljt`, `jumlah`, `tunai`, `ch_gb`, `bank`, `kode_bank`, `id_bkk`, `tanggal`, `keterangan`, `deleted`, `id_detail_dpph`) VALUES
('DPPH/00011', 'LPB/00013', 'TTF/00007', 14, '2017-05-20', 1000000000, 0, '', '1000', '', '2323``', '2017-05-19', '', 0, 1),
('DPPH/00001', 'LPB/00001', 'TTF/00001', 10, '0000-00-00', 1000000, 1500, 'CH', 'BNI', '091', 'BKK0101', '2017-11-04', 'Yes', 0, 4),
('DPPH/00003', 'LPB/00001', 'TTF/00001', 10, '0000-00-00', 1000000, 0, '', '', '', '', '0000-00-00', '', 0, 5),
('DPPH/00005', 'LPB/00001', 'TTF/00001', 10, '2017-04-28', 1000000, 0, '', '', '', '', '0000-00-00', '', 0, 6),
('DPPH/00015', 'LPB/00013', 'TTF/00007', 14, '0000-00-00', 0, 1212, ' sasa', 'wqqw', 'w', 'qwqwq', '2017-06-08', 'qsasasasas', 0, 10),
('DPPH/00015', 'LPB/00013', 'TTF/00007', 14, '0000-00-00', 0, 1, 'asas', 'bc', 'njhj', 'asas', '2017-06-08', 'assasas', 0, 11),
('DPPH/00014', 'LPB/00012', 'TTF/00005', 14, '2017-05-20', 0, 0, '', '100000', '', '', '0000-00-00', '', 0, 12),
('DPPH/00014', 'LPB/00013', 'TTF/00007', 14, '2017-05-20', 0, 0, '', '', '', '', '0000-00-00', '', 0, 13),
('DPPH/00014', 'LPB/00022', 'TTF/00004', 14, '2017-06-08', 0, 0, '', '', '', '', '0000-00-00', '', 0, 14),
('DPPH/00014', 'LPB/00023', 'TTF/00004', 14, '2017-06-08', 0, 0, '', '', '', '', '0000-00-00', '', 0, 15),
('DPPH/00014', 'LPB/00023', 'TTF/00009', 14, '2017-06-15', 0, 0, '', '', '', '', '0000-00-00', '', 0, 16),
('DPPH/00014', 'LPB/00024', 'TTF/00009', 14, '2017-06-15', 0, 0, '', '', '', '', '0000-00-00', '', 0, 17),
('DPPH/00013', 'LPB/00012', 'TTF/00005', 14, '0000-00-00', 0, 1000, '100', '10', '1212', 'gfhfgfh', '2017-06-01', 'asasa', 0, 18),
('DPPH/00013', 'LPB/00013', 'TTF/00007', 14, '0000-00-00', 0, 0, '', '', '', '', '0000-00-00', '', 0, 19),
('DPPH/00013', 'LPB/00022', 'TTF/00004', 14, '0000-00-00', 0, 0, '', '', '', '', '0000-00-00', '', 0, 20),
('DPPH/00013', 'LPB/00023', 'TTF/00004', 14, '0000-00-00', 0, 0, '', '', '', '', '0000-00-00', '', 0, 21),
('DPPH/00013', 'LPB/00023', 'TTF/00009', 14, '0000-00-00', 0, 0, '', '', '', '', '0000-00-00', '', 0, 22),
('DPPH/00013', 'LPB/00024', 'TTF/00009', 14, '0000-00-00', 0, 0, '', '', '', '', '0000-00-00', '', 0, 23),
('DPPH/00018', 'LPB/00012', 'TTF/00005', 14, '2017-05-20', 0, 10000, '', '', '', '', '0000-00-00', '', 0, 24),
('DPPH/00018', 'LPB/00013', 'TTF/00007', 14, '2017-05-20', 0, 10000, '', '', '', '', '0000-00-00', '', 0, 25),
('DPPH/00018', 'LPB/00022', 'TTF/00004', 14, '2017-06-08', 0, 10000, '', '', '', '', '0000-00-00', '', 0, 26),
('DPPH/00018', 'LPB/00022', 'TTF/00010', 14, '2017-06-14', 0, 10000, '', '', '', '', '0000-00-00', '', 0, 27),
('DPPH/00018', 'LPB/00023', 'TTF/00009', 14, '2017-06-15', 0, 10000, '', '', '', '', '0000-00-00', '', 0, 28),
('DPPH/00018', 'LPB/00023', 'TTF/00004', 14, '2017-06-08', 0, 10000, '', '', '', '', '0000-00-00', '', 0, 29),
('DPPH/00018', 'LPB/00024', 'TTF/00009', 14, '2017-06-15', 0, 10000, '', '', '', '', '0000-00-00', '', 0, 30),
('DPPH/00018', 'LPB/00025', 'TTF/00010', 14, '2017-06-14', 0, 10000, '', '', '', '', '0000-00-00', '', 0, 31),
('DPPH/00017', 'LPB/00022', 'TTF/00010', 14, '2017-06-14', 3000, 21111, '', '', '', '', '0000-00-00', '', 0, 32),
('DPPH/00017', 'LPB/00022', 'TTF/00004', 14, '2017-06-08', 10000, 0, '', '', '', '', '0000-00-00', '', 0, 33),
('DPPH/00017', 'LPB/00023', 'TTF/00009', 14, '2017-06-15', 10000, 0, '', '', '', '', '0000-00-00', '', 0, 34),
('DPPH/00017', 'LPB/00023', 'TTF/00004', 14, '2017-06-08', 10000, 0, '', '', '', '', '0000-00-00', '', 0, 35),
('DPPH/00017', 'LPB/00024', 'TTF/00009', 14, '2017-06-15', 10000, 0, '', '', '', '', '0000-00-00', '', 0, 36),
('DPPH/00017', 'LPB/00025', 'TTF/00010', 14, '2017-06-14', 2000, 0, '', '', '', '', '0000-00-00', '', 0, 37),
('DPPH/00019', 'LPB/00012', 'TTF/00005', 14, '0000-00-00', 10000000000, 400, '', '', '', '', '2017-06-15', '', 0, 38),
('DPPH/00019', 'LPB/00013', 'TTF/00007', 14, '0000-00-00', 1000000000, 400, '', '', '', '', '2017-02-02', '', 0, 39),
('DPPH/00019', 'LPB/00022', 'TTF/00010', 14, '0000-00-00', 3000, 400, '', '', '', '', '0000-00-00', '', 0, 40),
('DPPH/00019', 'LPB/00022', 'TTF/00004', 14, '0000-00-00', 10000, 400, '', '', '', '', '0000-00-00', '', 0, 41),
('DPPH/00019', 'LPB/00023', 'TTF/00009', 14, '0000-00-00', 10000, 400, '', '', '', '', '0000-00-00', '', 0, 42),
('DPPH/00019', 'LPB/00023', 'TTF/00004', 14, '0000-00-00', 10000, 400, '', '', '', '', '0000-00-00', '', 0, 43),
('DPPH/00019', 'LPB/00024', 'TTF/00009', 14, '0000-00-00', 10000, 400, '', '', '', '', '0000-00-00', '', 0, 44),
('DPPH/00019', 'LPB/00025', 'TTF/00010', 14, '0000-00-00', 2000, 400, '', '', '', '', '0000-00-00', '', 0, 45),
('DPPH/00020', 'LPB/00022', 'TTF/00010', 14, '2017-06-14', 3000, 0, '', '', '', '', '0000-00-00', '', 0, 46),
('DPPH/00020', 'LPB/00022', 'TTF/00004', 14, '2017-06-08', 10000, 0, '', '', '', '', '0000-00-00', '', 0, 47),
('DPPH/00020', 'LPB/00023', 'TTF/00004', 14, '2017-06-08', 10000, 0, '', '', '', '', '0000-00-00', '', 0, 48),
('DPPH/00020', 'LPB/00023', 'TTF/00009', 14, '2017-06-15', 10000, 0, '', '', '', '', '0000-00-00', '', 0, 49),
('DPPH/00020', 'LPB/00024', 'TTF/00009', 14, '2017-06-15', 10000, 0, '', '', '', '', '0000-00-00', '', 0, 50),
('DPPH/00020', 'LPB/00025', 'TTF/00010', 14, '2017-06-14', 2000, 0, '', '', '', '', '0000-00-00', '', 0, 51);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_kpso`
--

CREATE TABLE `detail_kpso` (
  `id_detail_kpso` int(11) NOT NULL,
  `id_kpso` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `qty_saldo_fisik` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_kpso`
--

INSERT INTO `detail_kpso` (`id_detail_kpso`, `id_kpso`, `item_id`, `satuan`, `qty_saldo_fisik`, `deleted`) VALUES
(1, 'SO00001', 1, 'Kg', 123, 0),
(2, 'SO00001', 6, 'Kg', 124, 0),
(3, 'SO00001', 9, 'Kg', 123, 0),
(4, 'SO00002', 6, 'Kg', 1200, 0),
(5, 'SO00003', 1, 'Kg', 234, 0),
(6, 'SO00003', 1, 'Kg', 234, 0),
(7, 'SO00003', 1, 'Kg', 234, 0),
(8, 'SO00003', 1, 'Kg', 234, 0),
(9, 'SO00003', 1, 'Kg', 234, 0),
(10, 'SO00003', 1, 'Kg', 234, 0),
(11, 'SO00004', 10, 'Kg', 55, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_lpb`
--

CREATE TABLE `detail_lpb` (
  `id_lpb` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `bruto` double NOT NULL,
  `weight_empty` double NOT NULL,
  `netto` double NOT NULL,
  `unit_price` double NOT NULL,
  `unit_qty` double NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `diskon` double NOT NULL,
  `custom2` varchar(50) NOT NULL,
  `custom3` varchar(50) NOT NULL,
  `custom4` varchar(50) NOT NULL,
  `custom5` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_lpb`
--

INSERT INTO `detail_lpb` (`id_lpb`, `item_id`, `bruto`, `weight_empty`, `netto`, `unit_price`, `unit_qty`, `note`, `deleted`, `diskon`, `custom2`, `custom3`, `custom4`, `custom5`) VALUES
('BPB/00001', 11, 222, 34, 200, 15, 0, '', 0, 2, '', '', '3178', '-848'),
('BPB/00002', 9, 150, 50, 100, 15, 0, '', 0, 2, '', '', '3178', '-848'),
('BPB/00003', 11, 2323, 2323, 23232, 15, 0, '', 0, 2, '', '', '3178', '-848'),
('LPB/00004', 14, 0, 0, 0, 15, 212, '', 0, 2, 'satuan', '', '3178', '-848'),
('LPB/00006', 16, 4, 0, 0, 15, 0, '', 0, 2, 'undefined', '', '3178', '-848'),
('LPB/00005', 16, 0, 0, 0, 15, 212, '', 0, 2, 'satuan', '', '3178 ', '-212'),
('LPB/00004', 14, 0, 0, 0, 15, 212, '', 0, 2, 'satuan', '', '3178', '-848'),
('BPB/00003', 9, 0, 0, 0, 15, 50, '', 0, 2, '', '', '3178', '-848'),
('LPB/00006', 14, 0, 0, 0, 12, 212, '', 0, 2, 'satuan', '', '2542', '-848'),
('DN/00002', 14, 0, 0, 0, 15, 212, '', 0, 2, 'wqwqw', '', '3178 ', '-212'),
('DN/00002', 16, 0, 0, 0, 15, 9, '', 0, 2, 'wqwqw', '', '3178 ', '-212'),
('SJR/00001', 14, 0, 0, 0, 0, 0, '', 0, 0, 'wqwqw', '', '', '0'),
('SJR/00001', 16, 0, 0, 0, 0, 0, '', 0, 0, 'wqwqw', '', '', '0'),
('LPB/00010', 16, 0, 0, 0, 15000000, 212, '', 0, 2000000, 'satuan', '', '3178000000', '-424'),
('LPB/00011', 16, 0, 0, 0, 10000, 212, '', 0, 0, 'satuan', '', '0 ', '-424'),
('LPB/00004', 16, 0, 0, 0, 15, 212, '', 0, 2, 'satuan', '', '3178  ', '-848'),
('LPB/00012', 13, 0, 0, 0, 3000, 1, '', 0, 0, 'satuan', '', '0 ', '-6'),
('LPB/00012', 16, 0, 0, 0, 10000, 2, '', 0, 0, 'satuan', '', '0 ', '-2'),
('LPB/00013', 13, 0, 0, 0, 3000, 1, '', 0, 0, 'satuan', '', '0 ', '-12'),
('LPB/00013', 16, 0, 0, 0, 10000, 2, '', 0, 0, 'satuan', '', '0 ', '-8'),
('LPB/00014', 13, 0, 0, 0, 3000, 1, '', 0, 0, 'satuan', '', '0 ', '-12'),
('LPB/00014', 16, 0, 0, 0, 10000, 2, '', 0, 0, 'satuan', '', '0 ', '-8'),
('LPB/00015', 13, 0, 0, 0, 3000, 1, '', 0, 0, 'satuan', '', '0 ', '-18'),
('LPB/00015', 16, 0, 0, 0, 10000, 2, '', 0, 0, 'satuan', '', '0 ', '-14'),
('LPB/00016', 13, 0, 0, 0, 3000, 1, '', 0, 0, 'satuan', '', '24000', '-26'),
('LPB/00016', 16, 0, 0, 0, 10000, 2, '', 0, 0, 'satuan', '', '0 ', '-22'),
('LPB/00017', 13, 0, 0, 0, 3000, 1, '', 0, 0, 'satuan', '', '0 ', '-32'),
('LPB/00017', 16, 0, 0, 0, 10000, 2, '', 0, 0, 'satuan', '', '0 ', '-26'),
('LPB/00025', 13, 0, 0, 0, 1000, 10, '', 0, 20, 'satuan', '', '9980', '-14'),
('LPB/00025', 16, 0, 0, 0, 15, 212, '', 0, 30, 'satuan', '', '3150', '-212'),
('LPB/00026', 16, 0, 0, 0, 10000, 6, '', 0, 0, 'kg', '', '60000 ', '0'),
('LPB/00027', 16, 0, 0, 0, 10000, 6, '', 0, 0, 'kg', '', '0 ', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_memo_rp`
--

CREATE TABLE `detail_memo_rp` (
  `id_detail_memo_rp` int(11) NOT NULL,
  `id_memo_rp` varchar(255) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_dikirm` double NOT NULL,
  `satuan_dikirim` varchar(10) NOT NULL,
  `qty_tdk_sesuai` double NOT NULL,
  `satuan_tdk_sesuai` varchar(10) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `deleted` int(11) NOT NULL,
  `satuan_diterima` varchar(255) NOT NULL,
  `qty_diterima` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_memo_rp`
--

INSERT INTO `detail_memo_rp` (`id_detail_memo_rp`, `id_memo_rp`, `item_id`, `qty_dikirm`, `satuan_dikirim`, `qty_tdk_sesuai`, `satuan_tdk_sesuai`, `keterangan`, `deleted`, `satuan_diterima`, `qty_diterima`) VALUES
(1, 'MRJ00001', 1, 0, 'KG', 10000, 'KG', 'Jelek', 0, 'KG', 5000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_mr`
--

CREATE TABLE `detail_mr` (
  `id_mr` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `qty` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_mr`
--

INSERT INTO `detail_mr` (`id_mr`, `item_id`, `note`, `qty`, `deleted`, `custom1`, `custom2`) VALUES
('MR/00001', 13, 'jelek', 10, 0, 'Hitam', 'satuan'),
('MR/00001', 16, 'jelek', 212, 0, 'Hitam', 'satuan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_np`
--

CREATE TABLE `detail_np` (
  `id_detail_np` int(11) NOT NULL,
  `id_np` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` double NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga_satuan` double NOT NULL,
  `jumlah` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `detail_np`
--

INSERT INTO `detail_np` (`id_detail_np`, `id_np`, `item_id`, `qty`, `satuan`, `harga_satuan`, `jumlah`, `deleted`) VALUES
(1, 'NP00001', 1, 120000, 'Kg', 1012120, 121454400000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_npb`
--

CREATE TABLE `detail_npb` (
  `id_npb` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_coly` double NOT NULL,
  `qty_ton` double NOT NULL,
  `harga` double NOT NULL,
  `diskon` double NOT NULL,
  `jumlah` double NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_npb`
--

INSERT INTO `detail_npb` (`id_npb`, `item_id`, `qty_coly`, `qty_ton`, `harga`, `diskon`, `jumlah`, `status`, `deleted`) VALUES
('NPB000001', 11, 10, 1000, 100000, 100, 1000000, 1, 0),
('NPB000002', 1, 100, 100, 10, 100, 1000000, 1, 0),
('NPB000002', 1, 5, 900, 5, 10, 6, 0, 0),
('NPB000002', 1, 10, 100, 1000, 10, 1000, 0, 0),
('NPP/00003', 1, 2, 3, 1, 5, -12, 0, 0),
('NPP/00004', 6, 4, 3, 500, 5, 1485, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_npum`
--

CREATE TABLE `detail_npum` (
  `id_detail_npum` int(11) NOT NULL,
  `id_npum` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` double NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga_satuan` double NOT NULL,
  `jumlah` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_npum`
--

INSERT INTO `detail_npum` (`id_detail_npum`, `id_npum`, `item_id`, `qty`, `satuan`, `harga_satuan`, `jumlah`, `deleted`) VALUES
(1, 'NPUM00001', 1, 120000, 'Kg', 1012120, 121454400000, 0),
(2, 'NPUM00002', 1, 10000000, 'Kg', 1012120, 10121200000000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_nrp`
--

CREATE TABLE `detail_nrp` (
  `id_nrp` varchar(255) NOT NULL,
  `id_detail_nrp` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty` double NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_nrp`
--

INSERT INTO `detail_nrp` (`id_nrp`, `id_detail_nrp`, `item_id`, `qty`, `satuan`, `harga`, `deleted`) VALUES
('NRJ00001', 3, 1, 5000, 'KG', 121212, 0),
('NRJ00002', 4, 1, 5000, 'KG', 20000000, 0),
('NRJ00003', 5, 1, 5000, 'KG', 2123232, 0),
('NRJ00004', 6, 0, 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_nump`
--

CREATE TABLE `detail_nump` (
  `id_nump` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `harga` double NOT NULL,
  `diskon` double NOT NULL,
  `jumlah` double NOT NULL,
  `qty` double NOT NULL,
  `qty_ton` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_nump`
--

INSERT INTO `detail_nump` (`id_nump`, `item_id`, `harga`, `diskon`, `jumlah`, `qty`, `qty_ton`, `deleted`) VALUES
('NUM/00002', 1, 20, 2, 36000, 0, 2000, 0),
('NUM/00003', 6, 1029, 100, 945722, 1000, 1000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_ph`
--

CREATE TABLE `detail_ph` (
  `id_ph` varchar(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `parameter` double NOT NULL,
  `qty` double NOT NULL,
  `qty_coly` int(11) NOT NULL,
  `harga_qty` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_ph`
--

INSERT INTO `detail_ph` (`id_ph`, `item_id`, `parameter`, `qty`, `qty_coly`, `harga_qty`, `satuan`, `deleted`) VALUES
('SP00001', 1, 12, 120000, 12, 1012120, 'Kg', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_phb`
--

CREATE TABLE `detail_phb` (
  `id_detail_phb` int(11) NOT NULL,
  `id_phb` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `harga_lama` double NOT NULL,
  `harga_baru` double NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  `deleted` int(11) NOT NULL,
  `acc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_phb`
--

INSERT INTO `detail_phb` (`id_detail_phb`, `id_phb`, `item_id`, `harga_lama`, `harga_baru`, `keterangan`, `deleted`, `acc`) VALUES
(1, 'PHB00002', 6, 3000, 1000, 'murah', 0, 1),
(4, 'PHB00001', 1, 2000, 3000, 'mahal', 1, 0),
(5, 'PHB00001', 1, 3000, 2000, 'turun', 1, 0),
(6, 'PHB00001', 1, 2000, 3000, 'mahal', 0, 0),
(7, 'PHB00003', 14, 10000, 1000, 'as', 0, 0),
(8, 'PHB00003', 13, 0, 3000, 'Harga Awal', 0, 0),
(16, 'PHB00006', 13, 3000, 1000, 'murah', 0, 1),
(17, 'PHB00006', 16, 1000, 0, '', 0, 1),
(18, 'PHB00007', 19, 10000, 11500, 'Harga naik', 0, 0),
(19, 'PHB00008', 13, 1000, 234, 'ad', 0, 0),
(20, 'PHB00004', 13, 1000, 2000, 'naik', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_po`
--

CREATE TABLE `detail_po` (
  `id_po` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_request` double NOT NULL,
  `qty_accept` double NOT NULL,
  `unit_price` double NOT NULL,
  `balance` double NOT NULL,
  `discount` double NOT NULL,
  `tax` double NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_po`
--

INSERT INTO `detail_po` (`id_po`, `item_id`, `qty_request`, `qty_accept`, `unit_price`, `balance`, `discount`, `tax`, `note`, `deleted`, `custom1`, `custom2`) VALUES
('PO/00004', 14, 3, 0, 0, 0, 0, 0, '', 1, 'Barang Umum', 'qwqw'),
('PO/00003', 16, 212, 0, 0, 0, 0, 0, '', 0, '', 'wqwqw'),
('PO/00005', 0, 0, 0, 1000000, 990000, 10000, 0, '', 1, '', 'wqwqw'),
('PO/00006', 0, 1, 0, 10000, 0, 1000, 0, '', 0, ' ', 'lp'),
('PO/00008', 0, 1, 0, 10000, 0, 1000, 0, '', 0, ' ', 'qwqw'),
('PO/00009', 0, 1, 0, 10000, 0, 100, 0, '', 0, ' ', 'qww'),
('PO/00010', 0, 1, 0, 10000000, 0, 1000, 0, '', 0, ' ', 'AC pak DAr Rusak'),
('PO/00012', 18, 5, 0, 5000, 24900, 100, 0, '', 0, 'Putih', 'satuan'),
('PO/00003', 16, 212, 0, 10000, 2119990, 10, 0, '', 0, 'undefined', 'undefined'),
('PO/00015', 0, 1, 0, 10000, 0, 100, 0, '', 0, ' ', '10000'),
('PO/00017', 0, 1, 0, 10000, 0, 500, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00017', 0, 1, 0, 10000, 0, 500, 0, '', 0, ' ', 'Tambah Freon '),
('PO/00017', 0, 1, 0, 10000, 0, 2, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00017', 0, 0, 0, 10000, 0, 1, 0, '', 0, ' ', 'Tambah Freon '),
('PO/00017', 0, 1, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon '),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon '),
('PO/00017', 0, 1, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon '),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon '),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon '),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00017', 0, 0, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon '),
('PO/00019', 13, 6, 0, 3000, 0, 0, 0, '', 0, 'Hitam', 'Hitam'),
('PO/00019', 16, 10, 0, 10000, 0, 0, 0, '', 0, 'Hitam', 'Hitam'),
('PO/00020', 0, 1, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00020', 0, 1, 0, 10000, 0, 0, 0, '', 0, ' ', 'Tambah Freon'),
('PO/00024', 13, 6, 0, 3000, 0, 0, 0, '', 0, 'Hitam', 'satuan'),
('PO/00024', 16, 10, 0, 10000, 0, 0, 0, '', 0, 'Hitam', 'satuan'),
('PO/00024', 13, 6, 0, 3000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00024', 16, 10, 0, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00024', 13, 6, 0, 3000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00024', 16, 10, 0, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00024', 13, 6, 0, 3000, 0, 0, 0, '', 0, 'Hitam', 'satuan'),
('PO/00024', 16, 10, 0, 10000, 0, 0, 0, '', 0, 'Hitam', 'satuan'),
('PO/00024', 13, 6, 0, 3000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00024', 16, 10, 0, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00024', 13, 6, 0, 3000, 0, 0, 0, '', 0, 'Hitam', 'satuan'),
('PO/00024', 16, 10, 0, 10000, 0, 0, 0, '', 0, 'Hitam', 'satuan'),
('PO/00024', 13, 6, 0, 3000, 0, 0, 0, '', 0, 'Hitam', 'satuan'),
('PO/00024', 16, 10, 0, 10000, 0, 0, 0, '', 0, 'Hitam', 'satuan'),
('PO/00024', 13, 6, 0, 3000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00024', 16, 10, 0, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00022', 16, 10, 10, 10000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00021', 13, 6, 6, 1000, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00021', 16, 10, 10, 0, 0, 0, 0, '', 0, 'satuan', 'Hitam'),
('PO/00026', 16, 12, 12, 0, 0, 0, 0, '', 0, 'undefined', 'undefined');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_psb`
--

CREATE TABLE `detail_psb` (
  `kode_supplier` varchar(30) NOT NULL,
  `nama_supplier` varchar(30) NOT NULL,
  `id_psb` varchar(30) NOT NULL,
  `id_detail_psb` int(11) NOT NULL,
  `alamat` varchar(40) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `contact_person` varchar(25) NOT NULL,
  `npwp` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `harga_satuan` double NOT NULL,
  `diskon` double NOT NULL,
  `ppn` varchar(0) NOT NULL,
  `nilai_ppn` double(255,0) NOT NULL,
  `pph` varchar(255) NOT NULL,
  `nilai_pph` int(255) NOT NULL,
  `total` double NOT NULL,
  `syarat_pembayaran` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_psb`
--

INSERT INTO `detail_psb` (`kode_supplier`, `nama_supplier`, `id_psb`, `id_detail_psb`, `alamat`, `phone`, `contact_person`, `npwp`, `item_id`, `harga_satuan`, `diskon`, `ppn`, `nilai_ppn`, `pph`, `nilai_pph`, `total`, `syarat_pembayaran`, `acc`, `deleted`) VALUES
('SUP/00008', 'Supplier2', 'PSB/00006', 8, 'Dukuh', '546456', 'vf', 3839388, 22, 1000000, 1000, '', 99900, '', 0, 1098900, 'tunai', 1, 0),
('SUP/00002', 'TJokoro', 'PSB/00001', 9, 'asdkkk', '303830', 'kd', 930399, 9, 15000, 5000, '', 1000, '', 0, 11000, 'tunai', 0, 0),
('SUP/00003', 'SUpplier', 'PSB/00001', 10, 'Manukan', '39383999', 'SN', 30393930, 9, 500000, 1000, '', 49900, '', 0, 548900, 'tunai', 0, 0),
('SUP/00004', 'Cengceng', 'PSB/00001', 11, 'Malang', '03838399', 'sj', 3938399, 9, 5000, 1000, '', 400, '', 0, 4400, 'tunai', 1, 0),
('SUP/00005', 'Rexplast', 'PSB/00002', 12, 'cokro', '3937', 'dj', 3838389, 24, 2000000, 1000, '', 199900, '', 0, 2198900, 'ok', 0, 0),
('SUP/00006', 'Kl', 'PSB/00002', 13, 'asd', '37', 'sd', 234, 24, 1000000, 100, '', 99990, '', 0, 1099890, 'tunai', 1, 0),
('SUP/00007', 'dd', 'PSB/00002', 14, 'malang', '3837', 'cn', 34234, 24, 300000, 1000, '', 29900, '', 0, 328900, 'tunai', 0, 0),
('SUP/00008', 'PT ABC', 'PSB/00003', 15, 'Jl. Warna Hitam No. 05', '085678432134', 'Bapak Budi', 2147483647, 17, 5000, 0, '', 500, '', 0, 5500, 'Tunai', 1, 0),
('', '1', 'PSB/00005', 56, 'a', 'a', '', 0, 18, 0, 0, '', 0, '', 0, 0, '', 0, 0),
('', 'as', 'PSB/00005', 57, 'dfghjkl', 'ertyuio', '2345678', 0, 18, 0, 12345678, '', 234567, '', 0, 1211111.1, '13322222.1', 0, 0),
('', 'erty', 'PSB/00005', 58, 'dfghj', '234567', 'ghj', 23, 18, 1234567890, 3456, '', 123456443, '', 0, 1358020877.4, 'kk', 1, 0),
('', 'erty', 'PSB/00005', 59, 'dfghj', '234567', 'ghj', 23, 18, 1234567890, 3456, '', 123456443, '', 0, 1358020877.4, 'kk', 0, 0),
('', 'CV Makmur Jaya', 'PSB/00007', 60, 'Jl. Makmur Jaya no 16', '085267189267', '', 2147483647, 22, 1000, 10, '', 99, '', 0, 1089, 'Tunai', 1, 0),
('', 'CV Makmur Jaya', 'PSB/00007', 61, 'Jl. Makmur Jaya no 16', '085267189267', '', 2147483647, 22, 1000, 10, '', 99, '', 0, 1089, 'Tunai', 0, 0),
('', 'CV Makmur Jaya', 'PSB/00007', 62, 'Jl. Makmur Jaya no 16', '085267189267', '', 2147483647, 22, 1000, 10, '', 0, '', 0, 1089, 'Tunai', 0, 0),
('', 'CV Makmur Jaya', 'PSB/00007', 63, 'Jl. Makmur Jaya no 16', '085267189267', '', 2147483647, 22, 1000, 10, '', 0, '', 0, 1089, 'Tunai', 0, 0),
('', 'dfghjk', 'PSB/00008', 64, '1', '12121212212', 'weweweewe1', 0, 9, 100000, 2880, '', 9712, '2428', 0, 109260, '', 0, 0),
('', 'dadda', 'PSB/00008', 65, 'rfgbnm', '12121212212', 'weweweewe1', 0, 9, 100000, 2880, '', 9712, '0', 2428, 109260, '', 0, 1),
('', 'danto', 'PSB/00008', 66, 'asasas', '21212121', 'asasasasa', 0, 9, 12121221212, 324321, '', 1212089689, '303022422', 0, 13636009002.375, '', 1, 0),
('', '', 'PSB/00009', 67, '', '', '', 0, 0, 0, 0, '', 0, '', 0, 0, '', 0, 1),
('', 'asd', 'PSB/00009', 68, 'sadad', '324234', 'asd', 0, 0, 0, 0, '', 0, '', 0, 0, 'sd', 0, 1),
('', 'asd', 'PSB/00009', 69, 'asd', '23423', 'asd', 234234, 22, 20000, 100, '', 1990, '', 0, 21890, 'sad', 1, 0),
('', 'asd', 'PSB/00009', 70, 'asd', '234', 'asdsad', 234, 22, 30000, 100, '', 2990, '', 0, 32890, 'sd', 0, 0),
('', 'asd', 'PSB/00009', 71, 'asd', '234234', 'asd', 234, 22, 40000, 100, '', 3990, '', 0, 43890, '', 0, 0),
('', 'kala', 'PSB/00010', 72, 'asd', '234', 'sd', 234, 9, 100000, 10, '', 9999, '', 0, 109989, 'tunai', 0, 0),
('', 'kaol', 'PSB/00010', 73, 'asdqweqw', '234324', 'asd', 384948, 9, 4000, 10, '', 0, '', 0, 3990, 'tunai', 0, 0),
('', 'pola', 'PSB/00010', 74, 'sad839', '748484', 'asd', 88, 9, 4000, 300, '', 0, '', 0, 3700, 'tunai', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_rk`
--

CREATE TABLE `detail_rk` (
  `id_detail_rk` int(11) NOT NULL,
  `id_rk` varchar(10) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `alamat` varchar(35) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `aktivitas` varchar(30) NOT NULL,
  `hasil` varchar(30) NOT NULL,
  `id_so` varchar(30) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_rpso`
--

CREATE TABLE `detail_rpso` (
  `id_detail_rpso` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `id_rpso` varchar(11) NOT NULL,
  `qty_sisa_saldo` int(11) NOT NULL,
  `qty_saldo_fisik` int(11) NOT NULL,
  `selisih` int(11) NOT NULL,
  `keterangan` varchar(11) NOT NULL,
  `tj` varchar(11) NOT NULL,
  `pj` varchar(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `detail_rpso`
--

INSERT INTO `detail_rpso` (`id_detail_rpso`, `item_id`, `id_rpso`, `qty_sisa_saldo`, `qty_saldo_fisik`, `selisih`, `keterangan`, `tj`, `pj`, `deleted`) VALUES
(1, 1, 'RSO0001', 1527, 5315, 3788, '', '', '', 0),
(2, 6, 'RSO0001', 1324, 2147483647, 2147482323, '', '', '', 0),
(3, 9, 'RSO0001', 123, 0, -123, '', '', '', 0),
(4, 1, 'RSO0002', 1527, -994685, -996212, '', '', '', 0),
(5, 6, 'RSO0002', 1324, 2147483647, 2147482323, '', '', '', 0),
(6, 9, 'RSO0002', 123, 0, -123, '', '', '', 0),
(7, 1, 'RSO0003', 1527, -994685, -996212, '', '', '', 0),
(8, 6, 'RSO0003', 1324, 2147483647, 2147483647, '', '', '', 0),
(9, 9, 'RSO0003', 123, 0, -123, '', '', '', 0),
(10, 1, 'RSO0004', 1527, -994685, -996212, '', '', '', 0),
(11, 6, 'RSO0004', 1324, 2147483647, 2147483647, '', '', '', 0),
(12, 9, 'RSO0004', 123, 0, -123, '', '', '', 0),
(13, 1, 'RSO0005', 1527, -994685, -996212, '', '', '', 0),
(14, 6, 'RSO0005', 1324, 2147483647, 2147482323, '', '', '', 0),
(15, 9, 'RSO0005', 123, 0, -123, '', '', '', 0),
(16, 1, 'RSO0006', 1527, -994685, -996212, 'saasa', '', '', 0),
(17, 6, 'RSO0006', 1324, 2147483647, 2147482323, '', 'sdd', '', 0),
(18, 9, 'RSO0006', 123, 0, -123, '', '', 'ssdd', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_sj`
--

CREATE TABLE `detail_sj` (
  `id_detail_sj` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `banyaknya` int(11) NOT NULL,
  `id_sj` varchar(20) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_sj`
--

INSERT INTO `detail_sj` (`id_detail_sj`, `item_id`, `satuan`, `banyaknya`, `id_sj`, `keterangan`, `deleted`) VALUES
(1, 1, '', 120000, 'sjp/00001', 'undefined ', 0),
(2, 6, '', 1000000, 'sjp/00003', 'undefined ', 0),
(3, 6, '', 1000000, 'sjp/00003', 'undefined ', 0),
(4, 1, 'Kg ', 120000, 'sjp/00005', '', 0),
(5, 1, 'Kg ', 10000000, 'sjp/00006', '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_sjm`
--

CREATE TABLE `detail_sjm` (
  `id_detail_sjm` int(10) NOT NULL,
  `id_sjm` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `banyaknya` int(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_sjm`
--

INSERT INTO `detail_sjm` (`id_detail_sjm`, `id_sjm`, `item_id`, `banyaknya`, `keterangan`, `deleted`) VALUES
(1, 'SJM/00009', 6, 900, 'Keterangan ', 0),
(2, 'SJM/00010', 6, 900, 'Keterangan ', 0),
(3, 'SJM/00010', 6, 900, 'Keterangan  ', 0),
(4, 'SJM/00011', 6, 900, 'Keterangan ', 0),
(5, 'SJM/00012', 1, 4, 'Keterangan ', 0),
(6, 'SJM/00013', 1, 1000000, 'ok ', 0),
(7, 'SJM/00013', 6, 1000000, 'ok ', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_sjr`
--

CREATE TABLE `detail_sjr` (
  `id_sjr` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `spek` varchar(30) NOT NULL,
  `satuan` varchar(20) NOT NULL,
  `qty` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_sjr`
--

INSERT INTO `detail_sjr` (`id_sjr`, `item_id`, `spek`, `satuan`, `qty`, `deleted`) VALUES
('SJR/00001', 14, 'wqwqw', '', 214, 0),
('SJR/00001', 16, 'wqwqw', '', 13, 0),
('SJR/00005', 1, 'jawa', 'Kg', 20000, 0),
('SJR/00006', 1, 'jawa', 'Kg', 20000, 0),
('SJR/00007', 13, 'Hitam', 'satuan', 10, 0),
('SJR/00007', 16, 'Hitam', 'satuan', 212, 0),
('SJR/00008', 13, 'Hitam', 'satuan', 10, 0),
('SJR/00008', 16, 'Hitam', 'satuan', 212, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_skb`
--

CREATE TABLE `detail_skb` (
  `id_skb` varchar(30) NOT NULL,
  `id_timbang` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `qty_coly` double NOT NULL,
  `qyt_ton` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_skb`
--

INSERT INTO `detail_skb` (`id_skb`, `id_timbang`, `tanggal`, `qty_coly`, `qyt_ton`, `deleted`) VALUES
('SKB00001', 'NT/00001', '2017-05-08', 30, 300, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_skj`
--

CREATE TABLE `detail_skj` (
  `id_detail_skj` int(11) NOT NULL,
  `no_kontrak` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `parameter` varchar(35) NOT NULL,
  `coly` double NOT NULL,
  `qty` double NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga` double NOT NULL,
  `jumlah` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_skj`
--

INSERT INTO `detail_skj` (`id_detail_skj`, `no_kontrak`, `item_id`, `parameter`, `coly`, `qty`, `satuan`, `harga`, `jumlah`, `deleted`) VALUES
(1, 'SK00001', 6, '01', 1000, 1000000, 'Kg', 120, 120000000, 0),
(2, 'SK00002', 6, 'KA 10', 10, 1000, 'Kg', 120, 1012120000, 0),
(3, 'SK00002', 1, 'KA 10', 10, 1000, 'Kg', 1012120, 1012120000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_so`
--

CREATE TABLE `detail_so` (
  `id_so` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_coly` int(11) NOT NULL,
  `qty_request` double NOT NULL,
  `qty_accept` double NOT NULL,
  `unit_price` double NOT NULL,
  `balance` double NOT NULL,
  `discount` double NOT NULL,
  `tax` double NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `parameter` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_so`
--

INSERT INTO `detail_so` (`id_so`, `item_id`, `qty_coly`, `qty_request`, `qty_accept`, `unit_price`, `balance`, `discount`, `tax`, `note`, `deleted`, `custom1`, `custom2`, `parameter`) VALUES
('SO/00001', 1, 12, 120000, 120000, 1012120, 121454400000, 0, 0, '', 0, '', '', '12'),
('SO/00002', 6, 1000, 1000000, 1000000, 120, 120000000, 0, 0, '', 0, '', '', '01'),
('SO/00003', 6, 39, 1000000, 1000000, 120, 120000000, 0, 0, '', 0, '', '', '0'),
('SO/00004', 18, 0, 19, 19, 1000, 0, 0, 0, '', 0, '', '', 'Parameter'),
('SO/00005', 1, 10, 10000000, 10000000, 1012120, 10121200000000, 0, 0, '', 0, '', '', 'Parameter'),
('SO/00006', 1, 0, 1000, 1000, 1012120, 1012120000, 0, 0, '', 0, '', '', 'KA 10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_spp`
--

CREATE TABLE `detail_spp` (
  `id_detail_spp` int(11) NOT NULL,
  `id_spp` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_request` double NOT NULL,
  `qty_accept` double NOT NULL,
  `qty_purchased` double NOT NULL,
  `date_accept` date NOT NULL,
  `balance` double NOT NULL,
  `discount` double NOT NULL,
  `tax` varchar(10) NOT NULL,
  `note` text NOT NULL,
  `deleted` int(11) NOT NULL,
  `cs1` varchar(100) NOT NULL,
  `cs2` varchar(100) NOT NULL,
  `cs3` varchar(100) NOT NULL,
  `cs4` varchar(100) NOT NULL,
  `cs5` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_spp`
--

INSERT INTO `detail_spp` (`id_detail_spp`, `id_spp`, `item_id`, `qty_request`, `qty_accept`, `qty_purchased`, `date_accept`, `balance`, `discount`, `tax`, `note`, `deleted`, `cs1`, `cs2`, `cs3`, `cs4`, `cs5`) VALUES
(1, 'PR/00001', 9, 50, 50, 0, '2017-03-27', 0, 0, '', '', 0, '', '', '', '', ''),
(2, 'PR/00004', 6, 12, 12, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(3, 'PR/00004', 1, 13, 13, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(4, 'PR/00004', 9, 1212, 1212, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(5, 'PR/00004', 10, 121212, 121212, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(6, 'PR/00004', 11, 1212, 1212, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(7, 'PR/00005', 1, 1, 1, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(8, 'PR/00005', 6, 12, 12, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(9, 'PR/00005', 9, 212, 212, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(10, 'PR/00005', 10, 122, 122, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(11, 'PR/00005', 11, 21212, 21212, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(12, 'PR/00005', 12, 12212121212, 12212121212, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(13, 'PR/00005', 13, 2121212, 2121212, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(14, 'PR/00005', 14, 1, 1, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(15, 'PR/00005', 16, 1, 1, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(16, 'PR/00005', 17, 12, 12, 0, '2017-04-11', 0, 0, '', '', 0, '', '', '', '', ''),
(22, 'PR/00006', 14, 3, 0, 0, '2017-04-28', 0, 0, '', '', 0, '', 'Barang Umum', '', '', ''),
(23, 'PR/00007', 14, 212, 0, 0, '2017-04-28', 0, 0, '', '', 0, 'satuan', 'spek', '', '', ''),
(24, 'PR/00007', 16, 212, 0, 0, '2017-04-28', 0, 0, '', '', 0, 'satuan', '', '', '', ''),
(25, 'PR/00007', 14, 212, 0, 0, '2017-04-28', 0, 0, '', '', 0, 'spek', 'satuan', '', '', ''),
(26, 'PR/00007', 16, 212, 0, 0, '2017-04-28', 0, 0, '', '', 0, '', 'satuan', '', '', ''),
(27, 'PR/00008', 0, 0, 0, 0, '2017-05-05', 0, 0, '', 'ac pak dar rusak', 0, '', '', '', '', ''),
(28, 'PR/00008', 0, 0, 0, 0, '2017-05-05', 0, 0, '', 'ac rusak', 0, '', '', '', '', ''),
(29, 'PR/00008', 0, 0, 0, 0, '2017-05-05', 0, 0, '', 'aas', 0, '', '', '', '', ''),
(30, 'PR/00008', 0, 0, 0, 0, '2017-05-05', 0, 0, '', 'aas', 0, '', '', '', '', ''),
(31, 'PR/00008', 0, 0, 0, 0, '2017-05-05', 0, 0, '', 'asas', 0, '', '', '', '', ''),
(32, 'PR/00009', 0, 0, 0, 0, '2017-05-05', 0, 0, '', 'ac', 0, '', '', '', '', ''),
(33, 'PR/00010', 18, 5, 0, 0, '2017-05-12', 0, 0, '', '', 0, 'satuan', 'Putih', '', '', ''),
(34, 'PR/00011', 0, 0, 0, 0, '2017-05-12', 0, 0, '', 'Tambah Freon', 0, '', '', '', '', ''),
(35, 'PR/00012', 0, 0, 0, 0, '2017-05-12', 0, 0, '', 'servis', 0, '', '', '', '', ''),
(36, 'PR/00013', 0, 0, 0, 0, '2017-05-12', 0, 0, '', 'Tambah Freon', 0, '', '', '', '', ''),
(37, 'PR/00014', 13, 10, 0, 0, '0000-00-00', 0, 0, '', '', 0, 'satuan', 'Hitam', '', '', ''),
(38, 'PR/00014', 16, 10, 0, 0, '0000-00-00', 0, 0, '', '', 0, 'Hitam', 'satuan', '', '', ''),
(39, 'PR/00001', 9, 50, 0, 0, '2017-05-12', 0, 0, '', '', 0, '', '', '', '', ''),
(40, 'PR/00014', 13, 10, 0, 0, '0000-00-00', 0, 0, '', '', 0, 'satuan', 'Hitam', '', '', ''),
(46, 'PR/00015', 0, 0, 0, 0, '2017-05-12', 0, 0, '', 'Tambah Freon', 0, '', '', '', '', ''),
(55, 'STB/00002', 1, 1000, 0, 0, '2017-05-19', 0, 0, '', '', 0, '', '', '', '', ''),
(56, 'PR/00019', 13, 10, 0, 0, '2017-06-10', 0, 0, '', '', 0, 'satuan', '2B', '', '', ''),
(57, 'PR/00019', 16, 10, 0, 0, '2017-06-10', 0, 0, '', '', 0, 'satuan', 'PUTIH', '', '', ''),
(58, 'PR/00021', 13, 3, 0, 0, '2017-06-12', 0, 0, '', '', 0, 'Pcs', 'asd', '', '', ''),
(59, 'PR/00022', 13, 2, 0, 0, '2017-06-12', 0, 0, '', '', 0, 'Pcs', 'wer', '', '', ''),
(60, 'PR/00023', 13, 2, 0, 0, '2017-06-12', 0, 0, '', '', 0, 'Pcs', 'sd', '', '', ''),
(61, 'PR/00025', 16, 12, 0, 0, '2017-07-19', 0, 0, '', '', 0, 'kg', 'hitam', '', '', ''),
(62, 'PR/00026', 0, 0, 0, 0, '2017-07-19', 0, 0, '', 'Servis Ac', 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_stb`
--

CREATE TABLE `detail_stb` (
  `id_stb` varchar(30) NOT NULL,
  `id_timbang` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `item_id` int(11) NOT NULL,
  `qty_coly` double NOT NULL,
  `qty_ton` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_stb`
--

INSERT INTO `detail_stb` (`id_stb`, `id_timbang`, `tanggal`, `item_id`, `qty_coly`, `qty_ton`, `deleted`) VALUES
('STB/00002', 'NT/00001', '2017-05-09', 1, 1000, 1000, 0),
('STB/00002', 'NT/00007', '2017-05-19', 6, 0, 1972, 0),
('STB/00001', 'NT/00007', '2017-05-19', 6, 0, 1972, 0),
('STB/00005', 'NT/00018', '2017-05-19', 1, 0, 1023, 0),
('STB/00008', 'NT/00018', '2017-05-24', 1, 0, 1023, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_suppliers`
--

CREATE TABLE `detail_suppliers` (
  `id_detail_suppliers` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `unit_price_acc` double NOT NULL,
  `unit_discount` double NOT NULL,
  `provision` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_suppliers`
--

INSERT INTO `detail_suppliers` (`id_detail_suppliers`, `supplier_id`, `item_id`, `unit_price`, `unit_price_acc`, `unit_discount`, `provision`, `acc`, `deleted`) VALUES
(1, 14, 13, 1000, 0, 0, '', 0, 0),
(2, 14, 16, 10000, 0, 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_suppliers_copy`
--

CREATE TABLE `detail_suppliers_copy` (
  `id_detail_suppliers` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `unit_price_acc` double NOT NULL,
  `unit_discount` double NOT NULL,
  `provision` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_suppliers_copy`
--

INSERT INTO `detail_suppliers_copy` (`id_detail_suppliers`, `supplier_id`, `item_id`, `unit_price`, `unit_price_acc`, `unit_discount`, `provision`, `acc`, `deleted`) VALUES
(0, 7, 1, 1222, 0, 0, '', 0, 0),
(1, 4, 9, 7000, 0, 0, '', 0, 0),
(2, 4, 10, 6500, 6000, 0, '', 1, 0),
(3, 6, 11, 3500, 3000, 0, '', 1, 0),
(4, 7, 9, 1000, 1000, 0, '', 1, 0),
(5, 10, 13, 1500, 0, 0, '', 0, 0),
(6, 10, 6, 150000, 0, 0, '', 1, 0),
(7, 9, 1, 5000, 5000, 10, '', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_ttf`
--

CREATE TABLE `detail_ttf` (
  `id_ttf` varchar(20) NOT NULL,
  `id_lpb` varchar(20) NOT NULL,
  `no_invoice` varchar(20) NOT NULL,
  `due_date` date NOT NULL,
  `balance` double NOT NULL,
  `custom1` date NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `id_bapj` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_ttf`
--

INSERT INTO `detail_ttf` (`id_ttf`, `id_lpb`, `no_invoice`, `due_date`, `balance`, `custom1`, `custom2`, `id_bapj`, `deleted`) VALUES
('TTF/00005', 'LPB/00012', 'sdfsdf', '0000-00-00', 10000000000, '2017-05-17', '', '', 0),
('TTF/00007', 'LPB/00013', 'asd', '0000-00-00', 1000000000, '2017-05-17', '', '', 0),
('TTF/00004', 'LPB/00023', 'jhk', '0000-00-00', 10000, '1970-01-01', '', '', 1),
('TTF/00004', 'LPB/00022', 'jhk', '0000-00-00', 10000, '0000-00-00', '', '', 1),
('TTF/00009', 'LPB/00024', 'asasa', '0000-00-00', 10000, '1970-01-01', '', '', 0),
('TTF/00009', 'LPB/00023', 'asasa', '0000-00-00', 10000, '0000-00-00', '', '', 0),
('TTF/00010', 'LPB/00025', 'wer1230', '0000-00-00', 2000, '1970-01-01', '', '', 0),
('TTF/00010', 'LPB/00022', '9348', '0000-00-00', 3000, '0000-00-00', '', '', 0),
('TTF/00011', '', 'fsdfs', '0000-00-00', 10000, '2017-06-12', '', '', 0),
('TTF/00013', '', 'asasas', '0000-00-00', 0, '2017-06-12', '', '', 0),
('TTF/00013', '', 'asas', '0000-00-00', 10000000, '2017-06-12', '', '', 0),
('TTF/00013', '', 'n89', '0000-00-00', 10000, '2017-06-12', '', ' ', 0),
('TTF/00015', '', 'asdasd', '0000-00-00', 222, '2017-06-12', '', 'BAPJ/00011', 0),
('TTF/00016', 'LPB/00027', '930399', '0000-00-00', 10000, '2017-07-19', '', '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dn`
--

CREATE TABLE `dn` (
  `id_dn` varchar(20) NOT NULL,
  `id_mr` varchar(20) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `note` text NOT NULL,
  `person_id` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `total` double NOT NULL,
  `diskon` double NOT NULL,
  `dpp` double NOT NULL,
  `ppn` double NOT NULL,
  `gt` double NOT NULL,
  `type_beli` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dn`
--

INSERT INTO `dn` (`id_dn`, `id_mr`, `date_transaction`, `date_input`, `note`, `person_id`, `acc`, `deleted`, `custom1`, `custom2`, `supplier_id`, `total`, `diskon`, `dpp`, `ppn`, `gt`, `type_beli`, `status`) VALUES
('DN/00001', 'MR/00001', '2017-06-19', '2017-06-19', 'cepet diretur ae', 1, 0, 0, '', '', 14, 26, 0, 26, 2.6, 123933, 'Kredit', 1),
('DN/00002', 'MR/00001', '2017-07-19', '2017-07-19', '', 1, 0, 0, '', '', 14, 0, 0, 0, 0, 0, 'Kredit', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dpp`
--

CREATE TABLE `dpp` (
  `id_dpp` varchar(30) NOT NULL,
  `tglaw` date NOT NULL,
  `tglak` date NOT NULL,
  `total` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dpp`
--

INSERT INTO `dpp` (`id_dpp`, `tglaw`, `tglak`, `total`, `deleted`, `status`, `jenis`) VALUES
('DPP/00001', '2017-06-01', '2017-07-19', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dpph`
--

CREATE TABLE `dpph` (
  `id_dpph` varchar(30) NOT NULL,
  `tglaw` date NOT NULL,
  `tglak` date NOT NULL,
  `total` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dpph`
--

INSERT INTO `dpph` (`id_dpph`, `tglaw`, `tglak`, `total`, `deleted`, `status`, `jenis`) VALUES
('DPPH/00001', '2017-04-23', '2017-07-07', 1000000, 0, 3, NULL),
('DPPH/00002', '2017-04-26', '0000-00-00', 0, 0, 0, NULL),
('DPPH/00003', '2017-01-01', '2017-06-08', 0, 0, 0, NULL),
('DPPH/00004', '2017-03-02', '2017-05-06', 0, 0, 0, NULL),
('DPPH/00005', '2017-03-02', '2017-05-06', 1000000, 0, 1, NULL),
('DPPH/00006', '2017-05-10', '2017-05-31', 0, 0, 0, NULL),
('DPPH/00007', '2017-01-01', '2017-05-31', 0, 0, 0, NULL),
('DPPH/00008', '2017-05-12', '2017-05-13', 0, 0, 0, NULL),
('DPPH/00009', '2017-04-11', '2017-05-13', 0, 0, 0, NULL),
('DPPH/00010', '2017-04-05', '2017-06-10', 0, 0, 0, NULL),
('DPPH/00011', '2017-01-01', '2017-05-31', 1000000000, 0, 3, NULL),
('DPPH/00012', '2010-02-02', '2017-05-20', 0, 0, 0, NULL),
('DPPH/00013', '2017-03-01', '2017-06-30', 0, 0, 3, NULL),
('DPPH/00014', '2017-03-01', '2017-06-30', 0, 0, 3, NULL),
('DPPH/00015', '2017-04-04', '2017-07-07', 0, 0, 3, NULL),
('DPPH/00016', '2017-06-01', '2017-06-30', 0, 0, 0, 'LPB'),
('DPPH/00017', '2017-06-01', '2017-06-30', 45, 0, 2, 'JT'),
('DPPH/00018', '2017-02-01', '2017-06-15', 0, 0, 2, 'LPB'),
('DPPH/00019', '2017-02-01', '2017-06-17', 0, 0, 3, 'LPB'),
('DPPH/00020', '2017-06-01', '2017-07-19', 0, 0, 1, 'LPB');

-- --------------------------------------------------------

--
-- Struktur dari tabel `employees`
--

CREATE TABLE `employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `type_user` int(11) NOT NULL,
  `blocked` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `employees`
--

INSERT INTO `employees` (`username`, `password`, `person_id`, `type_user`, `blocked`, `deleted`) VALUES
('admin', '0192023a7bbd73250516f069df18b500', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `giftcards`
--

CREATE TABLE `giftcards` (
  `record_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `giftcard_id` int(11) NOT NULL,
  `giftcard_number` int(10) NOT NULL,
  `value` decimal(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `person_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `grants`
--

CREATE TABLE `grants` (
  `permission_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `grants`
--

INSERT INTO `grants` (`permission_id`, `person_id`) VALUES
('config', 1),
('customers', 1),
('employees', 1),
('items', 1),
('items_stock', 1),
('reports', 1),
('reports_categories', 1),
('reports_customers', 1),
('reports_discounts', 1),
('reports_employees', 1),
('reports_inventory', 1),
('reports_items', 1),
('reports_payments', 1),
('reports_receivings', 1),
('reports_sales', 1),
('reports_suppliers', 1),
('reports_taxes', 1),
('sales', 1),
('sales_stock', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `home`
--

CREATE TABLE `home` (
  `id_home` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `notelp` varchar(15) NOT NULL,
  `fax` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `no_npwp` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `home`
--

INSERT INTO `home` (`id_home`, `nama`, `alamat`, `notelp`, `fax`, `email`, `no_npwp`, `deleted`) VALUES
('K001', 'PT. ACM', 'Margomulyo', '031394848', '031394848', 'acm@gmail.com', 1039847744, 0),
('K002', 'PT. ACS', 'Sidoarjo', '031838477', '031838477', 'acs@gmail.com', 394049944, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventory`
--

CREATE TABLE `inventory` (
  `trans_id` int(11) NOT NULL,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_bukti` varchar(30) NOT NULL,
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_location` int(11) NOT NULL,
  `trans_inventory` double NOT NULL DEFAULT '0',
  `trans_type` int(11) NOT NULL,
  `trans_jt` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `inventory`
--

INSERT INTO `inventory` (`trans_id`, `trans_items`, `trans_bukti`, `trans_user`, `trans_date`, `trans_comment`, `trans_location`, `trans_inventory`, `trans_type`, `trans_jt`) VALUES
(24, 6, 'NT00001', 1, '2017-04-29 03:21:34', '', 1, 20000000000, 0, 'pembelian'),
(25, 6, 'SJM/00009', 1, '2017-04-28 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Toko', 1, 900, 1, 'pembelian'),
(40, 6, 'SJM/00010', 1, '2017-04-28 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Toko', 1, 900, 1, 'pembelian'),
(41, 6, 'SJM/00010', 1, '2017-04-28 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Toko', 1, 900, 0, 'IC'),
(42, 6, 'SJM/00010', 1, '2017-04-28 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Toko', 1, 900, 1, 'pembelian'),
(43, 6, 'SJM/00010', 1, '2017-04-28 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Toko', 1, 900, 0, 'IC'),
(44, 6, 'SJM/00010', 1, '2017-04-28 17:00:00', 'Diterima oleh GUDANG (dari SJM/00010)', 1, 900, 1, 'IC'),
(45, 6, 'SJM/00010', 1, '2017-04-28 17:00:00', 'Diterima oleh GUDANG (dari SJM/00010)', 1, 900, 1, 'IC'),
(46, 6, 'SJM/00011', 1, '2017-04-28 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Toko', 1, 900, 1, 'pembelian'),
(47, 6, 'SJM/00011', 1, '2017-04-28 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Toko', 1, 900, 0, 'IC'),
(48, 6, 'SJM/00011', 1, '2017-04-28 17:00:00', 'Diterima oleh GUDANG (dari SJM/00011)', 1, 900, 1, 'IC'),
(49, 1, 'NT/00013', 1, '2017-05-05 17:00:00', 'pembelian dari PT. Panca Jaya Manunggal', 1, 200, 0, 'pembelian'),
(50, 1, 'SJR/00006', 1, '2017-05-18 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 20000, 1, 'Pembelian'),
(51, 1, 'SJR/00006', 1, '2017-05-18 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 20000, 0, 'IC'),
(52, 1, 'NT/00015', 1, '2017-05-18 17:00:00', ' dari sasa', 1, 2046, 0, ''),
(53, 1, 'NT/00016', 1, '2017-05-18 17:00:00', ' dari asas', 1, 2046, 0, ''),
(54, 1, 'NT/00018', 1, '2017-05-18 17:00:00', 'titipan dari PT. Artha Buana', 1, 1023, 0, 'titipan'),
(55, 13, 'SJR/00007', 1, '2017-06-11 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 10, 1, 'Pembelian'),
(56, 13, 'SJR/00007', 1, '2017-06-11 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 10, 0, 'IC'),
(57, 16, 'SJR/00007', 1, '2017-06-11 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 212, 1, 'Pembelian'),
(58, 16, 'SJR/00007', 1, '2017-06-11 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 212, 0, 'IC'),
(59, 1, 'sjp/00001', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 120000, 1, 'pembelian'),
(60, 1, 'sjp/00001', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 120000, 0, 'IC'),
(61, 6, 'sjp/00002', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 1, 'hasil jadi'),
(62, 6, 'sjp/00002', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 0, 'IC'),
(63, 6, 'sjp/00002', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 1, 'hasil jadi'),
(64, 6, 'sjp/00002', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 0, 'IC'),
(65, 6, 'sjp/00002', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 1, 'hasil jadi'),
(66, 6, 'sjp/00002', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 0, 'IC'),
(67, 6, 'sjp/00002', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 1, 'hasil jadi'),
(68, 6, 'sjp/00002', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 0, 'IC'),
(69, 6, 'sjp/00003', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 1, 'pembelian'),
(70, 6, 'sjp/00003', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 0, 'IC'),
(71, 6, 'sjp/00003', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 1, 'pembelian'),
(72, 6, 'sjp/00003', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 0, 'IC'),
(73, 6, 'sjp/00003', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 1, 'pembelian'),
(74, 6, 'sjp/00003', 1, '2017-06-14 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 1000000, 0, 'IC'),
(75, 1, 'SJM/00012', 1, '2017-06-14 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Gudang Romokalisari', 1, 4, 1, 'pembelian'),
(76, 1, 'SJM/00012', 1, '2017-06-14 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Gudang Romokalisari', 1, 4, 0, 'IC'),
(77, 1, 'sjp/00005', 1, '2017-06-15 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 120000, 1, 'hasil jadi'),
(78, 1, 'sjp/00005', 1, '2017-06-15 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 120000, 0, 'IC'),
(79, 1, 'SJM/00013', 1, '2017-06-15 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Gudang Romokalisari', 1, 1000000, 1, 'pembelian'),
(80, 1, 'SJM/00013', 1, '2017-06-15 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Gudang Romokalisari', 1, 1000000, 0, 'IC'),
(81, 6, 'SJM/00013', 1, '2017-06-15 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Gudang Romokalisari', 1, 1000000, 1, 'pembelian'),
(82, 6, 'SJM/00013', 1, '2017-06-15 17:00:00', 'Keluar Untuk Mutasi dari Toko ke Gudang Romokalisari', 1, 1000000, 0, 'IC'),
(83, 1, 'SJM/00013', 1, '2017-06-15 17:00:00', 'Diterima oleh GUDANG (dari SJM/00013)', 1, 1000000, 1, 'IC'),
(84, 6, 'SJM/00013', 1, '2017-06-15 17:00:00', 'Diterima oleh GUDANG (dari SJM/00013)', 1, 1000000, 1, 'IC'),
(85, 1, 'sjp/00006', 1, '2017-07-03 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 10000000, 1, 'pembelian'),
(86, 1, 'sjp/00006', 1, '2017-07-03 17:00:00', 'Keluar Untuk Penjualan dari Toko ke Toko', 1, 10000000, 0, 'IC'),
(87, 13, 'SJR/00008', 1, '2017-07-18 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 10, 1, 'Pembelian'),
(88, 13, 'SJR/00008', 1, '2017-07-18 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 10, 0, 'IC'),
(89, 16, 'SJR/00008', 1, '2017-07-18 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 212, 1, 'Pembelian'),
(90, 16, 'SJR/00008', 1, '2017-07-18 17:00:00', 'Keluar Untuk Mutasi dari  ke ', 1, 212, 0, 'IC');

-- --------------------------------------------------------

--
-- Struktur dari tabel `items`
--

CREATE TABLE `items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `reorder_level` decimal(15,3) NOT NULL DEFAULT '0.000',
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  `item_id` int(10) NOT NULL,
  `pic_id` int(10) DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `custom1` varchar(25) NOT NULL,
  `custom2` varchar(25) NOT NULL,
  `custom3` varchar(25) NOT NULL,
  `custom4` double NOT NULL,
  `custom5` varchar(25) NOT NULL,
  `custom6` varchar(25) NOT NULL,
  `custom7` varchar(25) NOT NULL,
  `custom8` varchar(25) NOT NULL,
  `custom9` varchar(25) NOT NULL,
  `custom10` varchar(25) NOT NULL,
  `id_city` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `items`
--

INSERT INTO `items` (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `reorder_level`, `receiving_quantity`, `item_id`, `pic_id`, `allow_alt_description`, `is_serialized`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `custom8`, `custom9`, `custom10`, `id_city`) VALUES
('Servis Ac', 'Jasa', 2, 'JS000001', '', '0.00', '0.00', '0.000', '1.000', 0, NULL, 0, 0, 0, '', '', '', 10000, '', '', '', '', '', '', 1),
('cokelat', 'Produk Utama', 5, 'CKLT0001', 'COKLAT', '1000.00', '1012120.00', '0.000', '1.000', 1, NULL, 0, 0, 0, 'cklt', '', 'Kg', 10000, 'img/coklat.png', '', 'jawa', '10000', '', '', 1),
('mente', 'Produk Utama', 4, 'bg0009', 'rempah', '100.00', '120.00', '0.000', '1.000', 6, NULL, 0, 0, 0, 'mt', '0', 'Kg', 10000, 'img/mente.png', 'mt', '0', '10000', '0', '0', 1),
('cengkeh', 'Produk Utama', 4, 'BG-101', '', '0.00', '7545.45', '0.000', '1.000', 9, NULL, 0, 0, 0, 'cgkh', '', 'Kg', 10000, 'img/cengkeh.png', 'cgkh', '', '10000', '', '', 1),
('gagang cengkeh', 'Produk Utama', 4, 'BG-102', '', '50000.00', '2000.00', '10.000', '50.000', 10, NULL, 0, 0, 0, 'ggcgkh', '', 'Kg', 10000, 'img/gcengkeh.png', 'ggcgkh', '', '10000', '', '', 1),
('jagung', 'Produk Utama', 6, 'fads', '', '0.00', '10000.00', '0.000', '1.000', 11, NULL, 0, 0, 0, 'jg', '', 'Kg', 10000, 'img/jagung.png', 'jg', '', '10000', '', '', 1),
('kopi', 'Produk Utama', 8, 'BG102939', '', '0.00', '2000.00', '0.000', '1.000', 12, NULL, 0, 0, 0, 'kp', '', 'Kg', 10000, 'img/kopi.png', 'kp', '', '10000', '', '', 1),
('Pensil', 'Barang umum', 1, 'BG10', 'alat kantor', '1500.00', '300.00', '0.000', '1.000', 13, NULL, 0, 0, 0, '', '', 'Pcs', 10000, '', '', '', '2000', '', '', 1),
('GElas', 'Barang Umum', 1, 'BG11', '', '0.00', '0.00', '0.000', '1.000', 14, NULL, 0, 0, 0, '', '', '', 10000, '', '', '', '', '', '', 1),
('Guci', 'Barang Umum', 1, 'BG12', '', '0.00', '0.00', '0.000', '1.000', 16, NULL, 0, 0, 0, 'Raw Material', '', 'kg', 10000, '', '', '', '10000', '', '', 1),
('karung Beras', 'Kemasan', 1, 'BG13', '', '0.00', '0.00', '0.000', '1.000', 17, NULL, 0, 0, 0, '', '', '', 10000, '', '', '', '10000', '', '', 1),
('Goni', 'Kemasan', 1, 'KMS0001', 'Karung Goni', '10000.00', '1000.00', '0.000', '1.000', 18, NULL, 0, 0, 0, 'WIP', '', 'Kg', 10000, '', '', '', '121212', '', '', 1),
('Plastik Besar', 'Kemasan', 1, 'KMS0002', 'kemsan', '10000.00', '1000.00', '0.000', '1.000', 19, NULL, 0, 0, 0, 'Kemasan', '', 'Pcs', 10000, '', '', '', '10000', '', '', 1),
('Plastik Kecil', 'Kemasan', 1, 'KMS0003', 'kemsan', '10000.00', '1000.00', '0.000', '1.000', 21, NULL, 0, 0, 0, 'Kemasan', '', 'Pcs', 10000, '', '', '', '10000', '', '', 1),
('Double Plastik', 'Kemasan', 1, 'KMS0004', 'kemsan', '10000.00', '1000.00', '0.000', '1.000', 22, NULL, 0, 0, 0, 'Kemasan', '', 'Pcs', 10000, '', '', '', '10000', '', '', 1),
('Pallet', 'Kemasan', 1, 'KMS0005', 'kemsan', '10000.00', '1000.00', '0.000', '1.000', 23, NULL, 0, 0, 0, 'Kemasan', '', 'Pcs', 10000, '', '', '', '10000', '', '', 1),
('Goni Plastik', 'Kemasan', 1, 'KMS0006', 'kemsan', '10000.00', '1000.00', '0.000', '1.000', 24, NULL, 0, 0, 0, 'Kemasan', '', 'Pcs', 10000, '', '', '', '10000', '', '', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `items_taxes`
--

CREATE TABLE `items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `items_taxes`
--

INSERT INTO `items_taxes` (`item_id`, `name`, `percent`) VALUES
(9, '', '0.000'),
(11, '', '0.000'),
(12, '', '0.000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `item_kits`
--

CREATE TABLE `item_kits` (
  `item_kit_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `item_kit_items`
--

CREATE TABLE `item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `item_quantities`
--

CREATE TABLE `item_quantities` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL DEFAULT '0.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `item_quantities`
--

INSERT INTO `item_quantities` (`item_id`, `location_id`, `quantity`) VALUES
(9, 1, '19.000'),
(10, 1, '49.000'),
(11, 1, '20.000'),
(12, 1, '20.000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lpb`
--

CREATE TABLE `lpb` (
  `id_lpb` varchar(20) NOT NULL,
  `id_po` varchar(20) NOT NULL,
  `no_sj` varchar(20) NOT NULL,
  `number_plate` varchar(20) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `tgl_jt_dpp` date NOT NULL,
  `tgl_jt_ppn` date NOT NULL,
  `note` text NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `custom1` varchar(30) NOT NULL,
  `custom2` varchar(30) NOT NULL,
  `custom3` varchar(30) NOT NULL,
  `custom4` varchar(30) NOT NULL,
  `custom5` varchar(255) NOT NULL,
  `custom6` varchar(255) NOT NULL,
  `custom7` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `lpb`
--

INSERT INTO `lpb` (`id_lpb`, `id_po`, `no_sj`, `number_plate`, `date_transaction`, `date_input`, `tgl_jt_dpp`, `tgl_jt_ppn`, `note`, `acc`, `deleted`, `person_id`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `status`) VALUES
('BPB/00001', 'PO/00001', '123t566', 'L123KL', '2017-01-18', '2017-01-18', '0000-00-00', '0000-00-00', 'oki', 1, 0, 0, '1', '', '', '', '', '', '', 0),
('BPB/00002', 'PO/00005', 'SJ001', 'w0010pp', '2017-03-27', '2017-03-27', '1970-01-01', '1970-01-01', 'sdf', 1, 0, 1, '', '7', '3178', '0', '3178', '317.8', '3495.8', 2),
('BPB/00003', 'PO/00001', 'asas', 'asasasa', '2017-04-19', '2017-04-19', '0000-00-00', '0000-00-00', '', 0, 0, 1, '', '', '0', '0', '0', '0', '0', 0),
('LPB/00004', 'PO/00003', 'SJL223', '', '2017-04-28', '2017-04-28', '2017-05-10', '2017-05-05', '', 0, 0, 1, '', '7', '3178', '0', '3178', '317.8', '3495.8', 2),
('LPB/00005', 'PO/00003', 'l11', '', '2017-05-05', '2017-05-05', '0000-00-00', '0000-00-00', '', 0, 0, 1, '', '7', '3178', '0', '3178', '317.8', '3495.8', 2),
('LPB/00006', 'PO/00003', 'asa', '', '2017-05-07', '2017-05-07', '0000-00-00', '0000-00-00', '', 0, 0, 1, '', '7', '5510', '0', '5510', '551', '6061', 0),
('LPB/00007', 'PO/00012', 'SJ0001', '', '2017-05-12', '2017-05-12', '0000-00-00', '0000-00-00', '', 0, 0, 1, '', '10', '', '', '', '', '', 0),
('LPB/00008', 'PO/00012', 'sf', '', '2017-05-12', '2017-05-12', '0000-00-00', '0000-00-00', '', 0, 0, 1, '', '10', '', '', '', '', '', 0),
('LPB/00009', 'PO/00012', 'sj', '', '2017-05-12', '2017-05-12', '0000-00-00', '0000-00-00', '', 0, 0, 1, '', '10', '', '', '', '', '', 0),
('LPB/00010', 'PO/00003', 'sjk', '', '2017-05-12', '2017-05-12', '0000-00-00', '0000-00-00', 'ok bos', 0, 0, 1, '', '7', '3178000000', '3000000', '3175000000', '317500000', '3492500000', 2),
('LPB/00011', 'PO/00003', 'sj0006', '', '2017-05-17', '2017-05-17', '0000-00-00', '0000-00-00', '', 0, 0, 1, '', '7', '0', '0', '0', '0', '0', 1),
('LPB/00012', 'PO/00019', 'sj', '', '2017-05-17', '2017-05-17', '1970-01-01', '1970-01-01', 'ok', 0, 0, 1, '', '14', '0', '0', '0', '0', '0', 1),
('LPB/00013', 'PO/00019', 'sjj', '', '2017-05-17', '2017-05-17', '1970-01-01', '1970-01-01', 'ok\n', 0, 0, 1, '', '14', '0', '0', '0', '0', '0', 2),
('LPB/00014', 'PO/00019', 'sj', '', '2017-05-18', '2017-05-18', '2017-05-18', '2017-05-18', 'lkj', 0, 0, 1, '', '14', '0', '0', '0', '0', '0', 2),
('LPB/00015', 'PO/00019', 'sfsdfs', '', '2017-05-18', '2017-05-18', '2017-05-18', '2017-05-18', 'ghjghjghj', 0, 0, 1, '', '14', '0', '0', '0', '0', '0', 2),
('LPB/00016', 'PO/00019', 'asas', '', '2017-05-18', '2017-05-18', '1970-01-01', '1970-01-01', '', 0, 0, 1, '', '14', '24000', '0', '24000', '2400', '26400', 2),
('LPB/00017', 'PO/00019', 'asas', '', '2017-05-18', '2017-05-18', '2017-05-27', '2017-05-26', '', 0, 0, 1, '', '14', '3178', '0', '3178', '317.8', '3495.8', 1),
('LPB/00018', 'PO/00019', 'sdgdfg', '', '2017-05-18', '2017-05-18', '2017-05-18', '2017-05-18', 'fhf', 0, 0, 1, '', '14', '0', '0', '0', '0', '0', 2),
('LPB/00019', 'PO/00019', 'dfgd', '', '2017-05-18', '2017-05-18', '2017-05-09', '2017-05-09', 'asdad', 0, 0, 1, '', '14', '0', '0', '0', '0', '0', 2),
('LPB/00020', 'PO/00019', 'assass', '', '2017-05-18', '2017-05-18', '2017-05-18', '2017-05-19', 'asasasassa', 0, 0, 1, '', '14', '0', '0', '0', '0', '0', 2),
('LPB/00021', 'PO/00019', 'sasas', '', '2017-05-18', '2017-05-18', '2017-05-10', '2017-05-18', 'asasasasas', 0, 0, 1, '', '14', '0', '0', '0', '0', '120000', 0),
('LPB/00022', 'PO/00024', 'dgdfg', '', '2017-06-07', '2017-05-18', '2017-05-18', '2017-05-18', 'hjkhjk', 0, 0, 1, '', '14', '76000', '100', '75900', '7590', '83490', 0),
('LPB/00023', 'PO/00024', 'dfgd', '', '2017-06-05', '2017-06-05', '1970-01-01', '1970-01-01', 'ok', 0, 0, 1, '', '14', '0', '0', '0', '0', '0', 1),
('LPB/00024', 'PO/00024', 'asd', '', '2017-06-12', '2017-06-12', '2017-06-12', '2017-06-13', 'ok', 0, 0, 1, '', '14', '6500', '100', '6400', '640', '7040', 2),
('LPB/00025', 'PO/00021', 'sj8787', '', '2017-06-12', '2017-06-12', '1970-01-01', '1970-01-01', 'ok', 0, 0, 1, '', '14', '13130', '500', '12630', '1263', '13893', 2),
('LPB/00026', 'PO/00026', 'SJ930391', '', '2017-07-19', '2017-07-19', '1970-01-01', '1970-01-01', 'ok', 0, 0, 1, '', '14', '60000', '0', '60000', '6000', '66000', 2),
('LPB/00027', 'PO/00026', 'l89', '', '2017-07-19', '2017-07-19', '1970-01-01', '1970-01-01', '', 0, 0, 1, '', '14', '0', '0', '0', '0', '0', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `modules`
--

CREATE TABLE `modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `modules`
--

INSERT INTO `modules` (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES
('module_config', 'module_config_desc', 110, 'config'),
('module_customers', 'module_customers_desc', 10, 'customers'),
('module_employees', 'module_employees_desc', 80, 'employees'),
('module_items', 'module_items_desc', 20, 'items'),
('module_reports', 'module_reports_desc', 50, 'reports'),
('module_sales', 'module_sales_desc', 70, 'sales');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mr`
--

CREATE TABLE `mr` (
  `id_mr` varchar(20) NOT NULL,
  `id_lpb` varchar(20) NOT NULL,
  `supplier_id` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `person_id` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `kategori` varchar(30) NOT NULL,
  `no_timbang` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mr`
--

INSERT INTO `mr` (`id_mr`, `id_lpb`, `supplier_id`, `note`, `date_transaction`, `date_input`, `person_id`, `acc`, `deleted`, `custom1`, `custom2`, `kategori`, `no_timbang`, `status`, `total`) VALUES
('MR/00001', 'LPB/00025', '', 'cepet diretur ae', '2017-06-19', '2017-06-19', 1, 0, 0, 'Ony Prabowo', 'super', 'Barang Umum', '', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `npb`
--

CREATE TABLE `npb` (
  `id_npb` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `id_timbang` varchar(30) NOT NULL,
  `id_nump` varchar(30) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `id_skb` varchar(30) NOT NULL,
  `tipe_beli` varchar(20) NOT NULL,
  `total` double NOT NULL,
  `diskon` double NOT NULL,
  `dpp` double NOT NULL,
  `ppn` double NOT NULL,
  `gt` double NOT NULL,
  `um` double NOT NULL,
  `sp` double NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `tgl_jt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `npb`
--

INSERT INTO `npb` (`id_npb`, `tanggal`, `id_timbang`, `id_nump`, `supplier_id`, `id_skb`, `tipe_beli`, `total`, `diskon`, `dpp`, `ppn`, `gt`, `um`, `sp`, `status`, `deleted`, `tgl_jt`) VALUES
('NPB000001', '2017-05-08', 'NT/00001', '', 0, '', 'Tunai', 1000000, 100, 100, 10, 1000000000000, 10000, 10000000, 1, 0, '2017-05-11'),
('NPB000002', '2017-05-10', 'NT/00002', '', 0, 'SK/00001', 'Kontrak', 1000, 100, 1000, 1000, 10000, 1000, 100000, 0, 0, '2017-05-17'),
('NPP/00003', '2017-06-05', 'NT/00001', '', 1, '', 'Tunai', -12, 0, -12, -1.2000000000000002, -13.2, 0, 0, 1, 0, '0000-00-00'),
('NPP/00004', '2017-06-06', 'NT/00001', 'NUM/00001', 14, '', 'Tunai', 1485, 10, 1475, 147.5, 1622.5, 100000, 0, 1, 0, '0000-00-00'),
('NPP/00005', '2017-06-06', 'NT/00001', 'NUM/00002', 14, '', 'Kredit', 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00'),
('NPP/00006', '2017-07-19', 'NT/00018', '', 14, '', 'Kontrak', 0, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nump`
--

CREATE TABLE `nump` (
  `id_nump` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `total` double NOT NULL,
  `diskon` double NOT NULL,
  `dpp` double NOT NULL,
  `ppn` double NOT NULL,
  `gt` double NOT NULL,
  `um` double NOT NULL,
  `status` int(10) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nump`
--

INSERT INTO `nump` (`id_nump`, `tanggal`, `supplier_id`, `total`, `diskon`, `dpp`, `ppn`, `gt`, `um`, `status`, `deleted`) VALUES
('NUM/00001', '2017-05-29', 14, 108702000, 100000, 108602000, 10860200, 119462200, 100000, 1, 0),
('NUM/00002', '2017-06-05', 14, 36000, 2, 35400, 3540, 38940, 1000, 1, 0),
('NUM/00003', '2017-06-06', 14, 945722, 1212, 944510, 94451, 1038961, 4, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `people`
--

CREATE TABLE `people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `type_people` int(11) NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `people`
--

INSERT INTO `people` (`first_name`, `last_name`, `gender`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `type_people`, `person_id`) VALUES
('Mamat', 'Suhimat', NULL, '555-555-5555', 'inoreds@gmail.com', 'Address 1', '', '', '', '', '', '', 0, 1),
('Andy ', 'Tour', NULL, '', '', '', '', '', '', '', '', '', 0, 2),
('ony', 'prabowo', 1, '', '', '', '', '', '', '', '', '', 0, 3),
('fifi', 'faristia', 1, '321', 'adi@ymail.com', 'a', 'b', 'v', 'b', 'b', 'b', 'b', 0, 4),
('jon', '', NULL, '', '', '', '', '', '', '', '', '', 1, 5),
('', '', NULL, '', '', '', '', '', '', '', '', '', 1, 6),
('', '', NULL, '', '', '', '', '', '', '', '', '', 1, 7),
('Ony', 'Prabowo', 1, '087852461990', 'onyprabowo@gmail.com', 'Jl. Dimana Mana', 'Penjaringan', 'Kedung Baruk', 'Jawa Timur', '60297', 'Indonesia', 'Ok Baru', 0, 8),
('PT. Island Globalindo', '', NULL, '', '', '', '', '', '', '', '', '', 1, 9),
('PT. Makmur Sejahtera', '', NULL, '', '', '', '', '', '', '', '', '', 1, 10),
('', '', NULL, '', '', '', '', '', '', '', '', '', 1, 11),
('PT. Panca Jaya Manunggal', '', NULL, '', '', '', '', '', '', '', '', '', 1, 12),
('Agus', '', NULL, '', '', '', '', '', '', '', '', '', 1, 13),
('sdf', '', NULL, '', '', '', '', '', '', '', '', '', 1, 14),
('asd', '', NULL, '', '', '', '', '', '', '', '', '', 1, 15),
('fdsdf', '', NULL, '', '', '', '', '', '', '', '', '', 1, 16),
('sdfsdf', '', NULL, '', '', '', '', '', '', '', '', '', 1, 17),
('fg', '', NULL, '', '', '', '', '', '', '', '', '', 1, 18),
('g', '', NULL, '', '', '', '', '', '', '', '', '', 1, 19),
('f', '', NULL, '', '', '', '', '', '', '', '', '', 1, 20),
('adsad', '', NULL, '', '', '', '', '', '', '', '', '', 1, 21),
('joko', '', NULL, '', '', '', '', '', '', '', '', '', 1, 22),
('coba', '', NULL, '', '', '', '', '', '', '', '', '', 1, 23);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `permission_id` varchar(255) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `location_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`permission_id`, `module_id`, `location_id`) VALUES
('config', 'config', NULL),
('customers', 'customers', NULL),
('employees', 'employees', NULL),
('items', 'items', NULL),
('items_stock', 'items', 1),
('reports', 'reports', NULL),
('reports_categories', 'reports', NULL),
('reports_customers', 'reports', NULL),
('reports_discounts', 'reports', NULL),
('reports_employees', 'reports', NULL),
('reports_inventory', 'reports', NULL),
('reports_items', 'reports', NULL),
('reports_payments', 'reports', NULL),
('reports_receivings', 'reports', NULL),
('reports_sales', 'reports', NULL),
('reports_suppliers', 'reports', NULL),
('reports_taxes', 'reports', NULL),
('sales', 'sales', NULL),
('sales_stock', 'sales', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `po`
--

CREATE TABLE `po` (
  `id_po` varchar(20) NOT NULL,
  `id_spp` varchar(20) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `type_op` varchar(30) NOT NULL,
  `consignment` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `total` double NOT NULL,
  `dp` double NOT NULL,
  `tax` double NOT NULL,
  `shipping_cost` double NOT NULL,
  `payment_type` varchar(30) NOT NULL,
  `tgl_jt` date DEFAULT NULL,
  `acc` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(50) NOT NULL,
  `custom2` date NOT NULL,
  `custom3` varchar(50) NOT NULL,
  `custom4` int(11) NOT NULL,
  `custom5` varchar(50) NOT NULL,
  `custom6` double NOT NULL,
  `custom7` double NOT NULL,
  `custom8` double NOT NULL,
  `custom9` double NOT NULL,
  `custom10` double NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `po`
--

INSERT INTO `po` (`id_po`, `id_spp`, `supplier_id`, `date_transaction`, `date_input`, `type_op`, `consignment`, `note`, `total`, `dp`, `tax`, `shipping_cost`, `payment_type`, `tgl_jt`, `acc`, `person_id`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `custom8`, `custom9`, `custom10`, `status`) VALUES
('', '', 0, '0000-00-00', '0000-00-00', '', '', '', 0, 0, 0, 0, '', NULL, 0, 0, 0, '', '0000-00-00', '', 0, '', 0, 0, 0, 1000000, 10000, 0),
('PO/00001', 'PR/00001', 10, '2017-04-22', '2017-04-22', 'Tunai', '', '1', 900, 0, 0, 0, '', NULL, 0, 1, 0, 'Barang Umum', '1970-01-01', 'n/30', 2, '900', 52000, 5200, 0, 57200, 10000, 0),
('PO/00002', 'PR/00002', 10, '2017-04-22', '2017-04-22', 'Kredit', '', '1', 900, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '1970-01-01', 'n/30', 2, '900', 99000, 0, 9900, 108900, 10000, 0),
('PO/00003', 'PR/00007', 7, '2017-04-28', '2017-04-28', 'Tunai', '', '1', 2119990, 0, 0, 0, '', '0000-00-00', 0, 1, 0, 'Barang Umum', '2017-05-05', 'asas', 2, '100', 2119890, 211989, 0, 2331879, 10000, 1),
('PO/00004', 'PR/00006', 7, '2017-04-28', '2017-04-28', 'Kredit', '', '1', 900, 0, 0, 0, '', NULL, 0, 0, 0, 'Barang Umum', '1970-01-01', 'n/30', 2, '', 0, 0, 0, 1000000, 10000, 0),
('PO/00005', 'PR/00009', 7, '2017-05-05', '2017-05-05', 'Kredit', '', '1', 900, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '1970-01-01', 'n/30', 2, '100', 989900, 0, 98990, 1088890, 0, 0),
('PO/00006', 'PR/00009', 11, '2017-05-05', '2017-05-05', 'Kredit', '', '', 9000, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '2017-05-13', 'n/30', 2, '100', 8900, 890, 0, 9790, 0, 0),
('PO/00007', 'PR/00006', 9, '2017-05-05', '2017-05-05', 'Kredit', '', '', 0, 0, 0, 0, '', NULL, 0, 1, 0, 'Barang Umum', '0000-00-00', '', 2, '', 0, 0, 0, 1000000, 10000, 0),
('PO/00008', 'PR/00009', 4, '2017-05-09', '2017-05-09', 'Kredit', '', '', 9000, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '1970-01-01', 'n/30', 2, '100', 8900, 890, 0, 9790, 0, 1),
('PO/00009', 'PR/00009', 6, '2017-05-09', '2017-05-09', 'Kredit', '', '', 9900, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '2017-05-10', 'n/30', 1, '10', 9890, 989, 0, 10879, 0, 1),
('PO/00010', 'PR/00009', 7, '2017-05-09', '2017-05-09', 'Kredit', '', '', 9999000, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '1970-01-01', 'n/30', 2, '1000', 9998000, 999800, 0, 10997800, 0, 1),
('PO/00011', 'PR/00006', 10, '2017-05-12', '2017-05-12', 'Tunai', '', '', 0, 0, 0, 0, '', NULL, 0, 1, 0, 'Barang Umum', '0000-00-00', '', 2, '', 0, 0, 0, 0, 0, 0),
('PO/00012', 'PR/00010', 10, '2017-05-12', '2017-05-12', 'Tunai', '', '', 24900, 0, 0, 0, '', NULL, 0, 1, 0, 'Barang Umum', '2017-05-12', 'n/30', 2, '900', 24000, 2400, 0, 26400, 0, 1),
('PO/00013', 'PR/00011', 10, '2017-05-12', '2017-05-12', 'Kredit', '', '', 0, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '0000-00-00', '', 2, '', 0, 0, 0, 0, 0, 0),
('PO/00014', 'PR/00014', 15, '2017-05-12', '2017-05-12', 'Tunai', '', '', 0, 0, 0, 0, '', NULL, 0, 1, 0, 'Barang Umum', '0000-00-00', '', 2, '', 0, 0, 0, 0, 0, 0),
('PO/00015', 'PR/00015', 15, '2017-05-12', '2017-05-12', 'Kredit', '', '', 9900, 0, 0, 0, '', '0000-00-00', 0, 1, 0, 'Jasa', '2017-05-12', 'tunai', 2, '900', 9000, 900, 0, 9900, 0, 1),
('PO/00016', 'PR/00015', 16, '2017-05-12', '2017-05-12', 'Kredit', '', '', 0, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '0000-00-00', '', 2, '', 0, 0, 0, 0, 0, 0),
('PO/00017', 'PR/00015', 14, '2017-05-12', '2017-05-12', 'Kredit', '', 'slow mn', 0, 0, 0, 0, '', '0000-00-00', 0, 1, 0, 'Jasa', '1970-01-01', '', 2, '', 0, 0, 0, 0, 0, 1),
('PO/00019', 'PR/00014', 14, '2017-05-17', '2017-05-17', 'Kredit', '', '', 0, 0, 0, 0, '', '0000-00-00', 0, 1, 0, 'Barang Umum', '1970-01-01', '', 2, '', 0, 0, 0, 1000000, 10000, 1),
('PO/00020', 'PR/00013', 1, '2017-05-17', '2017-05-17', 'Kontrak', '', '', 0, 0, 0, 0, '', '0000-00-00', 0, 1, 0, 'Jasa', '2017-05-17', 'asasas', 1, '', 0, 0, 0, 1000000, 10000, 1),
('PO/00021', 'PR/00014', 14, '2017-05-17', '2017-05-17', 'Kredit', '', '', 0, 0, 0, 0, '', '0000-00-00', 0, 1, 0, 'Barang Umum', '1970-01-01', 'n/30', 2, '', 0, 0, 0, 0, 0, 1),
('PO/00022', 'PR/00014', 14, '2017-05-17', '2017-05-17', 'Tunai', '', '', 0, 0, 0, 0, '', '0000-00-00', 0, 1, 0, 'Barang Umum', '1970-01-01', 'n/30', 2, '', 0, 0, 0, 0, 0, 1),
('PO/00023', 'PR/00015', 14, '2017-06-09', '2017-05-17', 'Tunai', '', '', 0, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '0000-00-00', '', 2, '', 0, 0, 0, 0, 0, 0),
('PO/00024', 'PR/00014', 14, '2017-06-07', '2017-05-18', 'Tunai', '', '', 0, 0, 0, 0, '', '0000-00-00', 0, 1, 0, 'Barang Umum', '2017-05-18', 'n/30', 2, '', 0, 0, 0, 0, 0, 1),
('PO/00025', 'PR/00023', 14, '2017-06-12', '2017-06-12', 'Tunai', '', '', 0, 0, 0, 0, '', NULL, 0, 1, 0, 'Barang Umum', '0000-00-00', '', 2, '', 0, 0, 0, 0, 0, 0),
('PO/00026', 'PR/00025', 14, '2017-07-19', '2017-07-19', 'Kredit', '', '', 0, 0, 0, 0, '', '0000-00-00', 0, 1, 0, 'Barang Umum', '1970-01-01', 'n/30', 1, '', 0, 0, 0, 0, 0, 1),
('PO/00027', 'PR/00026', 14, '2017-07-19', '2017-07-19', 'Kontrak', '', '', 0, 0, 0, 0, '', NULL, 0, 1, 0, 'Jasa', '0000-00-00', '', 2, '', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `receivings`
--

CREATE TABLE `receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `receiving_id` int(10) NOT NULL,
  `payment_type` varchar(20) DEFAULT NULL,
  `invoice_number` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `receivings_items`
--

CREATE TABLE `receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales`
--

CREATE TABLE `sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `invoice_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sales`
--

INSERT INTO `sales` (`sale_time`, `customer_id`, `employee_id`, `comment`, `invoice_number`, `sale_id`) VALUES
('2017-01-09 03:20:29', NULL, 1, '', NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales_items`
--

CREATE TABLE `sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sales_items`
--

INSERT INTO `sales_items` (`sale_id`, `item_id`, `description`, `serialnumber`, `line`, `quantity_purchased`, `item_cost_price`, `item_unit_price`, `discount_percent`, `item_location`) VALUES
(1, 9, '', '', 1, '1.000', '0.00', '30000.00', '0.00', 1),
(1, 10, '', '', 2, '1.000', '50000.00', '57000.00', '0.00', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales_items_taxes`
--

CREATE TABLE `sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sales_items_taxes`
--

INSERT INTO `sales_items_taxes` (`sale_id`, `item_id`, `line`, `name`, `percent`) VALUES
(1, 9, 1, '', '0.000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales_payments`
--

CREATE TABLE `sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sales_payments`
--

INSERT INTO `sales_payments` (`sale_id`, `payment_type`, `payment_amount`) VALUES
(1, 'Tunai', '87000.00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales_suspended`
--

CREATE TABLE `sales_suspended` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `invoice_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales_suspended_items`
--

CREATE TABLE `sales_suspended_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales_suspended_items_taxes`
--

CREATE TABLE `sales_suspended_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales_suspended_payments`
--

CREATE TABLE `sales_suspended_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sessions`
--

INSERT INTO `sessions` (`id`, `ip_address`, `data`, `timestamp`) VALUES
('0d5e5ee53b00ee1dea75f014e07bfed653150718', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333538343032303b, 1483584020),
('0fd3d78be553416226dbaa7ee2409f0a3daf4095', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333933313237363b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483931293),
('10c82c8f6600c476b44962898a68aca70be5a585', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333532353438353b, 1483525485),
('1c534eb1f2eaab7ed7d5bc2efb832b02b1c3d83c', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333538353338383b, 1483585388),
('240d5954b8b2e0873a18658bdab892bef5a8e4ce', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333532333132373b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483523154),
('2a7e3146d183a58d42b266021c6d9c4a501952a7', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433313730333b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483431206),
('337703467e9ebb495384e436795933076896b4a1', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438363730353737323b, 1486705772),
('485bf38df6ff46f13f4bb693c33c951ae941fd72', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343732303035343b, 1484720054),
('5df439b5ff38d36b8dee44f93885e3884233f9f9', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353231323632373b, 1485212628),
('5f3b4b30fad762eb4ff5e34f4622497ab0030959', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433303439303b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483430682),
('6e3e36e76c0ca99b3c9ce965c0748e3f1076cfd7', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433303132323b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483430305),
('95918bb47c692dab51a8e9e377719790d6f12ef3', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333538353938363b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b636172747c613a303a7b7d73616c655f6d6f64657c733a343a2273616c65223b73616c655f6c6f636174696f6e7c733a313a2231223b637573746f6d65727c693a2d313b7061796d656e74737c613a303a7b7d73616c65735f696e766f6963655f6e756d6265727c733a313a2230223b, 1483584436),
('963964551cc00a5eab859f99eaea03412b720c47', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433323230363b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b73616c655f6c6f636174696f6e7c733a313a2231223b, 1483429704),
('a03a648ccaa4ec642da38060dbe63ad1fb668e68', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353231323130373b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b636172747c613a313a7b693a313b613a31363a7b733a373a226974656d5f6964223b733a323a223130223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a343a22546f6b6f223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a383a22576972656d657368223b733a31313a226974656d5f6e756d626572223b733a363a2242472d313032223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a363a2234392e303030223b733a353a227072696365223b733a383a2235373030302e3030223b733a353a22746f74616c223b733a383a2235373030302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a31313a2235373030302e3030303030223b7d7d73616c655f6d6f64657c733a343a2273616c65223b73616c655f6c6f636174696f6e7c733a313a2231223b637573746f6d65727c693a2d313b7061796d656e74737c613a303a7b7d73616c65735f696e766f6963655f6e756d6265727c733a313a2230223b, 1485212159),
('b959043f5c7e4b7772aac34c755a8974f86bdb59', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333933313731343b, 1483931714),
('e6ea95b21099d69b3a77aabd53187ed050a454d1', '::1', 0x5f5f63695f6c6173745f726567656e65726174657c693a313438333433313134333b706572736f6e5f69647c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b, 1483431441);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sjr`
--

CREATE TABLE `sjr` (
  `id_sjr` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `id_dn` varchar(30) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `total` double NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sjr`
--

INSERT INTO `sjr` (`id_sjr`, `tanggal`, `id_dn`, `supplier_id`, `kategori`, `total`, `status`, `deleted`) VALUES
('SJR/00001', '2017-05-10', 'DN/00002', 7, 'Barang Umum', 0, 1, 0),
('SJR/00002', '2017-05-19', 'DN/00002', 7, 'Barang Umum', 0, 0, 0),
('SJR/00003', '2017-05-19', 'DN/00008', 5, 'Produk Utama', 0, 0, 0),
('SJR/00004', '2017-05-19', 'DN/00002', 7, 'Barang Umum', 0, 0, 0),
('SJR/00005', '2017-05-19', 'DN/00010', 5, 'Produk Utama', 0, 1, 0),
('SJR/00006', '2017-06-08', 'DN/00010', 5, 'Produk Utama', 0, 1, 0),
('SJR/00007', '2017-06-12', 'DN/00011', 14, 'Barang Umum', 0, 1, 0),
('SJR/00008', '2017-07-19', 'DN/00001', 14, 'Barang Umum', 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `so`
--

CREATE TABLE `so` (
  `id_so` varchar(30) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `note` text NOT NULL,
  `total` double NOT NULL,
  `discount` double NOT NULL,
  `dpp` double NOT NULL,
  `dp` double NOT NULL,
  `tax` double NOT NULL,
  `shipping_cost` double NOT NULL,
  `status` int(11) NOT NULL,
  `grand_total` double NOT NULL,
  `shipping_type` varchar(30) NOT NULL,
  `provision_payment` varchar(30) NOT NULL,
  `payment_type` varchar(30) NOT NULL,
  `person_request` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL,
  `custom4` varchar(100) NOT NULL,
  `custom5` varchar(100) NOT NULL,
  `id_ph` varchar(255) NOT NULL,
  `no_kontrak` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `so`
--

INSERT INTO `so` (`id_so`, `customer_id`, `sales_id`, `item_id`, `date_transaction`, `date_input`, `note`, `total`, `discount`, `dpp`, `dp`, `tax`, `shipping_cost`, `status`, `grand_total`, `shipping_type`, `provision_payment`, `payment_type`, `person_request`, `person_id`, `acc`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `id_ph`, `no_kontrak`) VALUES
('SO/00001', 1, 0, 0, '2017-06-15', '2017-06-15', 'ok', 121454400000, 0, 0, 0, 12145440000, 0, 1, 133599840000, 'loco', 'tunai', 'Tunai', 0, 1, 0, 0, 'ok', 'penawaran', '', '', 'produk', 'SP00001', ''),
('SO/00002', 1, 0, 0, '2017-06-15', '2017-06-15', 'ok', 120000000, 0, 0, 0, 12000000, 0, 1, 132000000, 'loco', 'tunai', 'Kontrak', 0, 1, 0, 0, 'ok', 'kontrak', '', '', 'produk', '', 'SK00001'),
('SO/00003', 1, 0, 0, '2017-06-15', '2017-06-15', 'lo', 120000000, 0, 0, 0, 12000000, 0, 1, 132000000, 'franco', 'n/30', 'Kredit', 0, 1, 0, 0, 'lo', '', '', '', 'produk', '', ''),
('SO/00004', 1, 0, 0, '2017-06-15', '2017-06-15', 'ok', 0, 0, 0, 0, 0, 0, 1, 0, 'ok', 'n/20', 'Kredit', 0, 1, 0, 0, 'ok', '', '', '', 'barang', '', ''),
('SO/00005', 1, 0, 0, '2017-07-04', '2017-07-04', 'asas', 10121200000000, 0, 0, 0, 1012120000000, 0, 1, 11133320000000, 'Franco', 'Tunai', 'Tunai', 0, 1, 0, 0, 'asas', '', '', '', 'produk', '', ''),
('SO/00006', 1, 0, 0, '2017-07-10', '2017-07-10', 'assas', 1012120000, 0, 0, 0, 101212000, 0, 1, 1113332000, 'asas', '', 'Kredit', 0, 1, 0, 0, 'assas', '', '', '', 'produk', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spp`
--

CREATE TABLE `spp` (
  `id_spp` varchar(20) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `type_spp` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `tax` varchar(10) NOT NULL,
  `person_request` varchar(50) NOT NULL,
  `person_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `custom1` varchar(100) NOT NULL,
  `custom2` varchar(100) NOT NULL,
  `custom3` varchar(100) NOT NULL,
  `custom4` varchar(100) NOT NULL,
  `custom5` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `spp`
--

INSERT INTO `spp` (`id_spp`, `date_transaction`, `date_input`, `type_spp`, `note`, `tax`, `person_request`, `person_id`, `deleted`, `acc`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `status`) VALUES
('PR/00001', '2017-03-27', '2017-03-27', 'Lokal', '', '', 'cece', 1, 0, 1, 'Produk', 'Kontrak', '', '', '', 1),
('PR/00002', '2017-03-27', '2017-03-27', 'Lokal', '', '', 'Dendi', 1, 0, 0, 'Produk', 'Kontrak', '', '', '', 0),
('PR/00003', '2017-03-29', '2017-03-29', 'Lokal', '', '', 'Jonie', 1, 0, 0, 'Produk', 'Tunai', '', '', '', 0),
('PR/00004', '2017-04-11', '2017-04-11', 'Lokal', 'aasas', '', 'Danto', 1, 0, 1, 'Barang Umum', 'Tunai', '', '', '', 0),
('PR/00005', '2017-04-11', '2017-04-11', 'Lokal', 'Catatan', '', 'DANTO', 1, 0, 1, 'Barang Umum', 'Tunai', '', '', '', 0),
('PR/00006', '2017-04-28', '2017-04-28', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'ok', '', '', '', 1),
('PR/00007', '2017-04-28', '2017-04-28', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'asas', '', '', '', 1),
('PR/00008', '2017-04-28', '2017-04-28', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Jasa', 'qw', '', '', '', 1),
('PR/00009', '2017-05-05', '2017-05-05', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Jasa', 'as', '', '', '', 1),
('PR/00010', '2017-05-12', '2017-05-12', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'sd', '', '', '', 1),
('PR/00011', '2017-05-12', '2017-05-12', '', 'Cepat diperbaiki\r\n', '', 'Ony Prabowo', 1, 0, 0, 'Jasa', 'asd', '', '', '', 1),
('PR/00012', '2017-05-12', '2017-05-12', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Jasa', 'sf', '', '', '', 0),
('PR/00013', '2017-05-12', '2017-05-12', '', 'cepet bos panas\r\n', '', 'Ony Prabowo', 1, 0, 0, 'Jasa', 'asd', '', '', '', 1),
('PR/00014', '2017-05-12', '2017-05-12', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'ko', '', '', '', 1),
('PR/00015', '2017-05-12', '2017-05-12', '', 'cepet bos panas\r\n', '', 'Ony Prabowo', 1, 0, 0, 'Jasa', 'kok', '', '', '', 1),
('PR/00016', '2017-07-05', '2017-05-18', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Jasa', 'adsd', '', '', '', 0),
('PR/00017', '2017-06-06', '2017-05-18', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'adada', '', '', '', 0),
('PR/00018', '2017-06-05', '2017-06-05', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'ad', '', '', '', 0),
('PR/00019', '2017-06-10', '2017-06-10', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'Marketing', '', '', '', 1),
('PR/00020', '2017-06-10', '2017-06-10', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'F', '', '', '', 0),
('PR/00021', '2017-06-12', '2017-06-12', '', 'asd', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'wer', '', '', '', 1),
('PR/00022', '2017-06-12', '2017-06-12', '', 'asd', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'super', '', '', '', 1),
('PR/00023', '2017-06-12', '2017-06-12', '', 'sd', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'super', '', '', '', 1),
('PR/00024', '2017-06-12', '2017-06-12', '', '', '', 'Ony Prabowo', 1, 0, 0, 'Jasa', 'super', '', '', '', 0),
('PR/00025', '2017-07-19', '2017-07-19', '', 'pesanyanghitam', '', 'Ony Prabowo', 1, 0, 0, 'Barang Umum', 'super', '', '', '', 1),
('PR/00026', '2017-07-19', '2017-07-19', '', 'perbaikisekarang', '', 'Ony Prabowo', 1, 0, 0, 'Jasa', 'super', '', '', '', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock_locations`
--

CREATE TABLE `stock_locations` (
  `location_id` int(11) NOT NULL,
  `id_city` int(11) NOT NULL,
  `location_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `jenis` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `stock_locations`
--

INSERT INTO `stock_locations` (`location_id`, `id_city`, `location_name`, `deleted`, `jenis`) VALUES
(1, 1, 'Toko', 0, ''),
(2, 1, 'Gudang Romokalisari', 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suppliers`
--

CREATE TABLE `suppliers` (
  `supplier_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_address` varchar(100) DEFAULT NULL,
  `company_phone` varchar(30) DEFAULT NULL,
  `company_email` varchar(30) DEFAULT NULL,
  `agency_name` varchar(100) NOT NULL,
  `account_number` varchar(100) DEFAULT NULL,
  `provision` varchar(50) DEFAULT NULL,
  `npwp` int(11) DEFAULT NULL,
  `npwp_name` varchar(30) NOT NULL,
  `pkp` varchar(30) NOT NULL,
  `date_pkp` date NOT NULL,
  `date_in` date NOT NULL,
  `contact_person` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `id_detail_psb` int(11) NOT NULL,
  `id_psb` varchar(30) CHARACTER SET latin1 NOT NULL,
  `ppn` varchar(10) NOT NULL,
  `syarat_bayar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `suppliers`
--

INSERT INTO `suppliers` (`supplier_id`, `person_id`, `company_code`, `company_name`, `company_address`, `company_phone`, `company_email`, `agency_name`, `account_number`, `provision`, `npwp`, `npwp_name`, `pkp`, `date_pkp`, `date_in`, `contact_person`, `acc`, `deleted`, `id_detail_psb`, `id_psb`, `ppn`, `syarat_bayar`) VALUES
(1, 1, 'SUP/00007', 'asas', 'asas', 'asas', NULL, '', NULL, NULL, 0, '', '', '0000-00-00', '0000-00-00', 'asas', 1, 0, 7, 'PSB/00005', '', 'n/30'),
(2, 1, 'SUP/00006', 'sasa', 'sasa', '121212', NULL, '', NULL, NULL, 0, '', '', '0000-00-00', '0000-00-00', 'asas', 1, 0, 6, 'PSB/00005', '', 'n/30'),
(4, 1, 'SP007', 'PT. Artha Buana', 'Surabaya', '031678898998', 'info@artha.om', 'Ana', NULL, 'Kredit', NULL, 'Ana', '3567', '2017-01-01', '2017-01-05', '', 1, 0, 3, '', 'PPN', 'n/30'),
(5, 11, 'SP002', 'Andri', '-', '-', '-', '-', NULL, '-', NULL, '-', '-', '2017-01-05', '2017-01-05', '', 0, 0, 3, '', 'PPN', 'n/30'),
(6, 1, 'SUP/00007', 'asas', 'asas', 'asas', NULL, '', NULL, NULL, 0, '', '', '0000-00-00', '0000-00-00', 'asas', 1, 0, 7, 'PSB/00005', '', 'n/30'),
(7, 1, 'SUP/00004', 'ass', 'asas', 'asas', NULL, '', NULL, NULL, 0, '', '', '0000-00-00', '0000-00-00', 'asas', 1, 0, 4, 'PSB/00002', '', 'n/30'),
(8, 1, 'SUP/00004', 'ass', 'asas', 'asas', NULL, '', NULL, NULL, 0, '', '', '0000-00-00', '0000-00-00', 'asas', 1, 0, 4, 'PSB/00002', '', 'n/30'),
(9, 1, 'SUP/00008', 'Supplier2', 'Dukuh', '546456', NULL, '', NULL, NULL, 3839388, '', '', '0000-00-00', '0000-00-00', 'vf', 1, 0, 8, 'PSB/00006', '', 'n/30'),
(14, 1, 'SUP/00001', 'Agus', 'Medokan Asri 22', '0819', NULL, '', NULL, NULL, 123123, '', '', '0000-00-00', '0000-00-00', 'agus', 1, 0, 1, 'PSB/00001', '', 'n/30'),
(15, 1, 'SUP/00004', 'Cengceng', 'Malang', '03838399', NULL, '', NULL, NULL, 3938399, '', '', '0000-00-00', '0000-00-00', 'sj', 1, 0, 11, 'PSB/00001', '', 'n/30'),
(16, 1, 'SUP/00006', 'Kl', 'asd', '37', NULL, '', NULL, NULL, 234, '', '', '0000-00-00', '0000-00-00', 'sd', 1, 0, 13, 'PSB/00002', '', 'n/30'),
(17, 1, 'SUP/00008', 'PT ABC', 'Jl. Warna Hitam No. 05', '085678432134', NULL, '', NULL, NULL, 2147483647, '', '', '0000-00-00', '0000-00-00', 'Bapak Budi', 1, 0, 15, 'PSB/00003', '', ''),
(18, 1, '', 'erty', 'dfghj', '234567', NULL, '', NULL, NULL, 23, '', '', '0000-00-00', '0000-00-00', 'ghj', 1, 0, 58, 'PSB/00005', '', ''),
(19, 1, '', 'CV Makmur Jaya', 'Jl. Makmur Jaya no 16', '085267189267', NULL, '', NULL, NULL, 2147483647, '', '', '0000-00-00', '0000-00-00', '', 1, 0, 60, 'PSB/00007', '', ''),
(20, 1, '', 'danto', 'asasas', '21212121', NULL, '', NULL, NULL, 0, '', '', '0000-00-00', '0000-00-00', 'asasasasa', 1, 0, 66, 'PSB/00008', '', ''),
(21, 1, '', 'asd', 'asd', '23423', NULL, '', NULL, NULL, 234234, '', '', '0000-00-00', '0000-00-00', 'asd', 1, 0, 69, 'PSB/00009', '', ''),
(22, 1, '', 'asd', 'asd', '23423', NULL, '', NULL, NULL, 234234, '', '', '0000-00-00', '2017-06-12', 'asd', 1, 0, 69, 'PSB/00009', '', ''),
(23, 1, 'SP/00018', 'asd', 'asd', '23423', NULL, '', NULL, NULL, 234234, '', '', '0000-00-00', '2017-06-12', 'asd', 1, 0, 69, 'PSB/00009', '', ''),
(24, 1, 'SP/00019', 'pola', 'sad839', '748484', NULL, '', NULL, NULL, 88, '', '', '0000-00-00', '2017-07-19', 'asd', 1, 0, 74, 'PSB/00010', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suppliers_copy`
--

CREATE TABLE `suppliers_copy` (
  `supplier_id` int(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_code` varchar(20) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_address` varchar(100) DEFAULT NULL,
  `company_phone` varchar(30) DEFAULT NULL,
  `company_email` varchar(30) DEFAULT NULL,
  `agency_name` varchar(100) NOT NULL,
  `account_number` varchar(100) DEFAULT NULL,
  `provision` varchar(50) DEFAULT NULL,
  `npwp` int(11) DEFAULT NULL,
  `npwp_name` varchar(30) NOT NULL,
  `pkp` varchar(30) NOT NULL,
  `date_pkp` date NOT NULL,
  `date_in` date NOT NULL,
  `contact_person` varchar(30) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `id_detail_psb` int(11) NOT NULL,
  `id_psb` varchar(30) CHARACTER SET latin1 NOT NULL,
  `ppn` varchar(10) NOT NULL,
  `syarat_bayar` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `suppliers_copy`
--

INSERT INTO `suppliers_copy` (`supplier_id`, `person_id`, `company_code`, `company_name`, `company_address`, `company_phone`, `company_email`, `agency_name`, `account_number`, `provision`, `npwp`, `npwp_name`, `pkp`, `date_pkp`, `date_in`, `contact_person`, `acc`, `deleted`, `id_detail_psb`, `id_psb`, `ppn`, `syarat_bayar`) VALUES
(0, 1, 'SUP/00006', 'sasa', 'sasa', '121212', NULL, '', NULL, NULL, 0, '', '', '0000-00-00', '0000-00-00', 'asas', 1, 0, 6, 'PSB/00005', '', ''),
(4, 1, 'SP007', 'PT. Artha Buana', 'Surabaya', '031678898998', 'info@artha.om', 'Ana', NULL, 'Kredit', NULL, 'Ana', '3567', '2017-01-01', '2017-01-05', '', 1, 0, 3, '', 'PPN', 'n/30'),
(5, 11, 'SP002', 'Andri', '-', '-', '-', '-', NULL, '-', NULL, '-', '-', '2017-01-05', '2017-01-05', '', 0, 0, 3, '', 'PPN', 'n/30'),
(6, 12, 'SP008', 'PT. Panca Jaya Manunggal', '', '', '', '', NULL, '', NULL, '', '', '1970-01-01', '2017-01-18', '', 1, 0, 3, '', 'PPN', 'n/30'),
(7, 13, 'SP001', 'Agus', 'sby', '08384484848', 'ag', 'asd', NULL, 'tunai', NULL, 'sdf', 'df', '2017-03-16', '2017-03-15', '', 1, 0, 3, '', 'PPN', 'n/30'),
(8, 22, 'SP005', 'joko', 'sido', '039', 'a@gmail.com', 'pppc', NULL, 'tunai', NULL, 'aa', '3', '2017-03-15', '2017-03-15', '', 0, 0, 3, '', 'PPN', 'n/30'),
(9, 23, 'SP004', 'coba', 'alamat dicoba', '083849484848', 'coba@gmail.com', '', NULL, 'Kredit', NULL, '', '', '2017-03-23', '2017-03-29', '', 1, 0, 3, '', 'PPN', 'n/30'),
(10, 24, 'SP009', 'sabtu besok', 'surabaya', '0838484847488', 'tanzil@ggmail.com', '', NULL, 'tunai', NULL, '', '', '2017-04-07', '2017-04-07', '', 1, 0, 3, '', 'PPN', 'n/30'),
(11, 25, 'SP006', 'jokor', 'surabaya', '08383838', 'h@gmail.com', 'sdsd', NULL, 'sd', 0, 'sd', 's', '2017-04-09', '2017-04-09', '', 1, 0, 3, '', 'PPN', 'n/30'),
(12, 26, 'SP0010', 'Supplor', 'Surabaya', '083838388', NULL, '', NULL, NULL, 3938333, '', '', '0000-00-00', '1970-01-01', 'Bintang', 1, 0, 3, '', 'PPN', 'n/30'),
(14, 1, 'SP003', 'asd', 'asd', '234', NULL, '', NULL, NULL, 234, '', '', '0000-00-00', '0000-00-00', 'sas', 0, 0, 0, '', '', 'n/30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbph`
--

CREATE TABLE `tbph` (
  `id_ph` varchar(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `id_home` varchar(10) NOT NULL,
  `tgl_trx` date NOT NULL,
  `lampiran` varchar(25) NOT NULL,
  `validity` date NOT NULL,
  `hari` int(11) NOT NULL,
  `persen` double NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `catatan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbph`
--

INSERT INTO `tbph` (`id_ph`, `customer_id`, `id_home`, `tgl_trx`, `lampiran`, `validity`, `hari`, `persen`, `status`, `deleted`, `catatan`) VALUES
('SP00001', 1, 'K001', '2017-06-15', '1', '2017-06-15', 12, 12, 1, 0, 'ok');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_phj`
--

CREATE TABLE `tb_phj` (
  `id_phj` varchar(11) NOT NULL,
  `item_id` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `harga_jual` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `harga_lama` double NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_phj`
--

INSERT INTO `tb_phj` (`id_phj`, `item_id`, `tanggal`, `keterangan`, `harga_jual`, `deleted`, `harga_lama`, `status`) VALUES
('PHJ00001', 1, '2017-03-27', 'a', 1222, 0, 0, 0),
('PHJ00002', 1, '2017-04-22', 'Diganti dari master', 1000, 0, 0, 0),
('PHJ00003', 1, '2017-04-22', 'Diganti dari master', 1000, 0, 0, 0),
('PHJ00004', 1, '2017-04-22', 'Diganti dari master', 1012120, 0, 0, 0),
('PHJ00005', 1, '2017-04-28', 'mahal', 2000, 0, 0, 0),
('PHJ00006', 11, '2017-06-08', 'Diganti dari master', 10000, 0, 0, 1),
('PHJ00007', 9, '2017-06-08', 'Naik Bos', 2000000, 0, 7545.45, 2),
('PHJ00008', 6, '2017-06-13', '', 120, 0, 100, 1),
('PHJ00009', 6, '2017-06-13', 'naik', 2000, 0, 100, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_skb`
--

CREATE TABLE `tb_skb` (
  `id_skb` varchar(10) NOT NULL,
  `tgl_jt` date NOT NULL,
  `tanggal` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `id_home` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `type_kontrak` varchar(20) NOT NULL,
  `volume` double NOT NULL,
  `kemasan` varchar(20) NOT NULL,
  `harga_kontrak` double NOT NULL,
  `um` double NOT NULL,
  `kadar_air` double DEFAULT NULL,
  `mouldy` double DEFAULT NULL,
  `insect` double DEFAULT NULL,
  `biji_pipih` double DEFAULT NULL,
  `triple_bean` double DEFAULT NULL,
  `broken_b` double DEFAULT NULL,
  `broken_k` double DEFAULT NULL,
  `sampah` double DEFAULT NULL,
  `slety` double DEFAULT NULL,
  `fm` double DEFAULT NULL,
  `total_kotor` double DEFAULT NULL,
  `bc1` double DEFAULT NULL,
  `bc2` double DEFAULT NULL,
  `bc3` double DEFAULT NULL,
  `bc4` double DEFAULT NULL,
  `bc5` double DEFAULT NULL,
  `bc6` double DEFAULT NULL,
  `total_bc` double DEFAULT NULL,
  `bc_rata` double DEFAULT NULL,
  `tester_ka_testsby` double DEFAULT NULL,
  `tester_ka_testdaerah` double DEFAULT NULL,
  `tester_ka_testbeli` double DEFAULT NULL,
  `abu_testsby` double DEFAULT NULL,
  `abu_testdaerah` double DEFAULT NULL,
  `abu_testbeli` double DEFAULT NULL,
  `ak_testsby` double DEFAULT NULL,
  `ak_testdaerah` double DEFAULT NULL,
  `ak_testbeli` double DEFAULT NULL,
  `bm_testsby` double DEFAULT NULL,
  `bm_testdaerah` double DEFAULT NULL,
  `bm_testbeli` double DEFAULT NULL,
  `pl_testsby` double DEFAULT NULL,
  `pl_testdaerah` double DEFAULT NULL,
  `pl_testbeli` double DEFAULT NULL,
  `gg_testsby` double DEFAULT NULL,
  `gg_testdaerah` double DEFAULT NULL,
  `gg_testbeli` double DEFAULT NULL,
  `daun` double DEFAULT NULL,
  `polong` double DEFAULT NULL,
  `bean_count` double DEFAULT NULL,
  `biji_bagus` double DEFAULT NULL,
  `triase` double DEFAULT NULL,
  `biji_rusak` double NOT NULL,
  `tongkol` double DEFAULT NULL,
  `biji_mati` double DEFAULT NULL,
  `wringkle` double DEFAULT NULL,
  `spotted` double DEFAULT NULL,
  `brown` double DEFAULT NULL,
  `oily` double DEFAULT NULL,
  `void` double DEFAULT NULL,
  `abu` double NOT NULL,
  `gg` double NOT NULL,
  `usd` double NOT NULL,
  `kurs` double NOT NULL,
  `nilai_fh` double NOT NULL,
  `harga_pasar` double NOT NULL,
  `nilai_pd` double NOT NULL,
  `tanggal_kirim` date NOT NULL,
  `alamat_kirim` varchar(40) NOT NULL,
  `syarat` varchar(40) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_skb`
--

INSERT INTO `tb_skb` (`id_skb`, `tgl_jt`, `tanggal`, `supplier_id`, `id_home`, `item_id`, `type_kontrak`, `volume`, `kemasan`, `harga_kontrak`, `um`, `kadar_air`, `mouldy`, `insect`, `biji_pipih`, `triple_bean`, `broken_b`, `broken_k`, `sampah`, `slety`, `fm`, `total_kotor`, `bc1`, `bc2`, `bc3`, `bc4`, `bc5`, `bc6`, `total_bc`, `bc_rata`, `tester_ka_testsby`, `tester_ka_testdaerah`, `tester_ka_testbeli`, `abu_testsby`, `abu_testdaerah`, `abu_testbeli`, `ak_testsby`, `ak_testdaerah`, `ak_testbeli`, `bm_testsby`, `bm_testdaerah`, `bm_testbeli`, `pl_testsby`, `pl_testdaerah`, `pl_testbeli`, `gg_testsby`, `gg_testdaerah`, `gg_testbeli`, `daun`, `polong`, `bean_count`, `biji_bagus`, `triase`, `biji_rusak`, `tongkol`, `biji_mati`, `wringkle`, `spotted`, `brown`, `oily`, `void`, `abu`, `gg`, `usd`, `kurs`, `nilai_fh`, `harga_pasar`, `nilai_pd`, `tanggal_kirim`, `alamat_kirim`, `syarat`, `deleted`) VALUES
('SKB00001', '0000-00-00', '2017-05-21', 1, '1', 1, 'kontrak', 1000, 'Goni', 1000, 1000, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, '0000-00-00', '', '', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_skb2`
--

CREATE TABLE `tb_skb2` (
  `id_skb` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `id_home` varchar(10) NOT NULL,
  `type_kontrak` varchar(20) NOT NULL,
  `ka` double NOT NULL,
  `bc` double NOT NULL,
  `m` double NOT NULL,
  `i` double NOT NULL,
  `kot` double NOT NULL,
  `jumlah_qty` double NOT NULL,
  `kemasan` varchar(20) NOT NULL,
  `harga_kontrak` double NOT NULL,
  `harga_dollar` double NOT NULL,
  `tanggal_kirim` date NOT NULL,
  `alamat_kirim` varchar(30) NOT NULL,
  `syarat` varchar(20) NOT NULL,
  `usd` double NOT NULL,
  `kurs` double NOT NULL,
  `nilai_fh` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tb_skb2`
--

INSERT INTO `tb_skb2` (`id_skb`, `tanggal`, `supplier_id`, `item_id`, `id_home`, `type_kontrak`, `ka`, `bc`, `m`, `i`, `kot`, `jumlah_qty`, `kemasan`, `harga_kontrak`, `harga_dollar`, `tanggal_kirim`, `alamat_kirim`, `syarat`, `usd`, `kurs`, `nilai_fh`, `deleted`) VALUES
('SK/00001', '2017-04-23', 10, 1, 'K001', 'Import USD', 10, 10, 10, 10, 12, 120, 'karung', 0, 500, '2017-04-23', 'surabaya', 'ok\r\n', 500, 0, 0, 0),
('SK/00002', '2017-05-09', 4, 10, 'K001', 'Lokal', 10, 100, 10, 10, 10, 100, '10', 1e15, 0, '2017-05-10', 'Agus Jaya ', 'sasas', 0, 0, 0, 0),
('SK/00003', '2017-05-10', 4, 1, 'K001', 'Lokal', 12, 12, 131, 3, 199, 12, '12', 1000000000000, 0, '2017-05-17', 'sdfghjk', 'xcvbnm,', 0, 0, 0, 0),
('SK/00004', '2017-05-18', 1, 9, 'K002', 'Lokal', 12, 12, 12, 12, 12, 0, '12', 0, 0, '2017-05-04', 'asas', '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_stb`
--

CREATE TABLE `tb_stb` (
  `id_stb` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `id_home` varchar(10) NOT NULL,
  `volume` double NOT NULL,
  `kemasan` varchar(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `um` double NOT NULL,
  `kadar_air` double DEFAULT NULL,
  `mouldy` double DEFAULT NULL,
  `insect` double DEFAULT NULL,
  `biji_pipih` double DEFAULT NULL,
  `triple_bean` double DEFAULT NULL,
  `broken_b` double DEFAULT NULL,
  `broken_k` double DEFAULT NULL,
  `sampah` double DEFAULT NULL,
  `slety` double DEFAULT NULL,
  `fm` double DEFAULT NULL,
  `total_kotor` double DEFAULT NULL,
  `bc1` double DEFAULT NULL,
  `bc2` double DEFAULT NULL,
  `bc3` double DEFAULT NULL,
  `bc4` double DEFAULT NULL,
  `bc5` double DEFAULT NULL,
  `bc6` double DEFAULT NULL,
  `total_bc` double DEFAULT NULL,
  `bc_rata` double DEFAULT NULL,
  `tester_ka_testsby` double DEFAULT NULL,
  `tester_ka_testdaerah` double DEFAULT NULL,
  `tester_ka_testbeli` double DEFAULT NULL,
  `abu_testsby` double DEFAULT NULL,
  `abu_testdaerah` double DEFAULT NULL,
  `abu_testbeli` double DEFAULT NULL,
  `ak_testsby` double DEFAULT NULL,
  `ak_testdaerah` double DEFAULT NULL,
  `ak_testbeli` double DEFAULT NULL,
  `bm_testsby` double DEFAULT NULL,
  `bm_testdaerah` double DEFAULT NULL,
  `bm_testbeli` double DEFAULT NULL,
  `pl_testsby` double DEFAULT NULL,
  `pl_testdaerah` double DEFAULT NULL,
  `pl_testbeli` double DEFAULT NULL,
  `gg_testsby` double DEFAULT NULL,
  `gg_testdaerah` double DEFAULT NULL,
  `gg_testbeli` double DEFAULT NULL,
  `daun` double DEFAULT NULL,
  `polong` double DEFAULT NULL,
  `bean_count` double DEFAULT NULL,
  `biji_bagus` double DEFAULT NULL,
  `triase` double DEFAULT NULL,
  `biji_rusak` double NOT NULL,
  `tongkol` double DEFAULT NULL,
  `biji_mati` double DEFAULT NULL,
  `wringkle` double DEFAULT NULL,
  `spotted` double DEFAULT NULL,
  `brown` double DEFAULT NULL,
  `oily` double DEFAULT NULL,
  `void` double DEFAULT NULL,
  `abu` double NOT NULL,
  `gg` double NOT NULL,
  `claim` double NOT NULL,
  `tanggal_kirim` date NOT NULL,
  `alamat_kirim` varchar(40) NOT NULL,
  `syarat` varchar(40) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_stb`
--

INSERT INTO `tb_stb` (`id_stb`, `tanggal`, `supplier_id`, `id_home`, `volume`, `kemasan`, `item_id`, `um`, `kadar_air`, `mouldy`, `insect`, `biji_pipih`, `triple_bean`, `broken_b`, `broken_k`, `sampah`, `slety`, `fm`, `total_kotor`, `bc1`, `bc2`, `bc3`, `bc4`, `bc5`, `bc6`, `total_bc`, `bc_rata`, `tester_ka_testsby`, `tester_ka_testdaerah`, `tester_ka_testbeli`, `abu_testsby`, `abu_testdaerah`, `abu_testbeli`, `ak_testsby`, `ak_testdaerah`, `ak_testbeli`, `bm_testsby`, `bm_testdaerah`, `bm_testbeli`, `pl_testsby`, `pl_testdaerah`, `pl_testbeli`, `gg_testsby`, `gg_testdaerah`, `gg_testbeli`, `daun`, `polong`, `bean_count`, `biji_bagus`, `triase`, `biji_rusak`, `tongkol`, `biji_mati`, `wringkle`, `spotted`, `brown`, `oily`, `void`, `abu`, `gg`, `claim`, `tanggal_kirim`, `alamat_kirim`, `syarat`, `deleted`) VALUES
('STB/00001', '2017-05-19', 2, '1', 1212, '1212', 6, 1212, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 121, '2017-05-27', 'asas', 'asassa', 0),
('STB/00002', '2017-05-19', 2, '1', 1212, '1212', 6, 1212, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 121, '2017-05-27', 'asas', 'asassa', 0),
('STB/00001', '2017-05-19', 2, '1', 1000, 'aasa', 6, 999999.8, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 10000, '2017-05-20', 'assa', 'asasas', 0),
('STB/00001', '2017-05-19', 2, '1', 100, '10', 1, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '2017-05-20', 'qwqwqw', 'qwqwq', 0),
('STB/00005', '2017-05-19', 2, '1', 11, 'aas', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '2017-05-20', '1', 'asas', 0),
('STB/00006', '2017-05-20', 1, '1', 12, 'goni', 12, 10000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, '2017-05-27', 'asa', 'asas', 0),
('STB/00007', '2017-08-05', 1, '1', 1000000, 'Goni', 6, 1000000000, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 12, 0, 0, 0, 0, 20, 20, 20, 20, 23, 0, 0, 1000, '2017-07-01', 'Jl.jawa', 'qwewe', 0),
('STB/00008', '2017-05-24', 1, '1', 90, '909', 1, 909, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 90, '2017-05-24', 'sasaasas', 'sas', 0),
('STB/00009', '2017-07-19', 4, '1', 15, 'asd', 6, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, '2017-07-20', 'asd', 'asd', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tms_kemasan`
--

CREATE TABLE `tms_kemasan` (
  `id_kemasan` int(11) NOT NULL,
  `kemasan` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tms_kemasan`
--

INSERT INTO `tms_kemasan` (`id_kemasan`, `kemasan`) VALUES
(2, 'Goni'),
(3, 'Plastik Besar'),
(4, 'Plastik Kecil'),
(5, 'Double Plastik'),
(6, 'Pallet'),
(7, 'Goni Plastik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trf`
--

CREATE TABLE `trf` (
  `id_trf` varchar(30) NOT NULL,
  `id_references` varchar(30) NOT NULL,
  `type_trf` varchar(50) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `received_by` varchar(100) NOT NULL,
  `paid_to` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `acc` int(11) NOT NULL,
  `posted` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `custom1` varchar(100) DEFAULT NULL,
  `custom2` varchar(100) DEFAULT NULL,
  `custom3` varchar(100) DEFAULT NULL,
  `custom4` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trf`
--

INSERT INTO `trf` (`id_trf`, `id_references`, `type_trf`, `date_transaction`, `date_input`, `received_by`, `paid_to`, `note`, `acc`, `posted`, `deleted`, `person_id`, `custom1`, `custom2`, `custom3`, `custom4`) VALUES
('trf0001', 'LPB/00023', 'BKK', '2017-05-20', '2017-05-20', 'agus', 'danto', 'note', 1, 1, 0, 1, '', '', '', ''),
('trf0002', 'LPB/00025', 'BKK', '2017-05-20', '2017-05-20', 'agus', 'danto', 'note', 1, 1, 0, 1, 'trf0001asasa', '', NULL, NULL),
('trf0003', 'LPB/00024', 'BKK', '2017-05-20', '2017-05-20', 'agus', 'danto', 'note', 1, 1, 0, 1, 'trf0001asasa', '', NULL, '`'),
('trf0004', 'LPB/00019', 'BKK', '2017-05-20', '2017-05-20', 'agus', 'danto', 'note', 1, 1, 0, 1, 'trf0001asasa', '', NULL, NULL),
('trf0006', 'NT/00011', 'BKK', '2017-05-24', '2017-05-24', 'Agus', 'danto', 'nite', 1, 1, 0, 1, NULL, NULL, NULL, NULL),
('trf0005', 'NT/00001', 'BKK', '2017-05-24', '2017-05-24', 'Agus', 'danto', 'nite', 1, 1, 0, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `trf_journal`
--

CREATE TABLE `trf_journal` (
  `id_trf_journal` bigint(20) NOT NULL,
  `id_transaction_trf` varchar(30) NOT NULL,
  `id_coa` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `type_trf_journal` varchar(30) NOT NULL,
  `note` text,
  `debit` double DEFAULT NULL,
  `kredit` double DEFAULT NULL,
  `kurs` double NOT NULL,
  `acc` int(11) NOT NULL,
  `posted` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `custom1` varchar(100) DEFAULT NULL,
  `custom2` varchar(100) DEFAULT NULL,
  `custom3` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `trf_journal`
--

INSERT INTO `trf_journal` (`id_trf_journal`, `id_transaction_trf`, `id_coa`, `id_currency`, `date_transaction`, `date_input`, `type_trf_journal`, `note`, `debit`, `kredit`, `kurs`, `acc`, `posted`, `deleted`, `person_id`, `custom1`, `custom2`, `custom3`) VALUES
(1, 'trf0003', 5, 1, '2017-05-20', '2017-05-20', 'BKK', '', 0, 12000, 0, 1, 1, 0, 1, '', '', ''),
(2, 'trf0002', 5, 1, '2017-05-20', '2017-05-20', 'BKK', 'v', 0, 12001, 0, 1, 1, 0, 1, NULL, NULL, NULL),
(4, 'trf0003', 5, 1, '2017-05-20', '2017-05-20', 'BKK', 'v', 0, 12001, 0, 1, 1, 0, 1, NULL, NULL, NULL),
(3, 'trf0001', 5, 1, '2017-05-20', '2017-05-20', 'BKK', 'v', 0, 12001, 0, 1, 1, 0, 1, NULL, NULL, NULL),
(5, 'trf0004', 5, 1, '2017-05-20', '2017-05-20', 'BKK', 'v', 0, 12001, 0, 1, 1, 0, 1, NULL, NULL, NULL),
(6, 'trf0005', 5, 1, '2017-05-20', '2017-05-20', 'BKK', 'v', 0, 12001, 0, 1, 1, 0, 1, NULL, NULL, NULL),
(7, 'trf0006', 5, 1, '2017-05-20', '2017-05-20', 'BKK', 'v', 0, 12001, 0, 1, 1, 0, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttf`
--

CREATE TABLE `ttf` (
  `id_ttf` varchar(25) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `date_input` date NOT NULL,
  `person_request` varchar(25) NOT NULL,
  `person_id` int(10) NOT NULL,
  `note` varchar(50) NOT NULL,
  `balance` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `total` double NOT NULL,
  `tanggal_bayar` date NOT NULL,
  `status` int(11) NOT NULL,
  `tgljt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttf`
--

INSERT INTO `ttf` (`id_ttf`, `supplier_id`, `date_transaction`, `date_input`, `person_request`, `person_id`, `note`, `balance`, `deleted`, `acc`, `kategori`, `total`, `tanggal_bayar`, `status`, `tgljt`) VALUES
('TTF/00001', 10, '2017-04-23', '2017-04-23', '', 1, 'jos\r\n', 0, 0, 0, 'Barang Umum', 1000000, '2017-04-28', 1, '2017-04-28'),
('TTF/00002', 10, '2017-04-23', '2017-04-23', '', 1, 'OK', 0, 0, 0, 'Jasa', 100000, '2017-04-24', 1, '2017-04-28'),
('TTF/00003', 10, '2017-05-12', '2017-05-12', '', 1, '', 0, 0, 0, 'Barang Umum', 0, '0000-00-00', 0, '0000-00-00'),
('TTF/00004', 14, '2017-06-08', '2017-05-18', '', 1, '', 0, 0, 0, 'Aset', 20000, '2017-06-12', 1, '2017-06-08'),
('TTF/00005', 14, '2017-05-19', '2017-05-19', '', 1, 'asdasd', 0, 0, 0, 'Barang Umum', 10000000000, '2017-05-19', 1, '2017-05-20'),
('TTF/00006', 14, '2017-05-19', '2017-05-19', '', 1, '', 0, 0, 0, 'Jasa', 0, '0000-00-00', 0, '0000-00-00'),
('TTF/00007', 14, '2017-05-19', '2017-05-19', '', 1, 'asdasd', 0, 0, 0, 'Barang Umum', 1000000000, '2017-05-17', 1, '2017-05-20'),
('TTF/00008', 14, '2017-06-06', '2017-06-06', '', 1, '', 0, 0, 0, 'Barang Umum', 0, '0000-00-00', 0, '0000-00-00'),
('TTF/00009', 14, '2017-06-13', '2017-06-13', '', 1, '', 0, 0, 0, 'Barang Umum', 20000, '2017-06-13', 1, '2017-06-15'),
('TTF/00010', 14, '2017-06-13', '2017-06-13', '', 1, 'ok', 0, 0, 0, 'Barang Umum', 5000, '2017-06-13', 1, '2017-06-14'),
('TTF/00011', 14, '2017-06-15', '2017-06-15', '', 1, 'ok', 0, 0, 0, 'Jasa', 10000, '2017-06-14', 1, '2017-06-16'),
('TTF/00012', 14, '2017-06-15', '2017-06-15', '', 1, '', 0, 0, 0, 'Jasa', 0, '0000-00-00', 0, '0000-00-00'),
('TTF/00013', 14, '2017-06-16', '2017-06-16', '', 1, 'assasa', 0, 0, 0, 'Jasa', 10000, '2017-06-16', 1, '2017-06-24'),
('TTF/00014', 14, '2017-06-19', '2017-06-19', '', 1, '', 0, 0, 0, 'Barang Umum', 0, '0000-00-00', 0, '0000-00-00'),
('TTF/00015', 14, '2017-06-19', '2017-06-19', '', 1, 'ok', 0, 0, 0, 'Jasa', 444, '2017-06-19', 1, '2017-06-20'),
('TTF/00016', 14, '2017-07-19', '2017-07-19', '', 1, 'ok', 0, 0, 0, 'Barang Umum', 10000, '2017-07-20', 1, '2017-07-21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttr_kemasan`
--

CREATE TABLE `ttr_kemasan` (
  `id_kemas` int(11) NOT NULL,
  `id_timbang` varchar(30) NOT NULL,
  `id_kemasan` int(11) NOT NULL,
  `jumlah_kemasan` int(11) NOT NULL,
  `total_berat` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttr_kemasan`
--

INSERT INTO `ttr_kemasan` (`id_kemas`, `id_timbang`, `id_kemasan`, `jumlah_kemasan`, `total_berat`) VALUES
(32, 'NT/00007', 18, 10, 1.44),
(33, 'NT/00007', 19, 10, 1.303),
(31, 'NT/00002', 18, 1, 1),
(29, 'NT/00001', 18, 1, 0.0008),
(30, 'NT/00001', 19, 12, 0.000009);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttr_main_scale`
--

CREATE TABLE `ttr_main_scale` (
  `id_timbang` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `jt` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `no_polisi` varchar(50) NOT NULL,
  `no_kontainer` varchar(50) NOT NULL,
  `seal1` int(50) NOT NULL,
  `seal2` int(50) NOT NULL,
  `catatan` text NOT NULL,
  `total_colly` double NOT NULL,
  `total_bruto` double NOT NULL,
  `total_tarra` double NOT NULL,
  `total_netto` double NOT NULL,
  `real_fisik` double NOT NULL,
  `rataka` double NOT NULL,
  `type_timbang` varchar(25) NOT NULL,
  `nota_lama` varchar(25) NOT NULL,
  `type_beli` varchar(255) NOT NULL,
  `um` double NOT NULL,
  `tgl_jt` date NOT NULL,
  `id_skb` varchar(255) NOT NULL,
  `No_User` int(11) NOT NULL,
  `acc` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttr_main_scale`
--

INSERT INTO `ttr_main_scale` (`id_timbang`, `item_id`, `location_id`, `jt`, `tanggal`, `supplier_id`, `no_polisi`, `no_kontainer`, `seal1`, `seal2`, `catatan`, `total_colly`, `total_bruto`, `total_tarra`, `total_netto`, `real_fisik`, `rataka`, `type_timbang`, `nota_lama`, `type_beli`, `um`, `tgl_jt`, `id_skb`, `No_User`, `acc`, `deleted`) VALUES
('NT/00001', 1, 1, 'pembelian', '2017-04-19', 4, 'asa', 'asas', 0, 0, 'asaa', 0, 5, 0.000908, 4.999092, 0, 0, 'awal', '', 'Kontrak', 999999.98, '2017-05-28', 'SK/00002', 0, 1, 0),
('NT/00002', 6, 1, 'pembelian', '2017-04-19', 4, 'L1234L', '', 0, 0, '', 0, 3, 1, 2, 0, 0, 'awal', '', 'Tunai', 0, '2017-05-28', '', 0, 1, 0),
('NT/00003', 9, 1, 'pembelian', '2017-04-19', 5, 'L1234L', '', 0, 0, '', 0, 3, 0, 3, 0, 0, 'awal', '', 'kredit', 0, '2017-05-28', '', 0, 1, 0),
('NT/00004', 10, 1, 'pembelian', '2017-04-19', 5, 'L1234L', '', 0, 0, '', 0, 3, 0, 3, 0, 0, 'awal', '', 'kredit', 0, '2017-05-28', '', 0, 1, 0),
('NT/00005', 11, 1, 'pembelian', '2017-04-19', 5, 'L1234L', '', 0, 0, '', 0, 4, 0, 4, 0, 0, 'awal', '', 'kredit', 0, '2017-05-28', '', 0, 1, 0),
('NT/00006', 12, 1, 'pembelian', '2017-04-19', 4, 'L0010l', '', 0, 0, '', 0, 3, 0, 3, 0, 0, 'awal', '', 'kredit', 0, '2017-05-28', '', 0, 1, 0),
('NT/00007', 6, 1, 'titipan', '2017-04-20', 4, 'L1234L', '', 0, 0, '', 0, 2000, 27.43, 1972.57, 0, 1, 'awal', '', 'kredit', 0, '2017-05-28', '', 0, 1, 0),
('NT/00008', 6, 1, 'pembelian', '2017-04-20', 4, 'L1234L', '', 0, 0, '', 0, 1000, 0, 1000, 0, 0, 'ulang', 'NT/00007', 'kredit', 0, '2017-05-25', '', 0, 1, 0),
('NT/00009', 6, 1, 'Proses', '2017-04-20', 4, 'L1234L', '', 0, 0, '', 0, 1000, 0, 1000, 0, 0, 'ulang', 'NT/00008', 'kredit', 0, '2017-05-28', '', 0, 1, 0),
('NT/00010', 6, 1, 'Hasil Jadi', '2017-04-20', 4, 'L1234L', '', 0, 0, '', 0, 1000, 0, 1000, 0, 0, 'ulang', 'NT/00009', 'kredit', 0, '2017-05-28', '', 0, 1, 0),
('NT/00011', 1, 1, 'pembelian', '2017-04-26', 4, 'L1234L', '123', 1, 2, 'OK', 200, 0, 0, 0, 0, 0, 'awal', '', 'kredit', 0, '2017-05-28', '', 0, 0, 0),
('NT/00012', 1, 1, 'nota retur penjualan', '2017-04-26', 4, 'L1234L', '', 0, 0, '', 0, 0, 0, 0, 0, 0, 'ulang', '', 'kredit', 0, '2017-05-28', '', 0, 0, 0),
('NT/00013', 1, 1, 'pembelian', '2017-05-06', 6, 'l12', '', 0, 0, '', 0, 200, 0, 200, 0, 0, 'awal', '', 'kredit', 0, '2017-05-28', '', 0, 1, 0),
('NT/00014', 1, 1, 'pembelian', '2017-05-09', 5, 'l123', '', 0, 0, '', 0, 0, 0, 20000, 0, 0, 'awal', '', 'Kredit', 100000, '2017-05-28', '', 0, 0, 0),
('NT/00015', 1, 1, '', '2017-05-19', 2, '1', '', 0, 0, '', 0, 2046, 0, 2046, 0, 0, 'awal', '', '', 0, '2017-05-28', '', 0, 1, 0),
('NT/00016', 1, 1, '', '2017-05-19', 1, '1', '', 0, 0, '', 0, 2046, 0, 2046, 0, 0, 'awal', '', '', 0, '2017-05-28', '', 0, 1, 0),
('NT/00017', 1, 1, 'titipan', '2017-05-19', 4, '1', '', 0, 0, '', 0, 0, 0, 0, 0, 0, 'awal', '', '', 0, '2017-05-28', '', 0, 0, 0),
('NT/00018', 1, 1, 'titipan', '2017-05-19', 4, '1', '', 0, 0, '', 0, 1023, 0, 1023, 0, 0, 'awal', '', '', 0, '2017-05-28', '', 0, 1, 0),
('', 0, 0, '', '0000-00-00', 0, '', '', 0, 0, '', 0, 0, 0, 0, 0, 0, '', '', '', 0, '0000-00-00', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ttr_scale`
--

CREATE TABLE `ttr_scale` (
  `id_scale` int(11) NOT NULL,
  `id_timbang` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `jenis_timbang` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `berat` varchar(10) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `no_polisi` varchar(10) NOT NULL,
  `no_kontainer` varchar(10) NOT NULL,
  `seal1` varchar(50) NOT NULL,
  `seal2` varchar(50) NOT NULL,
  `catatan` text NOT NULL,
  `ka1` varchar(10) NOT NULL,
  `ka2` varchar(10) NOT NULL,
  `ka3` varchar(10) NOT NULL,
  `No_User` int(11) NOT NULL,
  `acc` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ttr_scale`
--

INSERT INTO `ttr_scale` (`id_scale`, `id_timbang`, `item_id`, `location_id`, `jenis_timbang`, `tanggal`, `berat`, `id_supplier`, `no_polisi`, `no_kontainer`, `seal1`, `seal2`, `catatan`, `ka1`, `ka2`, `ka3`, `No_User`, `acc`) VALUES
(98, 'NT/00011', 1, 1, '', '2017-04-26', '12\r\n', 0, '', '', '132', '123', '', '2', '7', '8', 0, 0),
(97, 'NT/00011', 1, 1, '', '2017-04-26', '12\r\n', 0, '', '', '132', '123', '', '12', '3', '4', 0, 0),
(96, 'NT/00011', 1, 1, '', '2017-04-26', '12\r\n', 0, '', '', '132', '123', '', '12', '8', '3', 0, 0),
(95, 'NT/00010', 1, 1, '', '2017-04-20', '1000\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(94, 'NT/00009', 1, 1, '', '2017-04-20', '1000\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(93, 'NT/00008', 1, 1, '', '2017-04-20', '1000\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(92, 'NT/00007', 1, 1, '', '2017-04-20', '1000\r\n', 0, '', '', '132', '123', '', '1', '1', '1', 0, 0),
(91, 'NT/00007', 1, 1, '', '2017-04-20', '1000\r\n', 0, '', '', '132', '123', '', '1', '1', '1', 0, 0),
(90, 'NT/00006', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(89, 'NT/00006', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(88, 'NT/00006', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(87, 'NT/00005', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(86, 'NT/00005', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(85, 'NT/00005', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(84, 'NT/00005', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(83, 'NT/00004', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(82, 'NT/00004', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(81, 'NT/00004', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(80, 'NT/00003', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(79, 'NT/00003', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(78, 'NT/00003', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(77, 'NT/00002', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(76, 'NT/00002', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(72, 'NT/00001', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(73, 'NT/00001', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(74, 'NT/00001', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(75, 'NT/00002', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(71, 'NT/00001', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(70, 'NT/00001', 1, 1, '', '2017-04-19', '1', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00013', 1, 1, '', '2017-05-06', '200\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00014', 1, 1, '', '2017-05-09', '120\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00014', 1, 1, '', '2017-05-09', '120\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00014', 1, 1, '', '2017-05-09', '120\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00014', 1, 1, '', '2017-05-09', '120\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00014', 1, 1, '', '2017-05-09', '120\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00014', 1, 1, '', '2017-05-09', '120\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00014', 1, 1, '', '2017-05-09', '120\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00014', 1, 1, '', '2017-05-09', '120\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00014', 1, 1, '', '2017-05-09', '40\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00015', 1, 1, '', '2017-05-19', '1023\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00015', 1, 1, '', '2017-05-19', '1023\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00016', 1, 1, '', '2017-05-19', '1023\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00016', 1, 1, '', '2017-05-19', '1023\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0),
(0, 'NT/00018', 1, 1, '', '2017-05-19', '1023\r\n', 0, '', '', '132', '123', '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_as`
--

CREATE TABLE `t_as` (
  `id_as` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `id_sj` varchar(20) NOT NULL,
  `dari` varchar(25) NOT NULL,
  `ke` varchar(25) NOT NULL,
  `deleted` int(25) NOT NULL,
  `type` varchar(10) NOT NULL,
  `nopol` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_as`
--

INSERT INTO `t_as` (`id_as`, `tanggal`, `id_sj`, `dari`, `ke`, `deleted`, `type`, `nopol`) VALUES
('AS00001', '0000-00-00', '2017-04-11', 'Toko', 'Toko', 0, 'sjm', 'L0121L'),
('AS00002', '2017-04-11', 'SJM/00001', 'Toko', 'Toko', 0, 'sjm', 'L0012L'),
('AS00003', '2017-04-12', 'SJ0001', 'Toko', 'Gilang Saputra - Jl.Melat', 0, 'sj', 'L1212L'),
('AS00004', '2017-04-20', 'SJM/00001', 'Toko', 'Gudang Romokalisari', 0, 'sjm', ''),
('AS00005', '2017-06-16', 'sjp/00005', 'Toko', 'Hardanto - Jolotundo', 0, 'sj', 'zxcvbnm');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_bapj`
--

CREATE TABLE `t_bapj` (
  `id_bapj` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `no_spk` varchar(30) NOT NULL,
  `supplier` varchar(45) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `total` double NOT NULL,
  `diskon` double NOT NULL,
  `dpp` double NOT NULL,
  `pph` double NOT NULL,
  `gt` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `id_po` varchar(30) NOT NULL,
  `person_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `tgl_jt_dpp` date NOT NULL,
  `tgl_jt_pph` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_bapj`
--

INSERT INTO `t_bapj` (`id_bapj`, `tanggal`, `no_spk`, `supplier`, `keterangan`, `total`, `diskon`, `dpp`, `pph`, `gt`, `deleted`, `id_po`, `person_id`, `status`, `tgl_jt_dpp`, `tgl_jt_pph`) VALUES
('BAPJ/00001', '2017-04-23', 'SKJ19111', '10', 'Tolong cepat diperbaiki', 14800, 800, 14000, 1400, 15400, 0, 'PO/00002', 1, 2, '0000-00-00', '0000-00-00'),
('BAPJ/00002', '2017-05-07', 'asasa', '7', '', 16, 0, 16, 1.6, 17.6, 0, 'PO/00005', 1, 2, '0000-00-00', '0000-00-00'),
('BAPJ/00003', '2017-05-09', 'SPK0001', '6', '', 10000, 100, 9900, 990, 10890, 0, 'PO/00009', 1, 2, '0000-00-00', '0000-00-00'),
('BAPJ/00004', '2017-05-09', 'ASASS', '7', '', -15000, 0, -15000, -1500, -16500, 0, 'PO/00010', 1, 2, '1970-01-01', '1970-01-01'),
('BAPJ/00005', '2017-05-12', 'SKJ00001', '7', '', 0, 0, 0, 0, 0, 0, 'PO/00010', 1, 0, '0000-00-00', '0000-00-00'),
('BAPJ/00006', '2017-05-12', 'sdf', '7', '', 0, 0, 0, 0, 0, 0, 'PO/00010', 1, 0, '0000-00-00', '0000-00-00'),
('BAPJ/00007', '2017-05-12', 'skj', '7', '', 0, 0, 0, 0, 0, 0, 'PO/00010', 1, 0, '0000-00-00', '0000-00-00'),
('BAPJ/00008', '2017-06-06', 'fghfh', '1', 'asasasa', 9999999999900, 10000, 9999999989900, 999999998990, 10999999988890, 0, 'PO/00020', 1, 2, '1970-01-01', '1970-01-01'),
('BAPJ/00009', '2017-06-12', 'asd', '1', '', 0, 0, 0, 0, 0, 0, 'PO/00020', 1, 1, '1970-01-01', '1970-01-01'),
('BAPJ/00010', '2017-06-12', 'skal', '1', 'assasasas', 7, 0, 7, 0.7000000000000001, 7.7, 0, 'PO/00020', 1, 2, '2017-06-14', '2017-06-12'),
('BAPJ/00011', '2017-06-12', 'ksjs', '14', 'ok tok', 0, 0, 0, 0, 100000000, 0, 'PO/00017', 1, 2, '1970-01-01', '1970-01-01'),
('BAPJ/00012', '2017-07-19', 'SPK39303999', '1', 'ok', 14900, 900, 14000, 1400, 15400, 0, 'PO/00020', 1, 2, '2017-07-20', '2017-07-20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_bpb`
--

CREATE TABLE `t_bpb` (
  `id_bpb` varchar(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `category` varchar(40) NOT NULL,
  `tanggal` date NOT NULL,
  `total` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `tujuan` int(255) NOT NULL,
  `person_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_bpb`
--

INSERT INTO `t_bpb` (`id_bpb`, `item_id`, `category`, `tanggal`, `total`, `deleted`, `tujuan`, `person_id`, `status`) VALUES
('BPB00001', 1, '', '2017-04-19', 0, 0, 2, 1, 1),
('BPB00002', 1, '', '2017-04-19', 0, 0, 2, 1, 0),
('BPB00003', 1, '', '2017-04-19', 0, 0, 2, 1, 0),
('BPB00004', 1, 'Barang Umum', '2017-04-19', 0, 0, 2, 1, 0),
('BPB00005', 1, 'Barang Umum', '2017-04-19', 0, 0, 2, 1, 0),
('BPB00006', 1, 'Barang Umum', '2017-04-19', 0, 0, 2, 1, 1),
('BPB00007', 1, 'Produk Utama', '2017-04-20', 0, 0, 1, 1, 1),
('BPB00008', 1, 'Barang Umum', '2017-06-16', 0, 0, 1, 1, 0),
('BPB00009', 1, 'Produk Utama', '2017-06-16', 0, 0, 2, 1, 1),
('BPB00010', 1, 'Produk Utama', '2017-06-16', 0, 0, 2, 1, 1),
('BPB00011', 1, 'Barang Umum', '2017-06-16', 0, 0, 1, 1, 1),
('BPB00012', 1, 'Barang Umum', '2017-07-05', 0, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_categories`
--

CREATE TABLE `t_categories` (
  `id_category` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_categories`
--

INSERT INTO `t_categories` (`id_category`, `nama`, `jenis`, `deleted`) VALUES
(1, 'Barang Umum', 'ATK', 0),
(2, 'Produk Utama', 'CC', 0),
(3, 'Produk Sampingan', 'Bean Count', 0),
(4, 'Jasa', 'Jasa', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_detail_ps`
--

CREATE TABLE `t_detail_ps` (
  `id_ps` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL,
  `colly` double NOT NULL,
  `tonase` double NOT NULL,
  `qualify` int(11) NOT NULL,
  `tanggal_target` int(11) NOT NULL,
  `tanggal_realisasi` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_do`
--

CREATE TABLE `t_do` (
  `id_do` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `id_so` varchar(30) NOT NULL,
  `location_id` int(11) NOT NULL,
  `dikirimkan` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `total` double NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `catatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_do`
--

INSERT INTO `t_do` (`id_do`, `tanggal`, `id_so`, `location_id`, `dikirimkan`, `alamat`, `total`, `status`, `deleted`, `catatan`) VALUES
('do/00001', '2017-06-15', 'SO/00001', 1, '1', '', 0, 1, 0, ''),
('do/00002', '2017-06-15', 'SO/00002', 1, '1', '', 0, 1, 0, ''),
('do/00003', '2017-07-04', 'SO/00005', 1, '1', '', 0, 1, 0, ''),
('do/00004', '2017-07-10', 'SO/00006', 2, '1', '', 0, 1, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_kpso`
--

CREATE TABLE `t_kpso` (
  `id_kpso` varchar(10) NOT NULL,
  `location_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `pj` varchar(30) NOT NULL,
  `deleted` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_kpso`
--

INSERT INTO `t_kpso` (`id_kpso`, `location_id`, `tanggal`, `pj`, `deleted`, `status`) VALUES
('SO00001', 1, '2017-06-16', 'Ony Prabowo', 0, 1),
('SO00002', 1, '2017-06-16', 'Ony Prabowo', 0, 1),
('SO00003', 1, '2017-06-16', 'Ony Prabowo', 0, 1),
('SO00004', 2, '2017-06-16', 'Ony Prabowo', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_memo_rp`
--

CREATE TABLE `t_memo_rp` (
  `id_memo_rp` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `id_np` varchar(30) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `id_sj` varchar(20) NOT NULL,
  `total_diterima` double NOT NULL,
  `total_dikirim` double NOT NULL,
  `total_tidak_sesuai` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_memo_rp`
--

INSERT INTO `t_memo_rp` (`id_memo_rp`, `tanggal`, `id_np`, `customer_id`, `id_sj`, `total_diterima`, `total_dikirim`, `total_tidak_sesuai`, `deleted`, `status`, `keterangan`) VALUES
('MRJ00001', '2017-06-15', 'NP00001', 1, 'sjp/00001', 0, 0, 0, 0, 1, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_nc`
--

CREATE TABLE `t_nc` (
  `id_nc` varchar(30) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `id_stb` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `um` double NOT NULL,
  `jh` varchar(5) NOT NULL,
  `tot_cl` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_nc`
--

INSERT INTO `t_nc` (`id_nc`, `supplier_id`, `id_stb`, `tanggal`, `um`, `jh`, `tot_cl`, `deleted`) VALUES
('NC/00001', 1, 'STB/00007', '2017-06-12', 1000000000, '12', 1000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_np`
--

CREATE TABLE `t_np` (
  `id_np` varchar(30) NOT NULL,
  `id_npum` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `type` varchar(255) NOT NULL,
  `id_so` varchar(30) NOT NULL,
  `id_sj` varchar(20) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `total` double NOT NULL,
  `ppn` double NOT NULL,
  `gt` double NOT NULL,
  `claim` double NOT NULL,
  `um` double NOT NULL,
  `sisa_bayar` double NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `no_fp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_np`
--

INSERT INTO `t_np` (`id_np`, `id_npum`, `customer_id`, `tanggal`, `type`, `id_so`, `id_sj`, `keterangan`, `total`, `ppn`, `gt`, `claim`, `um`, `sisa_bayar`, `status`, `deleted`, `no_fp`) VALUES
('NP00001', 'NPUM00001', 1, '2017-06-15', 'so', 'SO/00001', 'sjp/00001', 'ok', 121454400000, 12145440000, 133599840000, 0, 13359984000, 120239856000, 1, 0, 'asasas'),
('NP00002', 'NPUM00002', 1, '2017-07-04', 'so', 'SO/00005', 'sjp/00006', '', 0, 0, 0, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_npum`
--

CREATE TABLE `t_npum` (
  `id_npum` varchar(30) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `type` varchar(255) NOT NULL,
  `id_so` varchar(30) NOT NULL,
  `id_sj` varchar(20) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `total` double NOT NULL,
  `ppn` double NOT NULL,
  `pph` double NOT NULL,
  `um` double NOT NULL,
  `total_um` double NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `no_fp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_npum`
--

INSERT INTO `t_npum` (`id_npum`, `customer_id`, `tanggal`, `type`, `id_so`, `id_sj`, `keterangan`, `total`, `ppn`, `pph`, `um`, `total_um`, `status`, `deleted`, `no_fp`) VALUES
('NPUM00001', 1, '2017-06-15', 'so', 'SO/00001', '', '', 121.454, 12145440000, 133599840000, 10, 13359984000, 1, 0, ''),
('NPUM00002', 1, '2017-07-04', 'so', 'SO/00005', 'sjp/00006', 'note', 10.121, 1012120000000, 11133320000000, 12, 1335998400000, 1, 0, 'qqw12'),
('NPUM00003', 1, '2017-07-10', 'skj', 'SK00002', '', '', 0, 0, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_nrp`
--

CREATE TABLE `t_nrp` (
  `id_nrp` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `id_memo_rp` varchar(30) NOT NULL,
  `id_np` varchar(30) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `id_sj` varchar(30) NOT NULL,
  `total` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_nrp`
--

INSERT INTO `t_nrp` (`id_nrp`, `tanggal`, `id_memo_rp`, `id_np`, `customer_id`, `id_sj`, `total`, `deleted`, `catatan`, `status`) VALUES
('NRJ00001', '2017-06-15', 'MRJ00001', 'NP00001', 1, 'sjp/00001', 0, 0, 'asas', 1),
('NRJ00002', '2017-06-15', 'MRJ00001', 'NP00001', 1, 'sjp/00001', 0, 0, 'ok\n', 1),
('NRJ00003', '2017-06-15', 'MRJ00001', 'NP00001', 1, 'sjp/00001', 0, 0, 'asdasd', 1),
('NRJ00004', '2017-07-03', 'MRJ00001', 'NP00001', 1, 'sjp/00001', 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_phb`
--

CREATE TABLE `t_phb` (
  `id_phb` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_phb`
--

INSERT INTO `t_phb` (`id_phb`, `tanggal`, `supplier_id`, `deleted`, `status`) VALUES
('PHB00001', '2017-04-22', 10, 0, 1),
('PHB00002', '2017-04-28', 4, 0, 1),
('PHB00003', '2017-05-12', 4, 0, 1),
('PHB00004', '2017-05-20', 4, 0, 1),
('PHB00005', '2017-05-01', 1, 0, 0),
('PHB00006', '2017-06-05', 1, 0, 0),
('PHB00007', '2017-06-10', 4, 0, 0),
('PHB00008', '2017-06-12', 14, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_phs`
--

CREATE TABLE `t_phs` (
  `id_phs` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `id_city` int(255) NOT NULL,
  `tanggal` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_phs`
--

INSERT INTO `t_phs` (`id_phs`, `item_id`, `id_city`, `tanggal`, `supplier_id`, `deleted`, `status`) VALUES
('PHB00001', 0, 0, '2017-04-22', 10, 0, 1),
('PHB00002', 0, 0, '2017-04-28', 4, 0, 1),
('PHB00003', 0, 0, '2017-05-12', 4, 0, 1),
('PHB00004', 0, 0, '2017-05-20', 4, 0, 0),
('PHB00005', 0, 0, '2017-05-01', 1, 0, 0),
('PHB00006', 0, 0, '2017-06-05', 1, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_pkb`
--

CREATE TABLE `t_pkb` (
  `id_pkb` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `npwp` varchar(20) NOT NULL,
  `sppkp` varchar(20) NOT NULL,
  `alamat` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `pembayaran` varchar(20) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_pl`
--

CREATE TABLE `t_pl` (
  `no_pl` varchar(11) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_code` varchar(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(50) NOT NULL,
  `company_phone` varchar(20) NOT NULL,
  `company_email` varchar(30) NOT NULL,
  `company_fax` varchar(15) NOT NULL,
  `agency_name` varchar(30) NOT NULL,
  `ppn` varchar(20) NOT NULL,
  `no_npwp` varchar(20) NOT NULL,
  `name_npwp` varchar(30) NOT NULL,
  `note` text NOT NULL,
  `pembayaran` varchar(15) NOT NULL,
  `date_in` date NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `acc` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `noktp` int(11) NOT NULL,
  `sppkp` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `t_pl`
--

INSERT INTO `t_pl` (`no_pl`, `person_id`, `company_code`, `company_name`, `company_address`, `company_phone`, `company_email`, `company_fax`, `agency_name`, `ppn`, `no_npwp`, `name_npwp`, `note`, `pembayaran`, `date_in`, `account_number`, `taxable`, `discount_percent`, `acc`, `deleted`, `noktp`, `sppkp`) VALUES
('PL00001', 1, '', 'Hardanto', 'Jolotundo', '09212121221', 'dantoking@gmail.com', '23232323232323', '', '1', '12121jkjkj121212', '', '', '1', '2017-06-07', '121212', 1, '0.00', 1, 0, 0, 'kjkjkj1k2j1k2j1k2jk1'),
('PL00002', 1, '', 'Dono', 'asdjlkajs', '23984093284', 'ahsdjashd', '234324o', '', '1', '345345435', '', '', '0', '2017-06-08', '9292039991', 1, '0.00', 0, 0, 0, '234325'),
('PL00003', 1, '', 'asda', 'asddd', '324343', 'asd', '23432', '', '1', '2343242342342', '', '', '0', '2017-06-13', '2342434', 1, '0.00', 0, 0, 0, '23423423423423'),
('PL00004', 1, '', 'asd', 'asd', '34534', 'as', '234', '', '0', '', '', '', '0', '2017-06-13', '23423', 1, '0.00', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_ps`
--

CREATE TABLE `t_ps` (
  `id_ps` varchar(30) NOT NULL,
  `tanggal_proses` date NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_ps`
--

INSERT INTO `t_ps` (`id_ps`, `tanggal_proses`, `status`, `deleted`) VALUES
('FPP00001', '2017-04-20', 0, 0),
('FPP00002', '2017-04-20', 0, 0),
('FPP00003', '2017-04-21', 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_psb`
--

CREATE TABLE `t_psb` (
  `id_psb` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `item_id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_psb`
--

INSERT INTO `t_psb` (`id_psb`, `tanggal`, `item_id`, `deleted`, `status`) VALUES
('PSB/00001', '2017-05-12', 9, 0, 2),
('PSB/00002', '2017-05-12', 24, 0, 2),
('PSB/00003', '2017-05-08', 17, 0, 2),
('PSB/00004', '2017-05-26', 24, 0, 0),
('PSB/00005', '2017-06-05', 18, 0, 2),
('PSB/00006', '2017-06-10', 9, 0, 0),
('PSB/00007', '2017-06-10', 22, 0, 2),
('PSB/00008', '2017-06-10', 9, 0, 2),
('PSB/00009', '2017-06-12', 22, 0, 2),
('PSB/00010', '2017-07-19', 9, 0, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_qc`
--

CREATE TABLE `t_qc` (
  `id_qc` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `id_timbang` varchar(30) NOT NULL,
  `kadar_air` double DEFAULT NULL,
  `mouldy` double DEFAULT NULL,
  `insect` double DEFAULT NULL,
  `biji_pipih` double DEFAULT NULL,
  `triple_bean` double DEFAULT NULL,
  `broken_b` double DEFAULT NULL,
  `broken_k` double DEFAULT NULL,
  `sampah` double DEFAULT NULL,
  `slety` double DEFAULT NULL,
  `fm` double DEFAULT NULL,
  `total_kotor` double DEFAULT NULL,
  `bc1` double DEFAULT NULL,
  `bc2` double DEFAULT NULL,
  `bc3` double DEFAULT NULL,
  `bc4` double DEFAULT NULL,
  `bc5` double DEFAULT NULL,
  `bc6` double DEFAULT NULL,
  `total_bc` double DEFAULT NULL,
  `bc_rata` double DEFAULT NULL,
  `tester_ka_testsby` double DEFAULT NULL,
  `tester_ka_testdaerah` double DEFAULT NULL,
  `tester_ka_testbeli` double DEFAULT NULL,
  `abu_testsby` double DEFAULT NULL,
  `abu_testdaerah` double DEFAULT NULL,
  `abu_testbeli` double DEFAULT NULL,
  `ak_testsby` double DEFAULT NULL,
  `ak_testdaerah` double DEFAULT NULL,
  `ak_testbeli` double DEFAULT NULL,
  `bm_testsby` double DEFAULT NULL,
  `bm_testdaerah` double DEFAULT NULL,
  `bm_testbeli` double DEFAULT NULL,
  `pl_testsby` double DEFAULT NULL,
  `pl_testdaerah` double DEFAULT NULL,
  `pl_testbeli` double DEFAULT NULL,
  `gg_testsby` double DEFAULT NULL,
  `gg_testdaerah` double DEFAULT NULL,
  `gg_testbeli` double DEFAULT NULL,
  `daun` double DEFAULT NULL,
  `polong` double DEFAULT NULL,
  `bean_count` double DEFAULT NULL,
  `biji_bagus` double DEFAULT NULL,
  `triase` double DEFAULT NULL,
  `biji_rusak` double NOT NULL,
  `tongkol` double DEFAULT NULL,
  `biji_mati` double DEFAULT NULL,
  `wringkle` double DEFAULT NULL,
  `spotted` double DEFAULT NULL,
  `brown` double DEFAULT NULL,
  `oily` double DEFAULT NULL,
  `void` double DEFAULT NULL,
  `abu` double NOT NULL,
  `gg` double NOT NULL,
  `harga_standar` double NOT NULL,
  `tot_tambah_harga` double NOT NULL,
  `tot_pot_harga` double NOT NULL,
  `harga_jadi` double NOT NULL,
  `grand_tot` double NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_qc`
--

INSERT INTO `t_qc` (`id_qc`, `item_id`, `id_timbang`, `kadar_air`, `mouldy`, `insect`, `biji_pipih`, `triple_bean`, `broken_b`, `broken_k`, `sampah`, `slety`, `fm`, `total_kotor`, `bc1`, `bc2`, `bc3`, `bc4`, `bc5`, `bc6`, `total_bc`, `bc_rata`, `tester_ka_testsby`, `tester_ka_testdaerah`, `tester_ka_testbeli`, `abu_testsby`, `abu_testdaerah`, `abu_testbeli`, `ak_testsby`, `ak_testdaerah`, `ak_testbeli`, `bm_testsby`, `bm_testdaerah`, `bm_testbeli`, `pl_testsby`, `pl_testdaerah`, `pl_testbeli`, `gg_testsby`, `gg_testdaerah`, `gg_testbeli`, `daun`, `polong`, `bean_count`, `biji_bagus`, `triase`, `biji_rusak`, `tongkol`, `biji_mati`, `wringkle`, `spotted`, `brown`, `oily`, `void`, `abu`, `gg`, `harga_standar`, `tot_tambah_harga`, `tot_pot_harga`, `harga_jadi`, `grand_tot`, `deleted`) VALUES
('QC/00001', 1, 'NT/00001', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1000, 10000, 1000, 0, 1000000, 0),
('QC/00002', 6, 'NT/00002', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 8, NULL, 0, NULL, NULL, 3, 4, 5, 6, 7, 0, 0, 9, 7, 7, 0, 7, 0),
('QC/00003', 9, 'NT/00003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 12, 12, 121, 0, 121, 0),
('QC/00004', 10, 'NT/00004', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 4, NULL, NULL, NULL, 0, NULL, 4, NULL, NULL, NULL, NULL, NULL, 2, 4, 2, 2, 2222, 0, 2, 0),
('QC/00005', 11, 'NT/00005', 12, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 2, 4, NULL, NULL, NULL, NULL, NULL, 0, 0, 132, 23, 232, 0, 233, 0),
('QC/00006', 12, 'NT/00006', 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, 4, 5, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1000, 1000, 1000, 0, 1000, 0),
('QC/00007', 6, 'NT/00007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0),
('QC/00008', 6, 'NT/00008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0),
('QC/00009', 6, 'NT/00009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0),
('QC/00010', 6, 'NT/00010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0),
('QC/00011', 1, 'NT/00011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 10000000, 0),
('QC/00012', 1, 'NT/00012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0),
('QC/00013', 1, 'NT/00013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 11000, 0),
('QC/00014', 1, 'NT/00014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 10000, 0),
('QC/00015', 1, 'NT/00015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0),
('QC/00016', 1, 'NT/00016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0),
('QC/00017', 1, 'NT/00017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0),
('QC/00018', 1, 'NT/00018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_rk`
--

CREATE TABLE `t_rk` (
  `id_rk` varchar(10) NOT NULL,
  `id_so` varchar(30) NOT NULL,
  `nama_market` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_rpso`
--

CREATE TABLE `t_rpso` (
  `id_rpso` varchar(10) NOT NULL,
  `tanggal_awal` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `location_id` int(11) NOT NULL,
  `id_kpso` varchar(10) NOT NULL,
  `id_ba` varchar(10) NOT NULL,
  `no_rpso` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_rpso`
--

INSERT INTO `t_rpso` (`id_rpso`, `tanggal_awal`, `tanggal_akhir`, `location_id`, `id_kpso`, `id_ba`, `no_rpso`, `status`, `deleted`) VALUES
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00001', '', 0, 1, 0),
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00001', '', 0, 1, 0),
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00001', '', 0, 1, 0),
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00002', '', 0, 1, 0),
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0001', '2017-06-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00001', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00001', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00001', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00002', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0002', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00001', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00001', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00001', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00002', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0003', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00001', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00001', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00001', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00002', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0004', '2017-01-01', '2017-06-30', 1, 'SO00003', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00001', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00001', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00001', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00002', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00003', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00003', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00003', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00003', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00003', '', 0, 1, 0),
('RSO0005', '2017-06-01', '2017-06-24', 1, 'SO00003', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00001', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00001', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00001', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00002', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0),
('RSO0006', '2017-01-01', '2017-06-16', 1, 'SO00003', '', 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_sj`
--

CREATE TABLE `t_sj` (
  `id_sj` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `id_do` varchar(20) NOT NULL,
  `nopol` varchar(11) NOT NULL,
  `dikirim` varchar(20) NOT NULL,
  `total` double NOT NULL,
  `tujuan` varchar(30) NOT NULL,
  `jb` varchar(30) NOT NULL,
  `dari` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `location_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_sj`
--

INSERT INTO `t_sj` (`id_sj`, `tanggal`, `customer_id`, `id_do`, `nopol`, `dikirim`, `total`, `tujuan`, `jb`, `dari`, `status`, `deleted`, `location_id`) VALUES
('sjp/00001', '2017-06-15', 1, 'do/00001', '', '', 0, 'Toko', 'Produk', 'pembelian', 1, 0, 1),
('sjp/00002', '2017-06-15', 1, 'do/00002', '', '', 0, 'Toko', 'Produk', 'hasil jadi', 0, 0, 1),
('sjp/00003', '2017-06-15', 1, 'do/00002', '', '', 0, 'Toko', 'Produk', 'pembelian', 1, 0, 1),
('sjp/00004', '2017-06-15', 1, 'do/00001', '', '', 0, 'Toko', 'Barang Umum', 'pembelian', 0, 0, 1),
('sjp/00005', '2017-06-16', 1, 'do/00001', '', '', 0, 'Toko', 'Produk', 'hasil jadi', 1, 0, 1),
('sjp/00006', '2017-07-04', 1, 'do/00003', '', '', 0, 'Toko', 'Produk', 'pembelian', 1, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_sjm`
--

CREATE TABLE `t_sjm` (
  `id_sjm` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `location_id` int(11) NOT NULL,
  `tujuan` varchar(25) NOT NULL,
  `jb` varchar(20) NOT NULL,
  `deleted` int(11) NOT NULL,
  `id_bpb` varchar(255) NOT NULL,
  `dari` varchar(25) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_sjm`
--

INSERT INTO `t_sjm` (`id_sjm`, `tanggal`, `location_id`, `tujuan`, `jb`, `deleted`, `id_bpb`, `dari`, `status`) VALUES
('SJM/00001', '2017-04-20', 1, 'Gudang Romokalisari', 'Barang Umum', 0, 'BPB00001', 'hasil jadi', 2),
('SJM/00002', '2017-04-20', 1, 'Gudang Romokalisari', 'Produk', 0, 'BPB00002', 'hasil jadi', 2),
('SJM/00003', '2017-04-20', 1, 'Toko', 'Produk', 0, 'BPB00007', 'hasil jadi', 2),
('SJM/00004', '2017-04-24', 1, 'Gudang Romokalisari', 'Barang Umum', 0, 'BPB00001', 'pembelian', 0),
('SJM/00005', '2017-04-26', 1, 'Gudang Romokalisari', 'Barang Umum', 0, 'BPB00001', 'pembelian', 2),
('SJM/00006', '2017-04-29', 1, 'Toko', 'Produk', 0, 'BPB00007', 'hasil jadi', 0),
('SJM/00007', '2017-04-29', 1, 'Toko', 'Barang Umum', 0, 'BPB00007', 'hasil jadi', 2),
('SJM/00008', '2017-04-29', 1, 'Toko', 'Produk', 0, 'BPB00007', 'pembelian', 2),
('SJM/00009', '2017-04-29', 1, 'Toko', 'Produk', 0, 'BPB00007', 'pembelian', 2),
('SJM/00010', '2017-04-29', 1, 'Toko', 'Produk', 0, 'BPB00007', 'pembelian', 2),
('SJM/00011', '2017-04-29', 1, 'Toko', 'Produk', 0, 'BPB00007', 'pembelian', 2),
('SJM/00012', '2017-06-15', 1, 'Gudang Romokalisari', 'Barang Umum', 0, 'BPB00001', 'pembelian', 1),
('SJM/00013', '2017-06-16', 1, 'Gudang Romokalisari', 'Produk Utama', 0, 'BPB00010', 'pembelian', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_skj`
--

CREATE TABLE `t_skj` (
  `no_kontrak` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type_kontrak` varchar(35) NOT NULL,
  `total` double NOT NULL,
  `ppn` double NOT NULL,
  `gt` double NOT NULL,
  `syarat_pembayaran` varchar(255) NOT NULL,
  `syarat_pengiriman` varchar(30) NOT NULL,
  `ketentuan` varchar(30) NOT NULL,
  `tanggal_deal_kurs` date NOT NULL,
  `nilai_kurs` double NOT NULL,
  `tanggal_ph` date NOT NULL,
  `harga_ditetapkan` double NOT NULL,
  `nilai_diff` double NOT NULL,
  `tanggal_hedging` date NOT NULL,
  `nilai_kurs_hedging` double NOT NULL,
  `deleted` int(11) NOT NULL,
  `custom1` varchar(30) NOT NULL,
  `tgl_jt` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_skj`
--

INSERT INTO `t_skj` (`no_kontrak`, `tanggal`, `customer_id`, `type_kontrak`, `total`, `ppn`, `gt`, `syarat_pembayaran`, `syarat_pengiriman`, `ketentuan`, `tanggal_deal_kurs`, `nilai_kurs`, `tanggal_ph`, `harga_ditetapkan`, `nilai_diff`, `tanggal_hedging`, `nilai_kurs_hedging`, `deleted`, `custom1`, `tgl_jt`, `status`) VALUES
('SK00001', '2017-06-15', 1, 'Kontrak Lokal', 0, 0, 132, 'tunai', 'loco', 'ok', '0000-00-00', 0, '0000-00-00', 0, 0, '0000-00-00', 0, 0, '', '0000-00-00', 1),
('SK00002', '2017-07-10', 1, 'Kontrak Lokal', 0, 0, 2.226, 'tunai', 'loco', 'secepatnya', '0000-00-00', 0, '0000-00-00', 0, 0, '0000-00-00', 0, 0, '', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `No_User` int(11) NOT NULL,
  `id_office` int(11) NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `hak_akses` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `bagian` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `id_session` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `location_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`No_User`, `id_office`, `username`, `password`, `nama_lengkap`, `email`, `hak_akses`, `level`, `bagian`, `blokir`, `id_session`, `location_id`) VALUES
(1, 0, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Ony Prabowo', 'ony.prabowo@jtanzilco.com', '1', 'admin', '1', 'N', '2m5ollppnqqv37rfqg6atsdf91', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_config`
--
ALTER TABLE `app_config`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id_city`);

--
-- Indexes for table `coa`
--
ALTER TABLE `coa`
  ADD PRIMARY KEY (`id_coa`);

--
-- Indexes for table `coa_classification`
--
ALTER TABLE `coa_classification`
  ADD PRIMARY KEY (`id_coa_classification`);

--
-- Indexes for table `coa_type`
--
ALTER TABLE `coa_type`
  ADD PRIMARY KEY (`id_coa_type`);

--
-- Indexes for table `count_paper`
--
ALTER TABLE `count_paper`
  ADD PRIMARY KEY (`id_count_paper`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id_currency`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `customers_copy`
--
ALTER TABLE `customers_copy`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `detail_bapj`
--
ALTER TABLE `detail_bapj`
  ADD PRIMARY KEY (`id_detail_bapj`);

--
-- Indexes for table `detail_bpb`
--
ALTER TABLE `detail_bpb`
  ADD PRIMARY KEY (`id_detail_bpb`),
  ADD KEY `id_bpb` (`id_bpb`,`item_id`),
  ADD KEY `item_fk3` (`item_id`);

--
-- Indexes for table `detail_cde`
--
ALTER TABLE `detail_cde`
  ADD PRIMARY KEY (`id_detail_cde`),
  ADD KEY `no_kontrak` (`no_kontrak`);

--
-- Indexes for table `detail_do`
--
ALTER TABLE `detail_do`
  ADD PRIMARY KEY (`id_detail_do`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `id_do` (`id_do`);

--
-- Indexes for table `detail_dpp`
--
ALTER TABLE `detail_dpp`
  ADD PRIMARY KEY (`id_detail_dpp`);

--
-- Indexes for table `detail_dpph`
--
ALTER TABLE `detail_dpph`
  ADD PRIMARY KEY (`id_detail_dpph`);

--
-- Indexes for table `detail_kpso`
--
ALTER TABLE `detail_kpso`
  ADD PRIMARY KEY (`id_detail_kpso`),
  ADD KEY `id_kpso` (`id_kpso`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `detail_memo_rp`
--
ALTER TABLE `detail_memo_rp`
  ADD PRIMARY KEY (`id_detail_memo_rp`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `detail_np`
--
ALTER TABLE `detail_np`
  ADD PRIMARY KEY (`id_detail_np`),
  ADD KEY `id_np` (`id_np`,`item_id`);

--
-- Indexes for table `detail_npum`
--
ALTER TABLE `detail_npum`
  ADD PRIMARY KEY (`id_detail_npum`),
  ADD KEY `id_detail_npum` (`id_detail_npum`,`id_npum`,`item_id`);

--
-- Indexes for table `detail_nrp`
--
ALTER TABLE `detail_nrp`
  ADD PRIMARY KEY (`id_detail_nrp`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `detail_ph`
--
ALTER TABLE `detail_ph`
  ADD KEY `id_ph` (`id_ph`),
  ADD KEY `item_id` (`item_id`) USING BTREE;

--
-- Indexes for table `detail_phb`
--
ALTER TABLE `detail_phb`
  ADD PRIMARY KEY (`id_detail_phb`),
  ADD KEY `id_phb` (`id_phb`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `detail_psb`
--
ALTER TABLE `detail_psb`
  ADD PRIMARY KEY (`id_detail_psb`,`syarat_pembayaran`),
  ADD KEY `supplier_id` (`id_psb`) USING BTREE;

--
-- Indexes for table `detail_rk`
--
ALTER TABLE `detail_rk`
  ADD PRIMARY KEY (`id_detail_rk`),
  ADD KEY `id_rk` (`id_rk`,`customer_id`),
  ADD KEY `id_so` (`id_so`);

--
-- Indexes for table `detail_rpso`
--
ALTER TABLE `detail_rpso`
  ADD PRIMARY KEY (`id_detail_rpso`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `detail_sj`
--
ALTER TABLE `detail_sj`
  ADD PRIMARY KEY (`id_detail_sj`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `id_sj` (`id_sj`);

--
-- Indexes for table `detail_sjm`
--
ALTER TABLE `detail_sjm`
  ADD PRIMARY KEY (`id_detail_sjm`),
  ADD KEY `id_sjm` (`id_sjm`,`item_id`),
  ADD KEY `item_fk4` (`item_id`);

--
-- Indexes for table `detail_skj`
--
ALTER TABLE `detail_skj`
  ADD PRIMARY KEY (`id_detail_skj`),
  ADD KEY `no_kontrak` (`no_kontrak`,`item_id`);

--
-- Indexes for table `detail_spp`
--
ALTER TABLE `detail_spp`
  ADD PRIMARY KEY (`id_detail_spp`);

--
-- Indexes for table `detail_suppliers`
--
ALTER TABLE `detail_suppliers`
  ADD PRIMARY KEY (`id_detail_suppliers`),
  ADD KEY `supplier_id` (`supplier_id`,`item_id`),
  ADD KEY `ngitem_fk` (`item_id`);

--
-- Indexes for table `detail_suppliers_copy`
--
ALTER TABLE `detail_suppliers_copy`
  ADD PRIMARY KEY (`id_detail_suppliers`),
  ADD KEY `supplier_id` (`supplier_id`,`item_id`),
  ADD KEY `ngitem_fk` (`item_id`);

--
-- Indexes for table `dn`
--
ALTER TABLE `dn`
  ADD PRIMARY KEY (`id_dn`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `giftcards`
--
ALTER TABLE `giftcards`
  ADD PRIMARY KEY (`giftcard_id`),
  ADD UNIQUE KEY `giftcard_number` (`giftcard_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `grants`
--
ALTER TABLE `grants`
  ADD PRIMARY KEY (`permission_id`,`person_id`),
  ADD KEY `grants_ibfk_2` (`person_id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id_home`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `trans_items` (`trans_items`),
  ADD KEY `trans_user` (`trans_user`),
  ADD KEY `trans_location` (`trans_location`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `item_number` (`item_number`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `items_taxes`
--
ALTER TABLE `items_taxes`
  ADD PRIMARY KEY (`item_id`,`name`,`percent`);

--
-- Indexes for table `item_kits`
--
ALTER TABLE `item_kits`
  ADD PRIMARY KEY (`item_kit_id`);

--
-- Indexes for table `item_kit_items`
--
ALTER TABLE `item_kit_items`
  ADD PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  ADD KEY `item_kit_items_ibfk_2` (`item_id`);

--
-- Indexes for table `item_quantities`
--
ALTER TABLE `item_quantities`
  ADD PRIMARY KEY (`item_id`,`location_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `lpb`
--
ALTER TABLE `lpb`
  ADD PRIMARY KEY (`id_lpb`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`),
  ADD UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  ADD UNIQUE KEY `name_lang_key` (`name_lang_key`);

--
-- Indexes for table `mr`
--
ALTER TABLE `mr`
  ADD PRIMARY KEY (`id_mr`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `permissions_ibfk_2` (`location_id`);

--
-- Indexes for table `po`
--
ALTER TABLE `po`
  ADD PRIMARY KEY (`id_po`),
  ADD KEY `id_spp` (`id_spp`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `custom4` (`custom4`);

--
-- Indexes for table `receivings`
--
ALTER TABLE `receivings`
  ADD PRIMARY KEY (`receiving_id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `receivings_items`
--
ALTER TABLE `receivings_items`
  ADD PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`sale_id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `sale_time` (`sale_time`);

--
-- Indexes for table `sales_items`
--
ALTER TABLE `sales_items`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `item_location` (`item_location`);

--
-- Indexes for table `sales_items_taxes`
--
ALTER TABLE `sales_items_taxes`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `sales_payments`
--
ALTER TABLE `sales_payments`
  ADD PRIMARY KEY (`sale_id`,`payment_type`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indexes for table `sales_suspended`
--
ALTER TABLE `sales_suspended`
  ADD PRIMARY KEY (`sale_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `sales_suspended_items`
--
ALTER TABLE `sales_suspended_items`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `sales_suspended_items_ibfk_3` (`item_location`);

--
-- Indexes for table `sales_suspended_items_taxes`
--
ALTER TABLE `sales_suspended_items_taxes`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `sales_suspended_payments`
--
ALTER TABLE `sales_suspended_payments`
  ADD PRIMARY KEY (`sale_id`,`payment_type`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sjr`
--
ALTER TABLE `sjr`
  ADD PRIMARY KEY (`id_sjr`);

--
-- Indexes for table `so`
--
ALTER TABLE `so`
  ADD PRIMARY KEY (`id_so`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `spp`
--
ALTER TABLE `spp`
  ADD PRIMARY KEY (`id_spp`);

--
-- Indexes for table `stock_locations`
--
ALTER TABLE `stock_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `id_city` (`id_city`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`supplier_id`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `detail_psb` (`id_detail_psb`);

--
-- Indexes for table `suppliers_copy`
--
ALTER TABLE `suppliers_copy`
  ADD PRIMARY KEY (`supplier_id`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `detail_psb` (`id_detail_psb`);

--
-- Indexes for table `tb_skb2`
--
ALTER TABLE `tb_skb2`
  ADD PRIMARY KEY (`id_skb`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `id_home` (`id_home`);

--
-- Indexes for table `ttf`
--
ALTER TABLE `ttf`
  ADD PRIMARY KEY (`id_ttf`),
  ADD KEY `supplier_id` (`supplier_id`,`person_id`),
  ADD KEY `person_fk` (`person_id`);

--
-- Indexes for table `t_categories`
--
ALTER TABLE `t_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `t_pl`
--
ALTER TABLE `t_pl`
  ADD PRIMARY KEY (`no_pl`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `detail_bapj`
--
ALTER TABLE `detail_bapj`
  MODIFY `id_detail_bapj` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `detail_bpb`
--
ALTER TABLE `detail_bpb`
  MODIFY `id_detail_bpb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `detail_do`
--
ALTER TABLE `detail_do`
  MODIFY `id_detail_do` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `detail_dpp`
--
ALTER TABLE `detail_dpp`
  MODIFY `id_detail_dpp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `detail_dpph`
--
ALTER TABLE `detail_dpph`
  MODIFY `id_detail_dpph` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `detail_kpso`
--
ALTER TABLE `detail_kpso`
  MODIFY `id_detail_kpso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `detail_memo_rp`
--
ALTER TABLE `detail_memo_rp`
  MODIFY `id_detail_memo_rp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `detail_np`
--
ALTER TABLE `detail_np`
  MODIFY `id_detail_np` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `detail_npum`
--
ALTER TABLE `detail_npum`
  MODIFY `id_detail_npum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `detail_nrp`
--
ALTER TABLE `detail_nrp`
  MODIFY `id_detail_nrp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `detail_phb`
--
ALTER TABLE `detail_phb`
  MODIFY `id_detail_phb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `detail_psb`
--
ALTER TABLE `detail_psb`
  MODIFY `id_detail_psb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `detail_rpso`
--
ALTER TABLE `detail_rpso`
  MODIFY `id_detail_rpso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `detail_sj`
--
ALTER TABLE `detail_sj`
  MODIFY `id_detail_sj` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `detail_sjm`
--
ALTER TABLE `detail_sjm`
  MODIFY `id_detail_sjm` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `detail_skj`
--
ALTER TABLE `detail_skj`
  MODIFY `id_detail_skj` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `detail_spp`
--
ALTER TABLE `detail_spp`
  MODIFY `id_detail_spp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `detail_suppliers`
--
ALTER TABLE `detail_suppliers`
  MODIFY `id_detail_suppliers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `t_categories`
--
ALTER TABLE `t_categories`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `stock_locations`
--
ALTER TABLE `stock_locations`
  ADD CONSTRAINT `kota_fk` FOREIGN KEY (`id_city`) REFERENCES `city` (`id_city`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
