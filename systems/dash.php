<?php
ob_start();
session_start();
error_reporting(0);
include "../auth/koneksi.php";
date_default_timezone_set('Asia/Jakarta');
$page=@$_GET['module'];
$NoUser     = $_SESSION['No_User'];
if (empty($_SESSION['namauser']) AND empty($_SESSION['level'])){
  echo "<link href='style.css' rel='stylesheet' type='text/css'>
 <center>Untuk mengakses modul, Anda harus login <br>";
  echo "<a href=../index.php><b>LOGIN</b></a></center>";
}
else{
	function dateBahasaIndo($date){
		$bulan=array(
					'01'=>'Januari',
					'02'=>'Februari',
					'03'=>'Maret',
					'04'=>'April',
					'05'=>'Mei',
					'06'=>'Juni',
					'07'=>'Juli',
					'08'=>'Agustus',
					'09'=>'September',
					'10'=>'Oktober',
					'11'=>'November',
					'12'=>'Desember',
					 );
		$pecah=explode('-',$date);

		$tgl_pecah=$pecah[2];
		$bln_pecah=$pecah[1];
		$thn_pecah=$pecah[0];
		return $tgl_pecah.' '.$bulan[$bln_pecah].' '.$thn_pecah;
		}
		
	Class Terbilang {
	function terbilang() {
		$this->dasar = array(1=>'SATU','DUA','TIGA','EMPAT','LIMA','ENAM','TUJUH','DELAPAN','SEMBILAN');
		$this->angka = array(1000000000,1000000,1000,100,10,1);
		$this->satuan = array('MILYAR','JUTA','RIBU','RATUS','PULUH','');
	}
	function eja($n) {
		$i=0;
		$str="";
		while($n!=0)
		{
			$count = (int)($n/$this->angka[$i]);
			if($count>=10) $str.= $this->eja($count). "".$this->satuan[$i]."";
			else if($count > 0 && $count < 10)
			$str .=" ".$this->dasar[$count]. " ".$this->satuan[$i]." ";
			$n -= $this->angka[$i] * $count;
			$i++;
		}
		$str = preg_replace("/SATU PULUH (\w+)/i"," \\1 BELAS ",$str);
		$str = preg_replace("/SATU (RIBU|RATUS|PULUH|BELAS)/i"," SE\\1 ",$str);
		return $str;
		}
	}

$bilangan = new Terbilang;
?>
<!doctype html>
<html>
<head>
<?php
 include "heading.php";
?>
<style type="text/css">
	body .modal-besar {
    /* new custom width */
    width: 80%;
    /* must be half of the width, minus scrollbar on the left (30px) */
    margin-left: -560px;
}

</style>
<style type="text/css">
 .scroll {
    /*background-color: #00FFFF;*/
    width: 100%;
    height: 400px;
    overflow-x: auto;
}
</style>		
</head>

<body data-layout-sidebar="fixed" data-layout-topbar="fixed">	
	<div id="navigation">
		<div class="container-fluid nav-hidden">
			<a href="#" id="brand">NSIM</a>
			<a href="#" class="toggle-nav" rel="tooltip" data-placement="bottom" title="Toggle navigation"><i class="icon-reorder"></i></a>
			
			<?php
			include "navbar.php";
			include "headuser.php";
			?>
			
		</div>
	</div>
	
	<!-- CONTENT -->
	
	<div class="container-fluid nav-hidden" id="content">
		<div id="main" style="margin-left: 0px;">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h1>NSIM (Sistem Informasi Manajemen)</h1>
					</div>
					<div class="pull-right">
						<ul class="minitiles">
							<li class='grey'>
								<a href="#"><i class="icon-cogs"></i></a>
							</li>
							<li class='lightgrey'>
								<a href="#"><i class="icon-globe"></i></a>
							</li>
						</ul>
						<ul class="stats">
							<li class='satgreen'>
								<i class="icon-money"></i>
								<div class="details">
									<span class="big">$1 = </span>
									<span>Balance</span>
								</div>
							</li>
							<li class='lightred'>
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">February 22, 2013</span>
									<span>Wednesday, 13:56</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<?php
				include "main.php"; 
				?>
						
			</div>
		</div>
	</div>
</body>
</html>
<?php } ?>