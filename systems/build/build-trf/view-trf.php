<?php
	$tglsekarang	= date('d-m-Y');
	$hp				= $_GET['hp'];
	
	if($hp=='penerimaan')
	{
		$judulhp = "Penerimaan";
	}
	else
	{
		$judulhp = "Pengeluaran";
	}
?>			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Keuangan</a>
						</li>
						<li>
							<a href="dash.php?hp=spp1&navbar=spp&parent=keuangan"><?php echo $judulhp; ?> Keuangan</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Transaksi <?php echo $judulhp; ?> Keuangan
								</h3>
							</div>
							<div class="box-content">
								<ul class="stats">
									<li class='green'>
										<i class="icon-pencil"></i>
										<div class="details">
											<span>Jurnal Umum</span>
										</div>
									</li>
									<li class='blue' id="kas-bank" data-id="<?php echo $hp; ?>" data-id2="<?php echo $_SESSION[No_User]; ?> " role="button" data-toggle="modal">
										<i class="icon-money"></i>
										<div class="details">
											<span>Kas & Bank</span>
										</div>
									</li>
									<?php
									if($hp=='penerimaan')
									{
										
									}
									else
									{
									?>
									<li class='pink'>
										<i class="icon-money"></i>
										<div class="details">
											<a href='#myModal' role="button" data-toggle="modal"><span>Bon Sementara</span></a>
										</div>
									</li>
									<?php
									}
									?>									
								</ul>
							</div>
						</div>
					</div>
				</div>
	<?php
		if($hp=='penerimaan')
		{
	?>
		<!-- PENERIMAAN KEUANGAN -->
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Daftar Transaksi Penerimaan Keuangan
								</h3>
							</div>
							<div class="box-content">
								<ul class="stats">
									<li class='orange'>
										<i class="icon-money"></i>
										<div class="details">
											<a href="dash.php?hp=bkm&navbar=pengeluaran&parent=keuangan"><span>Bukti Kas Masuk</span></a>
										</div>
									</li>
									<li class='purple'>
										<i class="icon-book"></i>
										<div class="details">
											<a href="dash.php?hp=bbm&navbar=pengeluaran&parent=keuangan"><span>Bukti Bank Masuk</span></a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
	<?php 
		}
		else
		{
	?>
		<!-- PENGELUARAN KEUANGAN -->
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Daftar Transaksi Pengeluaran Keuangan
								</h3>
							</div>
							<div class="box-content">
								<ul class="stats">
									<li class='orange'>
										<i class="icon-money"></i>
										<div class="details">
											<a href="dash.php?hp=bkk&navbar=pengeluaran&parent=keuangan"><span>Bukti Kas Keluar</span></a>
										</div>
									</li>
									<li class='purple' >
										<i class="icon-book"></i>
										<div class="details" >
											<a href="dash.php?hp=bbk&navbar=pengeluaran&parent=keuangan"><span>Bukti Bank Keluar</span></a>
										</div>
									</li>
									<li class='purple' >
										<i class="icon-book"></i>
										<div class="details" >
											<a href="dash.php?hp=dbs&navbar=pengeluaran&parent=keuangan"><span>Daftar Bon Sementara</span></a>
										</div>
									</li>
									<li class='purple' >
										<i class="icon-book"></i>
										<div class="details" >
											<a href="dash.php?hp=rbs&navbar=pengeluaran&parent=keuangan"><span>Register Bon Sementara</span></a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
	<?php
		}
	?>
		
				
	<script src="js/jquery.js"></script>
	<script>
		$(function(){
			$(document).on('click','#kas-bank',function(e){
				e.preventDefault();
				$("#view").modal('show');
				$.post('build/build-trf/form-edit.php',
					{id:$(this).attr('data-id'), id2:$(this).attr('data-id2')},
					function(html){
						$(".modal-body2").html(html);
					}  
				);
			});
		});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Transaksi <?php echo $judulhp; ?> Keuangan</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="dash.php?hp=bs1&navbar=pengeluaran&parent=keuangan" method="POST" class="form-horizontal" >
					<input type="hidden" name="id_home" class="form-control" value="<?php echo $homes; ?>">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Bon Sementara</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Perusahaan</label>
						  <div class="controls">
							<table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="perusahaan" id="select" class='chosen-select' >
									<option value="">--Perusahaan--</option>
									<?php 	
									$tampil=mysql_query("SELECT * from home");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_home']; ?>"><?php echo $row['nama']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="jenis" id="select2" class='chosen-select' required>
									<option value="">--Pilih Jenis--</option>
									<option value="KAS">KAS</option>
									<option value="BANK">BANK</option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Pemohon</label>
						  <div class="controls">
							<input type="text" name="pemohon" class="form-control" placeholder="Nama Pemohon" autocomplete="off" required>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jumlah</label>
						  <div class="controls">
							<input type="text" name="jumlah" class="form-control" placeholder="Isikan Jumlah" data-rule-email="true" autocomplete="off" required>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Keperluan</label>
						  <div class="controls">
							<textarea name="keperluan" id="keperluan" cols="30" rows="3" required></textarea></th>
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div>
              </div>
            </div>