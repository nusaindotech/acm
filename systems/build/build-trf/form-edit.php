	<?php 
		include "../../../auth/autho.php";
		$id		= $_POST['id'];
		$id2	= $_POST['id2'];
	?>
	<script src="jquery.min.js"></script>
	<script>
        //<![CDATA[
        $(window).load(function(){
        $("#mata_uang").on("change", function(){
            var nilai = $("#mata_uang :selected").attr("data-kurs");
            $("#nilai_kurs").val(nilai);
        });
        });//]]>
    </script>
		
	<div class="modal-body">
		<form role="form" action="dash.php?hp=trf1&navbar=<?php echo $id; ?>&parent=keuangan" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped'>
			<input type="hidden" value="<?php echo $id;?>" name="id">
			<input type="hidden" value="<?php echo $id2;?>" name="id2">
			<div class="control-group">
				<label for="textfield" class="control-label">Perusahaan</label>
				<div class="controls">
					<select name="perusahaan" class='chosen-select' required=="required">
						<option value="">--Pilih Perusahaan--</option>
						<?php 	
							$tampil=mysql_query("select * from home");
							$no=1;
							while ($row=mysql_fetch_array($tampil))
							{
						?>
							<option value="<?php echo $row['id_home']; ?>"><?php echo $row['nama']; ?></option>
						<?php 
							$no++;
						} 
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Jenis Transaksi</label>
				<div class="controls">
					<select name="jenistransaksi" class='chosen-select' required=="required">
						<option value="">--Jenis Transaksi--</option>
						<?php
						if($id=='penerimaan')
						{
						?>
						<option value="BKM">Kas</option>
						<option value="BBM">Bank</option>
						<?php	
						}
						else
						{
						?>
						<option value="BKK">Kas</option>
						<option value="BBK">Bank</option>	
						<?php
						}
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Keperluan Transaksi</label>
				<div class="controls">
					<select name="keperluantransaksi" class='chosen-select' required=="required">
						<option value="">--Keperluan Transaksi--</option>
						<?php
						if($id=='penerimaan')
						{
						?>
						<option value="UMPJ">Uang Muka Penjualan</option>
						<option value="PMB">Piutang Mitra Bisnis</option>
						<option value="LNPM">Lainnya</option>
						<?php	
						}
						else
						{
						?>
						<option value="UMPB">Uang Muka Pembelian</option>
						<option value="HMB">Hutang Mitra Bisnis</option>
						<option value="LNPL">Lainnya</option>
						<?php
						}
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Mata Uang</label>
				<div class="controls">
					<select name="mata_uang" id="mata_uang" class='chosen-select' required=="required">
						<option value="">--Mata Uang--</option>
						<?php 	
							$tampil=mysql_query("select * from currency");
							$no=1;
							while ($row=mysql_fetch_array($tampil))
							{
						?>
							<option value="<?php echo $row['id_currency']; ?>" data-kurs="<?php echo $row['value_currency']; ?>"><?php echo $row['name_currency']; ?></option>
						<?php 
							$no++;
						} 
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nilai Kurs</label>
				<div class="controls">
					<input type="text" name="nilai_kurs" id="nilai_kurs" placeholder="Nilai Kurs" class="input-xlarge" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label"><?php if($id=='penerimaan') { echo "Diterima"; } else { echo "Dibayarkan"; } ?></label>
				<div class="controls">
					<input type="text" name="person" id="textfield" placeholder="<?php if($id=='penerimaan') { echo "Diterima Dar"; } else { echo "Dibayarkan Kepada"; } ?>" class="input-xlarge" autocomplete="off">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Buat Transaksi</button>
			</div>
        </form>
	</div>