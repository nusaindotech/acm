<?php
	$tglsekarang2	= date('d-m-Y');
	$tglsekarang	= date('Y-m-d');
	$hp				= $_GET['hp'];
	$judulhp = "Cek Giro";
?>			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Keuangan</a>
						</li>
						<li>
							<a href="dash.php?hp=spp1&navbar=spp&parent=keuangan"><?php echo $judulhp; ?> Keuangan</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Transaksi <?php echo $judulhp; ?> Keuangan
								</h3>
							</div>
							<div class="box-content">
								<ul class="stats">
									<li class='green'>
									<a href='#myModal' role="button" data-toggle="modal">
										<i class="icon-money"></i>
										<div class="details">
											<span>Penerimaan Cek Giro</span>
										</div>
									</a>
									</li>
									<li class='blue'>
									<a href=''>
										<i class="icon-money"></i>
										<div class="details">
											<span>Pembukaan Cek Giro</span>
										</div>
									</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
		<!-- Daftar Cek Giro-->
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-th-large"></i>
									Daftar Transaksi Giro
								</h3>
							</div>
							<div class="box-content">
								<ul class="stats">
									<li class='orange'>
										<i class="icon-money"></i>
										<div class="details">
											<a href="dash.php?hp=bkk&navbar=pengeluaran&parent=keuangan"><span>Daftar Bukti Giro Masuk</span></a>
										</div>
									</li>
									<li class='purple' >
										<i class="icon-book"></i>
										<div class="details" >
											<a href="dash.php?hp=bbk&navbar=pengeluaran&parent=keuangan"><span>Daftar Bukti Giro Keluar</span></a>
										</div>
									</li>
									<li class='purple' >
										<i class="icon-book"></i>
										<div class="details" >
											<a href="dash.php?hp=dbs&navbar=pengeluaran&parent=keuangan"><span>Daftar CH/GB</span></a>
										</div>
									</li>
									<li class='purple' >
										<i class="icon-book"></i>
										<div class="details" >
											<a href="dash.php?hp=rbs&navbar=pengeluaran&parent=keuangan"><span>Register CH/GB</span></a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
					
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="dash.php?hp=chgb1&navbar=pengeluaran&parent=keuangan" method="POST" class="form-horizontal" >
					<input type="hidden" name="jenis" class="form-control" value="IN">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Penerimaan CHGB</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
					  <div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="hidden" name="No_User" class="form-control" value="<?php echo $_SESSION[No_User]; ?>" readonly>
							<input type="text" name="tanggal2" class="form-control" value="<?php echo $tglsekarang2; ?>" readonly>
							<input type="hidden" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Perusahaan</label>
						  <div class="controls">
							<table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="perusahaan" id="select" class='chosen-select' required>
									<option value="">--Perusahaan--</option>
									<?php 	
									$tampil=mysql_query("SELECT * from home");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_home']; ?>"><?php echo $row['nama']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Pelanggan</label>
						  <div class="controls">
							<table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="pelanggan" id="select2" class='chosen-select' required>
									<option value="">--Pelanggan--</option>
									<?php 	
									$tampil=mysql_query("SELECT customer_id, company_code, company_name from customers");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['customer_id']; ?>"><?php echo $row['company_code'] . " | " . $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Bank</label>
						  <div class="controls">
							<input type="text" name="bank" class="form-control" placeholder="Nama Bank" autocomplete="off" required>
						  </div>
						</div>	
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nomor CH/GB</label>
						  <div class="controls">
							<input type="text" name="nomor" class="form-control" placeholder="Nomor CH/GB" autocomplete="off" required>
						  </div>
						</div>	
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jatuh Tempo</label>
						  <div class="controls">
							<input type="text" name="jatuh_tempo" class="form-control datepick" placeholder="jatuh_tempo" autocomplete="off" required="=&quot;required&quot;" id="jatuh_tempo" required/>
						  </div>
						</div>	
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nominal</label>
						  <div class="controls">
							<input type="text" name="nominal" class="form-control" placeholder="Nominal" autocomplete="off" required>
						  </div>
						</div>	
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Dari</label>
						  <div class="controls">
							<input type="text" name="oleh" class="form-control" placeholder="Diterima Dari" autocomplete="off" required>
						  </div>
						</div>						
					  </div>
					  <br><br><br><br><br><br><br>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div>
              </div>
            </div>