			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-suppliers/insert-suppliers.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Simpan Data Supplier ?');">
					<div class="modal-content">
					  
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						
					  </div>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=supplier&navbar=supplier&parent=master">Supplier</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						   
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Supplier
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Date In</th>
											<th>Kode Supplier</th>
											<th>Nama Supplier</th>
											<th>Alamat</th>
											<th>Telepon</th>
											<th>Nama Barang</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("SELECT a.company_code, a.supplier_id, a.company_name,a.company_address,a.company_phone,a.date_in,c.`name`,b.item_id,b.id_psb FROM suppliers AS a ,items AS c ,detail_psb AS b WHERE a.deleted = 0 AND a.id_psb = b.id_psb AND c.item_id = b.item_id GROUP BY a.supplier_id ");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo dateBahasaIndo($row['date_in']); ?></td>
											<td><?php echo $row['company_code']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['company_address']; ?></td>
											<td><?php echo $row['company_phone']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td class='hidden-350'><center>
											<a class="btn btn-primary" id="edit-supplier" data-id="<?php echo $row['supplier_id'];?>"><i class="icon-edit"></i></a>
											<?php
											if($row['acc']==0)
											{
											?>
											<a class="btn btn-danger" href="build/build-suppliers/delete-suppliers.php?idsupplier=<?php echo $row['supplier_id']; ?>"><i class="icon-trash"></i></a>
											<?php
											}
											else
											{
											}
											?>	
											<!-- <a class="btn btn-success" href="dash.php?hp=supplierproduk&navbar=supplierproduk&parent=master&idsupplier=<?php echo $row['supplier_id']; ?>"><i class="icon-tags"></i></a> --></center>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-supplier',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-suppliers/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Supplier</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>