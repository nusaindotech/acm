	<?php 
		include "../../../auth/autho.php";
		$id			= $_POST['id'];
		$query 		= mysql_query("select * from suppliers where supplier_id='$id'") or die(mysql_error());
		$data 		= mysql_fetch_array($query);
		
		$formattanggal	= date('d-m-Y',strtotime($data['date_in']));
	?>
	<div class="modal-body">
		<form role="form" action="build/build-suppliers/update-suppliers.php" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped'>
			<input type="hidden" value="<?php echo $id;?>" name="id_supplier">
			<div class="control-group">
				<label for="textfield" class="control-label">Tanggal Masuk</label>
				<div class="controls">
					<input type="text" name="tanggal" id="textfield" class="input-medium datepick" value="<?php echo $formattanggal;?>" readonly>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Kode Supplier</label>
				<div class="controls">
					<input type="text" id="textfield" class="input-xlarge" name="kodesupplier" placeholder="Isikan Kode Supplier" required="require" value="<?php echo $data['company_code'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama Supplier</label>
				<div class="controls">
					<input type="text" name="nama" id="textfield" placeholder="Nama Supplier" class="input-xlarge" required="require" value="<?php echo $data['company_name'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Alamat</label>
				<div class="controls">
					<input type="text" name="alamat" id="textfield" placeholder="Alamat Supplier" class="input-xlarge" value="<?php echo $data['company_address'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Telepon</label>
				<div class="controls">
					<input type="text" name="telepon" id="textfield" placeholder="Telepon Supplier" class="input-xlarge" value="<?php echo $data['company_phone'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Contact Person</label>
				<div class="controls">
					<input type="text" name="cp" id="textfield" placeholder="Contact Person" class="input-xlarge" value="<?php echo $data['contact_person'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">NPWP</label>
				<div class="controls">
					<input type="text" name="npwp" id="textfield" placeholder="NPWP Supplier" class="input-xlarge" value="<?php echo $data['npwp'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Syarat Pembayaran</label>
				<div class="controls">
					<input type="text" name="syarat" id="textfield" placeholder="Syarat Pembayaran" class="input-xlarge" value="<?php echo $data['provision'];?>" autocomplete="off">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>