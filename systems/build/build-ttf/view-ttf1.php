<?php
	$tglsekarang	= date('d-m-Y');
?>	

			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-ttf/insert-ttf1.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Tanda Terima Tagihan?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Tanda Terima Tagihan</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<!-- <div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
						  <div class="controls">
							<input type="text" name="kode" id="kode" class="form-control" id="supplier" placeholder="supplier" autocomplete="off" required=="required" readonly>
						  </div>
						</div> -->
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="supplier" id="supplier" class='chosen-select' required=="required">
									<option value="">--Pilih Supplier--</option>
									<?php 	
									$tampil=mysql_query("select * from suppliers
											where deleted=0 and acc=1
											order by company_name ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['supplier_id']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kategori Pembelian</label>
						  <div class="controls">
							<table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="kategori" id="kategori" class='chosen-select' required=="required" >
									<option value="">--Pilih Kategori Pembelian--</option>
									<option value="Barang Umum">Barang Umum</option>
									<option value="Aset">Aset</option>
									<option value="Jasa">Jasa</option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>
						</div>
						
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Tanda Terima Tagihan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Pembelian</a>
						</li>
						<li>
							<a href="dash.php?hp=ttf1&navbar=ttf&parent=pembelian">Tanda Terima Tagihan</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Tanda Terima Tagihan</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Tanda Terima Tagihan
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>No. Tanda Terima Tagihan</th>
											<th>Tanggal</th>
											<!-- <th>ID Supplier</th> -->
											<th>Nama Supplier</th>
											<th>Kategori Pembelian</th>
											<th>Status</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.id_ttf, a.date_transaction, b.company_code, b.company_name,a.kategori,a.status
									from ttf a, suppliers b
									where a.supplier_id=b.supplier_id and a.deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_ttf']; ?></td>
											<td><?php echo dateBahasaIndo($row['date_transaction']); ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['kategori']; ?></td>
											<td>
											<?php
											if($row['status']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else
											{
												echo "<span class='label label-success'>Diterima</span>";
											}
											?>
											</td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<li>
													<a id="edit-ttf" data-id="<?php echo $row['id_ttf'];?>" role="button" data-toggle="modal">Update Tanda Terima Tagihan</a>
												</li>
													<?php
												if($row['status']==0)
												{
													?>
													<?php
													if($row['kategori']=="Jasa"){
														?>
													<li>
														<a href="dash.php?hp=ttf2j&idbaru=<?php echo $row['id_ttf']; ?>&navbar=ttf&parent=pembelian">Revisi Tanda Terima Tagihan</a>
													</li>
													<?php
													}
													else{
														?>
														<li>
														<a href="dash.php?hp=ttf2&idbaru=<?php echo $row['id_ttf']; ?>&navbar=ttf&parent=pembelian">Revisi Tanda Terima Tagihan</a>
													</li>
													<?php
														}
													}
													else
													{
														?>
													<?php
													if($row['kategori']=="Jasa"){
														?>
													<li>
														<a href="dash.php?hp=ttf3j&idbaru=<?php echo $row['id_ttf']; ?>&navbar=ttf&parent=pembelian">Cetak Tanda Terima Tagihan</a>
													</li>
													<?php
														}
													else{
														?>
													<li>
														<a href="dash.php?hp=ttf3&idbaru=<?php echo $row['id_ttf']; ?>&navbar=ttf&parent=pembelian">Cetak Tanda Terima Tagihan</a>
													</li>
													<?php
														}
													}
													 ?>
													
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-ttf',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-ttf/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Update Tanda Terima Tagihan</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>