<?php
	$tglsekarang	= date('d-m-Y');
?>	
	<script>
            //<![CDATA[
            $(window).load(function(){
            $("#skb").on("change", function(){
                var nilai  = $("#skb :selected").attr("data-nama");
                var nilai2  = $("#skb :selected").attr("data-sup");
                // var nilai2  = $("#pb :selected").attr("data-supplier_id");
                $("#nama").val(nilai);
                $("#supplier").val(nilai2);
            });
            });//]]>
    </script>
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-nump/insert-nump.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Nota Uang Muka Pembelian?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Nota Uang Muka Pembelian</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kontrak</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="skb" id="skb" class='chosen-select' required=="required">
									<option value="">--Pilih Kontrak--</option>
									<?php 	
									$a="Produk Utama";
									$tampil=mysql_query("select a.id_skb,b.supplier_id,b.company_name from tb_skb a
INNER JOIN suppliers b on b.supplier_id = a.supplier_id
 where  a.deleted=0 order by id_skb DESC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_skb']; ?>" data-sup="<?php echo $row['supplier_id']; ?>" data-nama="<?php echo $row['company_name']; ?>" ><?php echo $row['id_skb']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
						  <div class="controls">
							<input type="text" name="nama" id="nama" class=" form-control"" placeholder="nama" autocomplete="off" required=="required" readonly>
						  </div>
						</div>
						<input type="hidden" name="supplier" id="supplier">
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Nota Uang Muka Pembelian</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Pembelian</a>
						</li>
						<li>
							<a href="dash.php?hp=nump&navbar=nump&parent=pembelian">Nota Uang Muka Pembelian</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Nota Uang Muka Pembelian</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Nota Uang Muka Pembelian
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable">
									<thead>
										<tr class='thefilter'>
											<th>No.</th>
											<th>No. Form</th>
											<th>Tanggal</th>
											<th>Nama Supplier</th>
											<th>Alamat Supplier</th>
											<th>Status</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.id_nump,a.tanggal,b.company_address,b.company_name,a.status from nump a,suppliers b where a.supplier_id=b.supplier_id and a.deleted=0 order by a.id_nump DESC ");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_nump']; ?></td>
											<td><?php echo dateBahasaIndo($row['tanggal']); ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['company_address']; ?></td>
											<td>
											<?php
											if($row['status']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else
											{
												echo "<span class='label label-success'>Selesai</span>";
											}
											?>
											</td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
													<?php
													if($row['status']==0 )
													{
													?>
													<li>
														<a href="dash.php?hp=nump2&idbaru=<?php echo $row['id_nump']; ?>&navbar=nump&parent=pembelian">Revisi Nota Uang Muka Pembelian</a>
													</li>
												
													<?php
																										}
													else
													{
														?>
													<li>
														<a href="dash.php?hp=nump3&idbaru=<?php echo $row['id_nump']; ?>&navbar=nump&parent=pembelian">Cetak Nota Uang Muka Pembelian</a>
													</li>
													<li>
														<a id="buat-np" data-id="<?php echo $row['id_nump'];?>" role="button" data-toggle="modal">Buat Nota Pembelian</a>
													</li>
													<?php
													}
													?>
													
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#buat-np',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-nump/form-editnp.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Buat Nota Pembelian</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
 