<?php
	$tglsekarang	= date('d-m-Y');
?>	

	<!-- <script>
        //<![CDATA[
        $(window).load(function(){
        $("#lpb").on("change", function(){
            var nilai  = $("#lpb :selected").attr("data-kategori");
            // var nilai2  = $("#lpb :selected").attr("data-op");
            $("#kategori").val(nilai);
            // $("#op").val(nilai2);
        });
        });//]]>
    </script> -->
	
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-mr/insert-mr1.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Memo Retur Pembelian ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Memo Retur Pembelian</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Kategori</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="kategori" id="kategori" class='chosen-select' required=="required" onchange="jp()">
									<option value="">--Pilih Kategori--</option>
									<option value="Barang Umum">Barang Umum</option>
									<option value="Aset Tetap">Aset Tetap</option>
									<option value="Produk Utama">Produk Utama</option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" id="LPB">
						  <label for="inputEmail3" class="col-sm-2 control-label">LPB</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="lpb" id="lpb" class='chosen-select' >
									<option value="">--Pilih Laporan Penerimaan Barang--</option>
									<?php 	
									$a="Barang Umum";
									$tampil=mysql_query("select DISTINCT(a.id_lpb),e.item_id, d.category, a.id_po, c.company_name from lpb a, po b, suppliers c, items d, detail_lpb e where b.supplier_id=c.supplier_id and d.item_id=e.item_id and a.id_lpb=e.id_lpb and a.id_po=b.id_po and b.custom1='$a' GROUP BY id_lpb  order by a.id_lpb DESC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_lpb']; ?>" data-kategori="<?php echo $row['category']; ?>" data-supplier="<?php echo $row['company_name']; ?>"><?php echo $row['id_lpb']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" id="LPB2">
						  <label for="inputEmail3" class="col-sm-2 control-label">LPB</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="lpb2" id="lpb2" class='chosen-select'>
									<option value="">--Pilih Laporan Penerimaan Barang--</option>
									<?php 	
									$a="Aset Tetap";
									$tampil=mysql_query("select DISTINCT(a.id_lpb),e.item_id, d.category, a.id_po, c.company_name from lpb a, po b, suppliers c, items d, detail_lpb e where b.supplier_id=c.supplier_id and d.item_id=e.item_id and a.id_lpb=e.id_lpb and a.id_po=b.id_po and b.custom1='$a' GROUP BY id_lpb  order by a.id_lpb DESC ");
									
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_lpb']; ?>" data-kategori="<?php echo $row['category']; ?>" data-supplier="<?php echo $row['company_name']; ?>"><?php echo $row['id_lpb']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<!-- <input type="hidden" name="kategori" id="kategori" class="form-control" autocomplete="off" required=="required" > -->

						<div class="control-group" id="NT">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Timbang</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="nt" id="nt" class='chosen-select'>
									<option value=" ">--Pilih No. Timbang--</option>
									<?php 	
									// $a="Produk Utama";
									$tampil=mysql_query("select * from ttr_main_scale where deleted=0 and jt='pembelian' order by id_timbang DESC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_timbang']; ?>" ><?php echo $row['id_timbang']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Pemohon</label>
						  <div class="controls">
							<input type="text" name="pemohon" class="form-control" placeholder="Pemohon" autocomplete="off" required=="required" value="<?=$_SESSION['nama_lengkap']?>" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Divisi</label>
						  <div class="controls">
							<input type="text" name="divisi" class="form-control" placeholder="Divisi" autocomplete="off" required=="required" value="<?=$_SESSION[bagian]?>" readonly>
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Memo Retur Pembelian</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Memo Retur Pembelian</a>
						</li>
						<li>
							<a href="dash.php?hp=mr1&navbar=mr1&parent=pembelian">Memo Retur Pembelian</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Memo Retur Pembelian</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Memo Retur Pembelian
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>No. Memo Retur Pembelian</th>
											<th>Tanggal</th>
											<th>No. Nota Timbang</th>
											<th>No. Laporan Penerimaan Barang</th>
											<th>Kategori</th>
											<th>Status</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select mr.id_mr, mr.id_lpb, mr.date_transaction, mr.status,mr.kategori, mr.no_timbang from mr left outer join lpb on lpb.id_lpb=mr.id_lpb left outer join ttr_main_scale on ttr_main_scale.id_timbang=mr.no_timbang where mr.deleted=0 order by mr.id_mr DESC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_mr']; ?></td>
											<td><?php echo dateBahasaIndo($row['date_transaction']); ?></td>
											<td><?php echo $row['no_timbang']; ?></td>
											<td><?php echo $row['id_lpb']; ?></td>
											<td><?php echo $row['kategori']; ?></td>
											<td>
											<?php
											if($row['status']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else
											{
												echo "<span class='label label-success'>Selesai</span>";
											}
											?>
											</td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<li>
													<a id="edit-mr" data-id="<?php echo $row['id_mr'];?>" role="button" data-toggle="modal">Update Memo Retur Pembelian</a>
												</li>
												<?php
												if($row['status']==0)
												{
												?>
													<?php
													if($row['kategori']=="Produk Utama"){

														?>
														<li>
														<a href="dash.php?hp=mr2&idbaru=<?php echo $row['id_mr']; ?>&navbar=mr&parent=pembelian&jt=nt">Revisi Memo Retur Pembelian</a>
														</li>
														<?php
														}
														else{
															?>
															<li>
													<a href="dash.php?hp=mr2&idbaru=<?php echo $row['id_mr']; ?>&navbar=mr&parent=pembelian&jt=lpb">Revisi Memo Retur Pembelian</a>
													</li>
													<?php
														}
													?>
													
												<?php
												}
												else
												{?>
												<?php
													if($row['kategori']=="Produk Utama"){

														?>
														<li>
														<a href="dash.php?hp=mr3&idbaru=<?php echo $row['id_mr']; ?>&navbar=mr&parent=pembelian&jt=nt">Cetak Memo Retur Pembelian</a>
													</li>
														<li>
														<a id="buat-dnk" data-id="<?php echo $row['id_mr'];?>" role="button" data-toggle="modal">Buat Debet Nota</a>
													</li>
														<?php
														}
														else{
															?>
														<li>
														<a href="dash.php?hp=mr3&idbaru=<?php echo $row['id_mr']; ?>&navbar=mr&parent=pembelian&jt=lpb">Cetak Memo Retur Pembelian</a>
													</li>
													<li>
														<a id="buat-dn" data-id="<?php echo $row['id_mr'];?>" role="button" data-toggle="modal">Buat Debet Nota</a>
													</li>
													<?php
														}
													?>
													
												<?php
												}
												?>
													
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#buat-dn',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-mr/form-editdn.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#buat-dnk',function(e){
						e.preventDefault();
						$("#view3").modal('show');
						$.post('build/build-mr/form-editdnk.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body4").html(html);
							}  
						);
					});
				});
	</script>
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-mr',function(e){
						e.preventDefault();
						$("#view2").modal('show');
						$.post('build/build-mr/form-editmr.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body3").html(html);
							}  
						);
					});
				});
	</script>
	<script type="text/javascript">
			$("#LPB").hide();
			$("#NT").hide();
			$( document ).ready(function() {
   				
   				
			});
			function jp() {
   				var tt = $("#kategori").val();	

   				if(tt =='Barang Umum'){
   				//alert();-->untuk menampilkan popups
   					$("#LPB").show();

					$("#NT").hide();
					$("#LPB2").hide();
   					$("#NT").val(" ");
   					$("#LPB2").val(" ");
   				
   				}else if (tt=='Aset Tetap'){
   					$("#LPB2").show();
   					$("#LPB").hide();
					$("#NT").hide();
   					$("#NT").val(" ");
   					$("#LPB").val(" ");
   				}
   				else{
   					$("#NT").show();
   					$("#LPB2").hide();
					$("#LPB").hide();
   					$("#LPB2").val(" ");
   					$("#LPB").val(" ");
   				}
   				
			};

		</script>
	
	<div class="modal hide fade" id="view3" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Buat Debet Nota xxx</h4>
				</div>
				<div class="modal-body4">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Buat Debet Nota</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
	<div class="modal hide fade" id="view2" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Update Memo Retur Pembelian</h4>
				</div>
				<div class="modal-body3">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>