			<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=notatimbang&navbar=notatimbang&parent=persediaan">Nota Timbang</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Nota Timbang
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable" >
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Nomor Timbang</th>
											<th>Supplier</th>
											<th>Nomor Truck</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.id_timbang, b.company_name, a.no_polisi from ttr_main_scale a, supplier b where a.supplier_id=b.supplier_id and deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_timbang']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['no_polisi']; ?></td>	
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-pelanggan" data-id="<?php echo $row['cust_id'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-pl/delete-pl.php?idpelanggan=<?php echo $row['cust_id']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-pelanggan',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-pl/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});

				
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Pengajuan Pelanggan</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>