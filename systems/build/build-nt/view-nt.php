			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
					<div class="modal-content">
					  <div class="modal-header">
						
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">					
					</div>
					  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=nt&navbar=nt&parent=persediaan">Nota Timbang</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Nota Timbang
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable" >
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Nomor Timbang</th>
											<th>Jenis Timbang</th>
											<th>Produk</th>
											<th>Supplier</th>
											<th>Nomor Truck</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 
									// $jt=$_GET['jt'];	
									// if (empty($jt)) {
									// 	# code...
									$tampil=mysql_query("select a.id_timbang, a.type_timbang,b.company_name, a.no_polisi,c.name,a.jt,c.item_id from ttr_main_scale a, suppliers b,items c where a.supplier_id=b.supplier_id and a.item_id = c.item_id and c.category='Produk Utama' and a.deleted=0 order by a.id_timbang desc");
									// }else{
									// $tampil=mysql_query("select a.id_timbang, a.type_timbang,b.company_name, a.no_polisi,c.name,a.jt from ttr_main_scale a, suppliers b,items c where a.supplier_id=b.supplier_id and a.item_id = c.item_id and c.category = 'Produk Utama'  and a.deleted=0 and a.jt='Pembelian' OR a.jt='titipan'  order by a.id_timbang desc");

									// }

									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_timbang']; ?></td>
											<td><?php echo $row['jt']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['no_polisi']; ?></td>	
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<?php
												if($row['status']==0)
												{
												?>
													<li id="">
														<a href="dash.php?hp=form-qc&idph=<?php echo $row['id_timbang']; ?>&navbar=nt&parent=Persediaan">Hitung QC</a>
													</li>
												<?php
												}
												else
												{
													
												}
												?>
												<?php if ($jt=='pembelian'): ?>
													<li id="">
														<a href="dash.php?hp=form-npb&idph=<?php echo $row['id_timbang']; ?>&navbar=nt&parent=Persediaan">Nota Pembelian</a>
													</li>
												<?php endif ?>
												<?php if ($jt=='titipan'): ?>
													<li id="">
														<a href="dash.php?hp=form-npb&idph=<?php echo $row['id_timbang']; ?>&navbar=nt&parent=Persediaan">Surat titipan</a>
													</li>
												<?php endif ?>
													<li>
														<a href="dash.php?hp=ctnt&idbaru=<?php echo $row['id_timbang']; ?>&navbar=stb&parent=pembelian">Lihat Surat Titipan</a>
													</li>
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-pelanggan',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-pl/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});

				
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Pengajuan Pelanggan</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>