
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-emkl/insert-emkl.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah EMKL</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="form-group">
						  <label for="textfield" class="col-sm-2 control-label">ID EMKL</label>
						  <div class="col-sm-10">
							<input type="text" name="id_EMKL" class="form-control" placeholder="Auto" required="require" readonly>
						  </div>
						</div>
						<br>
						<div class="form-group">
							<label for="textfield" class="control-label">Nama</label>
							<div class="col-sm-10">
								<input type="text" name="name" class="form-control" placeholder="Nama" required="required">
							</div>
						</div>
						<br>
						<div class="form-group">
						  <label for="textfield" class="col-sm-2 control-label">Alamat</label>
						  <div class="col-sm-10">
							<input type="text" name="address" class="form-control" placeholder="Alamat" required="require">
						  </div>
						</div>
						</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=EMKL&navbar=EMKL&parent=master">EMKL</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah EMKL</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar EMKL
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable table-bordered">
									<thead>
										<tr>
											<th>ID EMKL</th>
											<th>Nama</th>
											<th>Alamat</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select * from emkl");
									//$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $row['id_emkl']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo $row['address']; ?></td>
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-emkl" data-id="<?php echo $row['id_emkl'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-emkl/delete-emkl.php?id_emkl=<?php echo $row['id_emkl']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									//$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-emkl',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-emkl/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data EMKL</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>