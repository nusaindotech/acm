<?php
	$tglsekarang	= date('d-m-Y');
	$bulan			= $_GET['idbaru'];
	$tahun		= $_GET['idbaru2'];
	$item_id		= $_GET['item_id'];
	
	function DateToIndo($date) { // fungsi atau method untuk mengubah tanggal ke format indonesia
   // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
		$BulanIndo = array("Januari", "Februari", "Maret",
						   "April", "Mei", "Juni",
						   "Juli", "Agustus", "September",
						   "Oktober", "November", "Desember");
	
		// $tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
		$bulan = $date; // memisahkan format bulan menggunakan substring
		// $tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring
		
		$result = " " . $BulanIndo[(int)$bulan-1];
		return($result);
}

?>		
		<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-po/insert-po2.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						
					  </div>
					  </div>
					   
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
			<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=lhpp&navbar=lhpp&parent=pembelian">Laporan Hutang Pembelian Produk Utama/Komoditi</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th"> Laporan Hutang Pembelian Produk Utama/Komoditi</i>								</h3>
								
							<!-- 	<div align="right">
									<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Pesanan Pembelian</a>								</div> -->
							</div>
							<!-- <div class="box-content"> -->
			<div class="invoice-info">
			<table >
				<tr>
                  <td style="width:100%"><h4><center><b>Laporan Hutang Pembelian Produk Utama/Komoditi</b></center></h4></td>
                  <td></td>
				</tr>
			</table>
            	<table>
			 	<tr>
                	 <th align="left">Bulan</th>
                	 <td style="width:50%;">:&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo(DateToIndo($bulan));?></b></td>
            	</tr>
            	<tr>
                 	 <th align="left">Tahun</th>
      		      	 <td>:&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo $tahun;?></b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                     
            	</tr>
				
				</table>
          <br />
	  </div>
			<table class="table table-striped table-invoice">
				<thead>
					<tr>
						
						<th rowspan="2">No.</th>
						<th rowspan="2">Kode Supplier</th>
						<th rowspan="2">Nama Supplier</th>
						<th colspan="2"><center>Saldo Awal</center></th>
						<th colspan="2"><center>Pembelian</center></th>
						<th colspan="2"><center>Jurnal Memorial</center></th>
						<th colspan="2"><center>Pembayaran</center></th>
						<th colspan="2"><center>Saldo Akhir</center></th>					
					</tr>
					<tr>
						<th>USD</th>
						<th>Rp</th>
						<th>USD</th>
						<th>Rp</th>
						<th>USD</th>
						<th>Rp</th>
						<th>USD</th>
						<th>Rp</th>
						<th>USD</th>
						<th>Rp</th>
					</tr>
				</thead>
				<tbody>
				<?php 	
													
				$tampil=mysql_query("SELECT supplier_id,suppliers.company_code,suppliers.company_name FROM suppliers	");
				$no=1;
				if ($item_id=='all') {
				$item='';
					
				} else {
					# code...
				$item=' and ttr_main_scale.item_id='.$item_id;
				}
				

				while ($row=mysql_fetch_array($tampil))
				{

					$supplier_id=$row['supplier_id'];
					$tampil_a=mysql_query("SELECT sum(t_qc.grand_tot) as gt  FROM t_qc INNER JOIN ttr_main_scale on t_qc.id_timbang=ttr_main_scale.id_timbang where ttr_main_scale.jt='pembelian' and type_beli='Kredit' and supplier_id='$supplier_id'".$item);
					$row_a=mysql_fetch_array($tampil_a);
					$tampil_b=mysql_query("SELECT  SUM(trf_journal.kredit) as kredit from trf_journal INNER JOIN trf on trf.id_trf=trf_journal.id_transaction_trf INNER JOIN ttr_main_scale on trf.id_references = ttr_main_scale.id_timbang WHERE supplier_id ='$supplier_id'".$item);
					$row_b=mysql_fetch_array($tampil_b);
					
					$sak = $row_a['gt']-$row_b['kredit'];
				?>
					<tr>
						<td>&nbsp;<?php echo $no; ?></td>
						<td >&nbsp;<?php echo $row['company_code']; ?></td>
						<td >&nbsp;<?php echo $row['company_name']; ?></td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;<?php echo number_format($row_a['gt'],0); ?></td>
						<td >&nbsp;<?php echo number_format($row['discount'],0); ?></td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td >&nbsp;<?php echo number_format($row_b['kredit'],0); ?></td>
						<td >&nbsp;</td>
						<td >&nbsp;<?php echo number_format($sak,0); ?></td>
					</tr> 
				<?php 
				
				$no++;
				} 
				?>
				              
				</tbody>
			</table>
		<!-- </div> -->
	</div>
</div>
</div>



