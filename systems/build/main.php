<?php
include "../auth/autho.php";

// Bagian Dashboard
if ($_GET[hp]=='home'){
  include "build/build-dashboard/view-dashboard.php";
}

///////////// MASTER  ////////////////////////////////////// MASTER   ///////////////////////////////////
// Bagian Kota
else if ($_GET[hp]=='kota'){
  include "build/build-city/view-city.php";
}
// Bagian Umum
else if ($_GET[hp]=='umum'){
  include "build/build-umum/view-umum.php";
}

// Bagian Jasa
else if ($_GET[hp]=='jasa'){
  include "build/build-jasa/view-jasa.php";
}

// Bagian Histori Harga Barang
else if ($_GET[hp]=='hb'){
  include "build/build-hb/view-hb.php";
}
// Bagian Supplier
else if ($_GET[hp]=='supplier'){
  include "build/build-suppliers/view-suppliers.php";
}


// Bagian Supplier Produk
else if ($_GET[hp]=='supplierproduk'){
  include "build/build-items-filling/view-items-filling.php";
}

// Bagian Konsumen
else if ($_GET[hp]=='konsumen'){
  include "build/build-customers/view-customers.php";
}

// Bagian Sales
else if ($_GET[hp]=='sales'){
  include "build/build-sales/view-sales.php";
}

// Bagian Produk
else if ($_GET[hp]=='produk'){
  include "build/build-items/view-items.php";
}

// Bagian Jenis Produk
else if ($_GET[hp]=='jp'){
  include "build/build-jp/view-jp.php";
}

// Bagian Gudang
else if ($_GET[hp]=='gudang'){
  include "build/build-locations/view-locations.php";
}

// Bagian Aset Tetap
else if ($_GET[hp]=='at'){
  include "build/build-at/view-at.php";
}
// Bagian Perkiraan
else if ($_GET[hp]=='perkiraan'){
  include "build/build-coa/view-coa-home.php";
}

// Bagian Perkiraan
else if ($_GET[hp]=='perkiraan2'){
  include "build/build-coa/view-coa.php";
}

// Bagian Mata Uang
else if ($_GET[hp]=='matauang'){
  include "build/build-currency/view-currency.php";
}
// Bagian Perubahan Harga Jual
else if ($_GET[hp]=='mphj'){
  include "build/build-mphj/view-mphj.php";
}
else if ($_GET[hp]=='mphj2'){
  include "build/build-mphj/view-mphj2.php";
}


//////////////////// CLOSE MASTER /////////////////////////////////////////

///////////// PEMBELIAN  ////////////////////////////////////// PEMBELIAN   ///////////////////////////////////
// // Bagian Pengajuan Supplier PS
// else if ($_GET[hp]=='ps'){
//   include "build/build-suppliers-filling/view-suppliers-filling.php";
// }
// Bagian Pengajuan Supplier PS
else if ($_GET[hp]=='ps'){
  include "build/build-psb/view-psb.php";
}
// Bagian Pengajuan Supplier PS 2
else if ($_GET[hp]=='ps2'){
  include "build/build-psb/view-psb2.php";
}
// Bagian Pengajuan Supplier PS 3
else if ($_GET[hp]=='ps3'){
  include "build/build-psb/view-psb3.php";
}
// Bagian Pengajuan Supplier PS KP
else if ($_GET[hp]=='ps2kp'){
  include "build/build-psb/view-psb2kp.php";
}
// Bagian Perubahan Harga 
else if ($_GET[hp]=='phb'){
  include "build/build-phb/view-phb.php";
}
// Bagian Perubahan Harga 2 
else if ($_GET[hp]=='phb2'){
  include "build/build-phb/view-phb2.php";
}
// Bagian Perubahan Harga 3 
else if ($_GET[hp]=='phb3'){
  include "build/build-phb/view-phb3.php";
}

// Bagian Surat Kontrak  
else if ($_GET[hp]=='skb'){
  include "build/build-skb/view-skb.php";
}
// Bagian Surat Kontrak  USD
else if ($_GET[hp]=='skbu3'){
  include "build/build-skb/view-skbu3.php";
}
// Bagian Surat Kontrak  fh
else if ($_GET[hp]=='skbfh3'){
  include "build/build-skb/view-skbu3.php";
}
// Bagian Surat Kontrak  USDKURS
else if ($_GET[hp]=='skbuk3'){
  include "build/build-skb/view-skbuk3.php";
}


// Bagian Surat Kontrak  2
else if ($_GET[hp]=='skb3'){
  include "build/build-skb/view-skb3.php";
}
// Bagian Surat Titipan  
else if ($_GET[hp]=='stb'){
  include "build/build-stb/view-stb1.php";
}
else if ($_GET[hp]=='stb2'){
  include "build/build-stb/view-stb2.php";
}
// Bagian Surat Titipan  2
else if ($_GET[hp]=='stb3'){
  include "build/build-stb/view-stb3.php";
}
// Bagian SPP 1
else if ($_GET[hp]=='spp1'){
  include "build/build-spp/view-spp1.php";
}

// Bagian SPP 2
else if ($_GET[hp]=='spp2'){
  include "build/build-spp/view-spp2.php";
}

// Bagian SPP 2j
else if ($_GET[hp]=='spp2j'){
  include "build/build-spp/view-spp2j.php";
}
// Bagian SPP 3j
else if ($_GET[hp]=='spp3j'){
  include "build/build-spp/view-spp3j.php";
}

// Bagian SPPJ
else if ($_GET[hp]=='sppj'){
  include "build/build-spp/view-sppj.php";
}
// Bagian SPP 3
else if ($_GET[hp]=='spp3'){
  include "build/build-spp/view-spp3.php";
}

// Bagian POJ 1
else if ($_GET[hp]=='poj'){
  include "build/build-po/view-poj.php";
}

// Bagian POJ 2
else if ($_GET[hp]=='po2j'){
  include "build/build-po/view-po2j.php";
}

// Bagian POJ 3
else if ($_GET[hp]=='po3j'){
  include "build/build-po/view-po3j.php";
}

// Bagian PO 1
else if ($_GET[hp]=='po1'){
  include "build/build-po/view-po1.php";
}

// Bagian PO 2
else if ($_GET[hp]=='po2'){
  include "build/build-po/view-po2.php";
}

// Bagian PO 3
else if ($_GET[hp]=='po3'){
  include "build/build-po/view-po3.php";
}
// Bagian SKPO 4
else if ($_GET[hp]=='po4'){
  include "build/build-po/view-po4.php";
}

// Bagian LPB 1
else if ($_GET[hp]=='lpb1'){
  include "build/build-lpb/view-lpb1.php";
}

// Bagian LPB 2
else if ($_GET[hp]=='lpb2'){
  include "build/build-lpb/view-lpb2.php";
}

// Bagian LPB 2ap
else if ($_GET[hp]=='lpb2ap'){
  include "build/build-lpb/view-lpb2ap.php";
}

// Bagian LPB 3
else if ($_GET[hp]=='lpb3'){
  include "build/build-lpb/view-lpb3.php";
}

// Bagian LPB 4
else if ($_GET[hp]=='lpb4'){
  include "build/build-lpb/view-lpb4.php";
}

// Bagian MR 1
else if ($_GET[hp]=='mr1'){
  include "build/build-mr/view-mr1.php";
}

// Bagian MR 2
else if ($_GET[hp]=='mr2'){
  include "build/build-mr/view-mr2.php";
}

// Bagian MR 3
else if ($_GET[hp]=='mr3'){
  include "build/build-mr/view-mr3.php";
}
// Bagian MR nt
else if ($_GET[hp]=='mr2nt'){
  include "build/build-mr/view-mr2nt.php";
}

// Bagian DN 1
else if ($_GET[hp]=='dn1'){
  include "build/build-dn/view-dn1.php";
}

// Bagian DN 2
else if ($_GET[hp]=='dn2'){
  include "build/build-dn/view-dn2.php";
}

// Bagian DN 3
else if ($_GET[hp]=='dn3'){
  include "build/build-dn/view-dn3.php";
}

// Bagian TTF J
else if ($_GET[hp]=='ttf2j'){
  include "build/build-ttf/view-ttf2j.php";
}

// Bagian TTF J
else if ($_GET[hp]=='ttf3j'){
  include "build/build-ttf/view-ttf3j.php";
}

// Bagian TTF 1
else if ($_GET[hp]=='ttf1'){
  include "build/build-ttf/view-ttf1.php";
}
// Bagian TTF 2
else if ($_GET[hp]=='ttf2'){
  include "build/build-ttf/view-ttf2.php";
}

// Bagian TTF 3
else if ($_GET[hp]=='ttf3'){
  include "build/build-ttf/view-ttf3.php";
}

// Bagian SJR 
else if ($_GET[hp]=='sjr'){
  include "build/build-sjr/view-sjr.php";
}

// Bagian SJR 2
else if ($_GET[hp]=='sjr2'){
  include "build/build-sjr/view-sjr2.php";
}

// Bagian SJR 3
else if ($_GET[hp]=='sjr3'){
  include "build/build-sjr/view-sjr3.php";
}

// Bagian HS 
else if ($_GET[hp]=='hs'){
  include "build/build-hs/view-hs.php";
}

// Bagian DPPH 
else if ($_GET[hp]=='dpph'){
  include "build/build-dpph/view-dpph-ah.php";
}
// Bagian nota claim 
else if ($_GET[hp]=='nc'){
  include "build/build-cl/view-cl.php";
}
// Bagian nota claim 2
else if ($_GET[hp]=='nc2'){
  include "build/build-cl/view-cl2.php";
}


// Bagian DPPH 
else if ($_GET[hp]=='dpph2'){
  include "build/build-dpph/view-dpph2.php";
}
else if ($_GET[hp]=='dpph2jt'){
  include "build/build-dpph/view-dpph2jt.php";
}
// Bagian DPPH  kc
else if ($_GET[hp]=='dpph2kc'){
  include "build/build-dpph/view-dpph2-kc.php";
}
// Bagian DPPH  ab
else if ($_GET[hp]=='dpph2ab'){
  include "build/build-dpph/view-dpph2-ab.php";
}

// Bagian DPPH 
else if ($_GET[hp]=='dpph3'){
  include "build/build-dpph/view-dpph3.php";
}

// Bagian NC 
else if ($_GET[hp]=='nc'){
  include "build/build-nc/view-nc.php";
}

// Bagian SJR 
else if ($_GET[hp]=='sjr'){
  include "build/build-sjr/view-sjr.php";
}

// Bagian BAPJ 
else if ($_GET[hp]=='bapj'){
  include "build/build-bapj/view-bapj.php";
}

// Bagian BAPJ 2
else if ($_GET[hp]=='bapj2'){
  include "build/build-bapj/view-bapj2.php";
}

// Bagian BAPJ 2 ap
else if ($_GET[hp]=='bapj2ap'){
  include "build/build-bapj/view-bapj2ap.php";
}
// Bagian BAPJ 3
else if ($_GET[hp]=='bapj3'){
  include "build/build-bapj/view-bapj3.php";
}

// Bagian NP 
else if ($_GET[hp]=='npb'){
  include "build/build-np/view-np.php";
}

// Bagian NP 2
else if ($_GET[hp]=='npb2'){
  include "build/build-np/view-np2.php";
}

// Bagian NP 2
else if ($_GET[hp]=='npb2um'){
  include "build/build-np/view-np2um.php";
}

// Bagian NP 3
else if ($_GET[hp]=='npb3um'){
  include "build/build-np/view-np3um.php";
}

// Bagian NP 3
else if ($_GET[hp]=='npb3'){
  include "build/build-np/view-np3.php";
}
// Bagian NP 
else if ($_GET[hp]=='nump'){
  include "build/build-nump/view-nump.php";
}

// Bagian NP 2
else if ($_GET[hp]=='nump2'){
  include "build/build-nump/view-nump2.php";
}

// Bagian NP 3
else if ($_GET[hp]=='nump3'){
  include "build/build-nump/view-nump3.php";
}


// Bagian KKHP
else if ($_GET[hp]=='khp'){
  include "build/build-khp/view-khp.php";
}

// Bagian KKHP
else if ($_GET[hp]=='khp2'){
  include "build/build-khp/view-khp2.php";
}

// Bagian KKLHP
else if ($_GET[hp]=='lhp'){
  include "build/build-khp/view-lhp.php";
}
// Bagian KKLHP
else if ($_GET[hp]=='lhp2'){
  include "build/build-khp/view-lhp2.php";
}
// Bagian lum
else if ($_GET[hp]=='lum'){
  include "build/build-khp/view-lum.php";
}

// Bagian lum
else if ($_GET[hp]=='lum2'){
  include "build/build-khp/view-lum2.php";
}
// Bagian KKLOH
else if ($_GET[hp]=='loh'){
  include "build/build-khp/view-loh.php";
}
// Bagian KKLOH
else if ($_GET[hp]=='loh2'){
  include "build/build-khp/view-loh2.php";
}
// Bagian KKLP
else if ($_GET[hp]=='lp'){
  include "build/build-khp/view-lp.php";
}

// Bagian KKLP
else if ($_GET[hp]=='lp2'){
  include "build/build-khp/view-lp2.php";
}
// Bagian KKLRP
else if ($_GET[hp]=='lrp'){
  include "build/build-khp/view-lrp.php";
}

// Bagian KKLRP
else if ($_GET[hp]=='lrp2'){
  include "build/build-khp/view-lrp2.php";
}

// Bagian KKHPP
else if ($_GET[hp]=='khpp2'){
  include "build/build-khpp/view-khpp2.php";
}

// Bagian KKHPP
else if ($_GET[hp]=='khpp'){
  include "build/build-khpp/view-khpp.php";
}
// Bagian KKPTP
else if ($_GET[hp]=='kptp'){
  include "build/build-khpp/view-kptp.php";
}

// Bagian KKPTP
else if ($_GET[hp]=='kptp2'){
  include "build/build-khpp/view-kptp2.php";
}

// Bagian KLPTP
else if ($_GET[hp]=='lptp'){
  include "build/build-khpp/view-lptp.php";
}

// Bagian KLPTP
else if ($_GET[hp]=='lptp2'){
  include "build/build-khpp/view-lptp2.php";
}
// Bagian KLPP
else if ($_GET[hp]=='lpp'){
  include "build/build-khpp/view-lpp.php";
}

// Bagian KLPP
else if ($_GET[hp]=='lpp2'){
  include "build/build-khpp/view-lpp2.php";
}
// Bagian KLKPPS
else if ($_GET[hp]=='lkpps'){
  include "build/build-khpp/view-lkpps.php";
}

// Bagian KLKPPS
else if ($_GET[hp]=='lkpps2'){
  include "build/build-khpp/view-lkpps2.php";
}

// Bagian KLPKPPB
else if ($_GET[hp]=='lpkppb'){
  include "build/build-khpp/view-lpkppb.php";
}

// Bagian KLPKPPB
else if ($_GET[hp]=='lpkppb2'){
  include "build/build-khpp/view-lpkppb2.php";
}

// Bagian KLRPKP
else if ($_GET[hp]=='lrpkp'){
  include "build/build-khpp/view-lrpkp.php";
}

// Bagian KLRPKP
else if ($_GET[hp]=='lrpkp2'){
  include "build/build-khpp/view-lrpkp2.php";
}


// Bagian KKPKP
else if ($_GET[hp]=='kpkp'){
  include "build/build-khpp/view-kpkp.php";
}

// Bagian KKPKP
else if ($_GET[hp]=='kpkp2'){
  include "build/build-khpp/view-kpkp2.php";
}


// Bagian KLHPK
else if ($_GET[hp]=='lhpk'){
  include "build/build-khpp/view-lhpk.php";
}

// Bagian KLHPK
else if ($_GET[hp]=='lhpk2'){
  include "build/build-khpp/view-lhpk2.php";
}

// Bagian KLHPM
else if ($_GET[hp]=='lhpm'){
  include "build/build-khpp/view-lhpm.php";
}

// Bagian KLHPM
else if ($_GET[hp]=='lhpm2'){
  include "build/build-khpp/view-lhpm2.php";
}

// Bagian KLHPJ
else if ($_GET[hp]=='lhpj'){
  include "build/build-khpp/view-lhpj.php";
}

// Bagian KLHPJ
else if ($_GET[hp]=='lhpj2'){
  include "build/build-khpp/view-lhpj2.php";
}

// Bagian KLHPB
else if ($_GET[hp]=='lhpcb'){
  include "build/build-khpp/view-lhpcb.php";
}

// Bagian KLHPB
else if ($_GET[hp]=='lhpcb2'){
  include "build/build-khpp/view-lhpcb2.php";
}


// Bagian KLHPC
else if ($_GET[hp]=='lhpck2'){
  include "build/build-khpp/view-lhpck2.php";
}

// Bagian KLHPC
else if ($_GET[hp]=='lhpck'){
  include "build/build-khpp/view-lhpck.php";
}

// Bagian KLHPC
else if ($_GET[hp]=='lhpc2'){
  include "build/build-khpp/view-lhpc2.php";
}

// Bagian KLHPC
else if ($_GET[hp]=='lhpc'){
  include "build/build-khpp/view-lhpc.php";
}


// Bagian KLHPP
else if ($_GET[hp]=='lhpp2'){
  include "build/build-khpp/view-lhpp2.php";
}

// Bagian KLHPP
else if ($_GET[hp]=='lhpp'){
  include "build/build-khpp/view-lhpp.php";
}
// Bagian KKHP
else if ($_GET[hp]=='kkhp'){
  include "build/build-khp/view-kkhp.php";
}


// Bagian KKHP
else if ($_GET[hp]=='khp'){
  include "build/build-khp/view-khp.php";
}

// Bagian KKHP
else if ($_GET[hp]=='khp2'){
  include "build/build-khp/view-khp2.php";
}

// Bagian KKLHP
else if ($_GET[hp]=='lhp'){
  include "build/build-khp/view-lhp.php";
}
// Bagian KKLHP
else if ($_GET[hp]=='lhp2'){
  include "build/build-khp/view-lhp2.php";
}
// Bagian lum
else if ($_GET[hp]=='lum'){
  include "build/build-khp/view-lum.php";
}

// Bagian lum
else if ($_GET[hp]=='lum2'){
  include "build/build-khp/view-lum2.php";
}
// Bagian KKLOH
else if ($_GET[hp]=='loh'){
  include "build/build-khp/view-loh.php";
}
// Bagian KKLOH
else if ($_GET[hp]=='loh2'){
  include "build/build-khp/view-loh2.php";
}
// Bagian KKLP
else if ($_GET[hp]=='lp'){
  include "build/build-khp/view-lp.php";
}

// Bagian KKLP
else if ($_GET[hp]=='lp2'){
  include "build/build-khp/view-lp2.php";
}
// Bagian KKLRP
else if ($_GET[hp]=='lrp'){
  include "build/build-khp/view-lrp.php";
}

// Bagian KKLRP
else if ($_GET[hp]=='lrp2'){
  include "build/build-khp/view-lrp2.php";
}

// Bagian KKHPP
else if ($_GET[hp]=='khpp2'){
  include "build/build-khpp /view-khpp2.php";
}

// Bagian KKHPP
else if ($_GET[hp]=='khpp'){
  include "build/build-khpp/view-khpp.php";
}

//////////////////// CLOSE PEMBELIAN /////////////////////////////////////////

///////////// PENJUALAN  ////////////////////////////////////// PENJUALAN   ///////////////////////////////////
// Bagian Pesanan Penjualan
else if ($_GET[hp]=='ppj'){
  include "build/build-ppj/view-ppj.php";
}
else if ($_GET[hp]=='pp1'){
  include "build/build-pp/view-pp1.php";
}

else if ($_GET[hp]=='pp2'){
  include "build/build-pp/view-pp2.php";
}

else if ($_GET[hp]=='pp3'){
  include "build/build-pp/view-pp3.php";
}


else if ($_GET[hp]=='sp'){
  include "build/build-sp/view-sp.php";
}
else if ($_GET[hp]=='sp2'){
  include "build/build-sp/view-sp2.php";
}
else if ($_GET[hp]=='sp3'){
  include "build/build-sp/view-sp3.php";
}
//surat kontrak penjualan
else if ($_GET[hp]=='sk'){
  include "build/build-sk/view-sk.php";
}
else if ($_GET[hp]=='sk2'){
  include "build/build-sk/view-sk2.php";
}
else if ($_GET[hp]=='sk3'){
  include "build/build-sk/view-sk3.php";
}
//Surat kontrak eksport
else if ($_GET[hp]=='skji'){
  include "build/build-skji/view-skji.php";
}
else if ($_GET[hp]=='skji2'){
  include "build/build-skji/view-skji2.php";
}
else if ($_GET[hp]=='skji3'){
  include "build/build-rev/sales-kontrak.php";
  // include "build/build-skji/view-skji3.php";
}
//Shiping Intruction
else if ($_GET[hp]=='si'){
  include "build/build-si/view-si.php";
}

else if ($_GET[hp]=='si2'){
  include "build/build-si/view-si2.php";
}
else if ($_GET[hp]=='si3'){
  include "build/build-si/view-si3.php";
}

//packing list
else if ($_GET[hp]=='pcl'){
  include "build/build-pcl/view-pcl.php";
}
else if ($_GET[hp]=='pcl3'){
  include "build/build-rev/packaging-list.php";
}

else if ($_GET[hp]=='pl'){
  include "build/build-pl/view-pl.php";
}
else if ($_GET[hp]=='phj'){
  include "build/build-phj/view-phj.php";
}else if ($_GET[hp]=='cde'){
  include "build/build-cde/view-cde.php";
}
//nota penualan uang muka
else if ($_GET[hp]=='npum'){
  include "build/build-npum/view-npum.php";
}
else if ($_GET[hp]=='npum2'){
  include "build/build-npum/view-npum2.php";
}
else if ($_GET[hp]=='npum3'){
  include "build/build-npum/view-npum3.php";
}
else if ($_GET[hp]=='npj'){
  include "build/build-npj/view-npj.php";
}
else if ($_GET[hp]=='npj2'){
  include "build/build-npj/view-npj2.php";
}
else if ($_GET[hp]=='npj3'){
  include "build/build-npj/view-npj3.php";
}
else if ($_GET[hp]=='claim'){
  include "build/build-claim/view-claim.php";
}
else if ($_GET[hp]=='claim2'){
  include "build/build-claim/view-claim2.php";
}
else if ($_GET[hp]=='claim3'){
  include "build/build-claim/view-claim3.php";
}

//Delevery Order
else if ($_GET[hp]=='do'){
  include "build/build-do/view-do.php";
}
else if ($_GET[hp]=='do2'){
  include "build/build-do/view-do2.php";
}
else if ($_GET[hp]=='do3'){
  include "build/build-do/view-do3.php";
}

//Memo Retur Penjuala
else if ($_GET[hp]=='mrj'){
  include "build/build-mrj/view-mrj.php";
}
else if ($_GET[hp]=='mrj2'){
  include "build/build-mrj/view-mrj2.php";
}
else if ($_GET[hp]=='mrj3'){
  include "build/build-mrj/view-mrj3.php";
}
//nota retur penjualan
else if ($_GET[hp]=='nrj'){
  include "build/build-nrj/view-nrj.php";
}
else if ($_GET[hp]=='nrj2'){
  include "build/build-nrj/view-nrj2.php";
}
else if ($_GET[hp]=='nrj3'){
  include "build/build-nrj/view-nrj3.php";
}
//kk penjualan
else if ($_GET[hp]=='lkpp'){
  include "build/build-kkj/view-lkpp.php";
}

// kk penjualan 
else if ($_GET[hp]=='lkpp2'){
  include "build/build-kkj/view-lkpp2.php";
}

//KK Penjualan
else if ($_GET[hp]=='lppj'){
  include "build/build-kkj/view-lpp.php";
}
//KK Penjualan
else if ($_GET[hp]=='lppj2'){
  include "build/build-kkj/view-lppj2.php";
}
//KK Penjualan
else if ($_GET[hp]=='lppju'){
  include "build/build-kkj/view-lppju.php";
}
//KK Penjualan
else if ($_GET[hp]=='lppju2'){
  include "build/build-kkj/view-lppju2.php";
}
//KK Penjualan
else if ($_GET[hp]=='kkl'){
  include "build/build-kkj/view-kkl.php";
}
//KK Penjualan
else if ($_GET[hp]=='lkpsk'){
  include "build/build-kkj/view-lkpsk.php";
}
//KK Penjualan
else if ($_GET[hp]=='lkpsk2'){
  include "build/build-kkj/view-lkpsk2.php";
}
//KK Penjualan
else if ($_GET[hp]=='kkl2'){
  include "build/build-kkj/view-kkl2.php";
}
//KK Penjualan
else if ($_GET[hp]=='pp'){
  include "build/build-kkj/view-lkpp.php";
}//KK Penjualan
else if ($_GET[hp]=='lkpp2'){
  include "build/build-kkj/view-lkpp2.php";
}
//KK Penjualan
else if ($_GET[hp]=='lkpb'){
  include "build/build-kkj/view-lkpb.php";
}//KK Penjualan
else if ($_GET[hp]=='lkkj'){
  include "build/build-kkj/view-lkkj.php";
}//KK Penjualan
else if ($_GET[hp]=='lkkj2'){
  include "build/build-kkj/view-lkkj2.php";
}//KK Penjualan
else if ($_GET[hp]=='lkk'){
  include "build/build-kkj/view-lkk.php";
}//KK Penjualan
else if ($_GET[hp]=='lkk2'){
  include "build/build-kkj/view-lkk2.php";
}//KK Penjualan
else if ($_GET[hp]=='lkpb2'){
  include "build/build-kkj/view-lkpb2.php";
}
//KK Penjualan
else if ($_GET[hp]=='lpl'){
  include "build/build-kkj/view-lpl.php";
}
//KK Penjualan
else if ($_GET[hp]=='lpl2'){
  include "build/build-kkj/view-lpl2.php";
}
//KK Penjualan
else if ($_GET[hp]=='rpp'){
  include "build/build-kkj/view-rpp.php";
}
//KK Penjualan
else if ($_GET[hp]=='rpp2'){
  include "build/build-kkj/view-rpp2.php";
}
//KK Penjualan
else if ($_GET[hp]=='kpp'){
  include "build/build-kkj/view-kpp.php";
}
//KK Penjualan
else if ($_GET[hp]=='kpp2'){
  include "build/build-kkj/view-kpp2.php";
}
//KK Penjualan
else if ($_GET[hp]=='kppu'){
  include "build/build-kkj/view-kppu.php";
}
//KK Penjualan
else if ($_GET[hp]=='kppu2'){
  include "build/build-kkj/view-kppu2.php";
}
//KK Penjualan
else if ($_GET[hp]=='kppu2'){
  include "build/build-kkj/view-kppu2.php";
}
//KK Penjualan
else if ($_GET[hp]=='rpmj'){
  include "build/build-kkj/view-rpmj.php";
}
//KK Penjualan
else if ($_GET[hp]=='rpmj2'){
  include "build/build-kkj/view-rpmj2.php";
}
//KK Penjualan
else if ($_GET[hp]=='lumj'){
  include "build/build-kkj/view-lum.php";
}
//KK Penjualan
else if ($_GET[hp]=='lumj2'){
  include "build/build-kkj/view-lum2.php";
}

///////////kk export
//kk penjualan Export
else if ($_GET[hp]=='lkpp'){
  include "build/build-kkj/view-lkpp.php";
}

// kk penjualan Export
else if ($_GET[hp]=='lkpp2'){
  include "build/build-kkj/view-lkpp2.php";
}

//KK Penjualan Export
else if ($_GET[hp]=='lppj'){
  include "build/build-kkj/view-lpp.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='lppj2'){
  include "build/build-kkj/view-lppj2.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='lppju'){
  include "build/build-kkj/view-lppju.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='lppju2'){
  include "build/build-kkj/view-lppju2.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='kkle'){
  include "build/build-kkje/view-kkle.php";
}
//KK Penjualan
else if ($_GET[hp]=='lkpsk'){
  include "build/build-kkj/view-lkpsk.php";
}
//KK Penjualan
else if ($_GET[hp]=='lkpsk2'){
  include "build/build-kkj/view-lkpsk2.php";
}
//KK Penjualan
else if ($_GET[hp]=='kkle2'){
  include "build/build-kkje/view-kkle2.php";
}
//KK Penjualan
else if ($_GET[hp]=='pp'){
  include "build/build-kkj/view-lkpp.php";
}//KK Penjualan
else if ($_GET[hp]=='lkpp2'){
  include "build/build-kkj/view-lkpp2.php";
}
//KK Penjualan
else if ($_GET[hp]=='lkpb'){
  include "build/build-kkj/view-lkpb.php";
}//KK Penjualan Export
else if ($_GET[hp]=='lkkj'){
  include "build/build-kkj/view-lkkj.php";
}//KK Penjualan Export
else if ($_GET[hp]=='lkkj2'){
  include "build/build-kkj/view-lkkj2.php";
}//KK Penjualan Export
else if ($_GET[hp]=='lkk'){
  include "build/build-kkj/view-lkk.php";
}//KK Penjualan Export
else if ($_GET[hp]=='lkk2'){
  include "build/build-kkj/view-lkk2.php";
}//KK Penjualan Export
else if ($_GET[hp]=='lkpb2'){
  include "build/build-kkj/view-lkpb2.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='lpl'){
  include "build/build-kkj/view-lpl.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='lpl2'){
  include "build/build-kkj/view-lpl2.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='rpp'){
  include "build/build-kkj/view-rpp.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='rpp2'){
  include "build/build-kkj/view-rpp2.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='kpp'){
  include "build/build-kkj/view-kpp.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='kpp2'){
  include "build/build-kkj/view-kpp2.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='kppu'){
  include "build/build-kkj/view-kppu.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='kppu2'){
  include "build/build-kkj/view-kppu2.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='kppu2'){
  include "build/build-kkj/view-kppu2.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='rpmj'){
  include "build/build-kkj/view-rpmj.php";
}
//KK Penjualan Export
else if ($_GET[hp]=='rpmj2'){
  include "build/build-kkj/view-rpmj2.php";
}


//////end kk export

else if ($_GET[hp]=='dpp'){
  include "build/build-dpp/view-dpp-ah.php";
}

else if ($_GET[hp]=='dpp2'){
  include "build/build-dpp/view-dpp2.php";
  // echo "string";
}
else if ($_GET[hp]=='dpp2kc'){
  // echo "string";
  include "build/build-dpp/view-dpp2-kc.php";
}
else if ($_GET[hp]=='dpp2ab'){
  include "build/build-dpp/view-dpp2-ab.php";
  // echo "string";
}

else if ($_GET[hp]=='dpp3'){
  include "build/build-dpp/view-dpp3.php";
}
//////////////////// CLOSE PENJUALAN /////////////////////////////////////////


///////////// PERSEDIAAN  ////////////////////////////////////// PERSEDIAAN   ///////////////////////////////////
///////////// PERSEDIAAN  ////////////////////////////////////// PERSEDIAAN   ///////////////////////////////////
else if ($_GET[hp]=='nt'){
  include "build/build-nt/view-nt.php";
}
else if ($_GET[hp]=='form-qc'){
  include "build/build-nt/form-qc.php";
}
else if ($_GET[hp]=='form-npb'){
  include "build/build-nt/form-npb.php";
}
else if ($_GET[hp]=='bpb'){
  include "build/build-bpb/view-bpb.php";
}
else if ($_GET[hp]=='bpb2'){
  include "build/build-bpb/view-bpb2.php";
}
else if ($_GET[hp]=='bpb3'){
  include "build/build-bpb/view-bpb3.php";
}
else if ($_GET[hp]=='stokopname'){
  include "build/build-opname/view-opname.php";
}
else if ($_GET[hp]=='stokopname2'){
  include "build/build-opname/view-opname2.php";
}else if ($_GET[hp]=='stokopname3'){
  include "build/build-opname/view-opname3.php";
}else if ($_GET[hp]=='ba'){
  include "build/build-rpso/view-ba.php";

}else if ($_GET[hp]=='rpso'){
  include "build/build-rpso/view-rpso.php";
}
else if ($_GET[hp]=='rpso2'){
  include "build/build-rpso/view-rpso2.php";
}
else if ($_GET[hp]=='rpso3'){
  include "build/build-rpso/view-rpso3.php";

}else if ($_GET[hp]=='sjm'){
  include "build/build-sjm/view-sjm.php";
}else if ($_GET[hp]=='sjm2'){
  include "build/build-sjm/view-sjm2.php";

}else if ($_GET[hp]=='sjm3'){
  include "build/build-sjm/view-sjm3.php";
}else if ($_GET[hp]=='sjm4'){
  include "build/build-sjm/update-sjm.php";
}
//surat jalan penjualan
else if ($_GET[hp]=='sjp'){
  include "build/build-sjp/view-sjp.php";
}else if ($_GET[hp]=='sjp2'){
  include "build/build-sjp/view-sjp2.php";
}else if ($_GET[hp]=='sjp3'){
  include "build/build-sjp/view-sjp3.php";
}else if ($_GET[hp]=='sjp4'){
  include "build/build-sjp/update-sjp.php";
}
//kartu persediaan
else if ($_GET[hp]=='kp'){
  include "build/build-kp/view-kp.php";
}else if ($_GET[hp]=='kp2'){
  include "build/build-kp/view-kp2.php";
}else if ($_GET[hp]=='kp2h'){
  include "build/build-kp/view-kp2h.php";
}else if ($_GET[hp]=='kp2gln'){
  include "build/build-kp/view-kp2gln.php";
}else if ($_GET[hp]=='kp3'){
  include "build/build-kp/view-kp3.php";
}

//kartu opname
else if ($_GET[hp]=='asuransi'){
  include "build/build-asuransi/view-asuransi.php";
}else if ($_GET[hp]=='asuransi2'){
  include "build/build-asuransi/view-asuransi2.php";
}else if ($_GET[hp]=='asuransi3'){
  include "build/build-asuransi/view-asuransi3.php";
}





//////////////////// CLOSE PERSEDIAAN /////////////////////////////////////////

//////////////////// PROSES /////////////////////////////////////////
//Form Permintaan
else if ($_GET[hp]=='fpp'){
  include "build/build-fpp/view-fpp.php";
}else if ($_GET[hp]=='fpp2'){
  include "build/build-fpp/view-fpp2.php";
}else if ($_GET[hp]=='fpp3'){
  include "build/build-fpp/view-fpp3.php";
}
//////////////////// CLOSE PROSES /////////////////////////////////////////

///////////// KEUANGAN  ////////////////////////////////////// KEUANGAN   ///////////////////////////////////
// Bagian Keuangan Penerimaan
else if ($_GET[hp]=='penerimaan'){
  include "build/build-trf/view-trf.php";
}

// Bagian Keuangan Penerimaan 1
else if ($_GET[hp]=='trf1'){
  include "build/build-trf/view-trf1.php";
}

else if ($_GET[hp]=='trf2'){
  include "build/build-trf/view-trf2.php";
}

else if ($_GET[hp]=='pengeluaran'){
  include "build/build-trf/view-trf.php";
}

else if ($_GET[hp]=='bbk'){
  include "build/build-trf/view-bbk.php";
}

else if ($_GET[hp]=='pp3'){
  include "build/build-pp/view-pp3.php";
}

///////////// Laporan  ////////////////////////////////////// KEUANGAN   ///////////////////////////////////
// Bagian Laporan Pembelian Laporan Oustanding
else if ($_GET[hp]=='laporanoutstanding'){
  include "#";
}

//Revisi mas Ardi
else if ($_GET[hp]=='numpk'){
  include "build/build-rev/kwitansi-umpenjualan.php";
}
else if ($_GET[hp]=='penj'){
  include "build/build-rev/kwipen.php";
}
else if ($_GET[hp]=='slsk'){
  include "build/build-rev/sales-kontrak.php";
}
// else if ($_GET[hp]=='si'){
//   include "build/build-rev/shipping-instruction.php";
// }
else if ($_GET[hp]=='inv'){
  include "build/build-rev/invoice.php";
}
// else if ($_GET[hp]=='pcl'){
//   include "build/build-rev/packaging-list.php";
// }
else if ($_GET[hp]=='qlc'){
  include "build/build-rev/quality-claim.php";
}

// Apabila modul tidak ditemukan
else{

  include "build/build-notification/tampil-notification.php";
}
?>
<script type="text/javascript">
      $( document ).ready(function() {
    
      $(".table").width("100%");    
      // $(".modal").width("80%");    
      // $(".modal").height("80%");    
      // $(".modal-content").height("100%");    
      // $(".modal").css('margin-left','-500px');
});
</script>