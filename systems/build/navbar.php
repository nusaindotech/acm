<?php
	$navbar	= $_GET['navbar'];
	$parent	= $_GET['parent'];
	$jenis	= $_GET['jenis'];
?>
<ul class='main-nav'>
				<li <?php if($navbar=='dashboard') { echo "class='active'"; } else { } ?> >
					<a href="dash.php?hp=home&navbar=dashboard">
						<span>Dashboard</span>
					</a>
				</li>
				<li <?php if($parent=='master') { echo "class='active'"; } else { } ?> >
					<a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						<span>Master</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">

					<li <?php if($navbar=='umum') { echo "class='active'"; } else { } ?> >
						
							<a href="dash.php?hp=umum&navbar=umum&parent=master">Barang Umum & Aset Tetap</a>
						</li>
						<li <?php if($navbar=='jasa') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=jasa&navbar=jasa&parent=master">Jasa</a>
						</li>
						<li <?php if($navbar=='kota') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=kota&navbar=kota&parent=master">Kota</a>
						</li>
						<li <?php if($navbar=='hb') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=hb&navbar=hb&parent=master">Histori Harga Barang</a>
						</li>
						<li <?php if($navbar=='supplier') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=supplier&navbar=supplier&parent=master">Supplier</a>
						</li>
						<li <?php if($navbar=='konsumen') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=konsumen&navbar=konsumen&parent=master">Konsumen</a>
						</li>
						<!-- <li <?php if($navbar=='jp') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=jp&navbar=jp&parent=master">Jenis Produk</a>
						</li> -->
						<li <?php if($navbar=='produk') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=produk&navbar=produk&parent=master">Produk</a>
						</li>
						<li <?php if($navbar=='sales') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=sales&navbar=sales&parent=master">Sales</a>
						</li>
						<li <?php if($navbar=='gudang') { echo "class='active'"; } else { } ?>>
							<a href="dash.php?hp=gudang&navbar=gudang&parent=master">Gudang</a>
						</li><li <?php if($navbar=='at') { echo "class='active'"; } else { } ?>>
							<a href="dash.php?hp=at&navbar=at&parent=master">Aset Tetap</a>
						</li>
						<li <?php if($navbar=='perkiraan') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=perkiraan&navbar=perkiraan&parent=master">Perkiraan</a>
						</li>
						<li <?php if($navbar=='matauang') { echo "class='active'"; } else { } ?>>
							<a href="dash.php?hp=matauang&navbar=matauang&parent=master">Mata Uang</a>
						</li>
						<li <?php if($navbar=='mphj') { echo "class='active'"; } else { } ?>>
							<a href="dash.php?hp=mphj&navbar=mphj&parent=master">Perubahan Harga Jual Produk</a>
						</li>
					</ul>
				</li>
				<li <?php if($parent=='pembelian') { echo "class='active'"; } else { } ?> >
					<a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						<span>Pembelian</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li class='dropdown-submenu'>
							<a href="#">Pembelian Barang Umum, Aset Tetap & Jasa</a>
							<ul class="dropdown-menu">
							<li <?php if($navbar=='ps') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=ps&navbar=ps&parent=pembelian">Pengajuan Supplier </a>
								</li>
								<li <?php if($navbar=='hs') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=hs&navbar=hs&parent=pembelian">Histori Supplier</a>
								</li>
								
								<li <?php if($navbar=='phb') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=phb&navbar=phb&parent=pembelian">Perubahan Harga Barang</a>
								</li>
								<li <?php if($navbar=='spp') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=spp1&navbar=spp&parent=pembelian">Permintaan Pembelian Barang</a>
								</li>
								<li <?php if($navbar=='sppj') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=sppj&navbar=sppj&parent=pembelian">Permintaan Pembelian Jasa</a>
								</li>
								<li <?php if($navbar=='po') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=po1&navbar=po&parent=pembelian">Pesanan Pembelian Barang</a>
								</li>
								<li <?php if($navbar=='poj') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=poj&navbar=poj&parent=pembelian">Pesanan Pembelian Jasa</a>
								</li>
								<li <?php if($navbar=='lpb') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=lpb1&navbar=lpb&parent=pembelian">Laporan Penerimaan Barang</a>
								</li>
								<li <?php if($navbar=='bapj') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=bapj&navbar=bapj&parent=pembelian">Berita Acara Penyelesaian Jasa</a>
								</li>
								
								
								
							</ul>
						</li>
						<li class='dropdown-submenu'>
							<a href="#">Pembelian Komoditi</a>
							<ul class="dropdown-menu">
								<li <?php if($navbar=='pk') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=nt&navbar=pk&parent=pembelian&jt=pembelian">Nota Pembelian</a>
								</li>
							</ul>
						</li>	

						<li class='dropdown-submenu'>
							<a href="#">Retur Pembelian</a>
							<ul class="dropdown-menu">
								<li <?php if($navbar=='mr') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=mr1&navbar=mr&parent=pembelian">Memo Retur Pembelian</a>
								</li>
								<li <?php if($navbar=='dn') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=dn1&navbar=dn&parent=pembelian">Debet Nota</a>
								</li>
								<li <?php if($navbar=='sjr') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=sjr&navbar=sjr&parent=pembelian">Surat Jalan Retur</a>
								</li>
							</ul>
						</li>
						<li class='dropdown-submenu'>
							<a href="#">Surat Kontrak & Titipan</a>
							<ul class="dropdown-menu">
								<li <?php if($navbar=='skb') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=skb&navbar=skb&parent=pembelian">Surat Kontrak Pembelian Produk</a>
									</li>
									<li <?php if($navbar=='stb') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=stb&navbar=stb&parent=pembelian">Surat Titipan Pembelian Produk</a>
								  </li>
								  <li <?php if($navbar=='np') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=npb&navbar=np&parent=pembelian">Nota Pembelian Produk</a>
								</li>
								<li <?php if($navbar=='nump') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=nump&navbar=nump&parent=pembelian">Nota Uang Muka Pembelian</a>
								</li>
							</ul>
						</li>

						<li <?php if($navbar=='ttf') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=ttf1&navbar=ttf&parent=pembelian">Tanda Terima Tagihan</a>
						</li>
						<li <?php if($navbar=='dpph') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=dpph&navbar=dpph&parent=pembelian">DPPH</a>
						</li>
						<li <?php if($navbar=='nc') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=nc&navbar=nc&parent=pembelian">Nota Claim</a>
						</li>
						
						<li class='dropdown-submenu'>
							<a href="#">KK Barang & Jasa</a>
							<ul class="dropdown-menu">

								<li class='dropdown-submenu'>
									<a href="#">KK Hutang Pembelian Barang & Jasa</a>
									<ul class="dropdown-menu">
										<li <?php if($navbar=='khp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=khp&navbar=khp&parent=pembelian">Kartu Hutang Pembelian Barang & Jasa</a>
										</li>
										<li <?php if($navbar=='lhp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lhp&navbar=lhp&parent=pembelian">Laporan Hutang Pembelian Barang & Jasa</a>
										</li>
											
									</ul>
								</li>
								<li class='dropdown-submenu'>
									<a href="#">KK Pembelian Barang & Jasa</a>
									<ul class="dropdown-menu">
										<li <?php if($navbar=='loh') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=loh&navbar=loh&parent=pembelian">Laporan Outstanding Pembelian</a>
										</li>
										<li <?php if($navbar=='lp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lp&navbar=lp&parent=pembelian">Laporan Pembelian</a>
										</li>
										<li <?php if($navbar=='lum') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lum&navbar=lum&parent=pembelian">Laporan Uang Muka</a>
										</li>
											
									</ul>
								</li>
								<li class='dropdown-submenu'>
									<a href="#">KK Retur Pembelian Barang & Jasa</a>
									<ul class="dropdown-menu">
										<li <?php if($navbar=='lrp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lrp&navbar=lrp&parent=pembelian">Laporan Retur Pembelian</a>
										</li>
											
									</ul>
								</li>	
							</ul>
						</li>
						<li class='dropdown-submenu'>
							<a href="#">Produk Utama/Komoditi</a>
							<ul class="dropdown-menu">
								<li class='dropdown-submenu'>
									<a href="#">KK Hutang Pembelian Produk</a>
									<ul class="dropdown-menu">
									<li <?php if($navbar=='khpp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=khpp&navbar=khpp&parent=pembelian">Kartu Hutang Pembelian Produk/Komoditi</a>
											</li>
										<li <?php if($navbar=='lhpp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lhpp&navbar=lhpp&parent=pembelian">Laporan Hutang Pembelian Produk/Komoditi</a>
										</li>
										<li <?php if($navbar=='lhpc') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lhpc&navbar=lhpc&parent=pembelian">Laporan Hutang Pembelian Coklat</a>
										</li>
										<li <?php if($navbar=='lhpck') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lhpck&navbar=lhpck&parent=pembelian">Laporan Hutang Pembelian Cengkeh Kering</a>
										</li>
										<li <?php if($navbar=='lhpcb') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lhpcb&navbar=lhpcb&parent=pembelian">Laporan Hutang Pembelian Cengkeh Basah</a>
										</li>
										<li <?php if($navbar=='lhpj') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lhpj&navbar=lhpj&parent=pembelian">Laporan Hutang Pembelian Jagung</a>
										</li>
										<li <?php if($navbar=='lhpm') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lhpm&navbar=lhpm&parent=pembelian">Laporan Hutang Pembelian Mente</a>
										</li>
										<li <?php if($navbar=='lhpk') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lhpk&navbar=lhpk&parent=pembelian">Laporan Hutang Pembelian Kopi</a>
										</li>
									</ul>
								</li>
								<li class='dropdown-submenu'>
									<a href="#">KK Pembelian Kontrak Produk (Per Komoditi)</a>
									<ul class="dropdown-menu">
										<li <?php if($navbar=='kpkp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=kpkp&navbar=kpkp&parent=pembelian">Kartu Pembelian Kontrak Produk</a>
										</li>
										<li <?php if($navbar=='lrpkp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lrpkp&navbar=lrpkp&parent=pembelian">Laporan Realisasi Pembelian Kontrak Produk</a>
										</li>
										<li <?php if($navbar=='lkpps') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lkpps&navbar=lkpps&parent=pembelian">Laporan Kontrak Pembelian Per-Supplier</a>
										</li>
										<li <?php if($navbar=='lpkppb') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lpkppb&navbar=lpkppb&parent=pembelian">Laporan Pembelian Kontrak Produk Per-Bulan</a>
										</li>
											
									</ul>
								</li>
								<li class='dropdown-submenu'>
									<a href="#">KK Pembelian Produk</a>
									<ul class="dropdown-menu">
										<li <?php if($navbar=='lpp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lpp&navbar=lpp&parent=pembelian">Laporan Pembelian Produk</a>
											</li>
											
									</ul>
								</li>
								<li class='dropdown-submenu'>
									<a href="#">KK Pembelian Titipan Produk</a>
									<ul class="dropdown-menu">
										<li <?php if($navbar=='kptp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=kptp&navbar=kptp&parent=pembelian">Kartu Pembelian Titipan Produk</a>
										</li>
										<li <?php if($navbar=='lptp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lptp&navbar=lptp&parent=pembelian">Laporan Pembelian Titipan Produk</a>
										</li>
											
									</ul>
								</li>
								</ul>
						
				</li>
						
					</ul>

				</li>
				<li <?php if($parent=='penjualan') { echo "class='active'"; } else { } ?> >
					<a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						<span>Penjualan</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li  class="dropdown-submenu <?php if($navbar=='kp') { echo 'active'; }  ?> ">
						<!--  -->
							<a href="#" class='dropdown-toggle' data-toggle="dropdown">Pengajuan Konsumen</a>
							<ul class="dropdown-menu">
							
							
							<li <?php if($navbar=='pl') { echo "class='active'"; } else { } ?> >
								<a href="dash.php?hp=pl&navbar=pl&parent=penjualan">Pengajuan Konsumen</a>
							</li>
							<li <?php if($navbar=='phj') { echo "class='active'"; } else { } ?> >
								<a href="dash.php?hp=phj&navbar=phj&parent=penjualan">Penetapan Harga Jual</a>
							</li>
						   
							</ul>
						</li>

						<li  class="dropdown-submenu <?php if($navbar=='kp') { echo 'active'; }  ?> ">
						<!--  -->
							<a href="#" class='dropdown-toggle' data-toggle="dropdown">Penawaran dan Pesanan</a>
							<ul class="dropdown-menu">
								<li <?php if($navbar=='ppj') { echo "class='active'"; } else { } ?> >

									<a href="dash.php?hp=ppj&navbar=ppj&parent=penjualan">Permintan Penjualan</a>
								</li>
								<li <?php if($navbar=='rpmj') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=rpmj&navbar=rpmj&parent=penjualan">Register Permintaan Penjualan</a>
											</li>
										
								<li <?php if($navbar=='sp') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=sp&navbar=sp&parent=penjualan">Surat Penawaran</a>
								</li>

								<li <?php if($navbar=='pp'&&$jenis=='produk') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=pp1&navbar=pp&parent=penjualan&jenis=produk">Pesanan Penjualan (Komoditi)</a>
								</li>
								<li <?php if($navbar=='pp'&&$jenis=='barang') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=pp1&navbar=pp&parent=penjualan&jenis=barang">Pesanan Penjualan (Barang Umum)</a>
								</li>
								<li <?php if($navbar=='rpp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=rpp&navbar=rpp&parent=penjualan">Register Pesanan Lokal</a>
											</li>
								<li <?php if($navbar=='lpl') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lpl&navbar=lpl&parent=penjualan">Laporan Penjualan Lokal</a>
											</li>
								<li <?php if($navbar=='sk') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=sk&navbar=sk&parent=penjualan">Surat Kontrak</a>
								</li>
								

							</ul>
						</li>
						<li  class="dropdown-submenu <?php if($navbar=='kp') { echo 'active'; }  ?> ">
						<!--  -->
							<a href="#" class='dropdown-toggle' data-toggle="dropdown">Kartu dan Laporan Kontrak</a>
							<ul class="dropdown-menu">
							
								<li <?php if($navbar=='kkl') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=kkl&navbar=kkl&parent=penjualan">Kartu Kontrak Lokal</a>
								</li>
								
								<li <?php if($navbar=='lkkj') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=lkkj&navbar=lkkj&parent=penjualan">Laporan Kontrak Per Konsumen</a>
								</li>
								<li <?php if($navbar=='lkpsk') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=lkpsk&navbar=lkpsk&parent=penjualan">Laporan Kontrak Per SK</a>
								</li>
								<li <?php if($navbar=='lkpb') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=lkpb&navbar=lkpb&parent=penjualan">Laporan Kontrak Per Bulan</a>
								</li>
								<li <?php if($navbar=='lkpp') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=lkpp&navbar=lkpp&parent=penjualan">Laporan Kontrak Per Produk</a>
								</li>
								<li <?php if($navbar=='lkk') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=lkk&navbar=lkk&parent=penjualan">Laporan Komoditi Per Kontrak</a>
								</li>	
									</ul>
								</li>
						<li  class="dropdown-submenu <?php if($navbar=='kp') { echo 'active'; }  ?> ">
						<!--  -->
							<a href="#" class='dropdown-toggle' data-toggle="dropdown">Nota Penjualan</a>
							<ul class="dropdown-menu">
								
								<li <?php if($navbar=='npum') :?> class='active'  <?php endif?> >
								<a href="dash.php?hp=npum&navbar=npum">Nota Penjualan Uang Muka</a>
								</li>
								<li <?php if($navbar=='lumj') :?> class='active'  <?php endif?> >
								<a href="dash.php?hp=lumj&navbar=lumj">Laporan Uang Muka</a>
								</li>
								<li <?php if($navbar=='npj') :?> class='active'  <?php endif?> >
									<a href="dash.php?hp=npj&navbar=npj">Nota Penjualan</a>
								</li>
								<li <?php if($navbar=='claim') :?> class='active'  <?php endif?>>
									<a href="dash.php?hp=claim&navbar=claim">Nota Claim</a>
								</li>
								
								<!-- <li <?php if($navbar=='penj') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=penj&navbar=npk&parent=penjualan">Kwitansi Penjualan Komoditi</a>
								</li>
								<li <?php if($navbar=='numpk') { echo "class='active'"; } else { } ?> >
									<a href="dash.php?hp=numpk&navbar=numpk&parent=penjualan">Kwitansi Uang Muka Penjualan Komoditi</a>
								</li> -->
								
							</ul>
						</li>
						<li <?php if($navbar=='do') :?> class='active'  <?php endif?> >
							<a href="dash.php?hp=do&navbar=do">Delivery Order</a>
						</li>	
						
						<li  class="dropdown-submenu <?php if($navbar=='kp') { echo 'active'; }  ?> ">
						<!--  -->
							<a href="#" class='dropdown-toggle' data-toggle="dropdown">Retur</a>
							<ul class="dropdown-menu">
								<li>
									<a href="dash.php?hp=mrj&navbar=mrj">Memo Retur Penjualan</a>
								</li>
								<li>
									<a href="dash.php?hp=nrj&navbar=nrj">Nota Retur Penjualan</a>
								</li>
								
							</ul>
						</li>
						<li class='dropdown-submenu'>
									<a href="#">Piutang</a>
									<ul class="dropdown-menu">
										<li <?php if($navbar=='kpp') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=kpp&navbar=kpp&parent=penjualan">Kartu Piutang Penjualan Rp.</a>
											</li>
										<li <?php if($navbar=='kppu') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=kppu&navbar=kppu&parent=penjualan">Kartu Piutang Penjualan USD.$</a>
											</li>
										<li <?php if($navbar=='lppj') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=lppj&navbar=lppj&parent=penjualan">Laporan Perincian Piutang Rp.</a>
										</li>
										<li <?php if($navbar=='lppju') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=lppju&navbar=lppju&parent=penjualan">Laporan Perincian Piutang USD.$</a>
										</li>
										<li>
											<a href="dash.php?hp=dpp&navbar=dpp">Daftar Penagihan Piutang</a>
										</li>						
									</ul>
						</li>
						
						
								<!-- <li class='dropdown-submenu'>
									<a href="#">Register Pesanan Dan Laporan Penjualan</a>
									<ul class="dropdown-menu">
										
										<li <?php if($navbar=='rpe') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=rpe&navbar=rpe&parent=penjualan">Register Pesanan Ekspor</a>
											</li>
										<li <?php if($navbar=='lpe') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lpe&navbar=lpe&parent=penjualan">Laporan Penjualan Ekspor</a>
											</li>	
									</ul>
								</li> -->
								
								
						
					</ul>
				</li>
				<li <?php if($parent=='export') { echo "class='active'"; } else { } ?> >
					<a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						<span>Penjualan Export</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li class='dropdown-submenu'>
							<a href="#">Form Ekspor</a>
								<ul class="dropdown-menu">
									 <li <?php if($navbar=='skji') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=skji&navbar=skji&parent=export">Sales Contract</a>
									</li>
									
									<li <?php if($navbar=='si') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=si&navbar=si&parent=export">Shipping Instruction</a>
									</li><!-- 
									<li <?php if($navbar=='pcl') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=pcl&navbar=pcl&parent=export">Packaging List</a>
									</li> -->
									<!-- <li <?php if($navbar=='qlc') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=qlc&navbar=qlc&parent=export">Quality Claim</a>
									</li> -->
								
								</ul>
						</li>	
						<li class='dropdown-submenu'>
									<a href="#">Register Pesanan Dan Laporan Penjualan</a>
									<ul class="dropdown-menu">
										
										<li <?php if($navbar=='rpe') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=kkle&navbar=kkle&parent=export">Contract Card</a>
											</li>
										<li <?php if($navbar=='lpe') { echo "class='active'"; } else { } ?> >
											<a href="dash.php?hp=lpe&navbar=lpe&parent=penjualan">Laporan Penjualan Ekspor</a>
											</li>	
										<li <?php if($navbar=='kkerp') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=kkerp&navbar=kkerp&parent=penjualan">Kartu Kontrak (Ekspor) Rp.</a>
										</li>
									<li <?php if($navbar=='kkeusd') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=kkeusd&navbar=kkeusd&parent=penjualan">Kartu Kontrak (Ekspor) USD.$</a>
									</li>
									<li <?php if($navbar=='kkerpusd') { echo "class='active'"; } else { } ?> >
										<a href="dash.php?hp=kkerpusd&navbar=kkerpusd&parent=penjualan">Kartu Kontrak (Ekspor) Rp-USD.$</a>
									</li>
									</ul>
								</li>		
					</ul>
				</li>

				<li <?php if($parent=='persediaan') { echo "class='active'"; } else { } ?>>
					<a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						<span>Persediaan</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
					   <li <?php if($navbar=='nt') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=nt&navbar=nt&parent=persediaan">Nota Timbang</a>
						</li>
						<li <?php if($navbar=='bpb') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=bpb&navbar=bpb&parent=persediaan">Bon Permintaan Barang</a>
						</li>
						<!-- <li>
							<a href="dash.php?hp=pengeluaranbarang&navbar=pengeluaranbarang">Pengeluaran Barang</a>
						</li> -->
						<!-- <li>
							<a href="dash.php?hp=transfergudang&navbar=transfergudang">Transfer Gudang</a>
						</li> -->
						<!-- <li>
							<a href="dash.php?hp=tandaterimatransfer&navbar=tandaterimatransfer">Tanda Terima Transfer</a>
						</li> -->
						<!-- <li>
							<a href="dash.php?hp=stokbarang&navbar=stokbarang">Stok Barang</a>
						</li> -->
						<li <?php if($navbar=='stokopname') { echo "class='active'"; } else { } ?>>
							<a href="dash.php?hp=stokopname&navbar=stokopname">Stok Opname</a>
						</li>
						<li <?php if($navbar=='rpso') { echo "class='active'"; } else { } ?>>
							<a href="dash.php?hp=rpso&navbar=rpso">Rekap Stok</a>
						</li>

						<li <?php if($navbar=='sjp') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=sjp&navbar=sjp&parent=persediaan">Surat Jalan Penjualan</a>
						</li>
						<li <?php if($navbar=='sjm') { echo "class='active'"; } else { } ?>>
							<a href="dash.php?hp=sjm&navbar=sjm&parent=persediaan">Surat Jalan Mutasi</a>
						</li>
						<li  class="dropdown-submenu <?php if($navbar=='kppro') { echo 'active'; }  ?> ">
						<!--  -->
							<a href="#" class='dropdown-toggle' data-toggle="dropdown">Kartu Persediaan Produk</a>
							<ul class="dropdown-menu">
								<li >
									<a href="dash.php?hp=kp&navbar=kppro&parent=persediaan&jenis=pembelian">Kartu Persedian Pembelian</a>
								</li>
								<li>
									<a href="dash.php?hp=kp&navbar=kppro&parent=persediaan&jenis=titipan">Kartu Persediaan Titipan</a>
								</li>
								<li>
									<a href="dash.php?hp=kp&navbar=kppro&parent=persediaan&jenis=titip+jemur">Kartu Persediaan Titip Jemur</a>
								</li>
								<li>
									<a href="dash.php?hp=kp&navbar=kppro&parent=persediaan&jenis=titip+ayak">Kartu Persediaan Titip Ayak</a>
								</li>
								<li>
									<a href="dash.php?hp=kp&navbar=kppro&parent=persediaan&jenis=proses">Kartu Persediaan Proses</a>
								</li>
								<li>
									<a href="dash.php?hp=kp&navbar=kppro&parent=persediaan&jenis=hasil+jadi">Kartu Persediaan Hasil Jadi</a>
								</li>
								<li>
									<a href="dash.php?hp=kp&navbar=kppro&parent=persediaan&jenis=IC">Kartu Persediaan In Container</a>
								</li>
								
								<li>
									<a href="dash.php?hp=kp&navbar=kppro&parent=persediaan&jenis=GLN">Kartu Persediaan Gudang Luar Negeri</a>
								</li>
							</ul>
						</li>
						<li  class="dropdown-submenu <?php if($navbar=='kp') { echo 'active'; }  ?> ">
						<!--  -->
							<a href="#" class='dropdown-toggle' data-toggle="dropdown">Kartu Persediaan Barang Umum</a>
							<ul class="dropdown-menu">
								<li>
									<a href="dash.php?hp=kp&navbar=kp&parent=persediaan&jenis=BU">Kartu Persediaan Barang Umum</a>
								</li>
								<li>
									<a href="dash.php?hp=kp&navbar=kp&parent=persediaan&jenis=karung+lama">Kartu Persediaan Karung Lama</a>
								</li>
								<li>
									<a href="dash.php?hp=kp&navbar=kp&parent=persediaan&jenis=karung+baru">Kartu Persediaan Karung Baru</a>
								</li>
							</ul>
						</li>
						<li <?php if($navbar=='asuransi') { echo "class='active'"; } else { } ?>>
							<a href="dash.php?hp=asuransi&navbar=asuransi&parent=persediaan">Form Asuransi</a>
						</li>
					</ul>
				</li>
				
				<li <?php if($parent=='proses') { echo "class='active'"; } else { } ?>>
					<a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						<span>Proses</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="dash.php?hp=fpp&navbar=fpp&parent=proses">Form Permintaan proses</a>
						</li>
						<li>
							<a href="dash.php?hp=Konsumen&navbar=Konsumen">xxxxx</a>
						</li>
					</ul>
				</li>
				<li <?php if($parent=='keuangan') { echo "class='active'"; } else { } ?> >
					<a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						<span>Keuangan</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li <?php if($navbar=='penerimaan') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=penerimaan&navbar=penerimaan&parent=keuangan">Penerimaan</a>
						</li>
						<li <?php if($navbar=='pengeluaran') { echo "class='active'"; } else { } ?> >
							<a href="dash.php?hp=pengeluaran&navbar=pengeluaran&parent=keuangan">Pengeluaran</a>
						</li>
						<li>
							<a href="more-faq.html">Cek/Giro</a>
						</li>
						<li>
							<a href="more-invoice.html">Jurnal Memorial</a>
						</li>
						<li class='dropdown-submenu'>
							<a href="#">Laporan</a>
							<ul class="dropdown-menu">
								<li>
									<a href="layouts-mobile-slide.html">Laporan Kas Harian</a>
								</li>
								<li>
									<a href="layouts-mobile-button.html">Laporan Bank Harian</a>
								</li>
								<li>
									<a href="layouts-mobile-button.html">Laporan Jurnal</a>
								</li>
								<li>
									<a href="layouts-mobile-slide.html">Buku Besar</a>
								</li>
								<li>
									<a href="layouts-mobile-button.html">Neraca Mutasi</a>
								</li>
								<li>
									<a href="layouts-mobile-button.html">Catatan Atas Laporan Keuangan</a>
								</li>
								<li>
									<a href="layouts-mobile-button.html">Laporan Perincian Biaya</a>
								</li>
								<li>
									<a href="layouts-mobile-button.html">Laporan Arus Kas</a>
								</li>
								<li>
									<a href="layouts-mobile-button.html">Analisa Rasio</a>
								</li>
								<li>
									<a href="layouts-mobile-button.html">Laporan Perubahan Ekuitas</a>
								</li>								
								<li>
									<a href="layouts-mobile-button.html">Laba Rugi</a>
								</li>
								<li>
									<a href="layouts-mobile-button.html">Neraca</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
			
			</ul>