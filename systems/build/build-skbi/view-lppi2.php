<?php
	$tglsekarang	= date('d-m-Y');
	$bulan			= $_GET['idbaru'];
	$tahun		= $_GET['idbaru2'];
	$jenisbelis	= $_GET['idbaru3'];
	// var_dump($jenisbelis);
	function DateToIndo($date) { // fungsi atau method untuk mengubah tanggal ke format indonesia
   // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
		$BulanIndo = array("Januari", "Februari", "Maret",
						   "April", "Mei", "Juni",
						   "Juli", "Agustus", "September",
						   "Oktober", "November", "Desember");
	
		// $tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
		$bulan = $date; // memisahkan format bulan menggunakan substring
		// $tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring
		
		$result = " " . $BulanIndo[(int)$bulan-1];
		return($result);
}


?>		
		<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-po/insert-po2.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						
					  </div>
					  </div>
					   
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
			<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=lppi&navbar=lppi&parent=pembelian">Laporan Pembelian Produk Import</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th"> Laporan Pembelian Produk Import <?php echo($jenisbelis); ?></i>								</h3>
								
							<!-- 	<div align="right">
									<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Pesanan Pembelian</a>								</div> -->
							</div>
							<!-- <div class="box-content"> -->
								<div class="invoice-info">
									
									<table >
										<tr>
			                              <td style="width:100%"><h4><center><b>Laporan Pembelian Produk Import <?php echo($jenisbelis); ?></b></center></h4></td>
			                              <td></td>
                    					</tr>
                    				</table>
                                    	<table>
									 	<tr>
		                                	 <th align="left">Bulan</th>
		                                	 <td style="width:50%;">:&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo(DateToIndo($bulan));?></b></td>
                                    	</tr>
                                    	<tr>
	                                     	 <th align="left">Tahun</th>
                              		      	 <td>:&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo $tahun;?></b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		                                     
                                    	</tr>
										
										</table>
                                  <br />
							  </div>
								<table class="table table-striped table-invoice">
									<thead>
										
										<tr>
											
											<th rowspan="2">No.</th>
											<th rowspan="2">No. Nota Timbang</th>
											<th rowspan="2">No. Truck/Kontainer</th>
											<th rowspan="2">Tanggal</th>
											<th rowspan="2">Kode Supplier</th>
											<th rowspan="2">Nama Supplier</th>
											<th rowspan="2">Kode Produk</th>
											<th rowspan="2">Nama Produk</th>
											<th colspan="2"><center>Kuantitas</center></th>
											<th colspan="51"><center>Hasil Quality Control</center></th>
											<th rowspan="2">Harga Standar</th>
											<th rowspan="2">Harga Menurut Perhitungan</th>
											<th rowspan="2">DPP</th>
											<th rowspan="2">PPN</th>
											<th rowspan="2">PPH</th>
											<th rowspan="2">Total</th>
											<th rowspan="2">No. Bukti Pembayaran</th>
										
										</tr>
										<tr>
											
											<th>Colly</th>
											<th>Tonase</th>
											<th>Kadar Air</th>
											<th>Mouldy</th>
											<th>Insect</th>
											<th>Biji Pipih</th>
											<th>Triple Bean</th>
											<th>Broken Besar</th>
											<th>Broken Kecil</th>
											<th>Sampah</th>
											<th>Slety</th>
											<th>Total Kotor</th>
											<th>BC1</th>
											<th>BC2</th>
											<th>BC3</th>
											<th>BC4</th>
											<th>BC5</th>
											<th>BC6</th>
											<th>Total BC</th>
											<th>BC Rata-Rata</th>
											<th>Tester/KA-Test SBY</th>
											<th>Tester/KA-Test Daerah</th>
											<th>Tester/KA-Test Beli</th>
											<th>Abu-Test SBY</th>
											<th>Abu-Test Daerah</th>
											<th>Abu-Test Beli</th>
											<th>AK-Test SBY</th>
											<th>AK-Test Daerah</th>
											<th>AK-Test Beli</th>
											<th>BM-Test SBY</th>
											<th>BM-Test Daerah</th>
											<th>BM-Test Beli</th>
											<th>PL-Test SBY</th>
											<th>PL-Test Daerah</th>
											<th>PL-Test Beli</th>
											<th>GG-Test SBY</th>
											<th>GG-Test Daerah</th>
											<th>GG-Test Beli</th>
											<th>Daun</th>
											<th>Polong</th>
											<th>Bean Count</th>
											<th>Biji Bagus</th>
											<th>Triase</th>
											<th>Biji Rusak</th>
											<th>Tongkol</th>
											<th>Biji Mati</th>
											<th>Wringkle</th>
											<th>Spotted</th>
											<th>Brown</th>
											<th>Oily</th>
											<th>Void</th>
											<th>Abu</th>
											<th>GG Besar</th>
											
											
										</tr>
										
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("SELECT t_qc.* , ttr_main_scale.id_timbang,suppliers.company_name,ttr_main_scale.type_beli,items.item_number,items.`name` 
FROM ttr_main_scale INNER JOIN t_qc on ttr_main_scale.id_timbang=t_qc.id_timbang
INNER JOIN suppliers on ttr_main_scale.supplier_id=suppliers.supplier_id
INNER JOIN items on ttr_main_scale.item_id = items.item_id
WHERE MONTH(ttr_main_scale.tanggal)='$bulan' and YEAR(ttr_main_scale.tanggal)='$tahun' and ttr_main_scale.jt='pembelian'");
									$no=1;
									$tqty_col=0;
									$tqty_ton=0;
									 
									while ($row=mysql_fetch_array($tampil))
									{// tes 2017-05-08
										$hargahitung=$row['qty_ton']*$row['grand_tot'];
									?>
										<tr>
											<td>&nbsp;<?php echo $no; ?></td>
											<td >&nbsp;<?php echo $row['id_npb']; ?></td>
											<td >&nbsp;<?php echo $row['tanggal']; ?></td>
											<td >&nbsp;<?php echo $row['id_timbang']; ?></td>
											<td >&nbsp;<?php echo $row['company_code']; ?></td>
											<td >&nbsp;<?php echo $row['company_name']; ?></td>
											<td >&nbsp;<?php echo $row['tipe_beli']; ?></td>
											<td >&nbsp;<?php echo $row['item_number']; ?></td>
											<td >&nbsp;<?php echo $row['name']; ?></td>
											<td >&nbsp;<?php echo number_format($row['qty_coly'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['qty_ton'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['kadar_air'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['mouldy'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['insect'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['biji_pipih'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['triple_bean'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['broken_b'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['broken_k'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['sampah'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['slety'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['fm'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['total_kotor'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bc1'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bc2'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bc3'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bc4'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bc5'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bc6'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['total_bc'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bc_rata'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['tester_ka_testsby'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['tester_ka_testdaerah'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['tester_ka_testbeli'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['abu_testsby'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['abu_testdaerah'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['abu_testbeli'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['ak_testsby'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['ak_testdaerah'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['ak_testbeli'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bm_testsby'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bm_testdaerah'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bm_testbeli'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['pl_testsby'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['pl_testdaerah'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['pl_testbeli'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['gg_testsby'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['gg_testdaerah'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['gg_testbeli'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['daun'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['polong'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['bean_count'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['biji_bagus'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['triase'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['biji_rusak'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['tongkol'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['biji_mati'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['wringkle'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['spotted'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['brown'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['oily'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['void'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['abu'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['gg'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['harga_standar'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['grand_tot'],0); ?></td>
											<td >&nbsp;<?php if($row['tipe_beli']=="Tunai"){echo number_format($hargahitung,0); }else{ echo "-"; } ?></td>
											<td >&nbsp;<?php if($row['tipe_beli']=="Kredit"){echo number_format($hargahitung,0); }else{ echo "-"; } ?></td>
											<td >&nbsp;<?php if($row['tipe_beli']=="Kontrak"){echo number_format($hargahitung,0); }else{ echo "-"; } ?></td>
										</tr>
									<?php 
									$tqty_col=$tqty_col+$row['qty_coly'];
									$tqty_ton=$tqty_ton+$row['qty_ton'];
									$no++;
									} 
									?>
									<tr>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td><strong>Total </strong></td>
										<td><strong><?php echo $tqty_col; ?></strong> </td>
										<td><strong><?php echo $tqty_ton; ?> </strong></td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
										<td> </td>
									<td><strong><?php echo $tqty_ton; ?> </strong></td>
									<td><strong><?php echo $tqty_ton; ?> </strong></td>
									<td><strong><?php echo $tqty_ton; ?> </strong></td>
									<td><strong><?php echo $tqty_ton; ?> </strong></td>
										
										<td> </td>

									</tr>
                                  
                                  
									</tbody>
								</table>
							<!-- </div> -->
						</div>
					</div>
					
				
				
				</div>
				
			
				
