<?php
$tglsekarang	= date('d-m-Y');

	?>
	   <script>
            //<![CDATA[
            $(window).load(function(){
            $("#supplier").on("change", function(){
                var nilai  = $("#supplier :selected").attr("data-alamat");
                var nilai2 = $("#supplier :selected").attr("data-ppn");
                $("#alamat").val(nilai);
                $("#ppn").val(nilai2);
            });
            $("#bank1").on("change", function(){
            	
                var nilai  = $("#bank1 :selected").attr("akun");
                var nilai1 = $("#bank1 :selected").attr("swift");
                var nilai2 = $("#bank1 :selected").attr("alamat");
                var nilai3 = $("#bank1 :selected").attr("iban");
                var nilai4 = $("#bank1 :selected").attr("remark");
                $("#ba1").val(nilai); 
                $("#swift_kode1").val(nilai1);
                $("#alamat_bank1").val(nilai2);
                $("#iban1").val(nilai3);

            });
            $("#bank2").on("change", function(){
            	
                var nilai  = $("#bank2 :selected").attr("akun");
                var nilai1 = $("#bank2 :selected").attr("swift");
                var nilai2 = $("#bank2 :selected").attr("alamat");
                var nilai3 = $("#bank2 :selected").attr("iban");
                var nilai4 = $("#bank2 :selected").attr("remark");
                $("#ba2").val(nilai); 
                $("#swift_kode2").val(nilai2);
                $("#alamat_bank2").val(nilai2);
                $("#iban2").val(nilai3);

            });
            });//]]>
            $(window).load(function(){
            $("#produk").on("change", function(){
                var nilai  = $("#produk :selected").attr("data-custom");
                $("#custom").val(nilai);
            });
            });//]]>
        </script>
<div class="modal hide fade modal-besar" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog ">
				<form action="build/build-skbi/insert-skbi.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Membuat Surat Kontrak ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Surat Kontrak Pembelian Import</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form Surat Kontrak Import</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">S/C Reference</label>
						  <div class="controls">
							<input type="text" name="reference" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Buyer</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="home" id="home" class='chosen-select'>
									<option value="">--Choice Buyer --</option>
										<?php 	
									$tampil=mysql_query("select * from home where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_home']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['nama']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier/Seller</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="supplier" id="supplier" class='chosen-select'>
									<option value="">--Choice Supplier --</option>
										<?php 	
									$tampil=mysql_query("select * from suppliers where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['supplier_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Product</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="produk" id="produk" class='chosen-select' onchange="jp2()">
									<option value="">--Pilih Produk--</option>
										<?php 	
									$tampil=mysql_query("select * from items where category ='produk utama'");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['item_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['name']; ?> - <?php echo $row['custom1']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						

						<table id="cklt" style="display: none;" class="items"> <!-- cokelat -->	
						<tr>
							<td width="30%">
								<div class="control-group">
									<label class="control-label">Kadar Air</label>
									<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?php echo $data1["kadar_air"] ?>">
									</div> 	
								</div>
								<div class="control-group">
									<label class="control-label">Mouldy</label>
									<div class="controls">
										<input type="text" class="form-control" name="Mouldy" placeholder="0"  value="<?php echo $data1['mouldy']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Insect</label>
									<div class="controls">
										<input type="text" class="form-control" name="Insect" placeholder="0"  value="<?php echo $data1['insect']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Biji Pipih</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="BijiPipih" placeholder="0"  value="<?php echo $data1['biji_pipih']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Triple Bean</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="TripleBean" placeholder="0"  value="<?php echo $data1['triple_bean']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Broken Besar</label>
									<div class="controls">
										<input type="text" class="form-control" name="BrokenBesar" placeholder="0"  value="<?php echo $data1['broken_b']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Broken Kecil</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="BrokenKecil" placeholder="0"  value="<?php echo $data1['broken_k']?>">
									</div>
									  								</div>
								<div class="control-group">
									<label class="control-label">Sampah</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="Sampah" placeholder="0"  value="<?php echo $data1['sampah']?>">
									</div>
									  	
								</div>

								<div class="control-group">
									<label class="control-label">Slety</label>
									<div class="controls">
										<input type="text" class="form-control" name="Slety" placeholder="0"  value="<?php echo $data1['slety']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">FM</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="FM" placeholder="0"  value="<?php echo $data1['fm']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Total Kotoran</label>
									<div class="controls">
										<input type="text" class="form-control"  placeholder="0"  value="<?php echo $data1['total_kotor']?>" id="TotalKotoran" name="TotalKotoran" readonly>
									</div>
									  	
								</div>

								<div class="control-group">
									<label class="control-label">BC </label>
									<div class="controls">
										<input type="text" class="form-control" id="TotalBC" name="TotalBC" placeholder="0"  value="<?php echo $data1['total_bc']?>" >
									</div>
									  	
								</div>	
							</td>
							
							
						</tr>
						
						</table>
				

				<div class="control-group"> <!-- cengkeh -->	
					<table border="1" id="cgkh" style="display: none;" class="items">	
						<tr >
								<th width="20%">Komponen</td>
								<th width="20%">Test SBY</td>
								<th width="20%">Test Daerah</td>
								<th width="20%">Test Beli</td>
								
						</tr>	
						<tr>	
								<td>	Tester KA </td>
								<td>	
											<input type="text" class="form-control" name="KA_Sby" placeholder="0"  value="<?php echo $data1['tester_ka_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="KA_Daerah" placeholder="0"  value="<?php echo $data1['tester_ka_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="KA_beli" placeholder="0"  value="<?php echo $data1['tester_ka_testbeli']?>">
								</td>
						</tr>
							
						<tr>	
								<td>	Abu </td>
								<td>	
											<input type="text" class="form-control" name="Abu_Sby" placeholder="0"  value="<?php echo $data1['abu_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="Abu_Daerah" placeholder="0"  value="<?php echo $data1['abu_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="Abu_beli" placeholder="0"  value="<?php echo $data1['abu_testbeli']?>">
								</td>
						</tr>
						<tr>	
								<td>	AK </td>
								<td>	
											<input type="text" class="form-control" name="Ak_Sby" placeholder="0"  value="<?php echo $data1['ak_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="Ak_Daerah" placeholder="0"  value="<?php echo $data1['ak_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="Ak_beli" placeholder="0"  value="<?php echo $data1['ak_testbeli']?>">
								</td>
						</tr>
						<tr>	
								<td>	 BM </td>
								<td>	
											<input type="text" class="form-control" name="BM_Sby" placeholder="0"  value="<?php echo $data1['bm_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="BM_Daerah" placeholder="0"  value="<?php echo $data1['bm_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="BM_beli" placeholder="0"  value="<?php echo $data1['bm_daerahsby']?>">
								</td>
						</tr>
						<tr>	
								<td>	PL </td>
								<td>	
											<input type="text" class="form-control" name="PL_Sby" placeholder="0"  value="<?php echo $data1['pl_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="PL_Daerah" placeholder="0"  value="<?php echo $data1['pl_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="PL_beli" placeholder="0"  value="<?php echo $data1['pl_testbeli']?>">
								</td>
						</tr>
						<tr>	
								<td>	GG </td>
								<td>	
											<input type="text" class="form-control" name="GG_Sby" placeholder="0"  value="<?php echo $data1['gg_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="GG_Daerah" placeholder="0"  value="<?php echo $data1['gg_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="GG_beli" placeholder="0"  value="<?php echo $data1['gg_testbeli']?>">
								</td>
						</tr>

					</table>	

			
				<table id="ggcgkh" style="display: none;" class="items">  <!-- gagangcengkeh -->	
					<tr>
						<td width="30%">
							<div class="control-group">
								<label class="control-label">Kadar Air</label>
								<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?=$data1["kadar_air"]?>" >
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Abu</label>
								<div class="controls">
										<input type="text" class="form-control  kotoran_gagang" onkeyup="kotoran_gagang()" name="Abu" placeholder="0"  value="<?php echo $data1['abu']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Daun</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()" name="Daun" placeholder="0"  value="<?php echo $data1['daun']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">BM</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()" name="BM" placeholder="0"  value="<?php echo $data1['biji_mati']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Polong</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()"name="polong" placeholder="0"  value="<?php echo $data1['polong']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">GG Besar</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()" name="GGBesar" placeholder="0"  value="<?php echo $data1['gg']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">FM</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()" name="FM" placeholder="0"  value="<?php echo $data1['fm']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Total Kotoran</label>
								<div class="controls">
										<input type="text" class="form-control "  id ="Total_kotoran_gagang" name="TotalKotoran" placeholder="0"  value="<?php echo $data1['total_kotor']?>" readonly>
								</div> 	
							</div>
						</td>
					</tr>

				</table>
			
				<table id="kp" style="display: none;" class="items"> <!-- kopi -->	
					<tr>
						<td width="30%">
							<div class="control-group">
								<label class="control-label">Kadar Air</label>
								<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?=$data1["kadar_air"]?>" >
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Biji Rusak</label>
								<div class="controls">
										<input type="text" class="form-control  kotoran_kopi" onkeyup="kotoran_kopi()" name="BijiRusak" placeholder="0"  value="<?php echo $data1['biji_rusak']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Mouldy</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_kopi" onkeyup="kotoran_kopi()" name="Mouldy" placeholder="0"  value="<?php echo $data1['mouldy']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Biji Bagus</label>
								<div class="controls">
										<input type="text" class="form-control " onkeyup="kotoran_kopi()" name="BijiBagus" placeholder="0"  value="<?php echo $data1['biji_bagus']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Triase</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_kopi" onkeyup="kotoran_kopi()" name="Triase" placeholder="0"  value="<?php echo $data1['triase']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Bean Count</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_kopi" onkeyup="kotoran_kopi()" name="BeanCount" placeholder="0"  value="<?php echo $data1['bean_count']?>">
								</div> 	
							</div>
							
							<div class="control-group">
								<label class="control-label">Total Kotoran</label>
								<div class="controls">
										<input type="text" class="form-control "  id ="Total_kotoran_kopi" name="TotalKotoran" placeholder="0"  value="<?php echo $data1['total_kotor']?>" readonly>
								</div> 	
							</div>
						</td>
					</tr>

				</table>
			
				<table id="jg" style="display: none;" class="items"> <!-- jagung -->	
					<tr>
						<td width="30%">
							<div class="control-group">
								<label class="control-label">Kadar Air</label>
								<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?=$data1["kadar_air"]?>" >
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Tongkol</label>
								<div class="controls">
										<input type="text" class="form-control  "  name="Tongkol" placeholder="0"  value="<?php echo $data1['tongkol']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Mouldy</label>
								<div class="controls">
										<input type="text" class="form-control" name="Mouldy" placeholder="0"  value="<?php echo $data1['mouldy']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Biji Mati</label>
								<div class="controls">
										<input type="text" class="form-control "  name="BijiMati" placeholder="0"  value="<?php echo $data1['biji_mati']?>">
								</div> 	
							</div>
							
						</td>
					</tr>

				</table>
			
				<table id="mt" style="display: none;" class="items"> <!-- mente -->	
					<tr>
						<td width="30%">
							<div class="control-group">
								<label class="control-label">Kadar Air</label>
								<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?=$data1["kadar_air"]?>" >
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Bean Count</label>
								<div class="controls">
										<input type="text" class="form-control " name="BeanCount" placeholder="0"  value="<?php echo $data1['bean_count']?>">
								</div> 	
							</div>
							
							<div class="control-group">
								<label class="control-label">Wringkle</label>
								<div class="controls">
										<input type="text" class="form-control"   name="Wringkle" placeholder="0"  value="<?php echo $data1['wringkle']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Spotted</label>
								<div class="controls">
										<input type="text" class="form-control " name="Spotted" placeholder="0"  value="<?php echo $data1['spotted']?>">
								</div> 	
							</div>
						
							<div class="control-group">
								<label class="control-label">Brown</label>
								<div class="controls">
										<input type="text" class="form-control " name="Brown" placeholder="0"  value="<?php echo $data1['brown']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Oily</label>
								<div class="controls">
										<input type="text" class="form-control " name="Oily" placeholder="0"  value="<?php echo $data1['oily']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Void</label>
								<div class="controls">
										<input type="text" class="form-control " name="Void" placeholder="0"  value="<?php echo $data1['void']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Biji Bagus</label>
								<div class="controls">
										<input type="text" class="form-control "  name="BijiBagus" placeholder="0"  value="<?php echo $data1['biji_bagus']?>">
								</div> 	
							</div>
						</td>
					</tr>

				</table>
				</div>

						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Country Of Origin</label>
						  <div class="controls">
							<input type="text" name="coo" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Quality Spesification</label>
						  <div class="controls">
							<textarea name="qs" id="qs" class="input-block-level">OUTTURN DISCOUNT (47 LBS MIN) : USD  43.75 /MT  FOR EVERY 1.00 LBS  SHORTFALL 
NUT COUNT DISCOUNT : USD 10.50/MT   FOR EVERY NUT 10 COUNTS EXCESS.  
MOISTURE DISCOUNT : USD 21.00 /MT FOR EVERY 1% EXCESS. FOR PART ON PRO –RATE.
</textarea>
						  </div>
						</div>

						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Quantity</label>
						  <div class="controls">
							<input type="text" name="qty" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Due Date</label>
						  <div class="controls">
							<input type="text" name="dd" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Price</label>
						  <div class="controls">
							<input type="text" name="price" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Total Price</label>
						  <div class="controls">
							<input type="text" name="tp" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Inco Term</label>
						  <div class="controls">
							<input type="text" name="it" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Value</label>
						  <div class="controls">
							<input type="text" name="valuee" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Qualities Discount</label>
						  <div class="controls">
							<textarea name="qd" id="qd" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Rejection Clause</label>
						  <div class="controls">
							<textarea name="rc" id="rc" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Insurance</label>
						  <div class="controls">
							<input type="text" name="insurance" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Packing</label>
						  <div class="controls">
							<input type="text" name="pack" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Shipment</label>
						  <div class="controls">
							<input type="text" name="shipment" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Term Of Shipment</label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Port Of Loading</label>
						  <div class="controls">
							<input type="text" name="pol" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Port Of Discharge</label>
						  <div class="controls">
							<input type="text" name="pod" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Term Of Payment</label>
						  <div class="controls">
							<textarea name="top" id="top" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Seller Bank Name 1</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="bank1" id="bank1" class='chosen-select'>
									<option value="">--Choice Bank Name --</option>
									<?php 	
									$tampil=mysql_query("select * from bank ");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_bank']; ?>"  akun="<?php echo $row['akun_bank']; ?>" swift="<?php echo $row['swift_kode']; ?>" alamat="<?php echo $row['alamat_bank']; ?>" iban="<?php echo $row['iban']; ?>" remark="<?php echo $row['remark']; ?>"><?php echo $row['nama_bank']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Bank Account 1</label>
						  <div class="controls">
							<input type="text" name="ba1" id="ba1" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Alamat 1</label>
						  <div class="controls">
							<input type="text" name="alamat_bank1" id="alamat_bank1" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Swift Kode 1</label>
						  <div class="controls">
							<input type="text" name="swift_kode1" id="swift_kode1" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">IBAN 1</label>
						  <div class="controls">
							<input type="text" name="iban1" id="iban1" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Seller Bank Name 2</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="bank2" id="bank2" class='chosen-select'>
									<option value="">--Choice Bank Name --</option>
									<?php 	
									$tampil=mysql_query("select * from bank ");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_bank']; ?>"  akun="<?php echo $row['akun_bank']; ?>" swift="<?php echo $row['swift_kode']; ?>" alamat="<?php echo $row['alamat_bank']; ?>" iban="<?php echo $row['iban']; ?>" remark="<?php echo $row['remark']; ?>"><?php echo $row['nama_bank']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Bank Account 2</label>
						  <div class="controls">
							<input type="text" name="ba2" id="ba2" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Alamat 2</label>
						  <div class="controls">
							<input type="text" name="alamat_bank2" id="alamat_bank2" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Swift Kode 2</label>
						  <div class="controls">
							<input type="text" name="swift_kode2" id="swift_kode2" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">IBAN 2</label>
						  <div class="controls">
							<input type="text" name="iban2" id="iban2" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Document</label>
						  <div class="controls">
							<textarea name="doc" id="doc" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Additional</label>
						  <div class="controls">
							<textarea name="add" id="add" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Force Majeure</label>
						  <div class="controls">
							<textarea name="fm" id="fm" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Arbitration and Governing Law </label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level"></textarea>
						  </div>
						</div> 
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Contract Validity</label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level">This contract begins to have effect on the parties on the date of signature through facsimile/email.</textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Others Terms and Conditions</label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Remarks</label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level"></textarea>
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Surat Kontrak Import</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=skbi&navbar=skbi&parent=pembelian">Surat Kontrak Pembelian Import </a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Surat Kontrak Pembelian Import</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Surat Kontrak Pembelian Import
								</h3>
					  </div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No.</th>
											<th>No. Surat Kontrak</th>
											<th>Tanggal</th>
											<th>Produk</th>
											<th>Supplier</th>
											<th>Bank Name</th>
											<!-- <th>Tipe Kontrak</th> -->
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select t_skbi.id_skbi,t_skbi.tanggal,items.name,suppliers.company_name,home.nama as perusahaan
FROM t_skbi left OUTER JOIN items on t_skbi.item_id=items.item_id left OUTER JOIN suppliers on t_skbi.supplier_id = suppliers.supplier_id left OUTER JOIN home on t_skbi.id_home=home.id_home");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_skbi']; ?></td>
											<td><?php echo $row['tanggal']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['perusahaan']; ?></td>
											<!-- <td><?php echo $row['type_kontrak']; ?></td> -->
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<li>
													<a id="edit-skb" data-id="<?php echo $row['id_skbi'];?>" role="button" data-toggle="modal">Update Surat Kontrak Import</a>
												</li>
												<?php
												if($row['status']=="1")
												{ 

												}
												else{
												?>
												<li>
													<a href="dash.php?hp=rblk&idbaru=<?php echo $row['id_skb']; ?>&navbar=skbi&parent=pembelian">Rekap BL Kontrak</a>
												</li>
													<?php
													}?> 
																									
													
												
													
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-skb',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-skbi/edit-skbi.php',
							{id:$(this).attr('data-id'), id2:$(this).attr('data-id2')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});

		
				
	</script>
	<script type="text/javascript">

			$("#npd").hide();
   				$("#usd").hide();
			$("#kurs").hide();
			$("#nfh").hide();
			$("#hkurs").hide();
			$("#husd").hide();
			$("#hpas").hide();
			//alert("Sas");
			
			$( document ).ready(function() {
   				
			});
			function coba(){
				var	isine=$("#custom").val();
				alert(isine);
			}
			function jp() {
   				var tt = $("#tk").val();	

   				if(tt =='Lokal'){
   				//alert();-->untuk menampilkan popups
   					// $("#jpn").show();
					$("#usd").hide();	
					$("#kurs").hide();
					$("#nfh").hide();
					$("#npd").hide();
			$("#hpas").hide();

					$("#husd").hide();
					$("#hkurs").show();
   					$("#husd").val(" ");
   					$("#usd").val(" ");
   					$("#kurs").val(" ");
   					$("#nfh").val(" ");

   				}
   				else if(tt =='Import USD'){
   				//alert();-->untuk menampilkan popups
   					$("#usd").show();	
   					$("#husd").hide();	
					$("#kurs").hide();
					$("#npd").hide();
			$("#hpas").hide();

					$("#hkurs").hide();
					$("#nfh").hide();
   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#nfh").val(" ");

   					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					$('#vkon').val(vusd);
					});
   				}
   				else if(tt =='Import USD Kurs'){
   				//alert();-->untuk menampilkan popups
   					$("#usd").show();	
   					$("#husd").hide();	
   					$("#hkurs").hide();	
					$("#npd").hide();
			$("#hpas").hide();

					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vkurs = $("#vkurs").val();
  					nilai = vusd * vkurs;
  					$('#vkon').val(nilai);
					});
					$( "#vkurs" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vkurs = $("#vkurs").val();
  					nilai = vusd * vkurs;
  					$('#vkon').val(nilai);
					});

					$("#kurs").show();
					$("#nfh").hide();
   					$("#nfh").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   				}
   				else if(tt =='Forward Hedging'){
   				//alert();-->untuk menampilkan popups

   					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vfh = $("#vfh").val();
  					nilai = vusd * vfh;
  					$('#vkon').val(nilai);
					});
					$( "#vfh" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vfh = $("#vfh").val();
  					nilai = vusd * vfh;
  					$('#vkon').val(nilai);
					});

   					$("#usd").show();	
					$("#kurs").hide();
					$("#nfh").show();
					$("#husd").hide();
					$("#hkurs").hide();
					$("#npd").hide();
			$("#hpas").hide();

   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#kurs").val(" ");
					$("#npd").hide();

   				}
   				else if(tt =='Premium Differential'){
   					$( "#vkurs" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});

   					$( "#vpd" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});

					$( "#vpas" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});



					$("#npd").show();
   					$("#usd").hide();	
					$("#kurs").show();
					$("#nfh").hide();
					$("#husd").hide();
					$("#hkurs").hide();
			$("#hpas").show();

   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#kurs").val(" ");
   				}
   				else{
					$("#usd").hide();
					$("#npd").hide();

					$("#kurs").hide();
					$("#nfh").hide();
					$("#husd").hide();
					$("#hkurs").hide();
   					$("#usd").val(" ");
   					$("#kurs").val(" ");
   					$("#nfh").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   				}
   				
			};
			
		

		$( document ).ready(function() {
   				
			});
        //<![CDATA[
     function jp2() {
     	// var nilai  = $("#produk:selected").attr("data-cat");
   			//	var tt = $("#produk").val();	
   			var custom1 = $("#produk :selected").attr("data-cat");	
   				custom1 = "#"+custom1;
   				// alert(custom1);
   				
     	$(".items").hide();
   				$(custom1).show();
				// alert(nilai);
   				
   				
			};

		</script>
		
		

		
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Surat Kontrak Pembelian Import</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>