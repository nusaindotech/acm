<?php
	$tglsekarang	= date('d-m-Y');
?>	

	<script>
        //<![CDATA[
        $(window).load(function(){
        $("#mr").on("change", function(){
            var nilai  = $("#mr :selected").attr("data-kp");
            var nilai2  = $("#mr :selected").attr("data-supplier");
            var nilai3  = $("#mr :selected").attr("data-as");
            var nilai4  = $("#mr :selected").attr("data-sid");
            $("#kp").val(nilai);
            $("#supplier").val(nilai2);
            $("#as").val(nilai3);
            $("#sid").val(nilai4);
        });
        });//]]>
    </script>
	
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-sjri/insert-sjri.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Surat Jalan Retur Import ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Surat Jalan Retur Import</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Memo Retur Pembelian Import</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="mr" id="mr" class='chosen-select' required=="required">
									<option value="">--Pilih No. Memo Retur Pembelian Import--</option>
									<?php 	
									$tampil=mysql_query("SELECT mr.id_mr, mr.kategori, mr.supplier_id,suppliers.company_address,suppliers.company_name FROM mr mr, suppliers suppliers where suppliers.supplier_id = mr.supplier_id ORDER BY id_mr DESC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_mr']; ?>" data-kp="<?php echo $row['kategori']; ?>" data-supplier="<?php echo $row['company_name']; ?>" data-as="<?php echo $row['company_address']; ?>" data-sid="<?php echo $row['supplier_id']; ?>"><?php echo $row['id_mr']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<input type="hidden" name="sid" id="sid" class="form-control" readonly>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kategori Pembelian</label>
						  <div class="controls">
							<input type="text" name="kp" id="kp" class="form-control" placeholder="Kategori Pembelian" readonly>
						  </div>
						</div>
						
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
						  <div class="controls">
							<input type="text" name="supplier" id="supplier" class="form-control" placeholder="Supplier" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
						  <div class="controls">
							<input type="text" name="as" id="as" class="form-control" placeholder="Alamat Supplier" readonly>
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Surat Jalan Retur Import </button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Pembelian</a>
						</li>
						<li>
							<a href="dash.php?hp=sjri&navbar=sjri&parent=pembelian">Surat Jalan Retur Import</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Surat Jalan Retur Import</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Surat Jalan Retur Import
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable">
									<thead>
										<tr class='thefilter'>
											<th>No.</th>
											<th>No. Surat Jalan Retur Import</th>
											<th>Tanggal</th>
											<th>Supplier</th>
											<th>No. Memo Retur Pembelian Import</th>
											<th>Kategori Pembelian</th>
											<th>Status</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("SELECT a.id_sjr,a.tanggal,b.company_name,a.id_mr,a.kategori,a.status from sjr a, suppliers b where a.supplier_id=b.supplier_id and a.deleted=0 order by id_sjr Desc");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_sjr']; ?></td>
											<td><?php echo dateBahasaIndo($row['tanggal']); ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['id_mr']; ?></td>
											<td><?php echo $row['kategori']; ?></td>
											<td>
											<?php
											if($row['status']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else
											{
												echo "<span class='label label-success'>Selesai</span>";
											}
											?>
											</td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<li>
													<a id="edit-sjr" data-id="<?php echo $row['id_sjr'];?>" role="button" data-toggle="modal">Update Surat Jalan Retur Import</a>
												</li>
												<?php
												if($row['status']==0)
												{
												?>
												<li>
													<a href="dash.php?hp=sjri2&idbaru=<?php echo $row['id_sjr']; ?>&navbar=sjri&parent=pembelian">Revisi Surat Jalan Retur Import</a>
												</li>
												<?php
												}
												else
												{?>
													<li>
														<a href="dash.php?hp=sjr3&idbaru=<?php echo $row['id_sjr']; ?>&navbar=sjr&parent=pembelian">Cetak Surat Jalan Retur Import</a>
													</li>
												<?php
												}
												?>
													
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-sjr',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-sjri/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Update Surat Jalan Retur Import</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>