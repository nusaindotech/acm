<?php
	$tglsekarang	= date('d-m-Y');
?>	
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-ibl/insert-ibl.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat BL</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. BL</label>
						  <div class="col-sm-10">
							<input type="text" name="id_bl" class="form-control" placeholder="Auto" required="require" readonly>
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="col-sm-10">
							<input type="text" name="tanggal" class="form-control" placeholder="Tanggal" value="<?php echo $tglsekarang; ?>" required="require" readonly>
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Kontainer</label>
						  <div class="col-sm-10">
							<input type="text" name="kontain" class="form-control" placeholder="Kontainer" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Seal</label>
						  <div class="col-sm-10">
							<input type="text" name="seal" class="form-control" placeholder="No. Seal" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Colly</label>
						  <div class="col-sm-10">
							<input type="text" name="col" class="form-control" placeholder="Colly" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tonase</label>
						  <div class="col-sm-10">
							<input type="text" name="ton" class="form-control" placeholder="Tonase" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Timbangan Tonase</label>
						  <div class="col-sm-10">
							<input type="text" name="tton" class="form-control" placeholder="Timbangan Tonase" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Barang Datang</label>
						  <div class="col-sm-10">
					<input type="text" name="tanggalBarangDatang" id= "tanggalBarangDatang" placeholder="Tanggal" class="input-medium datepick" value="<?php echo $formattanggal;?>" required=="required">
						</div>
						</div>
						<br>
						<br>
						<br>
						<br>
						<br>
					  </div>
					  </div>

					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=ibl&navbar=ibl&parent=master">BL</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat BL</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar BL
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable">
									<thead>
										<tr class='thefilter'>
											<th>No.</th>
											<th>No. BL</th>
											<th>Tanggal</th>
											<th>No. Kontainer</th>
											<th>No. Seal</th>
											<th>Colly</th>
											<th>Tonase</th>
											<th>Timbangan Tonase</th>
											<th>Tanggal Barang Datang</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("SELECT * from t_ibl where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_ibl']; ?></td>
											<td><?php echo dateBahasaIndo($row['tanggal']); ?></td>
											<td><?php echo $row['kontain']; ?></td>
											<td><?php echo $row['seal']; ?></td>
											<td><?php echo $row['col']; ?></td>
											<td><?php echo $row['ton']; ?></td>
											<td><?php echo $row['tton']; ?></td>
											<td><?php echo dateBahasaIndo($row['tgl_barangdatang']); ?></td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<li>
													<a id="edit-ibl" data-id="<?php echo $row['id_ibl'];?>" role="button" data-toggle="modal">Update BL</a>
												</li>
												<?php
												if($row['status']=="1")
												{ 

												}
												else{
												?>
												<li>
													<a href="dash.php?hp=ibl2&idbaru=<?php echo $row['id_ibl']; ?>&navbar=ibl&parent=pembelian">Revisi BL</a>
												</li>
													<?php
													}?> 
																									
													
												
													
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-ibl',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-ibl/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data ibl</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>