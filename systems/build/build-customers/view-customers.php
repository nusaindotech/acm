<?php 	
	$tglsekarang	= date('Y-m-d');

 ?>
			<script type="text/javascript">
   $(function() {
     $( "#input" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
   });
</script>
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-customers/insert-customers.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Simpan Data konsumen ?');">
				
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah konsumen</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
					  <div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Masuk</label>
						  <div class="controls">
							<input type="text" id="input" name="tanggal" value="<?= dateBahasaIndo($tglsekarang)?>" class="form-control datepicker" disabled="" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kode Konsumen</label>
						  <div class="controls">
							<input type="text" name="kodecustomers" class="form-control" placeholder="Kode konsumen" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Konsumen</label>
						  <div class="controls">
							<input type="text" name="namacustomers" class="form-control" placeholder="Nama konsumen" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Cara Pembayaran</label>
						  <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                              <td><input type="radio" class='icheck-me' value="Tunai" name="pembayaran" data-skin="square" data-color="blue" > <label>Tunai</label></td>
                     		   <td><input type="radio" class='icheck-me' value="Kredit" name="pembayaran" data-skin="square" data-color="blue" > <label>Kredit</label></td>
                     		   <td><input type="radio" class='icheck-me' value="Kontrak" name="pembayaran" data-skin="square" data-color="blue" > <label>Kontrak</label></td>
                              </tr>
                            </table>
						  </div>
						</div>
						
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jatuh Tempo</label>
							<div class="controls">
							<input type="text" name="duedate" id="textfield" placeholder="Jatuh Tempo" class="input-medium datepick" >
							</div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">PPN</label>
						  <div class="controls">
							<input type="text" name="ppn" class="form-control" placeholder="PPN" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
						  <div class="controls">
							<input type="text" name="alamat" class="form-control" placeholder="Alamat konsumen" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Contact Person</label>
						  <div class="controls">
							<input type="text" name="cp" class="form-control" placeholder="Contact Person" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Telepon</label>
						  <div class="controls">
							<input type="text" name="telepon" class="form-control" placeholder="Telepon" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. NPWP</label>
						  <div class="controls">
							<input type="text" name="npwp" class="form-control" placeholder="No. NPWP" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. SPPKP</label>
						  <div class="controls">
							<input type="text" name="sppkp" class="form-control" placeholder="No. SPPKP" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
						  <div class="controls">
							<input type="text" name="keterangan" class="form-control" placeholder="Keterangan" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
										
					</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=konsumen&navbar=konsumen&parent=master">konsumen</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah konsumen</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar konsumen
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Date In</th>
											<th>Kode Konsumen</th>
											<th>Nama Konsumen</th>
											<th>Cara Pembayaran</th>
											<th>Jatuh Tempo</th>
											<th>PPN</th>
											<th>Alamat</th>
											<th>Contact Person</th>
											<th>Telepon</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select * from customers
											where acc=1 and deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['date_in']; ?></td>
											<td><?php echo $row['company_code']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['pembayaran']; ?></td>
											<td><?php echo $row['due_date']; ?></td>
											<td><?php echo $row['ppn']; ?></td>
											<td><?php echo $row['company_address']; ?></td>
											<td><?php echo $row['contact_person']; ?></td>
											<td><?php echo $row['company_phone']; ?></td>
																						
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-konsumen" data-id="<?php echo $row['customer_id'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-customers/delete-customers.php?idcustomers=<?php echo $row['cust_id']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-konsumen',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-customers/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
  $( function() {
  	$.fn.datepicker.defaults.format = "yyyy-mm-dd";
    $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    
	});
  } );
				
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data konsumen</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>