	<?php 
	error_reporting(0);
		include "../../../auth/autho.php";
		$id=$_POST['id'];
		$query = mysql_query("select * from customers where customer_id='$id'") or die(mysql_error());
		$data = mysql_fetch_array($query);

		$formattanggal	= date('d-m-Y',strtotime($data['date_in']));
		$duedatetanggal	= date('d-m-Y',strtotime($data['due_date']));
	?>

	<div class="modal-body">
		<form role="form" action="build/build-customers/update-customers.php" method="POST" class='form-horizontal ' onsubmit="return confirm('Anda Yakin Mengubah Data ?');">
			<input type="hidden" value="<?php echo $id;?>" name="id_customers">
			<div class="control-group">
				<label for="textfield" class="control-label">Tanggal Masuk</label>
				<div class="controls">
					<input type="text" name="tanggal" id="textfield" class="input-medium datepick" value="<?php echo $formattanggal;?>" readonly>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Kode Konsumen</label>
				<div class="controls">
					<input type="text" id="textfield" class="input-xlarge" name="kodecustomers" placeholder="Isikan Kode Pelanggan" value="<?php echo $data['company_code'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama Konsumen</label>
				<div class="controls">
					<input type="text" name="namacustomers" id="textfield" placeholder="Nama Konsumen" class="input-xlarge" required="require" value="<?php echo $data['company_name'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Cara Pembayaran</label>
				  <div class="controls">
				    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                      <td>
						<input type="radio" class='icheck-me' value="Tunai" name="pembayaran" data-skin="square" data-color="blue" <?php if($data['pembayaran']=='Tunai'){echo "checked";}?>> <label>Tunai</label></td>

             		   <td><input type="radio" class='icheck-me' value="Kredit" name="pembayaran" data-skin="square" data-color="blue" <?php if($data['pembayaran']=='Kredit'){echo "checked";}?> > <label>Kredit</label></td>
             		   <td><input type="radio" class='icheck-me' value="Kontrak" name="pembayaran" data-skin="square" data-color="blue" <?php if($data['pembayaran']=='Kontrak'){echo "checked";}?>> <label>Kontrak</label></td>
                      </tr>
                    </table>
				  </div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Jatuh Tempo</label>
				<div class="controls">
					<input type="text" name="duedate" id="textfield" class="input-medium datepick" value="<?php echo $duedatetanggal;?>">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">PPN</label>
				<div class="controls">
					<input type="text" name="ppn" id="textfield" placeholder="PPN" class="input-xlarge" value="<?php echo $data['ppn'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Alamat</label>
				<div class="controls">
					<input type="text" name="alamat" id="textfield" placeholder="Alamat Konsumen" class="input-xlarge" value="<?php echo $data['company_address'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Contact Person</label>
				<div class="controls">
					<input type="text" name="cp" id="textfield" placeholder="Contact Person" class="input-xlarge" value="<?php echo $data['contact_person'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Telepon</label>
				<div class="controls">
					<input type="text" name="telepon" id="textfield" placeholder="Telepon Konsumen" class="input-xlarge" value="<?php echo $data['company_phone'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">No. NPWP</label>
				<div class="controls">
					<input type="text" name="npwp" id="textfield" placeholder="No. NPWP" class="input-xlarge" value="<?php echo $data['no_npwp'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">No. SPPKP</label>
				<div class="controls">
					<input type="text" name="sppkp" id="textfield" placeholder="No. SPPKP" class="input-xlarge" value="<?php echo $data['sppkp'];?>" autocomplete="off">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>