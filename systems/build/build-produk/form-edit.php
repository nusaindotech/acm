	<?php 
		include "../../../auth/autho.php";
		$id=$_POST['id'];
		$query = mysql_query("select * from tb_produk where id_produk='$id'") or die(mysql_error());
		$data = mysql_fetch_array($query);
	?>
	<div class="modal-body">
		<form role="form" action="build/build-produk/update-produk.php" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped' onsubmit="return confirm('Anda Yakin Mengubah Data ?');">
			<input type="hidden" value="<?php echo $id;?>" name="idproduk">
			
			<div class="control-group">
				<label class="control-label">Kode</label>
				<div class="controls">
					<input type="text" id="textfield" class="input-xlarge" name="kode" placeholder="Isikan Kode Produk" required="require" value="<?php echo $data['id_produk'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama</label>
				<div class="controls">
					<input type="text" name="nama" id="textfield" placeholder="Nama Produk" class="input-xlarge" required="require" value="<?php echo $data['nama_produk'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Satuan</label>
				<div class="controls">
					<input type="text" name="satuan" id="textfield" placeholder="Satuan Produk" class="input-xlarge" value="<?php echo $data['satuan'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Harga Satuan</label>
				<div class="controls">
					<input type="text" name="harga" id="textfield" placeholder="Harga Satuan" class="input-xlarge" value="<?php echo $data['harga_satuan'];?>" autocomplete="off">
				</div>
			</div>
			
			<div class="control-group">
				<label for="textfield" class="control-label">Harga Jual</label>
				<div class="controls">
					<input type="text" name="harga_jual" id="textfield" placeholder="Harga Jual" class="input-xlarge" value="<?php echo $data['harga_jual'];?>" autocomplete="off">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>