<?php
	$tglsekarang	= date('d-m-Y');
?>			
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-spp/insert-spp1.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Membuat Permintaan Pembelian Barang ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Permintaan Pembelian Barang Barang</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No Form Permintaan Pembelian Barang Barang</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Kategori</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="kategori" id="kategori" class='chosen-select' required=="required" onchange="jp()">
									<option value="">--Pilih Kategori--</option>
									<option value="Barang Umum">Barang Umum</option>
									<!-- <option value="Jasa">Jasa</option> -->
									<option value="Aset Tetap">Aset Tetap</option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<!-- <div class="control-group" id="JPU">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis Barang</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="jenis" class='chosen-select' required=="required" >
									<option value=" - ">--Jenis Barang--</option>
									<option value="Lokal">Lokal</option>
									<option value="Import">Import</option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div> -->
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Pemohon</label>
						  <div class="controls">
							<input type="text" name="pemohon" class="form-control" placeholder="Pemohon" autocomplete="off" required=="required" value="<?=$_SESSION['nama_lengkap']?>" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Divisi</label>
						  <div class="controls">
							<input type="text" name="divisi" class="form-control" placeholder="Divisi" autocomplete="off" required=="required" value="<?=$_SESSION[bagian]?>" readonly>
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Permintaan Pembelian Barang</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
			<div class="modal hide fade" id="myModal2" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-po/insert-po1.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Pesanan Pembelian Barang ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Pesanan Pembelian Barang</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No Permintaan</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Permintaan Penjualan</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="ps" id="ps" class='chosen-select' required=="required">
									<option value="">--Pilih Id Permintaan Pembelian Barang--</option>
									<?php 	
									$tampil=mysql_query("select id_spp from spp
											where deleted=0 and acc=1 order by id_spp ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_spp']; ?>"><?php echo $row['id_spp']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="supplier" id="supplier" class='chosen-select' required=="required">
									<option value="">--Pilih Supplier--</option>
									<?php 	
									$tampil=mysql_query("select supplier_id, company_name from suppliers
											where deleted=0 order by company_name ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['supplier_id']; ?>"><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Pesanan Pembelian Barang</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Pembelian</a>
						</li>
						<li>
							<a href="dash.php?hp=spp1&navbar=spp&parent=pembelian">Permintaan Pembelian Barang</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Permintaan Pembelian Barang</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Permintaan Pembelian Barang
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>No. Permintaan Pembelian Barang</th>
											<th>Tanggal</th>
											<th>Pemohon</th>
											<th>Divisi</th>
											<th>Kategori</th>
											<!-- <th>Jenis Barang</th> -->
											<th>Status</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
										$j="Jasa";								
									$tampil=mysql_query("select * from spp
											where deleted=0 and custom1 NOT LIKE '%$j%' order by id_spp DESC");
									$tt=mysql_query("select cs3 from spp where id_spp='$idsppne'");	
									$row2	= mysql_fetch_array($tt);
									$bpo=$row2['cs3'];
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_spp']; ?></td>
											<td><?php echo dateBahasaIndo($row['date_transaction']); ?></td>
											<td><?php echo $row['person_request']; ?></td>
											<td><?php echo $row['custom2']; ?></td>
											<td><?php echo $row['custom1']; ?></td>
											<td>
											<?php
											if($row['status']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else
											{
												echo "<span class='label label-success'>Selesai</span>";
											}
											?>
											</td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<li>
													<a id="edit-spp" data-id="<?php echo $row['id_spp'];?>" role="button" data-toggle="modal">Update Permintaan Pembelian Barang</a>
												</li>
												<?php
												if($row['status']==0)
												{
												?>
													<!-- <?php
													if($row['custom1']=="Jasa"){?><li>
														<a href="dash.php?hp=spp2j&idbaru=<?php echo $row['id_spp']; ?>&navbar=spp&parent=pembelian">Revisi Permintaan Pembelian Barang</a>
													</li>
													<?php
														} else
														{?>  -->
													<li>
														<a href="dash.php?hp=spp2&idbaru=<?php echo $row['id_spp']; ?>&navbar=spp&parent=pembelian">Revisi Permintaan Pembelian Barang</a>
													</li>
													<?php
													}?> 
																									
													
												<?php
												}
												
												else
												{?>
													
													<li>
														<a href="dash.php?hp=spp3&idbaru=<?php echo $row['id_spp']; ?>&navbar=spp&parent=pembelian">Cetak Permintaan Pembelian Barang</a>
													</li>
													<?php
													if ($bpo==0){
														$id_spp=$row['id_spp'];
														$ceks=mysql_query("select count(b.cs3) as jumlah , COUNT(case b.cs3 when 1  then 1 end) as jumlah_kepakai,b.id_spp from detail_spp as b where id_spp ='$id_spp'");
										$cek=mysql_fetch_array($ceks);
										if ($cek['jumlah_kepakai']<$cek['jumlah']) {
														?>

													<li>
														<a id="buat-po" data-id="<?php echo $row['id_spp'];?>" role="button" data-toggle="modal">Buat Pesanan Pembelian Barang</a>
													</li>

													<?php
													}

												}
													
												}
												?>
													
													
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-spp',function(e){
						e.preventDefault();
						$("#view2").modal('show');
						$.post('build/build-spp/form-editspp.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body3").html(html);
							}  
						);
					});
				});
	</script>
	<script>
				$(function(){
					$(document).on('click','#buat-po',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-spp/form-editpo.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
		<script type="text/javascript">
			$("#JPU").hide();
			$( document ).ready(function() {
   				
   				
			});
			function jp() {
   				var tt = $("#kategori").val();	

   				if(tt =='Barang Umum'){
   				//alert();-->untuk menampilkan popups
   					$("#JPU").show();
   				
   				}else if (tt=='Aset Tetap'){
   					$("#JPU").show();
   				}
   				else{
					$("#JPU").hide();
   					$("#JPU").val(" ");
   				}
   				
			};

		</script>
	<div class="modal hide fade" id="view2" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Update Permintaan Pembelian Barang</h4>
				</div>
				<div class="modal-body3">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
		<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Buat Pesanan Pembelian Barang</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
	