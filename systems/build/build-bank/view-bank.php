<script>
        //<![CDATA[
        $(window).load(function(){
        $("#nm_penerima").on("change", function(){
            var nilai  = $("#nm_penerima :selected").attr("data-code");
            $("#c_code").val(nilai);
        });
        });//]]>
    </script>
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-bank/insert-bank.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Bank</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Penerima</label>
                          <div class="col-sm-10">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="nm_penerima" id="nm_penerima" class='chosen-select' required="required">
									<option value="">--Pilih Penerima--</option>
									<?php 	
									$tampil=mysql_query("SELECT company_code, company_name from customers");
									$tampil2=mysql_query("SELECT company_code, company_name from suppliers");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option data-code="<?php echo $row['company_code']; ?>"><?php echo $row['company_name'];?> (customers)</option>
									<?php 
									$no++;
									} 
									while ($row=mysql_fetch_array($tampil2))
									{
									?>
									<option data-code="<?php echo $row['company_code']; ?>"><?php echo $row['company_name'];?> (Supplier)</option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<br>
						<input type="hidden" name="c_code" id="c_code">
						<div class="form-group">
							<label for="textfield" class="control-label">Nama Bank</label>
							<div class="col-sm-10">
								<input type="text" name="nama_bank" class="form-control" placeholder="Nama Bank" required="required">
							</div>
						</div>
						<br>
						<div class="form-group">
						  <label for="textfield" class="col-sm-2 control-label">Cabang</label>
						  <div class="col-sm-10">
							<input type="text" name="cabang" class="form-control" placeholder="Cabang" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="textfield" class="col-sm-2 control-label">Alamat Bank</label>
						  <div class="col-sm-10">
							<input type="text" name="alamat_bank" class="form-control" placeholder="Alamat Bank" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="textfield" class="col-sm-2 control-label">Akun Bank</label>
						  <div class="col-sm-10">
							<input type="text" name="akun_bank" class="form-control" placeholder="Akun Bank" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="textfield" class="col-sm-2 control-label">Swift Kode</label>
						  <div class="col-sm-10">
							<input type="text" name="swift_kode" class="form-control" placeholder="Swift Kode" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="textfield" class="col-sm-2 control-label">Koresponden Bank</label>
						  <div class="col-sm-10">
							<input type="text" name="koresponden_bank" class="form-control" placeholder="Koresponden Bank" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="textfield" class="col-sm-2 control-label">IBAN</label>
						  <div class="col-sm-10">
							<input type="text" name="iban" class="form-control" placeholder="IBAN" required="require">
						  </div>
						</div>
						<br>
						<div class="form-group">
						  <label for="textfield" class="col-sm-2 control-label">Remark</label>
						  <div class="col-sm-10">
							<input type="text" name="remark" class="form-control" placeholder="Remark" required="require">
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=bank&navbar=bank&parent=master">Bank</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Bank</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Bank
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable table-bordered">
									<thead>
										<tr>
											<th>ID Bank</th>
											<th>Nama Penerima</th>
											<th>Nama Bank</th>
											<th>Cabang</th>
											<th>Alamat Bank</th>
											<th>Akun Bank</th>
											<th>Swift Kode</th>
											<th>Koresponden Bank</th>
											<th>IBAN</th>
											<th>Remark</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select * from bank");
									//$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $row['id_bank']; ?></td>
											<td><?php echo $row['nama_penerima']; ?></td>
											<td><?php echo $row['nama_bank']; ?></td>
											<td><?php echo $row['cabang']; ?></td>
											<td><?php echo $row['alamat_bank']; ?></td>
											<td><?php echo $row['akun_bank']; ?></td>
											<td><?php echo $row['swift_kode']; ?></td>
											<td><?php echo $row['koresponden_bank']; ?></td>
											<td><?php echo $row['iban']; ?></td>
											<td><?php echo $row['remark']; ?></td>
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-bank" data-id="<?php echo $row['id_bank'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-bank/delete-bank.php?id_bank=<?php echo $row['id_bank']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									//$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-bank',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-bank/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Bank</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>