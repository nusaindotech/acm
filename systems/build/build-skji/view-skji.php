<?php
$tglsekarang	= date('d-m-Y');

	?>
	   <script>
            //<![CDATA[
            $(window).load(function(){
            $("#supplier").on("change", function(){
                var nilai  = $("#supplier :selected").attr("data-alamat");
                var nilai2 = $("#supplier :selected").attr("data-ppn");
                $("#alamat").val(nilai);
                $("#ppn").val(nilai2);
            });
            });//]]>
            $(window).load(function(){
            $("#produk").on("change", function(){
                var nilai  = $("#produk :selected").attr("data-custom");
                $("#custom").val(nilai);
            });
            });//]]>
        </script>
<div class="modal hide fade modal-besar" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog ">
				<form action="build/build-skji/insert-skji.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Membuat Surat Kontrak ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Surat Kontrak Penjualan Ekspor</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form Surat Kontrak Ekspor</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">S/C Reference</label>
						  <div class="controls">
							<input type="text" name="reference" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Currency</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>
								</td>
                                <td>
                                <div class="input-xlarge">
								<select name="currency" id="currency"  class='chosen-select' required="required">
									<option value="">--Choice Currency--</option>
									<?php 	

									$tampil=mysql_query("SELECT * FROM `currency`");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_currency']; ?>" data-alamat="<?php echo $row['company_address']; ?>" ><?php echo $row['name_currency']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
					  	
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Seller</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="seller" id="seller" class='chosen-select'>
									<option value="">--Choice Seller --</option>
										
										<?php 	
									$tampil=mysql_query("select * from home where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_home']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['nama']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Buyer</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="buyer" id="buyer" class='chosen-select'>
									
									<option value="">--Choice Buyer --</option>
										<?php 	
									$tampil=mysql_query("select * from customers where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['customer_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Notify Party/ Receiver/Agent</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="agent" id="agent" class='chosen-select'>
									
									<option value="">--Choice Buyer --</option>
										<?php 	
									$tampil=mysql_query("select * from customers where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['customer_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Represented by</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="Represented-by" id="Represented-by" class='chosen-select'>
									
									<option value="">--Choice Represented by --</option>
										<?php 	
									$tampil=mysql_query("select * from customers where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['customer_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
			<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Consignee</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="consignee" id="consignee" class='chosen-select'>
									
									<option value="">--Choice consignee --</option>
										<?php 	
									$tampil=mysql_query("select * from customers where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['customer_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Represented by Phone Number</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<input type="Number">
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>

						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Product</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="produk" id="produk" class='chosen-select'>
									<option value="">--Choice Product --</option>
										<?php 	
									$tampil=mysql_query("select * from items where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['item_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Country Of Origin</label>
						  <div class="controls">
							<input type="text" name="coo" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Quality Spesification</label>
						  <div class="controls">
							<textarea name="qs" id="qs" class="input-block-level">OUTTURN DISCOUNT (47 LBS MIN) : USD  43.75 /MT  FOR EVERY 1.00 LBS  SHORTFALL 
NUT COUNT DISCOUNT : USD 10.50/MT   FOR EVERY NUT 10 COUNTS EXCESS.  
MOISTURE DISCOUNT : USD 21.00 /MT FOR EVERY 1% EXCESS. FOR PART ON PRO –RATE.
</textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Quantity</label>
						  <div class="controls">
							<input type="text" name="qty" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Price</label>
						  <div class="controls">
							<input type="text" name="price" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Total Price</label>
						  <div class="controls">
							<input type="text" name="tp" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Inco Term</label>
						  <div class="controls">
							<input type="text" name="it" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Value</label>
						  <div class="controls">
							<input type="text" name="valuee" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Qualities Discount</label>
						  <div class="controls">
							<textarea name="qd" id="qd" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Rejection Clause</label>
						  <div class="controls">
							<textarea name="rc" id="rc" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Insurance</label>
						  <div class="controls">
							<input type="text" name="insurance" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Packing</label>
						  <div class="controls">
							<input type="text" name="pack" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Shipment</label>
						  <div class="controls">
							<input type="text" name="shipment" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Term Of Shipment</label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Port Of Loading</label>
						  <div class="controls">
							<input type="text" name="pol" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Port Of Discharge</label>
						  <div class="controls">
							<input type="text" name="pod" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Term Of Payment</label>
						  <div class="controls">
							<textarea name="top" id="top" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Seller Bank Name</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="bank1" id="bank1" class='chosen-select'>
									<option value="">--Choice Bank Name --</option>
										<?php 	
									$tampil=mysql_query("select * from home where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_home']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['nama']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Bank Account</label>
						  <div class="controls">
							<input type="text" name="ba1" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Seller Bank Name</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="bank2" id="bank2" class='chosen-select'>
									<option value="">--Choice Bank Name --</option>
										<?php 	
									$tampil=mysql_query("select * from home where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_home']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['nama']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Bank Account</label>
						  <div class="controls">
							<input type="text" name="ba2" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Document</label>
						  <div class="controls">
							<textarea name="doc" id="doc" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Additional</label>
						  <div class="controls">
							<textarea name="add" id="add" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Force Majeure</label>
						  <div class="controls">
							<textarea name="fm" id="fm" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Arbitration and Governing Law </label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level"></textarea>
						  </div>
						</div> 
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Contract Validity</label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level">This contract begins to have effect on the parties on the date of signature through facsimile/email.</textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Others Terms and Conditions</label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Remarks</label>
						  <div class="controls">
							<textarea name="tos" id="tos" class="input-block-level"></textarea>
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Surat Kontrak Ekspor</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=skji&navbar=skji&parent=Penjualan">Surat Kontrak Penjualan Ekspor</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Surat Kontrak Penjualan Ekspor</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Surat Kontrak Penjualan Ekspor
								</h3>
					  </div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No.</th>
											<th>No. Kontrak</th>
											<th>Tanggal</th>
											<th>Produk</th>
											<th>Supplier</th>
											<th>Bank Name</th>
											<!-- <th>Tipe Kontrak</th> -->
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("SELECT a.id_skji ,a.tanggal, b.company_name, c.name , a.id_bank1
from t_skji a
INNER JOIN suppliers b on b.supplier_id = a.customer_id
INNER JOIN items c on c.item_id = a.item_id where a.deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_skji']; ?></td>
											<td><?php echo dateBahasaIndo($row['tanggal']); ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['id_bank1']; ?></td>
											<!-- <td><?php echo $row['type_kontrak']; ?></td> -->
											<td class='hidden-350'>
												<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<?php
												if($row['status']==0)
												{
												?>
													<li>
														<a id="edit-phj" data-id="<?php echo $row['id_skji'];?>" data-id2="<?php echo $row['item_id'];?>" data-id3="<?php echo $row['customer_id'];?>">Revisi Kontrak</a>
													</li>
													<li>
														<a class="btn btn-success" href="dash.php?hp=pcl3&navbar=pcl&parent=export&idphj=<?php echo $row['id_skji']; ?>&status=1">SHOW PACKING LIST</a>
													</li>
													<li>
														<a class="btn btn-primary" href="dash.php?hp=inv&navbar=pcl&parent=export&idphj=<?php echo $row['id_skji']; ?>&status=1">SHOW INVOICE</a>
													</li>
												<?php
												}
												else
												{
													?>
													<li>
														<a class="btn btn-danger" href="build/build-ppj/delete-ppj.php?idphj=<?php echo $row['id_ppj']; ?>">Hapus</a>
													</li>
													
													<!-- <li>
														<a href="#">Cetak Surat Penawaran</a>
													</li> -->
													<li>
														<a href='#buat_pp' class="buat_pp"  role="button" data-id="<?php echo $row['id_ppj']; ?>" data-toggle="modal">Buat Kontrak</a>
													</li>
												<?php }
												?>
												</ul>
											</div>
											<!-- 
											<a class="btn btn-brown" id="edit-skb" data-id="<?php echo $row['id_skb'];?>"><i class="icon-edit"></i></a>
											<?php
												if($row['type_kontrak']=="Lokal")
												{ 

												?>
												<a class="btn btn-success" href="dash.php?hp=skb3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=Penjualan"><i class="icon-print"></i></a>
												<?php	
												} else if($row['type_kontrak']=="Ekspor USD"){
													?>
												<a class="btn btn-success" href="dash.php?hp=skbu3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=Penjualan"><i class="icon-print"></i></a>
												<?php
												} else if($row['type_kontrak']=="Ekspor USD Kurs"){
													?>
												<a class="btn btn-success" href="dash.php?hp=skbuk3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=Penjualan"><i class="icon-print"></i></a>
												<?php
												} else if($row['type_kontrak']=="Forward Hedging"){
													?>
												<a class="btn btn-success" href="dash.php?hp=skbfh3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=Penjualan"><i class="icon-print"></i></a>
												<?php
												} 
												?>
											 -->
											<!-- <a class="btn btn-success" data-id="<?php echo $row['id_skb'];?>"><i class="icon-print"></i></a> -->
											<!-- <a class="btn btn-danger" href="build/build-skb/delete-skb.php?idbaru=<?php echo $row['id_skb']; ?>"><i class="icon-trash"></i></a> -->
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-skb',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-skb/form-edit.php',
							{id:$(this).attr('data-id'), id2:$(this).attr('data-id2')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});

		
				
	</script>
	<script type="text/javascript">

			$("#npd").hide();
   				$("#usd").hide();
			$("#kurs").hide();
			$("#nfh").hide();
			$("#hkurs").hide();
			$("#husd").hide();
			$("#hpas").hide();
			// alert("Sas");
			
			$( document ).ready(function() {
   				
			});
			function coba(){
				var	isine=$("#custom").val();
				alert(isine);
			}
			function jp() {
   				var tt = $("#tk").val();	

   				if(tt =='Lokal'){
   				//alert();-->untuk menampilkan popups
   					// $("#jpn").show();
					$("#usd").hide();	
					$("#kurs").hide();
					$("#nfh").hide();
					$("#npd").hide();
			$("#hpas").hide();

					$("#husd").hide();
					$("#hkurs").show();
   					$("#husd").val(" ");
   					$("#usd").val(" ");
   					$("#kurs").val(" ");
   					$("#nfh").val(" ");

   				}
   				else if(tt =='Ekspor USD'){
   				//alert();-->untuk menampilkan popups
   					$("#usd").show();	
   					$("#husd").hide();	
					$("#kurs").hide();
					$("#npd").hide();
			$("#hpas").hide();

					$("#hkurs").hide();
					$("#nfh").hide();
   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#nfh").val(" ");

   					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					$('#vkon').val(vusd);
					});
   				}
   				else if(tt =='Ekspor USD Kurs'){
   				//alert();-->untuk menampilkan popups
   					$("#usd").show();	
   					$("#husd").hide();	
   					$("#hkurs").hide();	
					$("#npd").hide();
			$("#hpas").hide();

					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vkurs = $("#vkurs").val();
  					nilai = vusd * vkurs;
  					$('#vkon').val(nilai);
					});
					$( "#vkurs" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vkurs = $("#vkurs").val();
  					nilai = vusd * vkurs;
  					$('#vkon').val(nilai);
					});

					$("#kurs").show();
					$("#nfh").hide();
   					$("#nfh").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   				}
   				else if(tt =='Forward Hedging'){
   				//alert();-->untuk menampilkan popups

   					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vfh = $("#vfh").val();
  					nilai = vusd * vfh;
  					$('#vkon').val(nilai);
					});
					$( "#vfh" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vfh = $("#vfh").val();
  					nilai = vusd * vfh;
  					$('#vkon').val(nilai);
					});

   					$("#usd").show();	
					$("#kurs").hide();
					$("#nfh").show();
					$("#husd").hide();
					$("#hkurs").hide();
					$("#npd").hide();
			$("#hpas").hide();

   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#kurs").val(" ");
					$("#npd").hide();

   				}
   				else if(tt =='Premium Differential'){
   					$( "#vkurs" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});

   					$( "#vpd" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});

					$( "#vpas" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});



					$("#npd").show();
   					$("#usd").hide();	
					$("#kurs").show();
					$("#nfh").hide();
					$("#husd").hide();
					$("#hkurs").hide();
			$("#hpas").show();

   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#kurs").val(" ");
   				}
   				else{
					$("#usd").hide();
					$("#npd").hide();

					$("#kurs").hide();
					$("#nfh").hide();
					$("#husd").hide();
					$("#hkurs").hide();
   					$("#usd").val(" ");
   					$("#kurs").val(" ");
   					$("#nfh").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   				}
   				
			};
			
		

		</script>
		
		

		
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Surat Kontrak Penjualan Ekspor</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
