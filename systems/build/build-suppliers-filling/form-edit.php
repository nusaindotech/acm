	<?php 
		include "../../../auth/autho.php";
		$id			= $_POST['id'];
		$query 		= mysql_query("select company_name, company_code, company_address, company_phone, agency_name, supplier_id,
								acc, date_pkp, company_email, ppn, npwp_name, pkp, provision
								from suppliers 
								where supplier_id='$id'") or die(mysql_error());
		$data 		= mysql_fetch_array($query);
		
		$formattanggal	= date('d-m-Y',strtotime($data['date_pkp']));
	?>
	<div class="modal-body">
		<form role="form" action="build/build-suppliers-filling/update-suppliers-filling.php" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped' onsubmit="return confirm('Apakah Yakin Simpan Data Pengajuan Pemasok ?');">
			<input type="hidden" value="<?php echo $id;?>" name="id_detail_suppliers">
			<input type="hidden" value="<?php echo $data['supplier_id'];?>" name="id_suppliers">
			
			<div class="control-group">
				<label class="control-label">Kode</label>
				<div class="controls">
					<input type="text" id="textfield" class="input-xlarge" name="kodesupplier" placeholder="Isikan Kode Supplier" value="<?php echo $data['company_code'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama</label>
				<div class="controls">
					<input type="text" name="nama" id="textfield" placeholder="Nama Supplier" class="input-xlarge" required="require" value="<?php echo $data['company_name'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Alamat</label>
				<div class="controls">
					<input type="text" name="alamat" id="textfield" placeholder="Alamat Supplier" class="input-xlarge" value="<?php echo $data['company_address'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Telepon</label>
				<div class="controls">
					<input type="text" name="telepon" id="textfield" placeholder="Telepon Supplier" class="input-xlarge" value="<?php echo $data['company_phone'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Email</label>
				<div class="controls">
					<input type="text" name="email" id="textfield" placeholder="Email Supplier" class="input-xlarge" value="<?php echo $data['company_email'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">PIC</label>
				<div class="controls">
					<input type="text" name="pic" id="textfield" placeholder="PIC Supplier" class="input-xlarge" value="<?php echo $data['agency_name'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Syarat</label>
				<div class="controls">
					<input type="text" name="syarat" id="textfield" placeholder="Syarat Pembayaran" class="input-xlarge" value="<?php echo $data['provision'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Tanggal</label>
				<div class="controls">
					<input type="text" name="tanggalpkp" id="textfield" class="input-medium datepick" value="<?php echo $formattanggal;?>">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama NPWP</label>
				<div class="controls">
					<input type="text" name="namanpwp" class="form-control" placeholder="Nama NPWP" value="<?php echo $data['npwp_name'];?>"  autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nomor PKP</label>
				<div class="controls">
					<input type="text" name="nopkp" class="form-control" placeholder="Nomor PKP" value="<?php echo $data['pkp'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Setujui</label>
				 <div class="controls">
				    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td><input type="radio" class='icheck-me' value="0" name="acc" data-skin="square" data-color="blue" <?php if($data['acc']=='0') { echo "checked"; } else {} ?> > <label>Tidak Disetujui</label></td>
                        <td><input type="radio" class='icheck-me' value="1" name="acc" data-skin="square" data-color="blue" <?php if($data['acc']!='0') { echo "checked"; } else {} ?> > <label>Disetujui</label></td>
                    </tr>
                    </table>
				 </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>