	<script>
        //<![CDATA[
        $(window).load(function(){
        $("#item").on("change", function(){
            var nilai = $("#item :selected").attr("data-satuan");
            $("#satuan").val(nilai);
        });
        });//]]>
    </script>
		
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-suppliers-filling/insert-suppliers-filling.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Simpan Data Pengajuan Pemasok ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Pengajuan Pemasok</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
						  <div class="controls">
							<input type="text" name="namasupplier" class="form-control" placeholder="Nama Supplier" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
						  <div class="controls">
							<input type="text" name="alamat" class="form-control" placeholder="Alamat Supplier" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Telepon</label>
						  <div class="controls">
							<input type="text" name="telepon" class="form-control" placeholder="Telepon Supplier" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
						  <div class="controls">
							<input type="text" name="email" class="form-control" placeholder="Email Supplier" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">PIC</label>
						  <div class="controls">
							<input type="text" name="pic" class="form-control" placeholder="PIC Supplier" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Syarat</label>
						  <div class="controls">
							<input type="text" name="syarat" class="form-control" placeholder="Syarat Pembayaran" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal PKP</label>
						  <div class="controls">
							<input type="text" name="tanggalpkp" id="textfield" class="input-medium datepick">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama NPWP</label>
						  <div class="controls">
							<input type="text" name="namanpwp" class="form-control" placeholder="Nama NPWP" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nomor PKP</label>
						  <div class="controls">
							<input type="text" name="nopkp" class="form-control" placeholder="Nomor PKP" autocomplete="off">
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=ps&navbar=ps&parent=master">Pengajuan Pemasok</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Pengajuan Pemasok</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Pengajuan Pemasok
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable table-bordered" >
									<thead>
										<tr>
											<th>No</th>
											<th>Nama</th>
											<th>Alamat</th>
											<th>Telepon</th>
											<th>PIC</th>
											<th>Status</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select company_name, company_code, company_address, 
									company_phone, supplier_id, acc
									from suppliers order by supplier_id desc");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['company_address']; ?></td>
											<td><?php echo $row['company_phone']; ?></td>
											<td><?php echo $row['agency_name']; ?></td>
											<td>
											<?php
											if($row['acc']==0)
											{
												echo "<span class='label label-warning'>Belum Diterima</span>";
											}
											else
											{
												echo "<span class='label label-success'>Diterima</span>";
											}
											?>
											</td>
											<td>
											<a class="btn btn-primary" id="edit-supplier-filling" data-id="<?php echo $row['supplier_id'];?>"><i class="icon-edit"></i></a>
											<?php
											if($row['acc']==0)
											{
											?>
												<a class="btn btn-danger" href="build/build-suppliers-filling/delete-suppliers-filling.php?id_detail_suppliers=<?php echo $row['supplier_id']; ?>"><i class="icon-trash"></i></a>
											<?php
											}
											else
											{
												
											}
											?>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
		$(function(){
			$(document).on('click','#edit-supplier-filling',function(e){
				e.preventDefault();
				$("#view").modal('show');
				$.post('build/build-suppliers-filling/form-edit.php',
					{id:$(this).attr('data-id')},
					function(html){
						$(".modal-body2").html(html);
					}  
				);
			});
		});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Pengajuan Supplier</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>