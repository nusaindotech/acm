<?php
	$tglsekarang	= date('d-m-Y');
?>	
	<script>
            //<![CDATA[
            $(window).load(function(){
            $("#po").on("change", function(){
                var nilai  = $("#po :selected").attr("data-supplier");
                $("#supplier").val(nilai);
            });
            });//]]>
    </script>
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-lpb/insert-lpb1.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Bukti Penerimaan Barang ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Bukti Penerimaan Barang</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No Bukti</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Order Pembelian</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="po" id="po" class='chosen-select' required=="required">
									<option value="">--Pilih Order Pembelian--</option>
									<?php 	
									$tampil=mysql_query("select a.id_po, b.company_name from po a, suppliers b
											where a.deleted=0 and a.acc=1 and a.supplier_id=b.supplier_id
											order by a.id_po ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_po']; ?>" data-supplier="<?php echo $row['company_name']; ?>" ><?php echo $row['id_po']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
						  <div class="controls">
							<input type="text" name="supplier" class="form-control" id="supplier" placeholder="supplier" autocomplete="off" required=="required" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Surat Jalan</label>
						  <div class="controls">
							<input type="text" name="suratjalan" class="form-control" placeholder="No Surat Jalan" autocomplete="off" required=="required" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Plat Nomor</label>
						  <div class="controls">
							<input type="text" name="platnomor" class="form-control" placeholder="No. Kendaraan" autocomplete="off" required=="required" >
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Bukti Penerimaan Barang</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Pembelian</a>
						</li>
						<li>
							<a href="dash.php?hp=lpb1&navbar=lpb&parent=pembelian">Bukti Penerimaan Barang</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Bukti Penerimaan Barang</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Bukti Penerimaan Barang
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>No Bukti Penerimaan Barang</th>
											<th>Tanggal</th>
											<th>ID PO</th>
											<th>Nama Supplier</th>
											<th>Status</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.id_lpb, a.date_transaction, a.id_po, b.company_name, a.acc, a.custom1
									from lpb a, suppliers b, po c
									where c.supplier_id=b.supplier_id and a.id_po=c.id_po and a.deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_lpb']; ?></td>
											<td><?php echo dateBahasaIndo($row['date_transaction']); ?></td>
											<td><?php echo $row['id_po']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td>
											<?php
											if($row['acc']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else
											{
												echo "<span class='label label-success'>Diterima</span>";
											}
											?>
											</td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
													<?php
													if($row['acc']==0 and $row['custom1']=='')
													{
													?>
													<li>
														<a href="dash.php?hp=lpb2&idbaru=<?php echo $row['id_lpb']; ?>&navbar=lpb&parent=pembelian">Revisi Bukti Penerimaan Barang</a>
													</li>
													<?php
													}
													else if($row['acc']==1 and $row['custom1']!=1)
													{
													?>
													<li>
														<a href="dash.php?hp=lpb4&idbaru=<?php echo $row['id_lpb']; ?>&navbar=lpb&parent=pembelian">Edit Bukti Penerimaan Barang</a>
													</li>
													<?php	
													}
													else
													{
														
													}
													?>
													<li>
														<a href="#">Cetak Bukti Penerimaan Barang</a>
													</li>
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>