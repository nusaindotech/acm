	<?php 
		include "../../../auth/autho.php";
		$id=$_POST['id'];
		// var_dump($_POST);
		$query = mysql_query("select * from t_pl where no_pl='$id'") or die(mysql_error());
		$data = mysql_fetch_array($query);
	?>
	<div class="modal-body">
		<form role="form" action="build/build-pl/update-pl.php" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped' onsubmit="return confirm('Anda Yakin Mengubah Data ?');">
			<input type="hidden" value="<?php echo $id;?>" name="id_customers">
			
			<div class="control-group">
				<label class="control-label">Kode</label>
				<div class="controls">
					<input type="text" id="textfield" class="input-xlarge" name="kodepelanggan" placeholder="Isikan Kode Pelanggan" value="<?php echo $id;?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama</label>
				<div class="controls">
					<input type="text" name="nama" id="textfield" placeholder="Nama Pelanggan" class="input-xlarge" required="require" value="<?php echo $data['company_name'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">No. KTP</label>
				<div class="controls">
					<input type="text" name="noktp" id="textfield" placeholder="Nomor KTP" class="input-xlarge" required="require" value="<?php echo $data['account_number'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Alamat</label>
				<div class="controls">
					<input type="text" name="alamat" id="textfield" placeholder="Alamat Pelanggan" class="input-xlarge" value="<?php echo $data['company_address'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Telepon</label>
				<div class="controls">
					<input type="text" name="telepon" id="textfield" placeholder="Telepon Pelanggan" class="input-xlarge" value="<?php echo $data['company_phone'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Fax</label>
				<div class="controls">
					<input type="text" name="fax" id="textfield" placeholder="Fax Pelanggan" class="input-xlarge" value="<?php echo $data['company_fax'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Email</label>
				<div class="controls">
					<input type="text" name="email" id="textfield" placeholder="Email Pelanggan" class="input-xlarge" value="<?php echo $data['company_email'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
							<label for="textfield" class="control-label">PPN</label>
						  <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                              <td>                              	
                              <input type="radio" class='icheck-me' value="0" name="ppn" data-skin="square" data-color="blue" <?php if($data['ppn']=='0') { echo "checked='checked' "; } else {} ?> > <label>Non PPN</label></td>
                     		   <td><input type="radio" class='icheck-me' value="1" name="ppn" data-skin="square" data-color="blue" <?php if($data['ppn']=='1') { echo "checked"; } else {} ?> > <label>PPN</label></td>
                              </tr>
                            </table>
						  </div>
						</div>
			<div class="control-group">
				<label for="textfield" class="control-label">No NPWP</label>
				<div class="controls">
					<input type="text" name="nonpwp" id="textfield" placeholder="NPWP Pelanggan" class="input-xlarge" value="<?php echo $data['no_npwp'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">No SPPKP</label>
				<div class="controls">
					<input type="text" name="sppkp" id="textfield" placeholder="SPPKP Pelanggan" class="input-xlarge" value="<?php echo $data['sppkp'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
							<label for="textfield" class="control-label">Status</label>
						  <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                              <td><input type="radio" class='icheck-me' value="0" name="acc" data-skin="square" data-color="blue" <?php if($data['acc']=='0') { echo "checked"; } else {} ?> > <label>Tidak Disetujui</label></td>
                     		   <td><input type="radio" class='icheck-me' value="1" name="acc" data-skin="square" data-color="blue" <?php if($data['acc']=='1') { echo "checked"; } else {} ?> > <label>Disetujui</label></td>
                              </tr>
                            </table>
						  </div>
						</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>