<?php
	$tglsekarang	= date('d-m-Y');
?>				
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-pl/insert-pl.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Simpan Data Pengajuan Konsumen ?'); " id="from-awal">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Pengajuan Konsumen</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
					  <input type="hidden" value="<?php echo $tanggalsekarang;?>" name="tanggal">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No Form</label>
						  <div class="controls">
							<input type="text" name="noform" class="form-control" placeholder="Auto" readonly="" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
						  <div class="controls">
							<input type="text" name="namacustomers" class="form-control" placeholder="Nama Konsumen" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. KTP</label>
						  <div class="controls">
							<input type="text" name="noktp" class="form-control" placeholder="Nomor KTP" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
						  <div class="controls">
							<input type="text" name="alamat" class="form-control" placeholder="Alamat Konsumen" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Telepon</label>
						  <div class="controls">
							<input type="text" name="telepon" class="form-control" placeholder="Telepon Konsumen" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Fax</label>
						  <div class="controls">
							<input type="text" name="fax" class="form-control" placeholder="fax Konsumen" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
						  <div class="controls">
							<input type="text" name="email" class="form-control" placeholder="Email Konsumen" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
				<!-- 		<div class="control-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Cara Pembayaran</label>
						  <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                              <td><input type="radio" class='icheck-me' value="0" name="cp" data-skin="square" data-color="blue" > <label>Tunai</label></td>

                     		   <td><input type="radio" class='icheck-me' value="1" name="cp" data-skin="square" data-color="blue" > <label>Kredit</label></td>
                              <td><input type="radio" class='icheck-me' value="0" name="cp" data-skin="square" data-color="blue" > <label>Kontrak</label></td>  
                              </tr>
                            </table>
						  </div>
						</div> -->
						<div class="control-group">
							<label for="inputEmail3" class="col-sm-2 control-label">PPN</label>
						  <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                              <td><input type="radio" class='icheck-me' value="0" name="ppn" data-skin="square" data-color="blue" > <label>Non PPN</label></td>
                     		   <td><input type="radio" class='icheck-me' value="1" name="ppn" data-skin="square" data-color="blue" > <label>PPN</label></td>
                                
                              </tr>
                            </table>
						  </div>
						</div>
						<div class="control-group" id="divnpwp" style="display: none;">
						  <label for="inputEmail3" class="col-sm-2 control-label">No NPWP</label>
						  <div class="controls">
							<input type="text" name="nonpwp" id="nonpwp" class="form-control" placeholder="No NPWP Konsumen" autocomplete="off">
						  </div>
						</div>
						<div class="control-group" id="divsppkp" style="display: none;">
						  <label for="inputEmail3" class="col-sm-2 control-label">No SPPKP</label>
						  <div class="controls">
							<input type="text" name="sppkp" id="nosppkp" class="form-control" placeholder="No SPPKP Konsumen" autocomplete="off">
						  </div>
						</div>												
					</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=pl&navbar=pl&parent=penjualan">Pengajuan Konsumen</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Pengajuan Konsumen</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Pengajuan Konsumen
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable" >
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Kode Konsumen</th>
											<th>Nama</th>
											<th>Alamat</th>
											<th>Telepon</th>
											<th>Fax</th>
											<th>Email</th>
											<th>PPN</th>
											<th>NPWP</th>
											<th>Persetujuan</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select * from t_pl
											where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['no_pl']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['company_address']; ?></td>
											<td><?php echo $row['company_phone']; ?></td>
											<td><?php echo $row['company_fax']; ?></td>
											<td><?php echo $row['company_email']; ?></td>
											<td>	
											<?php 
											if($row['ppn']=='0') { echo "Non PPN"; } else {echo "PPN";} ?></td> 
											<td><?php echo $row['no_npwp']; ?></td>
											<td>
											<?php
											if($row['acc']=='0')
											{
												echo "<a href='#'< span class='label label-warning'>Belum Disetujui</span></a>";
											}
											else
											{
												echo "<a href='#'<span class='label label-success'>Disetujui</span></a>";
											}
											?>
											</td>
											<td class='hidden-350'>
											<?php if ($row['acc']=='0'): ?>
												<a class="btn btn-primary" id="edit-Konsumen" data-id="<?php echo $row['no_pl'];?>"><i class="icon-edit"></i></a>
												
											<?php endif ?>
											<a class="btn btn-danger" href="build/build-pl/delete-pl.php?idKonsumen=<?php echo $row['no_pl']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-Konsumen',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-pl/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});

				
	</script>
	<script type="text/javascript">
		$('input[name=ppn]').on('change', function() {
		ppn =$('input[name=ppn]:checked','#from-awal').val();
			// alert(ppn);
			if (ppn>0) {
				$("#divnpwp").show();
				$("#divsppkp").show();
			}else{
				$("#divnpwp").hide();
				$("#nonpwp").val("");
				$("#divsppkp ").hide();
				$("#nosppkp").val("");

			}
		});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Pengajuan Konsumen</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>