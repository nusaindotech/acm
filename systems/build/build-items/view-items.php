		
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-items/insert-items.php" method="POST" class="form-horizontal" onsubmit="return confirm('Anda Yakin Menambah Data ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Data Produk Utama/Komoditi</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
					  
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kode</label>
						  <div class="controls">
							<input type="text" name="kodej" class="form-control" placeholder="Auto" required="require" autocomplete="off" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kode Produk Utama/Komoditi</label>
						  <div class="controls">
							<table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="kobar" id="kobar" class='chosen-select' required onchange="jp()">
									<option value="">--Pilih Kode--</option>
									
									<option value="cklt">CKLT</option>
									<option value="mt">MT</option>
									<option value="cgkh">CGKH</option>
									<option value="cgkh">CGKH</option>
									<option value="ggcgkh">GGCGKH</option>
									<option value="JG">JG</option>
									<option value="kp">KP</option>
									
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Produk Utama/Komoditi</label>
						  <div class="controls">
							<input type="text" name="nama" class="form-control" placeholder="Nama Produk Utama/Komoditi" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kategori Produk Utama/Komoditi</label>
						  <div class="controls">
							<table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="kategori" id="kategori" class='chosen-select' required onchange="jp()">
									<option value="">--Pilih Kategori--</option>
									
									<option value="Produk Sampingan">Produk Sampingan</option>
									<option value="Produk Utama">Produk Utama</option>
									
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>
						</div>
					<!-- 	<div class="control-group" id="JPU">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis Produk Utama/Komoditi</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="jpu" id="select1" class='chosen-select' required>
								<option value=" ">--Pilih Jenis Produk Utama/Komoditi--</option>
									<?php 	
									$nama='Produk Utama/Komoditi Umum';
									$tampil=mysql_query("select * from t_categories where deleted=0 and nama= '$nama'");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_category']; ?>" data-jenis="<?php echo $row['jenis']; ?>" ><?php echo $row['jenis']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" id="JPP">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis Produk Utama/Komoditi</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="jpp" id="select2" class='chosen-select' required>
								<option value=" ">--Pilih Jenis Produk Utama/Komoditi--</option>
									<?php 	
									$nama='Produk Utama';
									$tampil=mysql_query("select * from t_categories where deleted=0 and nama= '$nama'");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_category']; ?>" data-jenis="<?php echo $row['jenis']; ?>" ><?php echo $row['jenis']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" id="JPS">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis Produk Utama/Komoditi</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="jps" id="select3" class='chosen-select' required>
								<option value=" ">--Pilih Jenis--</option>
									<?php 	
									$nama='Produk Sampingan';
									$tampil=mysql_query("select * from t_categories where deleted=0 and nama= '$nama'");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_category']; ?>" data-jenis="<?php echo $row['jenis']; ?>" ><?php echo $row['jenis']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" id="JPJ">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis Produk Utama/Komoditi</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="jpj" id="select4" class='chosen-select' required>
								<option value=" ">--Pilih Jenis--</option>
									<?php 	
									$nama='Jasa';
									$tampil=mysql_query("select * from t_categories where deleted=0 and nama= '$nama'");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_category']; ?>" data-jenis="<?php echo $row['jenis']; ?>" ><?php echo $row['jenis']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
					      <input type="hidden" id="jenis1" name="jenis1" class="form-control" placeholder="jenis1" autocomplete="off" readonly>
					      <input type="hidden" id="jenis2" name="jenis2" class="form-control" placeholder="jenis2" autocomplete="off" readonly>
					      <input type="hidden" id="jenis3" name="jenis3" class="form-control" placeholder="jenis3" autocomplete="off" readonly>
					      <input type="hidden" id="jenis4" name="jenis4" class="form-control" placeholder="jenis4" autocomplete="off"> -->
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Satuan</label>
						  <div class="controls">
							<input type="text" name="satuan" class="form-control" placeholder="Satuan Produk Utama/Komoditi" autocomplete="off">
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=items&navbar=items&parent=master">Produk Utama/Komoditi</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Data Produk Utama/Komoditi</a>
							</div>
				     
					  <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Produk Utama/Komoditi
								</h3>
					  </div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Kode Produk Utama/Komoditi</th>
											<th>Nama Produk Utama/Komoditi</th>
											<th>Kategori Produk Utama/Komoditi</th>
											<th>Satuan</th>
											<th>Harga Beli</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select * from items where category='Produk Utama' or category='Produk Sampingan'");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['item_number']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo $row['category']; ?></td>
											<td><?php echo $row['custom3']; ?></td>
											<td><?php echo $row['custom8']; ?></td>
											
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-items" data-id="<?php echo $row['item_id'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-items/delete-items.php?iditems=<?php echo $row['item_id']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-items',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-items/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<script type="text/javascript">
			$("#JPU").hide();
			$("#JPP").hide();
			$("#JPS").hide();
			$("#JPJ").hide();
			$( document ).ready(function() {
   				
   				
			});
			function jp() {
   				var tt = $("#kategori").val();	

   				if(tt =='Produk Utama/Komoditi Umum'){
   				//alert();-->untuk menampilkan popups
   					$("#JPU").show();
   					$("#JPP").hide();
   					$("#JPS").hide();
   					$("#JPJ").hide();
   					$("#JPP").val("");
   					$("#JPS").val("");
   					$("#JPJ").val("");
   				}else if (tt=='Produk Utama') {
   					 $("#JPP").show();
   					$("#JPU").hide();
   					$("#JPS").hide();
   					$("#JPJ").hide();
   					$("#JPS").val("");
   					$("#JPU").val("");
   					$("#JPJ").val("");

   				}
   				 else if (tt=='Produk Sampingan'){
   					$("#JPS").show();
   					$("#JPU").hide();
   					$("#JPP").hide();
   					$("#JPJ").hide();
   					$("#JPP").val("");
   					$("#JPU").val("");
   					$("#JPJ").val("");

   				}
   				else if (tt=='Jasa'){
   					$("#JPJ").show();
   					$("#JPS").hide();
   					$("#JPU").hide();
   					$("#JPP").hide();
					$("#S").hide();
   					$("#JPP").val("");
   					$("#S").val("-");
   					$("#JPU").val("");
   					$("#JPS").val("");

   				}
   				else{
					$("#JPU").hide();
					$("#JPP").hide();
					$("#JPJ").hide();
					$("#JPS").hide();
   					$("#JPP").val("");
   					$("#JPU").val("");
   					$("#JPS").val("");
   					$("#JPJ").val("");
   				}
   				
			};

		</script>
		<script>
        //<![CDATA[
        $(window).load(function(){
        	$("#select1").change(function () {
        	var nilai = $("#select1 :selected").attr("data-jenis");
            $("#jenis1").val(nilai);
        });
        });
        //<![CDATA[
        $(window).load(function(){
        $("#select2").on("change", function(){
            var nilai = $("#select2 :selected").attr("data-jenis");
            $("#jenis2").val(nilai);
        });
        });//]]>
        //<![CDATA[
        $(window).load(function(){
        $("#select3").on("change", function(){
            var nilai = $("#select3 :selected").attr("data-jenis");
            $("#jenis3").val(nilai);
        });
        });//]]>
        //<![CDATA[
        $(window).load(function(){
        $("#select4").on("change", function(){
            var nilai = $("#select4 :selected").attr("data-jenis");
            $("#jenis4").val(nilai);
        });
        });//]]>
    </script>	
		<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Produk Utama/Komoditi</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>