<?php
	$tglsekarang	= date('d-m-Y');
?>	
	<script>
            //<![CDATA[
            $(window).load(function(){
            $("#supplier").on("change", function(){
                var nilai  = $("#supplier :selected").attr("data-alamat");
                // var nilai2  = $("#pb :selected").attr("data-supplier_id");
                $("#alamat").val(nilai);
                // $("#supplier_id").val(nilai2);
            });
            });//]]>
    </script>
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-np/insert-np.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Nota Pembelian?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Nota Pembelian</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="supplier" id="supplier" class='chosen-select' required=="required">
									<option value="">--Pilih Supplier--</option>
									<?php 	
									$a="Produk Utama";
									$tampil=mysql_query("select * from suppliers where acc='1'	and deleted=0 order by company_name ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['supplier_id']; ?>" data-alamat="<?php echo $row['company_address']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Supplier</label>
						  <div class="controls">
							<input type="text" name="alamat" id="alamat" class=" form-control"" placeholder="Alamat" autocomplete="off" required=="required" readonly>
						  </div>
						</div>
					<div class="control-group">
					  <label for="inputEmail3" class="col-sm-2 control-label">Tipe Pembelian</label>
                      <div class="controls">
					    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td></td>
                            <td>
                            <div class="input-xlarge">
							<select name="tipebeli" id="select3" class='chosen-select' required=="required" onchange="tpt()">
							<option value="">--Tipe Pembelian--</option>
							<option value="Tunai">Pembelian Tunai</option>
							<option value="Kredit">Pembelian Kredit</option>
							<option value="Kontrak">Pembelian Kontrak</option>
							</select>
							</div>
                            </td>
                          </tr>
                        </table>
					  </div>	
					</div>
					<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nota Timbang</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="nt" id="nt" class='chosen-select' required=="required">
									<option value="">--Pilih Nota Timbang--</option>
									<?php 	
									$a="Produk Utama";
									$tampil=mysql_query("select * from ttr_main_scale where deleted=0 order by id_timbang DESC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_timbang']; ?>" ><?php echo $row['id_timbang']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" id="tpt">
						  <label for="inputEmail3" class="col-sm-2 control-label">Pakai Nota Uang Muka Pembelian</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="tp" id="tp" class='chosen-select' required=="required" onchange="jp()" >
								<option value="k">--Pilih Nota Uang Muka Pembelian--</option>
								<option value="m">Masukan Nota Uang Muka Pembelian</option>
								<option value="t">Tidak Perlu Masukan Nota Uang Muka Pembelian </option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>	
						</div>
						<div class="control-group" id="jnum">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nota Uang Muka Pembelian</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="num" id="num" class='chosen-select'>
									<option value="">--Pilih Nota Uang Muka Pembelian--</option>
									<?php 	
									$tampil=mysql_query("select * from nump where deleted=0 order by id_nump ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_nump']; ?>" ><?php echo $row['id_nump']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Nota Pembelian</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Pembelian</a>
						</li>
						<li>
							<a href="dash.php?hp=np&navbar=np&parent=pembelian">Nota Pembelian</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Nota Pembelian</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Nota Pembelian
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable">
									<thead>
										<tr class='thefilter'>
											<th>No.</th>
											<th>No. Form</th>
											<th>Tanggal</th>
											<th>Nama Supplier</th>
											<th>Alamat Supplier</th>
											<th>Nota Timbang</th>
											<th>Tipe Pembelian</th>
											<th>Nota Uang Muka</th>
											<th>Status</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.id_npb, a.tanggal,b.company_name,b.company_address,a.status,a.id_timbang,a.tipe_beli,a.id_nump from npb a, suppliers b where a.supplier_id=b.supplier_id and a.deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_npb']; ?></td>
											<td><?php echo dateBahasaIndo($row['tanggal']); ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['company_address']; ?></td>
											<td><?php echo $row['id_timbang']; ?></td>
											<td><?php echo $row['tipe_beli']; ?></td>
											<td><?php echo $row['id_nump']; ?></td>
											<td>
											<?php
											if($row['status']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else
											{
												echo "<span class='label label-success'>Selesai</span>";
											}
											?>
											</td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
													<?php
													if($row['status']==0 )
													{
													?>
													<li>
														<a href="dash.php?hp=npb2&idbaru=<?php echo $row['id_npb']; ?>&navbar=np&parent=pembelian">Revisi Nota Pembelian</a>
													</li>
												
													<?php
																										}
													else
													{
														if($row['id_nump']==""){
															?>
													<li>
														<a href="dash.php?hp=np3&idbaru=<?php echo $row['id_np']; ?>&navbar=np&parent=pembelian">Cetak Nota Pembelian</a>
													</li>
													<?php

														}
														else{
															?>
													<li>
														<a href="dash.php?hp=np3um&idbaru=<?php echo $row['id_np']; ?>&navbar=np&parent=pembelian">Cetak Nota Pembelian</a>
													</li>
													<?php
														}
														?>
													
													<?php
													}
													?>

												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>

	<script type="text/javascript">
			$("#jnum").hide();
			$("#tpt").hide();
			$( document ).ready(function() {
   				
   				
			});
			function jp() {
   				var tt = $("#tp").val();	
   				// alert("coba");
   				if(tt =='m'){
   				//alert();-->untuk menampilkan popups
   					$("#jnum").show();
   				}else if (tt=='t') {
   					$("#jnum").hide();
   					$("#jnum").val("");

   				}
   				else{
					$("#jnum").hide();
   					$("#jnum").val("");
   				}
   				
			};
		function tpt() {
   				var tt = $("#select3").val();	
   				// alert("coba");
   				if(tt =='Tunai'){
   				//alert();-->untuk menampilkan popups
   					$("#tpt").hide();
   				}
   				else{
					$("#tpt").show();
   					$("#tpt").val("");
   				}
   				
			};

</script>