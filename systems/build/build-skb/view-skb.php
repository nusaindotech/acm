<?php
$tglsekarang	= date('d-m-Y');

	?>
	   <script>
            //<![CDATA[
            $(window).load(function(){
            $("#supplier").on("change", function(){
                var nilai  = $("#supplier :selected").attr("data-alamat");
                var nilai2 = $("#supplier :selected").attr("data-ppn");
                $("#alamat").val(nilai);
                $("#ppn").val(nilai2);
            });
            });//]]>
            $(window).load(function(){
            $("#produk").on("change", function(){
                var nilai  = $("#produk :selected").attr("data-custom");
                $("#custom").val(nilai);
            });
            });//]]>
        </script>
<div class="modal hide fade modal-besar" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog ">
				<form action="build/build-skb/insert-skb.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Membuat Surat Kontrak ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Surat Kontrak</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form Permintaan</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<!-- <div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Jatuh Tempo</label>
						  <div class="controls">
							<input type="text" name="tgljt" class="form-control datepick" required="required" data-rule-email="true" autocomplete="off" >
						  </div>
						</div> -->
						
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="supplier" id="supplier" class='chosen-select'>
									<option value="">--Pilih Supplier --</option>
										<?php 	
									$tampil=mysql_query("select * from suppliers where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['supplier_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tipe Kontrak</label>
						  <div class="controls">
							<table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="tk" id="tk" class='chosen-select' required onchange="jp()">
									<option value="">--Pilih Tipe Kontrak--</option>
									<option value="Lokal">Lokal</option>
									<option value="Import USD">Import USD</option>
									<option value="Import USD Kurs">Import USD/Kurs</option>
									<option value="Forward Hedging">Forward Hedging</option>
									<option value="Premium Differential">Premium / Differential</option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
					  </div>
					  <div class="control-group" id="usd">
				  <label for="inputEmail3" class="col-sm-2 control-label">USD $ / karung</label>
				  <div class="controls">
					<input type="text" name="usd" id="vusd" class="form-control" placeholder="USD $" autocomplete="off" >
				  </div>
				</div>
				<div class="control-group" id="kurs">
				  <label for="inputEmail3" class="col-sm-2 control-label">Kurs Rp.</label>
				  <div class="controls">
					<input type="text" name="kurs" id="vkurs" class="form-control" placeholder="Kurs Rp." autocomplete="off" >
				  </div>
				</div>
				<div class="control-group" id="nfh">
				  <label for="inputEmail3" class="col-sm-2 control-label">Nilai Forward Hedging / karung</label>
				  <div class="controls">
					<input type="text" name="nfh" id="vfh" class="form-control" placeholder="Nilai Forward Hedging" autocomplete="off" >
				  </div>
				</div>
				<div class="control-group" id="npd">
				  <label for="inputEmail3" class="col-sm-2 control-label">Nilai Premium / Differential / karung</label>
				  <div class="controls">
					<input type="text" name="npd" id="vpd" class="form-control" placeholder="Nilai Premium / Differential" autocomplete="off" >
				  </div>
				</div>
				<div class="control-group" id="hpas">
				  <label for="inputEmail3" class="col-sm-2 control-label">Harga Pasar</label>
				  <div class="controls">
					<input type="text" name="hpas" id="vpas" class="form-control" placeholder="Harga Pasar" autocomplete="off" >
				  </div>
				<div class="control-group" id="hkon">
				  <label for="inputEmail3" class="col-sm-2 control-label">Harga Kontrak / Karung</label>
				  <div class="controls">
					<input type="text" name="hkon" id="vkon" class="form-control" placeholder="Harga Kontrak" autocomplete="off" >
				  </div>
				</div>
				</div>

						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Produk</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="produk" id="produk" class='chosen-select' onchange="jp2()">
									<option value="">--Pilih Produk--</option>
										<?php 	
									$tampil=mysql_query("select * from items where category ='produk utama'");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['item_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['name']; ?> - <?php echo $row['custom1']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						
				<table id="cklt" style="display: none;" class="items">	
						<tr>
							<td width="30%">
								<div class="control-group">
									<label class="control-label">Kadar Air</label>
									<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?php echo $data1["kadar_air"] ?>">
									</div> 	
								</div>
								<div class="control-group">
									<label class="control-label">Mouldy</label>
									<div class="controls">
										<input type="text" class="form-control" name="Mouldy" placeholder="0"  value="<?php echo $data1['mouldy']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Insect</label>
									<div class="controls">
										<input type="text" class="form-control" name="Insect" placeholder="0"  value="<?php echo $data1['insect']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Biji Pipih</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="BijiPipih" placeholder="0"  value="<?php echo $data1['biji_pipih']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Triple Bean</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="TripleBean" placeholder="0"  value="<?php echo $data1['triple_bean']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Broken Besar</label>
									<div class="controls">
										<input type="text" class="form-control" name="BrokenBesar" placeholder="0"  value="<?php echo $data1['broken_b']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Broken Kecil</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="BrokenKecil" placeholder="0"  value="<?php echo $data1['broken_k']?>">
									</div>
									  								</div>
								<div class="control-group">
									<label class="control-label">Sampah</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="Sampah" placeholder="0"  value="<?php echo $data1['sampah']?>">
									</div>
									  	
								</div>

								<div class="control-group">
									<label class="control-label">Slety</label>
									<div class="controls">
										<input type="text" class="form-control" name="Slety" placeholder="0"  value="<?php echo $data1['slety']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">FM</label>
									<div class="controls">
										<input type="text" class="form-control kotoran" onkeyup="load_Kotoran()" name="FM" placeholder="0"  value="<?php echo $data1['fm']?>">
									</div>
									  	
								</div>
								<div class="control-group">
									<label class="control-label">Total Kotoran</label>
									<div class="controls">
										<input type="text" class="form-control"  placeholder="0"  value="<?php echo $data1['total_kotor']?>" id="TotalKotoran" name="TotalKotoran" readonly>
									</div>
									  	
								</div>

								<div class="control-group">
									<label class="control-label">BC </label>
									<div class="controls">
										<input type="text" class="form-control" id="TotalBC" name="TotalBC" placeholder="0"  value="<?php echo $data1['total_bc']?>" >
									</div>
									  	
								</div>	
							</td>
							
							
						</tr>
						
				</table>
				

				<div class="control-group">	
					<table border="1" id="cgkh" style="display: none;" class="items">	
						<tr >
								<th width="20%">Komponen</td>
								<th width="20%">Test SBY</td>
								<th width="20%">Test Daerah</td>
								<th width="20%">Test Beli</td>
								
						</tr>	
						<tr>	
								<td>	Tester KA </td>
								<td>	
											<input type="text" class="form-control" name="KA_Sby" placeholder="0"  value="<?php echo $data1['tester_ka_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="KA_Daerah" placeholder="0"  value="<?php echo $data1['tester_ka_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="KA_beli" placeholder="0"  value="<?php echo $data1['tester_ka_testbeli']?>">
								</td>
						</tr>
							
						<tr>	
								<td>	Abu </td>
								<td>	
											<input type="text" class="form-control" name="Abu_Sby" placeholder="0"  value="<?php echo $data1['abu_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="Abu_Daerah" placeholder="0"  value="<?php echo $data1['abu_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="Abu_beli" placeholder="0"  value="<?php echo $data1['abu_testbeli']?>">
								</td>
						</tr>
						<tr>	
								<td>	AK </td>
								<td>	
											<input type="text" class="form-control" name="Ak_Sby" placeholder="0"  value="<?php echo $data1['ak_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="Ak_Daerah" placeholder="0"  value="<?php echo $data1['ak_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="Ak_beli" placeholder="0"  value="<?php echo $data1['ak_testbeli']?>">
								</td>
						</tr>
						<tr>	
								<td>	 BM </td>
								<td>	
											<input type="text" class="form-control" name="BM_Sby" placeholder="0"  value="<?php echo $data1['bm_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="BM_Daerah" placeholder="0"  value="<?php echo $data1['bm_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="BM_beli" placeholder="0"  value="<?php echo $data1['bm_daerahsby']?>">
								</td>
						</tr>
						<tr>	
								<td>	PL </td>
								<td>	
											<input type="text" class="form-control" name="PL_Sby" placeholder="0"  value="<?php echo $data1['pl_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="PL_Daerah" placeholder="0"  value="<?php echo $data1['pl_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="PL_beli" placeholder="0"  value="<?php echo $data1['pl_testbeli']?>">
								</td>
						</tr>
						<tr>	
								<td>	GG </td>
								<td>	
											<input type="text" class="form-control" name="GG_Sby" placeholder="0"  value="<?php echo $data1['gg_testsby']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="GG_Daerah" placeholder="0"  value="<?php echo $data1['gg_testdaerah']?>">
								</td>
								<td>	
											<input type="text" class="form-control" name="GG_beli" placeholder="0"  value="<?php echo $data1['gg_testbeli']?>">
								</td>
						</tr>

					</table>	

			
				<table id="ggcgkh" style="display: none;" class="items"> 	
					<tr>
						<td width="30%">
							<div class="control-group">
								<label class="control-label">Kadar Air</label>
								<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?=$data1["kadar_air"]?>" >
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Abu</label>
								<div class="controls">
										<input type="text" class="form-control  kotoran_gagang" onkeyup="kotoran_gagang()" name="Abu" placeholder="0"  value="<?php echo $data1['abu']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Daun</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()" name="Daun" placeholder="0"  value="<?php echo $data1['daun']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">BM</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()" name="BM" placeholder="0"  value="<?php echo $data1['biji_mati']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Polong</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()"name="polong" placeholder="0"  value="<?php echo $data1['polong']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">GG Besar</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()" name="GGBesar" placeholder="0"  value="<?php echo $data1['gg']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">FM</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_gagang" onkeyup="kotoran_gagang()" name="FM" placeholder="0"  value="<?php echo $data1['fm']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Total Kotoran</label>
								<div class="controls">
										<input type="text" class="form-control "  id ="Total_kotoran_gagang" name="TotalKotoran" placeholder="0"  value="<?php echo $data1['total_kotor']?>" readonly>
								</div> 	
							</div>
						</td>
					</tr>

				</table>
			
				<table id="kp" style="display: none;" class="items">	
					<tr>
						<td width="30%">
							<div class="control-group">
								<label class="control-label">Kadar Air</label>
								<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?=$data1["kadar_air"]?>" >
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Biji Rusak</label>
								<div class="controls">
										<input type="text" class="form-control  kotoran_kopi" onkeyup="kotoran_kopi()" name="BijiRusak" placeholder="0"  value="<?php echo $data1['biji_rusak']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Mouldy</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_kopi" onkeyup="kotoran_kopi()" name="Mouldy" placeholder="0"  value="<?php echo $data1['mouldy']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Biji Bagus</label>
								<div class="controls">
										<input type="text" class="form-control " onkeyup="kotoran_kopi()" name="BijiBagus" placeholder="0"  value="<?php echo $data1['biji_bagus']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Triase</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_kopi" onkeyup="kotoran_kopi()" name="Triase" placeholder="0"  value="<?php echo $data1['triase']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Bean Count</label>
								<div class="controls">
										<input type="text" class="form-control kotoran_kopi" onkeyup="kotoran_kopi()" name="BeanCount" placeholder="0"  value="<?php echo $data1['bean_count']?>">
								</div> 	
							</div>
							
							<div class="control-group">
								<label class="control-label">Total Kotoran</label>
								<div class="controls">
										<input type="text" class="form-control "  id ="Total_kotoran_kopi" name="TotalKotoran" placeholder="0"  value="<?php echo $data1['total_kotor']?>" readonly>
								</div> 	
							</div>
						</td>
					</tr>

				</table>
			
				<table id="jg" style="display: none;" class="items"> 	
					<tr>
						<td width="30%">
							<div class="control-group">
								<label class="control-label">Kadar Air</label>
								<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?=$data1["kadar_air"]?>" >
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Tongkol</label>
								<div class="controls">
										<input type="text" class="form-control  "  name="Tongkol" placeholder="0"  value="<?php echo $data1['tongkol']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Mouldy</label>
								<div class="controls">
										<input type="text" class="form-control" name="Mouldy" placeholder="0"  value="<?php echo $data1['mouldy']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Biji Mati</label>
								<div class="controls">
										<input type="text" class="form-control "  name="BijiMati" placeholder="0"  value="<?php echo $data1['biji_mati']?>">
								</div> 	
							</div>
							
						</td>
					</tr>

				</table>
			
				<table id="mt" style="display: none;" class="items">	
					<tr>
						<td width="30%">
							<div class="control-group">
								<label class="control-label">Kadar Air</label>
								<div class="controls">
										<input type="text" class="form-control" name="KA" placeholder="0"  value="<?=$data1["kadar_air"]?>" >
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Bean Count</label>
								<div class="controls">
										<input type="text" class="form-control " name="BeanCount" placeholder="0"  value="<?php echo $data1['bean_count']?>">
								</div> 	
							</div>
							
							<div class="control-group">
								<label class="control-label">Wringkle</label>
								<div class="controls">
										<input type="text" class="form-control"   name="Wringkle" placeholder="0"  value="<?php echo $data1['wringkle']?>">
								</div> 	
								</div>
						
							<div class="control-group">
								<label class="control-label">Spotted</label>
								<div class="controls">
										<input type="text" class="form-control " name="Spotted" placeholder="0"  value="<?php echo $data1['spotted']?>">
								</div> 	
							</div>
						
							<div class="control-group">
								<label class="control-label">Brown</label>
								<div class="controls">
										<input type="text" class="form-control " name="Brown" placeholder="0"  value="<?php echo $data1['brown']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Oily</label>
								<div class="controls">
										<input type="text" class="form-control " name="Oily" placeholder="0"  value="<?php echo $data1['oily']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Void</label>
								<div class="controls">
										<input type="text" class="form-control " name="Void" placeholder="0"  value="<?php echo $data1['void']?>">
								</div> 	
							</div>
							<div class="control-group">
								<label class="control-label">Biji Bagus</label>
								<div class="controls">
										<input type="text" class="form-control "  name="BijiBagus" placeholder="0"  value="<?php echo $data1['biji_bagus']?>">
								</div> 	
							</div>
						</td>
					</tr>

				</table>
				</div>
			
						<!-- <div class="control-group" id="JPU">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis Barang</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="jenis" class='chosen-select' required=="required" >
									<option value=" - ">--Jenis Barang--</option>
									<option value="Lokal">Lokal</option>
									<option value="Import">Import</option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div> -->
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Volume (Tonase)</label>
						  <div class="controls">
							<input type="number" step="0.001" name="volume" class="form-control" placeholder="volume" autocomplete="off" required=="required" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis Kemasan</label>
						  <div class="controls">
							<input type="text" name="kemasan" class="form-control" placeholder="kemasan" autocomplete="off" required=="required" >
						  </div>
						</div>
						<!-- <div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Uang Muka</label>
						  <div class="controls">
							<input type="number" step="0.1" name="um" class="form-control" placeholder="um" autocomplete="off" required=="required" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Claim</label>
						  <div class="controls">
							<input type="number" step="0.1" name="claim" class="form-control" placeholder="claim" autocomplete="off" required=="required" >
						  </div>
						</div> -->
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">tanggal kirim</label>
						  <div class="controls">
							<input type="text" name="tgl_kirim" class="form-control datepick" placeholder="tgl_kirim" autocomplete="off"  >
						  </div>
						  </div>
						  <div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">tanggal Jatuh Tempo</label>
						  <div class="controls">
							<input type="text" name="tgljt" class="form-control datepick" placeholder="tgl Jatuh Tempo" autocomplete="off"  >
						  </div>
						</div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Alamat Kirim</label>
						  <div class="controls">
							<input type="text" name="alamat" class="form-control" placeholder="alamat" autocomplete="off" required=="required" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Syarat dan Ketentuan</label>
						  <div class="controls">
							<input type="text" name="syarat" class="form-control" placeholder="syarat" autocomplete="off">
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Surat Kontrak</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=sk&navbar=sk&parent=pembelian">Surat Kontrak Pembelian</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Surat Kontrak Pembelian</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Surat Kontrak Pembelian
								</h3>
					  </div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No.</th>
											<th>No. Kontrak</th>
											<th>Tanggal</th>
											<th>Produk</th>
											<th>Supplier</th>
											<th>Perusahaan</th>
											<th>Tipe Kontrak</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select tb_skb.id_skb,tb_skb.tanggal,items.name,suppliers.company_name,home.nama as perusahaan,tb_skb.type_kontrak 
FROM tb_skb left OUTER JOIN items on tb_skb.item_id=items.item_id left OUTER JOIN suppliers on tb_skb.supplier_id = suppliers.supplier_id left OUTER JOIN home on tb_skb.id_home=home.id_home order by tb_skb.id_skb desc ");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_skb']; ?></td>
											<td><?php echo $row['tanggal']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['nama']; ?></td>
											<td><?php echo $row['type_kontrak']; ?></td>
											<td class='hidden-350'>
											<a class="btn btn-brown" id="edit-skb" data-id="<?php echo $row['id_skb'];?>"><i class="icon-edit"></i></a>
											<?php
												if($row['type_kontrak']=="Lokal")
												{ 

												?>
												<a class="btn btn-success" href="dash.php?hp=skb3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=pembelian"><i class="icon-print"></i></a>
												<?php	
												} else if($row['type_kontrak']=="Import USD"){
													?>
												<a class="btn btn-success" href="dash.php?hp=skbu3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=pembelian"><i class="icon-print"></i></a>
												<?php
												} else if($row['type_kontrak']=="Import USD Kurs"){
													?>
												<a class="btn btn-success" href="dash.php?hp=skbuk3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=pembelian"><i class="icon-print"></i></a>
												<?php
												} else if($row['type_kontrak']=="Forward Hedging"){
													?>
												<a class="btn btn-success" href="dash.php?hp=skbfh3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=pembelian"><i class="icon-print"></i></a>
												<?php
												} 
												?>
											
											<!-- <a class="btn btn-success" data-id="<?php echo $row['id_skb'];?>"><i class="icon-print"></i></a> -->
											<a class="btn btn-danger" href="build/build-skb/delete-skb.php?idbaru=<?php echo $row['id_skb']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-skb',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-skb/form-edit.php',
							{id:$(this).attr('data-id'), id2:$(this).attr('data-id2')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});

		
				
	</script>
	<script type="text/javascript">

			$("#npd").hide();
   				$("#usd").hide();
			$("#kurs").hide();
			$("#nfh").hide();
			$("#hkurs").hide();
			$("#husd").hide();
			$("#hpas").hide();
			// alert("Sas");
			
			$( document ).ready(function() {
   				
			});
			function coba(){
				var	isine=$("#custom").val();
				alert(isine);
			}
			function jp() {
   				var tt = $("#tk").val();	

   				if(tt =='Lokal'){
   				//alert();-->untuk menampilkan popups
   					// $("#jpn").show();
					$("#usd").hide();	
					$("#kurs").hide();
					$("#nfh").hide();
					$("#npd").hide();
			$("#hpas").hide();

					$("#husd").hide();
					$("#hkurs").show();
   					$("#husd").val(" ");
   					$("#usd").val(" ");
   					$("#kurs").val(" ");
   					$("#nfh").val(" ");

   				}
   				else if(tt =='Import USD'){
   				//alert();-->untuk menampilkan popups
   					$("#usd").show();	
   					$("#husd").hide();	
					$("#kurs").hide();
					$("#npd").hide();
			$("#hpas").hide();

					$("#hkurs").hide();
					$("#nfh").hide();
   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#nfh").val(" ");

   					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					$('#vkon').val(vusd);
					});
   				}
   				else if(tt =='Import USD Kurs'){
   				//alert();-->untuk menampilkan popups
   					$("#usd").show();	
   					$("#husd").hide();	
   					$("#hkurs").hide();	
					$("#npd").hide();
			$("#hpas").hide();

					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vkurs = $("#vkurs").val();
  					nilai = vusd * vkurs;
  					$('#vkon').val(nilai);
					});
					$( "#vkurs" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vkurs = $("#vkurs").val();
  					nilai = vusd * vkurs;
  					$('#vkon').val(nilai);
					});

					$("#kurs").show();
					$("#nfh").hide();
   					$("#nfh").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   				}
   				else if(tt =='Forward Hedging'){
   				//alert();-->untuk menampilkan popups

   					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vfh = $("#vfh").val();
  					nilai = vusd * vfh;
  					$('#vkon').val(nilai);
					});
					$( "#vfh" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vfh = $("#vfh").val();
  					nilai = vusd * vfh;
  					$('#vkon').val(nilai);
					});

   					$("#usd").show();	
					$("#kurs").hide();
					$("#nfh").show();
					$("#husd").hide();
					$("#hkurs").hide();
					$("#npd").hide();
			$("#hpas").hide();

   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#kurs").val(" ");
					$("#npd").hide();

   				}
   				else if(tt =='Premium Differential'){
   					$( "#vkurs" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});

   					$( "#vpd" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});

					$( "#vpas" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});



					$("#npd").show();
   					$("#usd").hide();	
					$("#kurs").show();
					$("#nfh").hide();
					$("#husd").hide();
					$("#hkurs").hide();
			$("#hpas").show();

   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#kurs").val(" ");
   				}
   				else{
					$("#usd").hide();
					$("#npd").hide();

					$("#kurs").hide();
					$("#nfh").hide();
					$("#husd").hide();
					$("#hkurs").hide();
   					$("#usd").val(" ");
   					$("#kurs").val(" ");
   					$("#nfh").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   				}
   				
			};
			
		function load_Kotoran() {
					
					var total = 0;
  							$.each($('.kotoran'), function( index, value ) {
			  					var isi = $(this).val();
			  					// var total =0;
			  					isi = parseInt(isi);
			  					console.log("isi ="+isi);

			  				
			  					if(isi==='NAN'){
			  						alert("NAN");
			  					}
			  					total = total + isi;

							});
								console.log("total = "+total);
							
			  					// console.log(total);
			  					$("#TotalKotoran").val(total);
			  					//$("#BCR").val(total/6);
			  					
				}
	
		function kotoran_gagang() {
					
					var total = 0;
  							$.each($('.kotoran_gagang'), function( index, value ) {
			  					var isi = $(this).val();
			  					// var total =0;
			  					isi = parseInt(isi);
			  					console.log("isi ="+isi);

			  				

			  					total = total + isi;

							});
								console.log("total = "+total);
							
			  					// console.log(total);
			  					$("#Total_kotoran_gagang ").val(total);
			  					//$("#BCR").val(total/6);
			  					
				}

		function kotoran_kopi() {
					
					var total = 0;
  							$.each($('.kotoran_kopi'), function( index, value ) {
			  					var isi = $(this).val();
			  					// var total =0;
			  					isi = parseInt(isi);
			  					console.log("isi ="+isi);

			  				

			  					total = total + isi;

							});
								console.log("total = "+total);
							
			  					// console.log(total);
			  					$("#Total_kotoran_kopi ").val(total);
			  					//$("#BCR").val(total/6);
			  					
				}

		</script>
		<script type="text/javascript">
		
			$(window).load(function(){	
   			function mproduk(){
   			    var custom1 = $("#produk :selected").attr("data-custom");	
   				custom1 = "#"+custom1;
   				alert(custom1);
   				$(custom1).show();
   			}
   				// var custom1 = $("#produk :selected").attr("data-custom");	
   				// custom1 = "#"+custom1;
   				// alert(custom1);
   				// $(custom1).show();

   				
   				
			
			});	

$( document ).ready(function() {
   				
			});
        //<![CDATA[
     function jp2() {
     	// var nilai  = $("#produk:selected").attr("data-cat");
   			//	var tt = $("#produk").val();	
   			var custom1 = $("#produk :selected").attr("data-cat");	
   				custom1 = "#"+custom1;
   				// alert(custom1);
   				
     	$(".items").hide();
   				$(custom1).show();
				// alert(nilai);
   				
   				
			};
   
		</script>
		

		
	<div class="modal hide modal-besar fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Surat Kontrak Pembelian</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>