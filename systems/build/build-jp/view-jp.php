			
			<div class="modal hide fade" id="myModalJenis" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-jp/insert-categories.php" method="POST" class="form-horizontal" onsubmit="return confirm('Anda Yakin Menambah Data ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Jenis Barang</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kode Jenis Barang</label>
						  <div class="controls">
							<input type="text" name="kodej" class="form-control" placeholder="Auto" required="require" autocomplete="off" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kategori Barang</label>
						  <div class="controls">
							<table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="kategorij" id="kategorij" class='chosen-select' required>
									<option value="">--Pilih Kategori--</option>
									<option value="Barang Umum">Barang Umum</option>
									<option value="Produk Sampingan">Produk Sampingan</option>
									<option value="Produk Utama">Produk Utama</option>
									<option value="Jasa">Jasa</option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Jenis Barang</label>
						  <div class="controls">
							<input type="text" name="namaj" class="form-control" placeholder="Nama Jenis Barang" required="require" autocomplete="off">
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=jp&navbar=jp&parent=master">Jenis Barang</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModalJenis' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Jenis Barang</a>
							
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Jenis Barang
								</h3>
					  </div>
					  <div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Kategori Barang</th>
											<th>Jenis Barang</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select * from t_categories where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['nama']; ?></td>
											<td><?php echo $row['jenis']; ?></td>
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-jenis" data-id="<?php echo $row['id_category'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-items/delete-items.php?iditems=<?php echo $row['id_category']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
					 
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>

	<script>
				$(function(){
					$(document).on('click','#edit-jenis',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-jp/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	<script type="text/javascript">
			$("#JPU").hide();
			$("#JPP").hide();
			$("#JPS").hide();
			$( document ).ready(function() {
   				
   				
			});
			function jp() {
   				var tt = $("#kategori").val();	

   				if(tt =='Barang Umum'){
   				//alert();-->untuk menampilkan popups
   					$("#JPU").show();
   					$("#JPP").hide();
   					$("#JPS").hide();
   					$("#JPP").val("");
   					$("#JPS").val("");
   				}else if (tt=='Produk Utama') {
   					 $("#JPP").show();
   					$("#JPU").hide();
   					$("#JPS").hide();
   					$("#JPS").val("");
   					$("#JPU").val("");

   				}
   				 else if (tt=='Produk Sampingan'){
   					$("#JPS").show();
   					$("#JPU").hide();
   					$("#JPP").hide();
   					$("#JPP").val("");
   					$("#JPU").val("");

   				}
   				else{
					$("#JPU").hide();
					$("#JPP").hide();
					$("#JPS").hide();
   					$("#JPP").val("");
   					$("#JPU").val("");
   					$("#JPS").val("");
   				}
   				
			};

		</script>
	
		<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Jenis Barang</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>