<?php
$tglsekarang	= date('d-m-Y');

	?>
	   <script>
            //<![CDATA[
            $(window).load(function(){
            $("#supplier").on("change", function(){
                var nilai  = $("#supplier :selected").attr("data-alamat");
                var nilai2 = $("#supplier :selected").attr("data-ppn");
                $("#alamat").val(nilai);
                $("#ppn").val(nilai2);
            });
            });//]]>
            $(window).load(function(){
            $("#produk").on("change", function(){
                var nilai  = $("#produk :selected").attr("data-custom");
                $("#custom").val(nilai);
            });
            });//]]>
        </script>
<div class="modal hide fade modal-besar" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog ">
				<form action="build/build-stb/insert-stb1.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Membuat Invoice Import ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Invoice Import</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form Invoice Import</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Number Letter of Contract </label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="pc" id="pc" class='chosen-select'>
									<option value="">--Choice Contract --</option>
										<?php 	
									$tampil=mysql_query("select id_skbi from t_skbi");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_skbi']; ?>"><?php echo $row['id_skbi']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Buyer</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="home" id="home" class='chosen-select'>
									<option value="">--Choice Buyer --</option>
										<?php 	
									$tampil=mysql_query("select * from home where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_home']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['nama']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier/Seller</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="supplier" id="supplier" class='chosen-select'>
									<option value="">--Choice Supplier --</option>
										<?php 	
									$tampil=mysql_query("select * from suppliers where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['supplier_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Agent</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="agent" id="agent" class='chosen-select'>
									<option value="">--Choice Agent --</option>
										<?php 	
									$tampil=mysql_query("select * from items where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['item_id']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Country Of Origin</label>
						  <div class="controls">
							<input type="text" name="coo" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Consignee</label>
						  <div class="controls">
							<textarea name="consignee" id="consignee" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Inco Term</label>
						  <div class="controls">
							<input type="text" name="it" id="it" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">CV/Rev No.</label>
						  <div class="controls">
							<input type="text" name="cv" id="cv" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Broker Ref</label>
						  <div class="controls">
							<input type="text" name="broker" id="broker" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Term Of Payment</label>
						  <div class="controls">
							<textarea name="top" id="top" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Description Of Goods</label>
						  <div class="controls">
							<textarea name="dog" id="dog" class="input-block-level"></textarea>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Quantity</label>
						  <div class="controls">
							<input type="text" name="qty" id="qty" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Unit Price</label>
						  <div class="controls">
							<input type="text" name="price" id="price" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Amount (USD)</label>
						  <div class="controls">
							<input type="text" name="jml" id="jml" class="form-control" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						<div class="control-group" >
						  <label for="inputEmail3" class="col-sm-2 control-label">Seller Bank Name</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="bank1" id="bank1" class='chosen-select'>
									<option value="">--Choice Bank Name --</option>
										<?php 	
									$tampil=mysql_query("select * from home where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_home']; ?>"  data-cat="<?php echo $row['custom1']; ?>" ><?php echo $row['nama']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Bank Address</label>
						  <div class="controls">
							<input type="text" name="ba1" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Reference Number</label>
						  <div class="controls">
							<input type="text" name="rn" class="form-control" data-rule-email="true" autocomplete="off" >
						  </div>
						</div>
						
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Invoice Import</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=invpi&navbar=invpi&parent=pembelian">Invoice Import </a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Invoice Import</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Invoice Import 
								</h3>
					  </div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No.</th>
											<th>No. Invoice</th>
											<th>Tanggal</th>
											<th>No. Kontrak</th>
											<th>Buyer</th>
											<th>Seller/Supplier</th>
											<th>Bank Name</th>
											<!-- <th>Tipe Kontrak</th> -->
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select tb_skb.id_skb,tb_skb.tanggal,items.name,suppliers.company_name,home.nama as perusahaan,tb_skb.type_kontrak 
FROM tb_skb left OUTER JOIN items on tb_skb.item_id=items.item_id left OUTER JOIN suppliers on tb_skb.supplier_id = suppliers.supplier_id left OUTER JOIN home on tb_skb.id_home=home.id_home");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_skb']; ?></td>
											<td><?php echo $row['tanggal']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['nama']; ?></td>
											<!-- <td><?php echo $row['type_kontrak']; ?></td> -->
											<td class='hidden-350'>
											<a class="btn btn-brown" id="edit-skb" data-id="<?php echo $row['id_skb'];?>"><i class="icon-edit"></i></a>
											<?php
												if($row['type_kontrak']=="Lokal")
												{ 

												?>
												<a class="btn btn-success" href="dash.php?hp=skb3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=pembelian"><i class="icon-print"></i></a>
												<?php	
												} else if($row['type_kontrak']=="Import USD"){
													?>
												<a class="btn btn-success" href="dash.php?hp=skbu3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=pembelian"><i class="icon-print"></i></a>
												<?php
												} else if($row['type_kontrak']=="Import USD Kurs"){
													?>
												<a class="btn btn-success" href="dash.php?hp=skbuk3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=pembelian"><i class="icon-print"></i></a>
												<?php
												} else if($row['type_kontrak']=="Forward Hedging"){
													?>
												<a class="btn btn-success" href="dash.php?hp=skbfh3&idbaru=<?php echo $row['id_skb']; ?>&navbar=skb&parent=pembelian"><i class="icon-print"></i></a>
												<?php
												} 
												?>
											
											<!-- <a class="btn btn-success" data-id="<?php echo $row['id_skb'];?>"><i class="icon-print"></i></a> -->
											<a class="btn btn-danger" href="build/build-skb/delete-skb.php?idbaru=<?php echo $row['id_skb']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-skb',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-skb/form-edit.php',
							{id:$(this).attr('data-id'), id2:$(this).attr('data-id2')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});

		
				
	</script>
	<script type="text/javascript">

			$("#npd").hide();
   				$("#usd").hide();
			$("#kurs").hide();
			$("#nfh").hide();
			$("#hkurs").hide();
			$("#husd").hide();
			$("#hpas").hide();
			
			$( document ).ready(function() {
   				
			});
			function coba(){
				var	isine=$("#custom").val();
				alert(isine);
			}
			function jp() {
   				var tt = $("#tk").val();	

   				if(tt =='Lokal'){
   				//alert();-->untuk menampilkan popups
   					// $("#jpn").show();
					$("#usd").hide();	
					$("#kurs").hide();
					$("#nfh").hide();
					$("#npd").hide();
			$("#hpas").hide();

					$("#husd").hide();
					$("#hkurs").show();
   					$("#husd").val(" ");
   					$("#usd").val(" ");
   					$("#kurs").val(" ");
   					$("#nfh").val(" ");

   				}
   				else if(tt =='Import USD'){
   				//alert();-->untuk menampilkan popups
   					$("#usd").show();	
   					$("#husd").hide();	
					$("#kurs").hide();
					$("#npd").hide();
			$("#hpas").hide();

					$("#hkurs").hide();
					$("#nfh").hide();
   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#nfh").val(" ");

   					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					$('#vkon').val(vusd);
					});
   				}
   				else if(tt =='Import USD Kurs'){
   				//alert();-->untuk menampilkan popups
   					$("#usd").show();	
   					$("#husd").hide();	
   					$("#hkurs").hide();	
					$("#npd").hide();
			$("#hpas").hide();

					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vkurs = $("#vkurs").val();
  					nilai = vusd * vkurs;
  					$('#vkon').val(nilai);
					});
					$( "#vkurs" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vkurs = $("#vkurs").val();
  					nilai = vusd * vkurs;
  					$('#vkon').val(nilai);
					});

					$("#kurs").show();
					$("#nfh").hide();
   					$("#nfh").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   				}
   				else if(tt =='Forward Hedging'){
   				//alert();-->untuk menampilkan popups

   					$( "#vusd" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vfh = $("#vfh").val();
  					nilai = vusd * vfh;
  					$('#vkon').val(nilai);
					});
					$( "#vfh" ).keyup(function() {
						// alert("vusd");
  					vusd = $("#vusd").val();
  					vfh = $("#vfh").val();
  					nilai = vusd * vfh;
  					$('#vkon').val(nilai);
					});

   					$("#usd").show();	
					$("#kurs").hide();
					$("#nfh").show();
					$("#husd").hide();
					$("#hkurs").hide();
					$("#npd").hide();
			$("#hpas").hide();

   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#kurs").val(" ");
					$("#npd").hide();

   				}
   				else if(tt =='Premium Differential'){
   					$( "#vkurs" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});

   					$( "#vpd" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});

					$( "#vpas" ).keyup(function() {
						// alert("vusd");
  					vkurs = parseInt($("#vkurs").val());
  					vpd = parseInt($("#vpd").val());
  					vpas = parseInt($("#vpas").val());
  					if(vkurs>0){

  					nilai = (vpas+vpd)*vkurs;
  					}else{
  					nilai = (vpas+vpd);

  					}
  					$('#vkon').val(nilai);
					});



					$("#npd").show();
   					$("#usd").hide();	
					$("#kurs").show();
					$("#nfh").hide();
					$("#husd").hide();
					$("#hkurs").hide();
			$("#hpas").show();

   					$("#usd").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   					$("#kurs").val(" ");
   				}
   				else{
					$("#usd").hide();
					$("#npd").hide();

					$("#kurs").hide();
					$("#nfh").hide();
					$("#husd").hide();
					$("#hkurs").hide();
   					$("#usd").val(" ");
   					$("#kurs").val(" ");
   					$("#nfh").val(" ");
   					$("#hkurs").val(" ");
   					$("#husd").val(" ");
   				}
   				
			};
			
		

		</script>
		
		

		
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Invoice Import Pembelian Import</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>