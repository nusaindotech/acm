<?php

	include "../../../auth/autho.php";

	function getData1()
	{
		$tampil1=mysql_query("select a.id_coa, a.number_coa1, a.number_coa2, a.number_coa3, a.number_coa4, a.coa_name, b.name_classification, c.name_coa_type from coa a, coa_classification b, coa_type c where a.custom1=b.id_coa_classification and a.custom2=c.id_coa_type and a.deleted=0 and parent = '1'");

		$data = [];
		while ($row = mysql_fetch_array($tampil1)) {
			$data[] = $row;
		}

		return $data;
	}

	function getData2()
	{
		$tampil2=mysql_query("select a.id_coa, a.number_coa1, a.number_coa2, a.number_coa3, a.number_coa4, a.coa_name, b.name_classification, c.name_coa_type from coa a, coa_classification b, coa_type c where a.custom1=b.id_coa_classification and a.custom2=c.id_coa_type and a.deleted=0 and parent = '2'");

		$data = [];
		while ($row = mysql_fetch_array($tampil2)) {
			$data[] = $row;
		}

		return $data;
	}

	function getData3()
	{
		$tampil3=mysql_query("select a.id_coa, a.number_coa1, a.number_coa2, a.number_coa3, a.number_coa4, a.coa_name, b.name_classification, c.name_coa_type from coa a, coa_classification b, coa_type c where a.custom1=b.id_coa_classification and a.custom2=c.id_coa_type and a.deleted=0 and parent = '3'");

		$data = [];
		while ($row = mysql_fetch_array($tampil3)) {
			$data[] = $row;
		}

		return $data;
	}

	function getData4()
	{
		$tampil4=mysql_query("select a.id_coa, a.number_coa1, a.number_coa2, a.number_coa3, a.number_coa4, a.coa_name, b.name_classification, c.name_coa_type from coa a, coa_classification b, coa_type c where a.custom1=b.id_coa_classification and a.custom2=c.id_coa_type and a.deleted=0 and parent = '4'");

		$data = [];
		while ($row = mysql_fetch_array($tampil4)) {
			$data[] = $row;
		}

		return $data;
	}

	$data1 = getData1();
	$data2 = getData2();
	$data3 = getData3();
	$data4 = getData4();

?>
			<link href="../dist/css/jquery.treegrid.css" rel="stylesheet" type="text/css" />
			<div class="modal fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-master-perkiraan/insert-coa.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Perkiraan</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Induk 1</label>
						  <div class="col-sm-10">
							<input type="text" name="induk1" class="form-control" placeholder="Kode COA Induk 1" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Induk 2</label>
						  <div class="col-sm-10">
							<input type="text" name="induk2" class="form-control" placeholder="Kode COA Induk 2" autocomplete="off">
						  </div>
						</div>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Induk 3</label>
						  <div class="col-sm-10">
							<input type="text" name="induk3" class="form-control" placeholder="Kode COA Induk 3" autocomplete="off">
						  </div>
						</div>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Induk 4</label>
						  <div class="col-sm-10">
							<input type="text" name="induk4" class="form-control" placeholder="Kode COA Induk 4" autocomplete="off">
						  </div>
						</div>
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
						  <div class="col-sm-10">
							<input type="text" name="nama" class="form-control" placeholder="Nama Perkiraan" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis</label>
                          <div class="col-sm-10">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="jenis" id="select2" class='chosen-select' required>
									<option value="">--Pilih Jenis--</option>
									<?php 	
									$tampil=mysql_query("select * from coa_type
											order by name_coa_type ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
										if($row['id_coa']==$data['id_coa'])
										{
									?>
									<option value="<?php echo $row['id_coa_type']; ?>"><?php echo $row['name_coa_type']; ?></option>
									<?php 		
										}
										else
										{
									?>
									<option value="<?php echo $row['id_coa_type']; ?>"><?php echo $row['name_coa_type']; ?></option>
									<?php 		
										}
									
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Klasifikasi</label>
                          <div class="col-sm-10">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="klasifikasi" id="select" class='chosen-select' required>
									<option value="">--Pilih Klasifikasi--</option>
									<?php 	
									$tampil=mysql_query("select * from coa_classification
											order by name_classification ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_coa_classification']; ?>"><?php echo $row['name_classification']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Master Perkiraan</li>
						</ul><!-- /.breadcrumb -->

					</div>

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">
						<!-- #section:settings.box -->
						<?php include "container.php"; ?>

						<!-- /section:settings.box -->
						<div class="page-header">
							<h1>
								List COA
							</h1>
						</div><!-- /.page-header -->
						<div class="row">
						<div class="col-xs-12">
						<div class="row">
						<div class="col-xs-12">
							<a href="#myModal" data-toggle="modal" class="btn btn-white btn-info btn-bold" >
							<i class="ace-icon fa fa-plus bigger-160"></i>
								Tambah Perkiraan
							</a><br/><br/>
								<div class="table-header">
									Daftar COA
								</div>
								<div>
								<?php $no = 1; ?>
								<table id="example1" class="table table-striped table-bordered table-hover tree">
										<thead>
											<tr>
												<th></th>
												<th>No</th>
												<th>Induk 1</th>
												<th>Induk 2</th>
												<th>Induk 3</th>
												<th>Induk 4</th>
												<th>Nama Perkiraan</th>
												<th>Jenis</th>
												<th>Klasifikasi</th>
												<th class='hidden-350'>Aksi</th>
											</tr>
										</thead>
										<tbody>
										<?php
											foreach ($data1 as $d1):
												$num1 = $no;
										?>
											<tr class="treegrid-<?= $no ?>">
												<td></td>
												<td><?= $no++ ?></td>
												<td><?= $d1['number_coa1'] ?></td>
												<td><?= $d1['number_coa2'] ?></td>
												<td><?= $d1['number_coa3'] ?></td>
												<td><?= $d1['number_coa4'] ?></td>
												<td><?= $d1['coa_name'] ?></td>
												<td><?= $d1['name_coa_type'] ?></td>
												<td><?= $d1['name_classification'] ?></td>
												<td>
												<div class="hidden-sm hidden-xs btn-group">
													<a class="btn btn-xs btn-info">
														<i class="ace-icon fa fa-pencil bigger-120" id="edit-coa" data-id="<?php echo $row['id_coa'];?>"></i></a>


													<a class="btn btn-xs btn-danger" href="build/build-coa/delete-coa.php?idcoa=<?php echo $d1['id_coa']; ?>">
														<i class="ace-icon fa fa-trash-o bigger-120"></i></a>
												</div>
												</td>
											</tr>
											<?php
												foreach ($data2 as $d2):
													if ($d2['number_coa1'] == $d1['number_coa1']):
														$num2 = $no;
											?>
												<tr class="treegrid-<?= $no ?> treegrid-parent-<?= $num1 ?>">
													<td></td>
													<td><?= $no++ ?></td>
													<td><?= $d2['number_coa1'] ?></td>
													<td><?= $d2['number_coa2'] ?></td>
													<td><?= $d2['number_coa3'] ?></td>
													<td><?= $d2['number_coa4'] ?></td>
													<td><?= $d2['coa_name'] ?></td>
													<td><?= $d2['name_coa_type'] ?></td>
													<td><?= $d2['name_classification'] ?></td>
													<td>
													<div class="hidden-sm hidden-xs btn-group">
														<a class="btn btn-xs btn-info">
															<i class="ace-icon fa fa-pencil bigger-120" id="edit-coa" data-id="<?php echo $d2['id_coa'];?>"></i></a>


														<a class="btn btn-xs btn-danger" href="build/build-coa/delete-coa.php?idcoa=<?php echo $d2['id_coa']; ?>">
															<i class="ace-icon fa fa-trash-o bigger-120"></i></a>
													</div>
													</td>
													
												</tr>
												<?php
													foreach ($data3 as $d3):
														if ($d3['number_coa2'] == $d2['number_coa2']):
															$num3 = $no;
												?>
													<tr class="treegrid-<?= $no ?> treegrid-parent-<?= $num2 ?>">
														<td></td>
														<td><?= $no++ ?></td>
														<td><?= $d3['number_coa1'] ?></td>
														<td><?= $d3['number_coa2'] ?></td>
														<td><?= $d3['number_coa3'] ?></td>
														<td><?= $d3['number_coa4'] ?></td>
														<td><?= $d3['coa_name'] ?></td>
														<td><?= $d3['name_coa_type'] ?></td>
														<td><?= $d3['name_classification'] ?></td>
														<td>
														<div class="hidden-sm hidden-xs btn-group">
															<a class="btn btn-xs btn-info">
																<i class="ace-icon fa fa-pencil bigger-120" id="edit-coa" data-id="<?php echo $d3['id_coa'];?>"></i></a>


															<a class="btn btn-xs btn-danger" href="build/build-coa/delete-coa.php?idcoa=<?php echo $d2['id_coa']; ?>">
																<i class="ace-icon fa fa-trash-o bigger-120"></i></a>
														</div>
														</td>
														
													</tr>
													<?php
														foreach ($data4 as $d4):
															if ($d4['number_coa3'] == $d3['number_coa3']):
													?>
														<tr class="treegrid-<?= $no ?> treegrid-parent-<?= $num3 ?>">
															<td></td>
															<td><?= $no++ ?></td>
															<td><?= $d4['number_coa1'] ?></td>
															<td><?= $d4['number_coa2'] ?></td>
															<td><?= $d4['number_coa3'] ?></td>
															<td><?= $d4['number_coa4'] ?></td>
															<td><?= $d4['coa_name'] ?></td>
															<td><?= $d4['name_coa_type'] ?></td>
															<td><?= $d4['name_classification'] ?></td>
															<td>
															<div class="hidden-sm hidden-xs btn-group">
																<a class="btn btn-xs btn-info">
																	<i class="ace-icon fa fa-pencil bigger-120" id="edit-coa" data-id="<?php echo $d4['id_coa'];?>"></i></a>


																<a class="btn btn-xs btn-danger" href="build/build-coa/delete-coa.php?idcoa=<?php echo $d2['id_coa']; ?>">
																	<i class="ace-icon fa fa-trash-o bigger-120"></i></a>
															</div>
															</td>
															
														</tr>
													<?php
															endif;
														endforeach;
													?>
												<?php
														endif;
													endforeach;
												?>
											<?php
													endif;
												endforeach;
											?>
										<?php
											endforeach;
										?>
										</tbody>
									</table>
								</div>
							</div><!-- /.col -->
					</div><!-- /.page-content -->
					</div>
				
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="../dist/js/jquery.treegrid.js" type="text/javascript"></script>
	<script src="../dist/js/jquery.treegrid.bootstrap3.js" type="text/javascript"></script>
	<!-- <script src="build/build-master-perkiraan/script.js"></script> -->
	<script>
	// $("#example1 tr:odd").click(function(){ // :odd not .odd
	//    $(this).next("tr").toggle();
	//    $(this).find(".arrow").toggleClass("up");
	// });
		$(function(){
			$(document).on('click','#edit-coa',function(e){
				e.preventDefault();
				$("#view").modal('show');
				$.post('build/build-master-perkiraan/form-edit.php',
					{id:$(this).attr('data-id')},
					function(html){
						$(".modal-body2").html(html);
					}  
				);
			});

			$('.tree').treegrid();
		});

	</script>
	
	<div class="modal fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Perkiraan</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>