<?php
include "../../../auth/autho.php";
// $sql = "SELECT id, name, text, link as href, parent_id FROM treeview";
// $res = mysqli_query($conn, $sql) or die("database error:". mysqli_error($conn));	
// //iterate on results row and create new index array of data
// while( $row = mysqli_fetch_assoc($res) ) { 
//   $data[] = $row;
// }
// $itemsByReference = array();
// // Build array of item references:
// foreach($data as $key => &$item) {
//    $itemsByReference[$item['id']] = &$item;
// }
// // Set items as children of the relevant parent item.
// foreach($data as $key => &$item)  {    
//    if($item['parent_id'] && isset($itemsByReference[$item['parent_id']])) {
// 	  $itemsByReference [$item['parent_id']]['nodes'][] = &$item;
// 	}
// }
// // Remove items that were added to parents elsewhere:
// foreach($data as $key => &$item) {
//    if($item['parent_id'] && isset($itemsByReference[$item['parent_id']]))
// 	  unset($data[$key]);
// }

$sql = "SELECT id_coa AS id, coa_name AS text, number_coa1, number_coa2, number_coa3, number_coa4 FROM coa";
$res = mysql_query($sql);

while( $row = mysql_fetch_assoc($res) ) { 
  $data[] = $row;
}

$dataByReference = [];
foreach ($data as $key => &$item) {
	if (empty($item['number_coa2'])) {
		$dataByReference[$item['number_coa1']] = &$item;
	} elseif (empty($item['number_coa3'])) {
		$dataByReference[$item['number_coa2']] = &$item;
	} elseif (empty($item['number_coa4'])) {
		$dataByReference[$item['number_coa3']] = &$item;
	} elseif (!empty($item['number_coa4'])) {
		$dataByReference[$item['number_coa4']] = &$item;
	} 
}


foreach ($data as $key => &$item) {
	if (!empty($item['number_coa2']) && 
		empty($item['number_coa3']) &&
		isset($dataByReference[$item['number_coa1']])) {

		$dataByReference[$item['number_coa1']]['nodes'][] = &$item;
	} elseif (!empty($item['number_coa3']) &&  
			empty($item['number_coa4']) &&
			isset($dataByReference[$item['number_coa2']])) {

		$dataByReference[$item['number_coa2']]['nodes'][] = &$item;
	} elseif (!empty($item['number_coa4']) && 
			isset($dataByReference[$item['number_coa3']])) {

		$dataByReference[$item['number_coa3']]['nodes'][] = &$item;
	}
}

$newdata = [];

foreach ($data as $key => &$item) {
	if (empty($item['number_coa2'])) {
		$newdata[] = $item;
	}
}

// Encode:
echo json_encode($newdata);
?>