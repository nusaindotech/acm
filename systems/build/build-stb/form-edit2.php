	<?php 
		include "../../../auth/autho.php";
		$idspp			=$_POST['id'];
		
		$tglsekarang	= date('d-m-Y');
	?>
	<div class="modal-body">
		<form action="build/build-po/insert-po1.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Pesanan Pembelian ?');">
			<input type="hidden" value="<?php echo $idspp;?>" name="idspp">
			
			<div class="control-group">
				<label for="textfield" class="control-label">Tanggal</label>
				<div class="controls">
					<input type="text" name="tanggal" id="tanggal" class="input-xlarge" value="<?php echo $tglsekarang; ?>" autocomplete="off" readonly>
				</div>
			</div>
			
			<div class="control-group">
				<label for="textfield" class="control-label">No. Permintaan Pembelian </label>
				<div class="controls">
					<input type="text" name="ps" id="ps" placeholder="No. Permintaan Pembelian" class="input-xlarge" value="<?php echo $idspp; ?>" autocomplete="off" readonly>
				</div>
			</div>
			
			<div class="control-group">
				<label for="textfield" class="control-label">Supplier</label>
				<div class="controls">
					<select name="supplier" id="supplier" class='chosen-select' required=="required">
						<option value="">--Pilih Supplier--</option>
						<?php 	
						$tampil=mysql_query("select supplier_id, company_name from suppliers
								where deleted=0 and acc=1
								order by company_name ASC");
						$no=1;
						while ($row=mysql_fetch_array($tampil))
						{
						?>
						<option value="<?php echo $row['supplier_id']; ?>"><?php echo $row['company_name']; ?></option>
						<?php 
						$no++;
						} 
						?>
					</select>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Buat Pesanan Pembelian/PO</button>
			</div>
        </form>
	</div>