<?php
	$tglsekarang	= date('d-m-Y');
	$bulan			= $_GET['idbaru'];
	$tahun		= $_GET['idbaru2'];
	// $jenisbeli		= $_GET['idbaru3'];

function DateToIndo($date) { // fungsi atau method untuk mengubah tanggal ke format indonesia
   // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
		$BulanIndo = array("Januari", "Februari", "Maret",
						   "April", "Mei", "Juni",
						   "Juli", "Agustus", "September",
						   "Oktober", "November", "Desember");
	
		// $tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
		$bulan = $date; // memisahkan format bulan menggunakan substring
		// $tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring
		
		$result = " " . $BulanIndo[(int)$bulan-1];
		return($result);
}
?>		
		<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-po/insert-po2.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						
					  </div>
					  </div>
					   
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
			<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=lhi&navbar=lhi&parent=pembelian"> Laporan Hutang Import</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th">  Laporan Hutang Import</i>								</h3>
								
							<!-- 	<div align="right">
									<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Pesanan Pembelian</a>								</div> -->
							</div>
							<!-- <div class="box-content"> -->
								<div class="invoice-info">
								<table >
										<tr>
			                             <td style="width:100%"><h4><center><b> Laporan Hutang Import </b></center></h4></td>
			                              <td></td>
                    					</tr>
                    			
								</table>	
                    				<table>
									 <!-- 	<tr>
		                                 <th align="left">Bulan</th>
		                                 <td style="width:50%;">:&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo(DateToIndo($bulan));?></b></td>
                                    	</tr>
                                    	<tr>
	                                     <th align="left">Tahun</th>
                              		      <td>:&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo $tahun;?></b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		                                     
                                    	</tr> -->
                                    	<tr>
		                                 <th align="left">Periode</th>
		                                 <td style="width:50%;">:&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo(DateToIndo($bulan)." ".$tahun);?></b></td>
                                    	</tr>
										
								</table>
                                  <br />
							  </div>
								<table class="table table-striped table-invoice" >
									<thead>
										<tr>
											<th rowspan ="2">No. </th>
											<th rowspan ="2">No. Kode Supplier</th>
											<th rowspan="2">Nama Supplier</th>
											<th colspan="3">Pengurangan</th>
											<th colspan="3">Penambahan</th>
											<th colspan="3">Pembayaran</th>
											<th colspan="3">Penambahan Biaya Import Ocean Freight</th>
											<th colspan="3">Penambahan Biaya Import Insurance</th>
											<th colspan="3">Pembayaran Biaya Import Ocean Freight</th>
											<th colspan="3">Pembayaran Biaya Import Insurance</th>
											<th colspan="3">Saldo Akhir</th>
											<th colspan="3">Hutang</th>
											<th colspan="3">Piutang</th>
											
										</tr>
										<tr>
											
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											<th>Dollar</th>
											<th>Euro</th>
											<th>Rupiah</th>
											
										
										</tr>
										
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("SELECT lpb.id_lpb, (lpb.date_transaction) as tanggallpb,(lpb.id_po) as No_pesanan_pembelian,(po.custom1) as jenis_pembelian, (suppliers.company_code) as kode_supplier, suppliers.company_name, suppliers.ppn, (po.type_op) as tipe_pembelian,(items.item_number) as kode_barang, (items.name) as nama_barang, (detail_lpb.unit_qty) as kuantitas, (detail_lpb.unit_price) as harga_satuan,(detail_lpb.custom3) as diskon,(lpb.custom5) as dpp,(lpb.custom6) as ppnlpb,(lpb.custom7) as grand_total, (po.custom10) as umk from lpb left outer JOIN po on lpb.id_po=po.id_po left OUTER join suppliers on po.supplier_id=suppliers.supplier_id LEFT OUTER join detail_lpb on detail_lpb.id_lpb=lpb.id_lpb left OUTER join items on items.item_id=detail_lpb.item_id where month(lpb.date_transaction) ='$bulan' and year(lpb.date_transaction) ='$tahun' order by lpb.id_lpb ASC");

									$no=1;
									 
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											 <td>&nbsp;<?php echo $no; ?></td>
											<td >&nbsp;<?php echo $row['id_lpb']; ?></td>
											<td >&nbsp;<?php echo $row['tanggallpb']; ?></td>
											<td >&nbsp;<?php echo $row['No_pesanan_pembelian']; ?></td>
											<td >&nbsp;<?php echo $row['jenis_pembelian']; ?></td>
											<td >&nbsp;<?php echo ($row['kode_supplier']); ?></td>
											<td >&nbsp;<?php echo ($row['company_name']); ?></td>
											<td >&nbsp;<?php echo ($row['ppn']); ?></td>
											<td >&nbsp;<?php echo ($row['tipe_pembelian']); ?></td>
											<td >&nbsp;<?php echo ($row['kode_barang']); ?></td>
											<td >&nbsp;<?php echo ($row['nama_barang']); ?></td>
											<td >&nbsp;<?php echo number_format($row['kuantitas'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['harga_satuan'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['diskon'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['dpp'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['ppnlpb'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['grand_total'],0); ?></td>
											<td >&nbsp;<?php if($row['tipe_pembelian']=="Tunai"){echo number_format($row['grand_total'],0);}else{echo ("-");}?></td>
											<td >&nbsp;<?php if($row['tipe_pembelian']=="Kredit"){echo number_format($row['grand_total'],0);}else{echo ("-");}?></td>
											<td >&nbsp;<?php if($row['tipe_pembelian']=="Kredit"){echo number_format($row['umk'],0);}else{echo ("-");}?></td>
											<td >&nbsp;<?php 
											$tampung=$row['grand_total']-$row['umk'];
											if($row['tipe_pembelian']=="Kredit"){echo number_format($tampung,0);}else{echo ("-");}?></td>
											<td >&nbsp;<?php echo ("-"); ?></td>
											<td >&nbsp;<?php echo ("-"); ?></td>

											<!-- nama_barang -->
											

										</tr>
									<?php 
									
									$no++;
									} 
									
									$tampil=mysql_query("SELECT detail_bapj.*,t_bapj.tanggal as tanggalbapj, (po.custom1) as jenis_pembelian, (suppliers.company_code) as kode_supplier, suppliers.company_name, suppliers.ppn, (po.type_op) as tipe_pembelian,(items.item_number) as kode_barang, (items.name) as nama_barang, (detail_bapj.harga) as harga_satuan,detail_bapj.diskon,t_bapj.dpp,t_bapj.pph ,(t_bapj.gt) as grand_total, (po.custom10) as umk from detail_bapj 
INNER JOIN t_bapj on t_bapj.id_bapj=detail_bapj.id_bapj INNER JOIN po on po.id_po = t_bapj.id_po INNER JOIN suppliers on t_bapj.supplier=suppliers.supplier_id INNER JOIN items on items.item_id=detail_bapj.item_id
where MONTH(t_bapj.tanggal)='$bulan' and YEAR(t_bapj.tanggal)='$tahun'");

									
									 
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											 <td>&nbsp;<?php echo $no; ?></td>
											<td >&nbsp;<?php echo $row['id_bapj']; ?></td>
											<td >&nbsp;<?php echo $row['tanggalbapj']; ?></td>
											<td >&nbsp;<?php echo $row['No_pesanan_pembelian']; ?></td>
											<td >&nbsp;<?php echo $row['jenis_pembelian']; ?></td>
											<td >&nbsp;<?php echo ($row['kode_supplier']); ?></td>
											<td >&nbsp;<?php echo ($row['company_name']); ?></td>
											<td >&nbsp;<?php echo ($row['ppn']); ?></td>
											<td >&nbsp;<?php echo ($row['tipe_pembelian']); ?></td>
											<td >&nbsp;<?php echo ($row['kode_barang']); ?></td>
											<td >&nbsp;<?php echo ($row['nama_barang']); ?></td>
											<td >&nbsp;<?php echo number_format($row['kuantitas'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['harga_satuan'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['diskon'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['dpp'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['pph'],0); ?></td>
											<td >&nbsp;<?php echo number_format($row['grand_total'],0); ?></td>
											<td >&nbsp;<?php if($row['tipe_pembelian']=="Tunai"){echo number_format($row['grand_total'],0);}else{echo ("-");}?></td>
											<td >&nbsp;<?php if($row['tipe_pembelian']=="Kredit"){echo number_format($row['grand_total'],0);}else{echo ("-");}?></td>
											<td >&nbsp;<?php if($row['tipe_pembelian']=="Kredit"){echo number_format($row['umk'],0);}else{echo ("-");}?></td>
											<td >&nbsp;<?php 
											$tampung=$row['grand_total']-$row['umk'];
											if($row['tipe_pembelian']=="Kredit"){echo number_format($tampung,0);}else{echo ("-");}?></td>
											<td >&nbsp;<?php echo ("-"); ?></td>
											<td >&nbsp;<?php echo ("-"); ?></td>

											<!-- nama_barang -->
											

										</tr>
									<?php 
									
									$no++;
									} 
									?>
									
                                  
									</tbody>
								</table>
								
							<!-- </div> -->
						</div>
					</div>
					
				
				
				</div>
				
			
				
