	<?php 
		include "../../../auth/autho.php";
		$id=$_POST['id'];
		$query = mysql_query("select * from coa where id_coa='$id'") or die(mysql_error());
		$data = mysql_fetch_array($query);
	?>
	<div class="modal-body">
		<form role="form" action="build/build-coa/update-coa.php" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped'>
			<input type="hidden" value="<?php echo $id;?>" name="id_coa">
			
			<div class="control-group">
				<label class="control-label">Induk 1</label>
				<div class="controls">
					<input type="text" id="textfield" class="input-xlarge" name="induk1" placeholder="Isikan Kode Induk 1" required="require" value="<?php echo $data['number_coa1'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Induk 2</label>
				<div class="controls">
					<input type="text" name="induk2" id="textfield" placeholder="Isikan Kode Induk 2" class="input-xlarge" value="<?php echo $data['number_coa2'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Induk 3</label>
				<div class="controls">
					<input type="text" name="induk3" id="textfield" placeholder="Isikan Kode Induk 3" class="input-xlarge" value="<?php echo $data['number_coa3'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Induk 4</label>
				<div class="controls">
					<input type="text" name="induk4" id="textfield" placeholder="Isikan Kode Induk 4" class="input-xlarge" value="<?php echo $data['number_coa4'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama</label>
				<div class="controls">
					<input type="text" name="nama" id="textfield" placeholder="Nama Perkiraan" class="input-xlarge" value="<?php echo $data['coa_name'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Jenis</label>
				<div class="controls">
					<select name="jenis" id="select2" class='chosen-select'>
						<option value="">--Pilih Jenis--</option>
						<?php 	
						$tampil=mysql_query("select * from coa_type
								order by name_coa_type ASC");
						$no=1;
						while ($row=mysql_fetch_array($tampil))
						{
							if($row['id_coa_type']==$data['custom2'])
							{
						?>
						<option value="<?php echo $row['id_coa_type']; ?>" selected><?php echo $row['name_coa_type']; ?></option>
						<?php 		
							}
							else
							{
						?>
						<option value="<?php echo $row['id_coa_type']; ?>"><?php echo $row['name_coa_type']; ?></option>
						<?php 		
							}
						$no++;
						} 
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Klasifikasi</label>
				<div class="controls">
					<select name="klasifikasi" id="select" class='chosen-select'>
						<option value="">--Pilih Klasifikasi--</option>
						<?php 	
						$tampil=mysql_query("select * from coa_classification
								order by name_classification ASC");
						$no=1;
						while ($row=mysql_fetch_array($tampil))
						{
							if($row['id_coa_classification']==$data['custom1'])
							{
						?>
						<option value="<?php echo $row['id_coa_classification']; ?>" selected><?php echo $row['name_classification']; ?></option>
						<?php		
							}
							else
							{
						?>
						<option value="<?php echo $row['id_coa_classification']; ?>"><?php echo $row['name_classification']; ?></option>
						<?php 		
							}
						$no++;
						} 
						?>
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>