			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-coa/insert-coa.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Perkiraan</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Induk 1</label>
						  <div class="controls">
							<input type="text" name="induk1" class="form-control" placeholder="Kode COA Induk 1" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Induk 2</label>
						  <div class="controls">
							<input type="text" name="induk2" class="form-control" placeholder="Kode COA Induk 2" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Induk 3</label>
						  <div class="controls">
							<input type="text" name="induk3" class="form-control" placeholder="Kode COA Induk 3" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Induk 4</label>
						  <div class="controls">
							<input type="text" name="induk4" class="form-control" placeholder="Kode COA Induk 4" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
						  <div class="controls">
							<input type="text" name="nama" class="form-control" placeholder="Nama Perkiraan" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
						
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Jenis</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="jenis" id="select2" class='chosen-select' required>
									<option value="">--Pilih Jenis--</option>
									<?php 	
									$tampil=mysql_query("select * from coa_type
											order by name_coa_type ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
										if($row['id_coa']==$data['id_coa'])
										{
									?>
									<option value="<?php echo $row['id_coa_type']; ?>"><?php echo $row['name_coa_type']; ?></option>
									<?php 		
										}
										else
										{
									?>
									<option value="<?php echo $row['id_coa_type']; ?>"><?php echo $row['name_coa_type']; ?></option>
									<?php 		
										}
									
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Klasifikasi</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="klasifikasi" id="select" class='chosen-select' required>
									<option value="">--Pilih Klasifikasi--</option>
									<?php 	
									$tampil=mysql_query("select * from coa_classification
											order by name_classification ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_coa_classification']; ?>"><?php echo $row['name_classification']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=perkiraan&navbar=perkiraan&parent=master">Perkiraan</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
						<div class="span12">
							<div class="box box-color">
								<div class="box-title">
									<h3>
										<i class="icon-reorder"></i>
										Master Perkiraan
									</h3>
									<ul class="tabs">
										<li class="active">
											<a href="#t7" data-toggle="tab">Daftar Perkiraan</a>
										</li>
										<li>
											<a href="#t8" data-toggle="tab">Gambaran Perkiraan</a>
										</li>
									</ul>
								</div>
								<div class="box-content">
									<div class="tab-content">
										<div class="tab-pane active" id="t7">
												<div class="box box-color box-bordered">
													<br>						
													<div align="right">
													<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Perkiraan</a>
													</div>
													<div class="box-title">
														<h3>
															<i class="icon-reorder"></i>
															Daftar Perkiraan
														</h3>
													</div>
													<div class="box-content nopadding">
														<table class="table table-hover table-nomargin dataTable table-bordered">
															<thead>
																<tr>
																	<th>No</th>
																	<th>Induk 1</th>
																	<th>Induk 2</th>
																	<th>Induk 3</th>
																	<th>Induk 4</th>
																	<th>Nama Perkiraan</th>
																	<th>Jenis</th>
																	<th>Klasifikasi</th>
																	<th class='hidden-350'>Aksi</th>
																</tr>
															</thead>
															<tbody>
															<?php 	
																								
															$tampil=mysql_query("select a.id_coa, a.number_coa1, a.number_coa2, a.number_coa3, a.number_coa4,
																	a.coa_name, b.name_classification, c.name_coa_type
																	from coa a, coa_classification b, coa_type c
																	where a.custom1=b.id_coa_classification and a.custom2=c.id_coa_type and a.deleted=0");
															$no=1;
															while ($row=mysql_fetch_array($tampil))
															{
															?>
																<tr>
																	<td><?php echo $no; ?></td>
																	<td><?php echo $row['number_coa1']; ?></td>
																	<td><?php echo $row['number_coa2']; ?></td>
																	<td><?php echo $row['number_coa3']; ?></td>
																	<td><?php echo $row['number_coa4']; ?></td>
																	<td><?php echo $row['coa_name']; ?></td>
																	<td><?php echo $row['name_coa_type']; ?></td>
																	<td><?php echo $row['name_classification']; ?></td>
																	<td class='hidden-350'>
																	<a class="btn btn-primary" id="edit-coa" data-id="<?php echo $row['id_coa'];?>"><i class="icon-edit"></i></a>
																	<a class="btn btn-danger" href="build/build-coa/delete-coa.php?idcoa=<?php echo $row['id_coa']; ?>"><i class="icon-trash"></i></a>
																	</td>
																</tr>
															<?php 
															$no++;
															} 
															?>
															</tbody>
														</table>
													</div>
												</div>
										</div>
										<div class="tab-pane" id="t8">
											<h4>Second tab</h4>
											<div class="span4">
									<h4>Default filetree</h4>
									<div class="filetree">
										<ul>
								            <li id="key1" title="Look, a tool tip!">item1 with key and tooltip</li>
								            <li id="key2" class="selected">item2: selected on init</li>
								            <li id="key3" class="folder">Folder with some children
								                <ul>
								                    <li id="key3.1">Sub-item 3.1</li>
								                    <li id="key3.2">Sub-item 3.2</li>
								                </ul>
											</li>
								            <li id="key4" class="expanded">Document with some children (expanded on init)
								                <ul>
								                    <li id="key4.1">Sub-item 4.1</li>
								                    <li id="key4.2">Sub-item 4.2</li>
								                </ul>
											</li>
								        </ul>
									</div>
								</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				<div class="row-fluid">
					
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-coa',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-coa/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Perkiraan</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>