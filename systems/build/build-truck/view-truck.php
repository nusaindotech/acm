			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-truck/insert-truck.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Simpan Data Truck ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Data Truck</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kode</label>
						  <div class="controls">
							<input type="text" name="kodetruck" class="form-control" placeholder="Kode Truck" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Truck</label>
						  <div class="controls">
							<input type="text" name="namatruck" class="form-control" placeholder="Nama Truck" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">NoPol Truck</label>
						  <div class="controls">
							<input type="text" name="nopol" class="form-control" placeholder="NoPol Truck" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tahun Truck</label>
						  <div class="controls">
							<input type="text" name="tahun" class="form-control" placeholder="Tahun Truck" autocomplete="off">
						  </div>
						</div>					
					</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=truck&navbar=truck&parent=master">Truck</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Data Truck</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Truck
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin  table-bordered datatable" >
									<thead>
										<tr>
											<th>No</th>
											<th>Kode Truck</th>
											<th>Nama Truck</th>
											<th>NoPol Truck</th>
											<th>Tahun Truck</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select * from truck
											where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_truck']; ?></td>
											<td><?php echo $row['nama_truck']; ?></td>
											<td><?php echo $row['nopol']; ?></td>
											<td><?php echo $row['tahun']; ?></td>
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-Truck" data-id="<?php echo $row['id_truck'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-truck/delete-truck.php?idtruck=<?php echo $row['id_truck']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-Truck',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-truck/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});

				
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Truck</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>