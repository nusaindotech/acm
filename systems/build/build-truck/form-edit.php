	<?php 
		include "../../../auth/autho.php";
		$id=$_POST['id'];
		$query = mysql_query("select * from truck where id_truck='$id'") or die(mysql_error());
		$data = mysql_fetch_array($query);
	?>
	<div class="modal-body">
		<form role="form" action="build/build-truck/update-truck.php" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped' onsubmit="return confirm('Anda Yakin Mengubah Data Truck?');">
			<input type="hidden" value="<?php echo $id;?>" name="id_truck">
			
			<div class="control-group">
				<label class="control-label">Kode</label>
				<div class="controls">
					<input type="text" id="textfield" class="input-xlarge" name="kodetruck" placeholder="Isikan Kode Truck" value="<?php echo $id;?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama Truck</label>
				<div class="controls">
					<input type="text" name="nama" id="textfield" placeholder="Nama Truck" class="input-xlarge" required="require" value="<?php echo $data['nama_truck'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">NoPol Truck</label>
				<div class="controls">
					<input type="text" name="nopol" id="textfield" placeholder="Nopol Truck" class="input-xlarge" value="<?php echo $data['nopol'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Tahun Truck</label>
				<div class="controls">
					<input type="text" name="tahun" id="textfield" placeholder="Tahun Truck" class="input-xlarge" value="<?php echo $data['tahun'];?>" autocomplete="off">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>