
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-user/insert-user.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah User</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="form-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
						  <div class="col-sm-10">
							<input type="text" name="username" class="form-control" placeholder="Nama User" required="require">
						  </div>
						</div>
						<br>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Lengkap</label>
						  <div class="col-sm-10">
							<input type="text" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap" required="require">
						  </div>
						</div>
						
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
						  <div class="col-sm-10">
							<input type="email" name="email" class="form-control" placeholder="" required="require">
						  </div>
						</div>
						
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
						  <div class="col-sm-10">
							<input type="password" name="password" class="form-control" placeholder="" required="require">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Konsumen</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>
								</td>
                                <td>
                                <div class="input-xlarge">
								<select name="bagian" id="bagian" class='chosen-select' required="required">
									<option value="">--Pilih Bagian--</option>
									<?php 	

									$tampil=mysql_query("select * from bagian where deleted=0" );
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_bagian']; ?>" ><?php echo $row['nama']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
					  </div>
					  <div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Blokir</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>
								</td>
                                <td>
                                <div class="input-xlarge">
								<select name="blokir" id="blokir" class='chosen-select' required="required">
									<!-- <option value="">--Pilih Bagian--</option> -->
									<option value="N">TIDAK</option>
									<option value="Y">IYA</option>
									
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=User&navbar=User&parent=master">User</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah User</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar User
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable 	 table-bordered">
									<thead>
										<tr>
											<th >No</th>
											<th>Nama User</th>
											<th>Nama Lengkap</th>
											<th>Email</th>
											<th>Bagian</th>
											<th>Blokir</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.*,b.nama as nama_bagian from users  a INNER JOIN bagian b on a.bagian = b.id_bagian 
											");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['username']; ?></td>
											<td><?php echo $row['nama_lengkap']; ?></td>
											<td><?php echo $row['email']; ?></td>
											<td><?php echo $row['nama_bagian']; ?></td>
											<td>
											<?php 
											if ($row['blokir']=='N') {
												echo "<span class='label label-success'> TIDAK DI BLOKIR</span>";
											
											}else if ($row['status']=='Y') {
												echo "<span class='label label-red'> DI BLOKIR</span>";
											}
											 ?></td>
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-User" data-id="<?php echo $row['id_city'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-city/delete-city.php?idcity=<?php echo $row['id_city']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-User',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-city/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data User</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>