<?php
$tglsekarang	= date('d-m-Y');
$id_city = $_SESSION[id_city];
$No_User = $_SESSION[No_User];

// var_dump($_SESSION);
// die();
	?>
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-ppj/insert-ppj.php" method="POST" class="form-horizontal" onsubmit="return confirm('Anda Yakin Menambah Data ?');">
					<input type="hidden" name="No_User" value="<?php echo $No_User?>">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Permintaan Penjualan</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kode</label>
						  <div class="controls">
							<input type="text" name="kode" class="form-control" placeholder="Auto" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						 <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Kirim</label>
						 <div class="controls">
							<input type="text" name="tgl_kirim" class="form-control datepick" >
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Konsumen</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>
								</td>
                                <td>
                                <div class="input-xlarge">
								<select name="konsumen" id="konsumen" class='chosen-select' required="required">
									<option value="">--Pilih Konsumen--</option>
									<?php 	

									$tampil=mysql_query("select * from customers where acc = 1" );
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['customer_id']; ?>" ><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Produk</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>
								</td>
                                <td>
                                <div class="input-xlarge">
								<select name="produk" id="produk" class='chosen-select' required="required">
									<option value="">--Pilih Produk--</option>
									<?php 	

									$tampil=mysql_query("select * from items where deleted=0 and category= 'Produk Utama' and id_city='$id_city'" );
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['item_id']; ?>" data-harga="<?php echo $row['unit_price']; ?>"><?php echo $row['name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tonase (Kg)</label>
						  <div class="controls">
							<input type="number" name="tonase" step="0.01" class="form-control" placeholder="Tonase" id="tonase" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Mata Uang</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>
								</td>
                                <td>
                                <div class="input-xlarge">
								<select name="currency" id="currency"  class='chosen-select' required="required">
									<option value="">--Pilih Mata Uang--</option>
									<?php 	

									$tampil=mysql_query("SELECT * FROM `currency`");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_currency']; ?>" data-alamat="<?php echo $row['company_address']; ?>" ><?php echo $row['name_currency']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
					  	
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">kurs</label>
						  <div class="controls">
							<input type="number" name="kurs" id="kurs" step="0.01" pattern="^\d+(\.|\,)\d{2}$" class="form-control" placeholder="kurs" value="1" autocomplete="off">


						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Harga/ Kg</label>
						  <div class="controls">
							<input type="number" step="0.01" class="form-control" placeholder="harga" autocomplete="off">


						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Keterangan</label>
						  <div class="controls">
							<textarea name="keterangan" id="textarea" class="input-block-level"></textarea>
						  </div>
						</div>
						
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>

                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=phj&navbar=phj&parent=penjualan">Permintaan Penjualan</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Permintaan Penjualan</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Permintaan Penjualan
								</h3>
					  </div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Kode Form</th>
											<th>Tanggal</th>
											<th>Konsumen</th>
											<th>Produk</th>
											<th>Tonase</th>
											<th>Harga / Kg</th>
											<th>Keterangan</th>
											<th>Status</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.*,b.customer_id,b.company_name,c.name,a.tonase from tb_ppj  as a  INNER JOIN customers b on a.customer_id = b.customer_id INNER JOIN items c on a.item_id = c.item_id WHERE a.deleted=0 ORDER BY a.id_ppj DESC");
									$no=1;

									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><a target="_blank" href="dash.php?hp=hpj2a&idbaru3=<?php echo $row['id_ppj']; ?>&navbar=hpj&parent=penjualan"> <?php echo $row['id_ppj']; ?></a></td>
											<td><?php echo $row['tanggal']; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo number_format($row['tonase']); ?></td>
											<td><?php echo number_format($row['harga'],2,',','.'); ?></td>
											<td><?php echo $row['keterangan']; ?></td>
											<td>
											<?php 
											if ($row['status']==0) {
												echo "<span class='label label-warning'>Belum Disetujui</span>";
											} else if ($row['status']==1) {
												echo "<span class='label label-success'> Disetujui</span>";
											}else if ($row['status']==2) {
												echo "<span class='label label-red'> Ditolak</span>";
											}
											 ?></td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<?php
												if($row['status']==0)
												{
												?>
													<li>
														<a id="edit-phj" data-id="<?php echo $row['id_ppj'];?>" data-id2="<?php echo $row['item_id'];?>" data-id3="<?php echo $row['customer_id'];?>" data-id4="<?php echo $No_User;?>">Revisi Permintaan</a>
													</li>
													<li>
														<a class="btn btn-success" href="build/build-ppj/update-ppj2.php?idphj=<?php echo $row['id_ppj']; ?>&status=1">Terima</a>
													</li>
													<li>
														<a class="btn btn-danger" href="build/build-ppj/update-ppj2.php?idphj=<?php echo $row['id_ppj']; ?>&status=2">Tolak</a>
													</li>
												<?php
												}
												else
												{
													?>
													<li>
														<a class="btn btn-danger" href="build/build-ppj/delete-ppj.php?idphj=<?php echo $row['id_ppj']; ?>">Hapus</a>
													</li>
													
													<!-- <li>
														<a href="#">Cetak Surat Penawaran</a>
													</li> -->
													<li>
														<a href='#buat_pp' class="buat_pp"  role="button" data-id="<?php echo $row['id_ppj']; ?>" data-toggle="modal">Buat Kontrak</a>
													</li>
												<?php }
												?>
												</ul>
											</div>
											<?php if ($row['status']==0): ?>
											
											<?php else: ?>
											<!-- <a class="btn btn-primary" id="edit-phj" data-id="<?php echo $row['id_ppj'];?>" data-id2="<?php echo $row['item_id'];?>"><i class="icon-edit"></i></a> -->
												
											<a ></i></a>
												
											<?php endif ?>
											<!-- <a class="btn btn-success" id="print-phj" data-id="<?php echo $row['id_phj'];?>"><i class="icon-print"></i></a> -->
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-phj',function(e){
						e.preventDefault();
						$("#view").modal('show');
						No_User = '<?php echo $No_User?>';
						console.log(No_User);
						$.post('build/build-ppj/form-edit.php',
							{id:$(this).attr('data-id'), id2:$(this).attr('data-id2'),id3:$(this).attr('data-id3'),id4:$(this).attr('data-id4')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
				$(function(){
					$(document).on('click','.buat_pp',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-ppj/form-edit2.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});

		
				
	</script>
	<script type="text/javascript">
		   //<![CDATA[
             $(window).load(function(){
       		 $("#produk").on("change", function(){
            var nilai  = $("#produk :selected").attr("data-harga");
            
            $("#harga_lama").val(nilai);
            
        });
        });//]]>
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Permintaan Penjualan</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	function number_f(id) {
		// id = $(this).attr('id');
		ids = ("'"+id+"'");
		$('#harga').number(true,2,'.');
		// body...
	}
	</script>