<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div class="breadcrumbs">
          <ul>
            <li>
              <a href="more-login.html">Home</a>
              <i class="icon-angle-right"></i>
            </li>
            <li>
              <a href="#">Pembelian</a>
              <i class="icon-angle-right"></i>
            </li>
            <li>
              <a href="dash.php?hp=pp1&navbar=pp&parent=pembelian">Pesanan Penjualan</a>
            </li>
          </ul>
          <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
          </div>
        </div>
        </br>
<table cellspacing="0" cellpadding="0" border="1">
          <col width="194" />
          <col width="13" span="2" />
          <col width="97" />
          <col width="73" span="4" />
          <col width="103" />
          <col width="112" />
          <tr>
            <td width="194">No Surat Kontrak</td>
            <td width="13"></td>
            <td width="13">:</td>
            <td width="97" align="right">1</td>
            <td colspan="4">/ACM-SCX/V/2017</td>
            <td width="103"></td>
            <td width="112"></td>
          </tr>
          <tr>
            <td>Produk</td>
            <td></td>
            <td>:</td>
            <td align="center">Cashew Nut</td>
            <td width="73" align="center"></td>
            <td width="73" align="center">Realisasi</td>
            <td width="73" align="center">Selisih</td>
            <td width="73" align="center">Every</td>
            <td align="center">Claim Value</td>
            <td align="center">Quality Claim</td>
          </tr>
          <tr>
            <td>Product </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Harvest </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Country Origin</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Origin Place</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Out Turn Min</td>
            <td></td>
            <td>:</td>
            <td align="right">48</td>
            <td>min</td>
            <td align="right">47.5</td>
            <td align="right">0.50</td>
            <td align="right">1</td>
            <td align="right">-43.75</td>
            <td align="right">-21.875</td>
          </tr>
          <tr>
            <td>Nut Count Max</td>
            <td></td>
            <td>:</td>
            <td align="right">200</td>
            <td>max</td>
            <td align="right">201</td>
            <td align="right">-1.00</td>
            <td align="right">10</td>
            <td align="right">10.5</td>
            <td align="right">-1.05</td>
          </tr>
          <tr>
            <td>Moisture Max</td>
            <td></td>
            <td>:</td>
            <td align="right">10%</td>
            <td>max</td>
            <td align="right">11%</td>
            <td align="right">-0.01</td>
            <td align="right">1</td>
            <td align="right">21</td>
            <td align="right">-0.21</td>
          </tr>
          <tr>
            <td>Wasted</td>
            <td></td>
            <td>:</td>
            <td align="right">11</td>
            <td></td>
            <td align="right">10</td>
            <td align="right">1.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Other</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Quality Claim</td>
            <td align="right">-23.135</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>No Surat Kontrak</td>
            <td></td>
            <td>:</td>
            <td align="right">2</td>
            <td align="right">0</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Produk</td>
            <td></td>
            <td>:</td>
            <td>Cocoa</td>
            <td></td>
            <td>Realisasi</td>
            <td>Selisih</td>
            <td>Every</td>
            <td>Claim Value</td>
            <td>Quality Claim</td>
          </tr>
          <tr>
            <td>Product </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Harvest </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Country Origin</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Origin Place</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Type</td>
            <td></td>
            <td>:</td>
            <td align="right">48</td>
            <td>min</td>
            <td align="right">47.5</td>
            <td align="right">0.00</td>
            <td align="right">1</td>
            <td align="right">-43.75</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Grade</td>
            <td></td>
            <td>:</td>
            <td align="right">200</td>
            <td>max</td>
            <td align="right">201</td>
            <td align="right">0.00</td>
            <td align="right">10</td>
            <td align="right">10.5</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Bean Count Max</td>
            <td></td>
            <td>:</td>
            <td align="right">10%</td>
            <td>max</td>
            <td align="right">11%</td>
            <td align="right">-0.01</td>
            <td align="right">1</td>
            <td align="right">21</td>
            <td align="right">-0.21</td>
          </tr>
          <tr>
            <td>Moisture</td>
            <td></td>
            <td>:</td>
            <td align="right">11</td>
            <td></td>
            <td align="right">10</td>
            <td align="right">1.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Slaty </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Insect </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Wasted</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Germinated</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Other </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Quality Claim</td>
            <td align="right">-0.21</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>No Surat Kontrak</td>
            <td></td>
            <td>:</td>
            <td align="right">3</td>
            <td align="right">0</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Produk</td>
            <td></td>
            <td>:</td>
            <td>Coffee</td>
            <td></td>
            <td>Realisasi</td>
            <td>Selisih</td>
            <td>Every</td>
            <td>Claim Value</td>
            <td>Quality Claim</td>
          </tr>
          <tr>
            <td>Product </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Harvest </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Variety</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Process</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Country Origin</td>
            <td></td>
            <td>:</td>
            <td align="right">48</td>
            <td>min</td>
            <td align="right">47.5</td>
            <td align="right">0.00</td>
            <td align="right">1</td>
            <td align="right">-43.75</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Origin Place</td>
            <td></td>
            <td>:</td>
            <td align="right">200</td>
            <td>max</td>
            <td align="right">201</td>
            <td align="right">0.00</td>
            <td align="right">10</td>
            <td align="right">10.5</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Grade</td>
            <td></td>
            <td>:</td>
            <td align="right">10%</td>
            <td>max</td>
            <td align="right">11%</td>
            <td align="right">0.00</td>
            <td align="right">1</td>
            <td align="right">21</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Defect Max</td>
            <td></td>
            <td>:</td>
            <td align="right">11</td>
            <td></td>
            <td align="right">10</td>
            <td align="right">1.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Moisture Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Triage</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Bean </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Bean Count</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Wasted</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Foreign Matter</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Other</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Quality Claim</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>No Surat Kontrak</td>
            <td></td>
            <td>:</td>
            <td align="right">4</td>
            <td align="right">0</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Produk</td>
            <td></td>
            <td>:</td>
            <td>Clove</td>
            <td></td>
            <td>Realisasi</td>
            <td>Selisih</td>
            <td>Every</td>
            <td>Claim Value</td>
            <td>Quality Claim</td>
          </tr>
          <tr>
            <td>Product </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Harvest </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Country Origin</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Origin Place</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Quality </td>
            <td></td>
            <td>:</td>
            <td align="right">48</td>
            <td>min</td>
            <td align="right">47.5</td>
            <td align="right">0.00</td>
            <td align="right">1</td>
            <td align="right">-43.75</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Style</td>
            <td></td>
            <td>:</td>
            <td align="right">200</td>
            <td>max</td>
            <td align="right">201</td>
            <td align="right">0.00</td>
            <td align="right">10</td>
            <td align="right">10.5</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Variety</td>
            <td></td>
            <td>:</td>
            <td align="right">10%</td>
            <td>max</td>
            <td align="right">11%</td>
            <td align="right">0.00</td>
            <td align="right">1</td>
            <td align="right">21</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Moisture Max</td>
            <td></td>
            <td>:</td>
            <td align="right">11</td>
            <td></td>
            <td align="right">10</td>
            <td align="right">1.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Stem Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Baby Cloves Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Headless Cloves Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Wasted</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Foreign Matter</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Dust Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Petal Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Bad Clove Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Pod</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Impurities Cloves Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Small Cloves Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Harvest </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Color</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Other</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Quality Claim</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>No Surat Kontrak</td>
            <td></td>
            <td>:</td>
            <td align="right">5</td>
            <td align="right">0</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Produk</td>
            <td></td>
            <td>:</td>
            <td>Corn</td>
            <td></td>
            <td>Realisasi</td>
            <td>Selisih</td>
            <td>Every</td>
            <td>Claim Value</td>
            <td>Quality Claim</td>
          </tr>
          <tr>
            <td>Product </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Harvest </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Country Origin</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Origin Place</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Moisture Max</td>
            <td></td>
            <td>:</td>
            <td align="right">48</td>
            <td>min</td>
            <td align="right">47.5</td>
            <td align="right">0.50</td>
            <td align="right">1</td>
            <td align="right">-43.75</td>
            <td align="right">-21.875</td>
          </tr>
          <tr>
            <td>Aflatoxin </td>
            <td></td>
            <td>:</td>
            <td align="right">200</td>
            <td>max</td>
            <td align="right">201</td>
            <td align="right">0.00</td>
            <td align="right">10</td>
            <td align="right">10.5</td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Mouldy Max</td>
            <td></td>
            <td>:</td>
            <td align="right">10%</td>
            <td>max</td>
            <td align="right">11%</td>
            <td align="right">-0.01</td>
            <td align="right">1</td>
            <td align="right">21</td>
            <td align="right">-0.21</td>
          </tr>
          <tr>
            <td>Broken Kernel Max</td>
            <td></td>
            <td>:</td>
            <td align="right">11</td>
            <td></td>
            <td align="right">10</td>
            <td align="right">1.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Damage Kernel Max</td>
            <td></td>
            <td>:</td>
            <td align="right">11</td>
            <td></td>
            <td align="right">14</td>
            <td align="right">-3.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>White Kernel Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Bad Corn Max</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Wasted/Foreign Matter </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Pest </td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Color</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td>Other</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0.00</td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td align="right">0</td>
            <td></td>
            <td>:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td align="right">0</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Quality Claim</td>
            <td align="right">-22.085</td>
          </tr>
</table>
</body>
</html>
