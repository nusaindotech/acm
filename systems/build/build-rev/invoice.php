<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div class="breadcrumbs">
          <ul>
            <li>
              <a href="more-login.html">Home</a>
              <i class="icon-angle-right"></i>
            </li>
            <li>
              <a href="#">Pembelian</a>
              <i class="icon-angle-right"></i>
            </li>
            <li>
              <a href="dash.php?hp=pp1&navbar=pp&parent=pembelian">Pesanan Penjualan</a>
            </li>
          </ul>
          <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
          </div>
        </div>
        </br>

<table cellspacing="0" cellpadding="0" border="1">
          <col width="208" />
          <col width="45" />
          <col width="32" />
          <col width="257" />
          <col width="147" />
          <col width="126" />
          <col width="29" />
          <col width="257" />
          <tr>
            <td colspan="4" rowspan="2" width="542"><a name="Invoice!C3:J51" id="Invoice!C3:J51">CV. AGRO CITA MANDIRI</a></td>
            <td colspan="2" width="273">PROFORMA INVOICE</td>
            <td width="29"></td>
            <td width="257"></td>
          </tr>
          <tr>
            <td colspan="2" width="273">Number : 001/ACM-INV/V/17</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="4" rowspan="3" width="542">BRATANG WETAN I/31, NGAGELREJO SURABAYA 60245, INDONESIA</td>
            <td>Invoice Date :</td>
            <td colspan="3" width="412">19-May-17</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Purchase Contract No </td>
            <td></td>
            <td>:</td>
            <td>001/ACM-SCX/V/2017</td>
            <td></td>
            <td>Consignee </td>
            <td>:</td>
            <td>-</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2" width="404"></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Seller and Shipper</td>
            <td></td>
            <td>:</td>
            <td colspan="2" width="404">CV. AGRO CITA MANDIRI</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2" rowspan="3" width="404">BRATANG WETAN I/31, NGAGELREJO, WONOKROMO SURABAYA 60245, INDONESIA</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Buyer</td>
            <td></td>
            <td>:</td>
            <td colspan="2" rowspan="2" width="404">MY PHI ONE MEMBER TRADING IMPORT EXPORT COMPANY LIMITED.</td>
            <td>Inco Term</td>
            <td>:</td>
            <td>CIF, Ho Chi Minh City , Vietnam</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>CV/Rev No.</td>
            <td>:</td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2" rowspan="3" width="404">ADD: HAMLET4, XUAN TAM COMMUNE, XUAN LOC DISTRICT, DONG NAI PROVINCE, VIETNAM.<br />
              Represented by: Ms. Duong My Phi<br />
              Telp: +84862833024<br /></td>
            <td>Broker Ref</td>
            <td>:</td>
            <td>-</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="3" width="412">Country Of Origin goods :</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="3" width="412">Ivory Coast</td>
          </tr>
          <tr>
            <td>Agent</td>
            <td></td>
            <td>:</td>
            <td colspan="2" rowspan="4" width="404">Pham Nguyen Song Binh<br />
              +84907866866<br />
              Binh.pham@ibvietnambusiness.com<br /></td>
            <td colspan="3" width="412">Terms of Payment :</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="3" rowspan="6" width="412">10% cash in advance, within three days of after signing the contract, 88% cash against document within five days of receiving the document at Buyer’s Bank &amp; 2% Against Vina control Certificate report within five days of total invoice value.</td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2" width="253">Number of kind Packages</td>
            <td width="289" colspan="2" align="center">Desription of Goods</td>
            <td align="center">Quantity</td>
            <td width="155" colspan="2" align="center">Unit Price </td>
            <td align="center">Amount (USD) :</td>
          </tr>
          <tr>
            <td rowspan="5" width="208"></td>
            <td></td>
            <td colspan="2" rowspan="3" width="289">Raw Cashew Nut (Ivory Coast Origin) 260.72MT<br /></td>
            <td width="147" align="right">260.72</td>
            <td align="right">2100</td>
            <td></td>
            <td align="right"> 547,512.00 </td>
          </tr>
          <tr>
            <td></td>
            <td width="147" rowspan="2" align="right">MT in revolving <br />
              (5% more or less)<br /></td>
            <td align="right">USD/MT</td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td colspan="2" width="289">10% Cash in Advance</td>
            <td width="147"></td>
            <td></td>
            <td></td>
            <td align="right"> 54,751.20 </td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="7" width="844">Total</td>
            <td> 54,751.20 </td>
          </tr>
          <tr>
            <td colspan="5" width="689">In Words : FIFTY FOUR THOUSAND SEVEN HUNDRED FIFTY ONE AND TWENTY CENT UNITED STATE DOLLARS ONLY</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Please Transfer to </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="2" width="155">Surabaya, 19 May 2017</td>
            <td></td>
          </tr>
          <tr>
            <td>Beneficiary Name</td>
            <td colspan="3" width="334">: CV. AGRO CITA MANDIRI</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Bank Name</td>
            <td colspan="2" width="77">: OCBC NISP</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Branch</td>
            <td colspan="3" width="334">: GOLDEN PALACE – SURABAYA</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Bank Adresses</td>
            <td colspan="3" rowspan="2" width="334">: RUKO GOLDEN PALACE, JL. HR. MUHAMMAD BLOK A NO. 17 SURABAYA</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Bank Account</td>
            <td colspan="3" width="334">: 5548 1001 5988 (USD)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Swift Code</td>
            <td colspan="2" width="77">: NISPIDJA</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Note</td>
            <td>: -</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="3" width="412">(..................................................)</td>
          </tr>
          <tr>
            <td colspan="4" width="542">If you have any questions concerning this invoice, </td>
            <td></td>
            <td colspan="2" width="155">       President Director</td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2" width="253">contact (Name, phone number, e-mail)</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td>Thank you for your business.</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
</table>
</body>
</html>
