<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div class="breadcrumbs">
          <ul>
            <li>
              <a href="more-login.html">Home</a>
              <i class="icon-angle-right"></i>
            </li>
            <li>
              <a href="#">Pembelian</a>
              <i class="icon-angle-right"></i>
            </li>
            <li>
              <a href="dash.php?hp=pp1&navbar=pp&parent=pembelian">Pesanan Penjualan</a>
            </li>
          </ul>
          <div class="close-bread">
          <a href="#"><i class="icon-remove"></i></a></div>
</div>
<br>
<table cellspacing="0" cellpadding="0">
  <col width="18" />
  <col width="168" />
  <col width="13" />
  <col width="127" />
  <col width="62" />
  <col width="107" />
  <col width="26" />
  <col width="153" />
  <col width="13" />
  <col width="315" />
  <col width="18" />
  <tr>
    <td width="1020" colspan="11" align="center"><p><strong>RAW CASHEW NUT SALES CONTRACT</strong></p>
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td></td>
    <td>Seller</td>
    <td>:</td>
    <td colspan="3" width="803">CV. AGRO CITA MANDIRI</td>
    <td width="26"></td>
    <td>S/C Number</td>
    <td>:</td>
    <td colspan="2" width="333">001/ACM-SCX/V/2017</td>
  </tr>
  <tr>
    <td></td>
    <td>Address</td>
    <td>:</td>
    <td colspan="4" rowspan="3" width="803">BRATANG WETAN I/31, <br />
      NGAGELREJO, WONOKROMO, SURABAYA 60245<br />
      <br /></td>
    <td>S/C Reference </td>
    <td>:</td>
    <td colspan="2" width="333">-</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td>Date             </td>
    <td>:</td>
    <td colspan="2" width="333">18 May 2017</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Telephone</td>
    <td>:</td>
    <td colspan="3" width="803">+628175106747</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Email</td>
    <td>:</td>
    <td colspan="3" width="803">nasya.sadhaka@acmsg.co</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Represented By</td>
    <td>:</td>
    <td colspan="3" width="803">Hartono</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Position </td>
    <td>:</td>
    <td colspan="3" width="803">Director</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Website</td>
    <td>:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Country</td>
    <td>:</td>
    <td colspan="3" width="803">Indonesia</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Buyer</td>
    <td>:</td>
    <td colspan="3" width="803">MY PHI ONE MEMBER TRADING IMPORT EXPORT COMPANY LIMITED.</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Address</td>
    <td>:</td>
    <td colspan="5" rowspan="3" width="803">ADD: HAMLET4, XUAN TAM COMMUNE, XUAN LOC DISTRICT, DONG NAI PROVINCE, VIETNAM.</td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>Telephone</td>
    <td>:</td>
    <td colspan="5" width="803">+84862833024</td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Email</td>
    <td>:</td>
    <td colspan="5" width="803">duongmyphi@gmail.com</td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Represented By</td>
    <td>:</td>
    <td width="803">Duong My Phi </td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Position </td>
    <td>:</td>
    <td width="803">Director</td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Website</td>
    <td>:</td>
    <td width="803">-</td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Country</td>
    <td>:</td>
    <td colspan="3" width="803">Vietnam</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Notify Party/ Receiver/Agent</td>
    <td>:</td>
    <td colspan="3" width="803">Pham Nguyen Song Binh</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Address</td>
    <td>:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Telephone</td>
    <td>:</td>
    <td colspan="3" width="803">+84907866866</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Email</td>
    <td>:</td>
    <td colspan="3" width="803">Binh.pham@ibvietnambusiness.com</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Represented By</td>
    <td>:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Position </td>
    <td>:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Website</td>
    <td>:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Country</td>
    <td>:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="9" rowspan="2" width="984">IT HAS BEEN MUTUALLY AGREED THAT THE BUYER AGREES TO BUY AND THE SELLER AGRESS TO SELL THE COMMODITY ON THE THE TERMS AND CONDITIONS SPECIFIED AS FOLLOWING :</td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Product</td>
    <td width="13">:</td>
    <td colspan="3" width="803">Raw Cashew Nut</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Country Of Origin</td>
    <td width="13">:</td>
    <td colspan="3" width="803">Ivory Coast </td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td rowspan="2" width="984">Quality Specification</td>
    <td width="13">:</td>
    <td colspan="3" rowspan="5" width="803">Out Turn   : 48Lbs min <br />
      Nut Count : 200 max<br />
      Moisture   : 10% Max<br />
      <br /></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="13"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Quanity</td>
    <td width="13">:</td>
    <td width="803"> 260.72 </td>
    <td width="62">MT</td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Price</td>
    <td width="13">:</td>
    <td width="803"> 2,100.00 </td>
    <td>USD/MT</td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Total Price</td>
    <td width="13">:</td>
    <td width="803"> 547,512.00 </td>
    <td width="62">USD</td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td rowspan="2" width="984">Total Amount in Words</td>
    <td width="13">:</td>
    <td colspan="7" rowspan="2" width="803">FIVE HUNDRED FORTY SEVEN THOUSAND FIVE HUNDRED TWELVE UNITED STATES DOLLARS ONLY</td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Inco Term</td>
    <td width="13">:</td>
    <td colspan="3" width="803">CIF, Ho Chi Minh City , Vietnam</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Value</td>
    <td width="13">:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td rowspan="2" width="984">Qualities Discount</td>
    <td width="13">:</td>
    <td colspan="7" rowspan="5" width="803">OUTTURN DISCOUNT (47 LBS MIN) : USD  43.75 /MT  FOR EVERY 1.00 LBS  SHORTFALL <br />
      NUT COUNT DISCOUNT : USD 10.50/MT   FOR EVERY NUT 10 COUNTS EXCESS.  <br />
      MOISTURE DISCOUNT : USD 21.00 /MT FOR EVERY 1% EXCESS. FOR PART ON PRO –RATE.<br /></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td>Rejection Clause</td>
    <td width="13">:</td>
    <td colspan="7" rowspan="5" width="803">OUTTURN : BELOW 46 LBS/KG<br />
      NUT COUNT : ABOVE 210 NUTS/KG<br />
      MOISTURE : ABOVE 12%<br />
      <br /></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Insurance</td>
    <td width="13">:</td>
    <td colspan="5" width="803">INSURANCE WILL BE COVERED BY THE SELLER</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Packing</td>
    <td width="13">:</td>
    <td colspan="3" width="803">Sound Sea Worthy New Jute Bags</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Shipment</td>
    <td width="13">:</td>
    <td width="803">-</td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Last Date of Shipment</td>
    <td width="13">:</td>
    <td width="803">30 April 2017</td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Terms of Shipment</td>
    <td width="13">:</td>
    <td colspan="3" width="803">PARTIAL SHIPMENT IS ALLOWED</td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Port of Loading</td>
    <td width="13">:</td>
    <td colspan="2" width="803">Abidjan, Ivory Coast </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Port of Discharge</td>
    <td width="13">:</td>
    <td colspan="3" width="803">Ho Chi Minh City , Vietnam</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Term of Payment</td>
    <td width="13">:</td>
    <td colspan="7" rowspan="4" width="803">10% cash in advance, within three days of after signing the contract, 88% cash against document within five days of receiving the document at Buyer&rsquo;s Bank &amp; 2% Against Vina control Certificate report within five days of total invoice value.</td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="4" width="984">SELLER'S BANKING INFORMATION</td>
    <td></td>
    <td></td>
    <td colspan="3" width="481">SELLER'S BANKING INFORMATION</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Benificary Name</td>
    <td width="13">:</td>
    <td colspan="3" width="803">CV. AGRO CITA MANDIRI</td>
    <td width="26"></td>
    <td>Benificary Name</td>
    <td>:</td>
    <td>-</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Bank Name</td>
    <td width="13">:</td>
    <td colspan="3" width="803">OCBC NISP</td>
    <td width="26"></td>
    <td>Bank Name</td>
    <td>:</td>
    <td rowspan="2" width="333">MILITARY COMMERCIAL JOINT STOCK BANK – LONG KHANH BRANCH</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Branch </td>
    <td width="13">:</td>
    <td colspan="3" width="803">GOLDEN PALACE – SURABAYA</td>
    <td width="26"></td>
    <td>Branch </td>
    <td>:</td>
    <td>-</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Bank Address</td>
    <td width="13">:</td>
    <td colspan="3" rowspan="3" width="803">RUKO GOLDEN PALACE, JL. HR MUHAMMAD BLOK A NO 17 SURABAYA</td>
    <td width="26"></td>
    <td>Bank Address</td>
    <td>:</td>
    <td rowspan="3" width="333">N6-N7, HUNG VUONG STR, Xuan Binh Ward, Long Khanh, Dong Nai</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="26"></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="26"></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Bank Account</td>
    <td width="13">:</td>
    <td colspan="3" width="803">5548 1001 5988 (USD)</td>
    <td width="26"></td>
    <td>Bank Account</td>
    <td>:</td>
    <td>7241100118001</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Swift Code</td>
    <td width="13">:</td>
    <td colspan="3" width="803">NISPIDJA</td>
    <td width="26"></td>
    <td>Swift Code</td>
    <td>:</td>
    <td>MSCBVNVX</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Correspondent bank</td>
    <td width="13">:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td width="481">Correspondent bank</td>
    <td>:</td>
    <td>-</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">IBAN</td>
    <td width="13">:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td width="481">IBAN</td>
    <td width="328">:</td>
    <td>-</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Remark</td>
    <td width="13">:</td>
    <td colspan="3" width="803">-</td>
    <td width="26"></td>
    <td>Remark</td>
    <td>:</td>
    <td>-</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Document Required</td>
    <td width="13">:</td>
    <td colspan="7" rowspan="15" width="803">The sellers shall present<br />
      1. SIGNED COMMERCIAL INVOICE IN THREE COPIES.( 1 original &amp; 2 Photocopy)<br />
      2. FULL SET OF 3/3 SIGNED &ldquo;ON BOARD&rdquo; OCEAN BILL OF LADING MADE OUT TO THE ORDER OF VIETNAM EXPORT IMPORT COMMERCIAL JOIN STOCK BANK AND NOTIFY AS MY PHI ONE MEMBER TRADING IMPORT EXPORT COMPANY LIMITED., ENDORSED MARKED FREIGHT PREPAIDEVIDENCING SHIPMENT OF MERCHANDISE. <br />
      3. SIGNED PACKING LIST THREE COPIES.<br />
      4. CERTIFICATE OF ORIGIN ISSUED BY THE CHAMBER OF COMMERCE/CONCERNED GOVERNMENT AGENCY APPROVED AUTHORITY/ORGANISATION OF THE COUNTRY OF ORIGIN OF GOODS.<br />
      5. CERTIFICATE OF PHYTOSANITARY.<br />
      6. CERTIFICATE OF FUMIGATION.<br />
      7. VINA CONTROL INSPECTION SHOULD TAKE PLACE WITHIN THREE DAYS OF CARGO REACHING THE PORT OF DESTINATION, IN FRONT OF SELLER&rsquo;S REPRESENTATIVE.<br />
      8. QUALITY CERTIFICATE ISSUED BY IVORY COAST OFFICIAL/SELLER&rsquo;S APPOINTED SURVEYOR.<br /></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Additional Conditions</td>
    <td width="13">:</td>
    <td colspan="7" rowspan="11" width="803">1. ALL DOCUMENTS MUST BE ISSUED IN ENGLISH LANGUAGE<br />
      2. IT IS A FIRM PRICE CONTRACT.<br />
      3. DOCUMENTS PRODUCED BY REPOGRAPHIC PROCESS/COMPUTERISED CARBON COPIES ARE NOT ACCEPTABLE UNLESS MARKED ORIGINAL AND SIGNED.<br />
      4. IF THE PRICES OR TERMS OF THIS CONTRACT CHANGE WHILE THE CONTRACT IS STILL VALID, IT WILL BE CHANGED ONLY AFTER THE MUTUAL UNDERSTANDING AND AGREEMENT OF BOTH BUYER &amp; SELLER.<br />
      5. BUYER WILL PAY THE SELLER IN &ldquo;FULL AMOUNT&rdquo; BOTH THE ADVANCE PAYMENT, THE CAD, AND THE FINAL SETTLEMENT.<br />
      6. INSURANCE WILL BE COVERED BY THE SELLER.<br /></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Force Majeure </td>
    <td width="13">:</td>
    <td colspan="7" rowspan="14" width="803">Notwithstanding anything said in this agreement to the contrary, neither CV. AGRO CITA MANDIRI Nor MY PHI ONE MEMBER TRADING IMPORT EXPORT COMPANY LIMITED shall be liable deemed to be in default if any failure or delay in performance hereunder are caused by force majeure which term shall include but shall not be limited to bar, civil unrest, riot, fire, explosion, flood, typhoon, earthquake, governmental actions or other causes beyond the reasonable control of the party affected. The party affected by such event shall inform the other party immediately by fax or cable of the occurrences of force majeure situation. The affected party shall inform other immediately by fax on the termination of the case of force majeure subject to confirmation by registered airmail letter. Should the effect of the force majeure mentioned above, last over a period of 90 days, both parties shall try to settle execution by amicable consultation and enter in to agreement. In case no agreement is reached within 12 months (twelve months) from the start of the force majeure situation either party shall have right to terminate the contract. Payment due for the orders already executed or delivery of the products already paid for shall not be affected by force majeure under this clause.</td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td rowspan="2" width="984">Arbitration and Governing Law </td>
    <td width="13">:</td>
    <td colspan="7" rowspan="12" width="803">1. All disputes arising out of or related to the agreement shall be settled through friendly consultations between both parties. In case no agreement can be reached through consultation, they shall be submitted <br />
      2. Any dispute or difference whatsoever arising between the parties out of or relating to the construction, meaning, scope, operation or effect of this contract or the validity or the breach thereof, shall be settled by arbitration in accordance with the Rules of Arbitration of the Singapore Council of Arbitration and the award made in pursuance thereof shall be binding on the parties. The arbitration shall take place in Singapore.<br />
      3. The arbitration award shall be accepted by both parties as final and they shall act accordingly.<br />
      4. The arbitration fee and reasonable attorney fees shall be borne by the losing party.<br />
      5. The governing Law shall the Laws of Singapore.<br /></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Contract Validity </td>
    <td width="13">:</td>
    <td colspan="7" width="803">This contract begins to have effect on the parties on the date of signature through facsimile/email.</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Others Terms and Conditions</td>
    <td width="13">:</td>
    <td colspan="7" width="803">-</td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984">Remarks</td>
    <td width="13">:</td>
    <td colspan="7" width="803">-</td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr align="center">
    <td></td>
    <td width="984" colspan="9">ACCEPTED &amp; CONFIRMED</td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" rowspan="2" width="984">CV. AGRO CITA MANDIRI</td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td colspan="3" rowspan="2" align="center">MY PHI ONE MEMBER TRADING IMPORT EXPORT COMPANY LIMITED</td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" width="984">Seller&rsquo;s Authorized Signature and Stamp</td>
    <td width="62"></td>
    <td width="107"></td>
    <td></td>
    <td colspan="3" align="center">Buyer&rsquo;s Authorized Signature and Stamp</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td width="984"></td>
    <td width="13"></td>
    <td width="803"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td width="481"></td>
    <td width="328"></td>
    <td width="333"></td>
    <td width="18"></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3" width="984"></td>
    <td width="62"></td>
    <td width="107"></td>
    <td width="26"></td>
    <td></td>
    <td colspan="2" width="328"></td>
    <td></td>
  </tr>
</table>
</body>
</html>
