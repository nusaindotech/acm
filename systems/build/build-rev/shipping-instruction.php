<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<div class="breadcrumbs">
          <ul>
            <li>
              <a href="more-login.html">Home</a>
              <i class="icon-angle-right"></i>
            </li>
            <li>
              <a href="#">Pembelian</a>
              <i class="icon-angle-right"></i>
            </li>
            <li>
              <a href="dash.php?hp=pp1&navbar=pp&parent=pembelian">Pesanan Penjualan</a>
            </li>
          </ul>
          <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
          </div>
        </div>
        </br>
<table border="1" cellpadding="0" cellspacing="0">
          <col width="207" />
          <col width="64" />
          <col width="73" />
          <col width="120" />
          <col width="73" />
          <col width="141" />
          <col width="101" />
          <col width="95" />
          <col width="129" />
          <col width="157" span="2" />
          <col width="31" />
          <tr>
            <td height="57" colspan="6" align="center" bgcolor="#0000FF"><strong>CV AGRO CITA MANDIRI</strong></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6">BRATANG WETAN I/31, NGANGELREJO,</td>
            <td colspan="6" rowspan="2" align="center" bgcolor="#0000FF"><strong>Shipping Instruction</strong></td>
          </tr>
          <tr>
            <td colspan="6">WONOKROMO, SURABAYA 60245</td>
          </tr>
          <tr>
            <td colspan="6">INDONESIA</td>
            <td colspan="6">(All fields marked by * are mandatory)</td>
          </tr>
          <tr>
            <td width="72"></td>
            <td colspan="2"></td>
            <td colspan="3"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="12"></td>
          </tr>
          <tr>
            <td width="72">SI Number</td>
            <td colspan="5"></td>
            <td colspan="2">EMKL </td>
            <td colspan="4"></td>
          </tr>
          <tr>
            <td width="72">Date</td>
            <td colspan="5"></td>
            <td colspan="2">(complete name and address)*</td>
            <td colspan="4"></td>
          </tr>
          <tr>
            <td width="72">To</td>
            <td colspan="5" rowspan="2"></td>
            <td colspan="2">ATT</td>
            <td colspan="4"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td colspan="2">Subj.</td>
            <td colspan="4"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td colspan="12"></td>
          </tr>
          <tr>
            <td width="72">Vessel Space:</td>
            <td colspan="5"></td>
            <td colspan="2">Contract Code:</td>
            <td colspan="4"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td colspan="6">Shipper (complete name and address):</td>
            <td colspan="6">Buyer (complete name and address):</td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6">Consignee 1 (complete name and address):</td>
            <td colspan="6">Notify party (complete name and address):</td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6"></td>
          </tr>
          <tr>
            <td colspan="2" align="center">Product</td>
            <td colspan="2" align="center">Number of Bag</td>
            <td colspan="2" align="center">Net Weight * (KGS)</td>
            <td colspan="2" align="center">Gross Weight * (KGS)</td>
            <td width="65" align="center">Packing</td>
            <td width="62" align="center">Carton</td>
            <td colspan="2"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td colspan="2">TOTAL</td>
            <td colspan="2"></td>
            <td colspan="2"></td>
            <td colspan="2"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td colspan="2"></td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td width="125"></td>
            <td width="7"></td>
            <td width="65"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td colspan="3">Insurance:</td>
            <td colspan="3">Dryer:</td>
            <td colspan="3">Detention:</td>
            <td colspan="3">Demurage:</td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td colspan="3" rowspan="3"></td>
            <td width="62"></td>
            <td width="25"></td>
            <td width="14"></td>
          </tr>
          <tr>
            <td colspan="3"></td>
            <td colspan="3">Fumigation Treatment:</td>
            <td colspan="3">Storage:</td>
          </tr>
          <tr>
            <td width="72"></td>
            <td width="0"></td>
            <td width="35"></td>
            <td width="46"></td>
            <td width="38"></td>
            <td width="4"></td>
            <td colspan="3"></td>
          </tr>
          <tr>
            <td colspan="5">Stuffing Date:</td>
            <td></td>
            <td colspan="6">Closing Date:</td>
          </tr>
          <tr>
            <td colspan="12"></td>
          </tr>
          <tr>
            <td width="72">Fedder Vessel:</td>
            <td width="0"></td>
            <td width="35"></td>
            <td colspan="3"></td>
            <td colspan="6">Place of destination (Only mandatory in case of inland transport under carriers responsibility)*:</td>
          </tr>
          <tr>
            <td colspan="6"></td>
            <td colspan="6" rowspan="3"></td>
          </tr>
          <tr>
            <td colspan="3">Port of loading*:</td>
            <td colspan="3">Port of discharge*:</td>
          </tr>
          <tr>
            <td colspan="3"></td>
            <td colspan="3"></td>
          </tr>
</table>
</body>
</html>
