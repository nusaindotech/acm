<?php
	$idsupplier		= $_GET['idsupplier'];
	
	$a			= mysql_query("SELECT company_name, company_code from suppliers where supplier_id='$idsupplier'");
	$row		= mysql_fetch_array($a);
	$nama		= $row['company_name'];
	$kode		= $row['company_code'];
?>
	
	<script>
        //<![CDATA[
        $(window).load(function(){
        $("#item").on("change", function(){
            var nilai = $("#item :selected").attr("data-satuan");
            $("#satuan").val(nilai);
        });
        });//]]>
    </script>
		
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-items-filling/insert-items-filling.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Simpan Data Pengajuan Pemasok ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Pengajuan Item Pemasok</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Item</label>
                          <div class="controls">
						  <input type="hidden" name="idsupplier" class="form-control" value="<?php echo $idsupplier; ?>" readonly>
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="item" id="item" class='chosen-select' required=="required">
									<option value="">--Pilih Item--</option>
									<?php 	

									$tampil=mysql_query("select * from items
											where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['item_id']; ?>" data-satuan="<?php echo $row['custom3']; ?>"><?php echo $row['name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Satuan</label>
						  <div class="controls">
							<input type="text" name="satuan" class="form-control" id="satuan" placeholder="Satuan" autocomplete="off" required=="required" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Harga</label>
						  <div class="controls">
							<input type="text" name="harga" class="form-control" placeholder="Harga Pengajuan" autocomplete="off" required=="required">
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=ps&navbar=ps&parent=master">Pengajuan Pemasok</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Pengajuan Item Pemasok</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Pengajuan Item Pemasok | Kode Supplier : <?php echo $kode; ?> Nama : <?php echo $nama; ?>
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable table-bordered" >
									<thead>
										<tr>
											<th>No</th>
											<th>Supplier</th>
											<th>Item</th>
											<th>Pengajuan Harga</th>
											<th>Realisasi Harga</th>
											<th>Diskon</th>
											<th>Status</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.id_detail_suppliers, b.company_name, b.company_code, b.company_address, b.company_phone, b.agency_name, b.supplier_id,
									c.name , a.unit_price, a.unit_price_acc, a.unit_discount , a.acc
									from detail_suppliers a, suppliers b, items c
									where a.supplier_id=b.supplier_id and c.item_id=a.item_id and a.supplier_id=$idsupplier and a.deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['name']; ?></td>
											<td><?php echo number_format($row['unit_price'],0); ?></td>
											<td><?php echo number_format($row['unit_price_acc'],0); ?></td>
											<td><?php echo $row['unit_discount']; ?></td>
											<td>
											<?php
											if($row['acc']==0)
											{
												echo "<span class='label label-warning'>Belum Diterima</span>";
											}
											else
											{
												echo "<span class='label label-success'>Diterima</span>";
											}
											?>
											</td>
											<td>
											<a class="btn btn-primary" id="edit-supplier-filling" data-id="<?php echo $row['id_detail_suppliers'];?>"><i class="icon-edit"></i></a>
											<?php
											if($row['acc']==0)
											{
											?>
												<a class="btn btn-danger" href="build/build-items-filling/delete-items-filling.php?id_detail_suppliers=<?php echo $row['id_detail_suppliers']; ?>&idsupplier=<?php echo $idsupplier; ?>"><i class="icon-trash"></i></a>
											<?php
											}
											else
											{
											
											}
											?>
											
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
		$(function(){
			$(document).on('click','#edit-supplier-filling',function(e){
				e.preventDefault();
				$("#view").modal('show');
				$.post('build/build-items-filling/form-edit.php',
					{id:$(this).attr('data-id')},
					function(html){
						$(".modal-body2").html(html);
					}  
				);
			});
		});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Pengajuan Supplier</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>