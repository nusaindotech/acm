	<?php 
		include "../../../auth/autho.php";
		$id			= $_POST['id'];
		$query 		= mysql_query("select a.id_detail_suppliers, b.company_name, b.company_code, b.company_address, b.company_phone, b.agency_name, b.supplier_id,
								c.name , a.unit_price, a.unit_price_acc, a.unit_discount , b.acc, b.date_pkp, b.company_email,
								a.provision, b.ppn, b.npwp_name, b.pkp, c.custom3, a.acc
								from detail_suppliers a, suppliers b, items c
								where a.supplier_id=b.supplier_id and c.item_id=a.item_id and a.id_detail_suppliers='$id'") or die(mysql_error());
		$data 		= mysql_fetch_array($query);
		
		$formattanggal	= date('d-m-Y',strtotime($data['date_pkp']));
	?>
	<div class="modal-body">
		<form role="form" action="build/build-items-filling/update-items-filling.php" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped' onsubmit="return confirm('Apakah Yakin Simpan Data Pengajuan Pemasok ?');">
			<input type="hidden" value="<?php echo $id;?>" name="id_detail_suppliers">
			<input type="hidden" value="<?php echo $data['supplier_id'];?>" name="id_suppliers">
			<div class="control-group">
				<label for="textfield" class="control-label">Item</label>
				<div class="controls">
					<input type="text" name="item" class="form-control" placeholder="Nama Item" value="<?php echo $data['name'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Satuan</label>
				<div class="controls">
					<input type="text" name="satuan" class="form-control" placeholder="Satuan" value="<?php echo $data['custom3'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Harga Pengajuan</label>
				<div class="controls">
					<input type="text" name="hargapengajuan" class="form-control" placeholder="Harga Pengajuan" value="<?php echo $data['unit_price'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Harga Realisasi</label>
				<div class="controls">
					<input type="text" name="hargarealisasi" class="form-control" placeholder="Harga Realisasi" value="<?php echo $data['unit_price_acc'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Diskon</label>
				<div class="controls">
					<input type="text" name="diskon" class="form-control" placeholder="Harga Realisasi" value="<?php echo $data['unit_discount'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Setujui</label>
				 <div class="controls">
				    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td><input type="radio" class='icheck-me' value="0" name="acc" data-skin="square" data-color="blue" <?php if($data['acc']=='0') { echo "checked"; } else {} ?> > <label>Tidak Disetujui</label></td>
                        <td><input type="radio" class='icheck-me' value="1" name="acc" data-skin="square" data-color="blue" <?php if($data['acc']!='0') { echo "checked"; } else {} ?> > <label>Disetujui</label></td>
                    </tr>
                    </table>
				 </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>