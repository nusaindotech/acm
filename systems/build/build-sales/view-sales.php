			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-sales/insert-sales.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Sales</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kode</label>
						  <div class="controls">
							<input type="text" name="kodesales" class="form-control" placeholder="Kode Sales" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
						  <div class="controls">
							<input type="text" name="namasales" class="form-control" placeholder="Nama Sales" required="require" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Alamat</label>
						  <div class="controls">
							<input type="text" name="alamat" class="form-control" placeholder="Alamat Sales" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Telepon</label>
						  <div class="controls">
							<input type="text" name="telepon" class="form-control" placeholder="Telepon Sales" autocomplete="off">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
						  <div class="controls">
							<input type="text" name="email" class="form-control" placeholder="Email Sales" data-rule-email="true" autocomplete="off">
						  </div>
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=sales&navbar=sales&parent=master">Sales</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Sales</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Sales
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable table-bordered">
									<thead>
										<tr>
											<th>No</th>
											<th>Kode Sales</th>
											<th>Nama</th>
											<th>Alamat</th>
											<th>Telepon</th>
											<th>Email</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select * from people
											where type_people = 'sales' and deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['people_code']; ?></td>
											<td><?php echo $row['first_name']; ?></td>
											<td><?php echo $row['address_1']; ?></td>
											<td><?php echo $row['phone_number']; ?></td>
											<td><?php echo $row['email']; ?></td>
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-sales" data-id="<?php echo $row['person_id'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-sales/delete-sales.php?idsales=<?php echo $row['person_id']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-sales',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-sales/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Sales</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>