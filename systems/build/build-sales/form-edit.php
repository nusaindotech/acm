	<?php 
		include "../../../auth/autho.php";
		$id=$_POST['id'];
		$query = mysql_query("select * from people where person_id='$id'") or die(mysql_error());
		$data = mysql_fetch_array($query);
	?>
	<div class="modal-body">
		<form role="form" action="build/build-sales/update-sales.php" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped'>
			<input type="hidden" value="<?php echo $id;?>" name="id_people">
			
			<div class="control-group">
				<label class="control-label">Kode</label>
				<div class="controls">
					<input type="text" id="textfield" class="input-xlarge" name="kodesales" placeholder="Isikan Kode Sales" required="require" value="<?php echo $data['people_code'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama</label>
				<div class="controls">
					<input type="text" name="nama" id="textfield" placeholder="Nama Sales" class="input-xlarge" required="require" value="<?php echo $data['first_name'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Alamat</label>
				<div class="controls">
					<input type="text" name="alamat" id="textfield" placeholder="Alamat Sales" class="input-xlarge" value="<?php echo $data['address_1'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Telepon</label>
				<div class="controls">
					<input type="text" name="telepon" id="textfield" placeholder="Telepon Sales" class="input-xlarge" value="<?php echo $data['phone_number'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Email</label>
				<div class="controls">
					<input type="text" name="email" id="textfield" placeholder="Email Sales" class="input-xlarge" value="<?php echo $data['email'];?>" autocomplete="off">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>