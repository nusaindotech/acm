<?php
	$tglsekarang	= date('d-m-Y');
?>	

	<script>
        //<![CDATA[
        $(window).load(function(){
        $("#pelanggan").on("change", function(){
        	var nilai = $("#pelanggan :selected").attr("data-fax");
            var nilai2 = $("#pelanggan :selected").attr("data-nohp");
            var nilai3 = $("#pelanggan :selected").attr("data-email");
            $("#fax").val(nilai);
             $("#telepon").val(nilai2);
              $("#email").val(nilai3);
        });
        });
        
        //<![CDATA[
        $(window).load(function(){
        $("#Kantor").on("change", function(){
            var nilai = $("#Kantor :selected").attr("data-fax");
            var nilai2 = $("#Kantor :selected").attr("data-email");
            $("#fax1").val(nilai);
              $("#email1").val(nilai2);
        });
        });//]]>
    </script>
		
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-opname/insert-opname.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Membuat Kartu Opname ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Kartu Opname</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form</label>
						  <div class="controls">
							<input type="text" name="nopenawaran" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Gudang yang di Opname</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td>
								</td>
                                <td>
                                <div class="input-xlarge">
								<select name="gudang" id="gudang" class='chosen-select' required="required">
									<option value="">--Pilih Gudang--</option>
									<?php 	

									$tampil=mysql_query("SELECT * FROM `stock_locations`  where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['location_id']; ?>" ><?php echo $row['location_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						   <label for="inputEmail3" class="col-sm-2 control-label">Nama Pemohon</label>
						  <div class="controls">
							<input type="text" id="nama" name="nama" class="form-control" placeholder="Nama" data-rule-email="true" autocomplete="off"  value="<?=$_SESSION['nama_lengkap']?>" readonly>
						  </div>
						</div>
					  
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Kartu Opname</button>
					  </div>
					  
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Persediaan</a>
						</li>
						<li>
							<a href="dash.php?hp=stokopname&navbar=stokopname&parent=persediaan">Kartu Opname</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Kartu Opname</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Kartu Opname
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Kode Kartu Opname</th>
											<th>Tanggal</th>
											<th>Gudang Tujuan</th>
											<th>Status</th>
											<th>Nama</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select * from t_kpso  INNER JOIN stock_locations ON t_kpso.location_id = stock_locations.location_id  where t_kpso.deleted=0
									");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
										
									?>
										<tr>
												<td><?php echo $no; ?></td>
											<td><?php echo $row['id_kpso']; ?></td>
											<td><?php echo dateBahasaIndo($row['tanggal']); ?></td>
											<td><?php echo $row['location_name']; ?></td>
											<td><?php 
											if ($row['status']==0) {
												echo "<span class='label label-warning'>Belum Diterima</span>";
											} else {
												echo "<span class='label label-success'> Diterima</span>";
											}
											 ?></td>
											<td><?php echo $row['pj']; ?></td>
											<!-- <td><?php echo $row['company_phone']; ?></td>
											<td><?php echo $row['company_email']; ?></td>
 -->										<!-- 	<td>
											<?php
											if($row['status']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else
											{
												echo "<span class='label label-success'>Diterima</span>";
											}
											?>
											</td> -->
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<?php
												if($row['status']==0)
												{
												?>
													
													<li>
														<a href="dash.php?hp=stokopname2&navbar=stokopname&idph=<?php echo $row['id_kpso']; ?>&navbar=sp&parent=persediaan">Revisi Kartu Opname</a>
													</li>
												<?php
												}
												else
												{
													
												}
												?>
													
													<li>
														<a href="dash.php?hp=stokopname3&navbar=stokopname&idph=<?php echo $row['id_kpso']; ?>&navbar=stokopname&parent=persediaan">Lihat Kartu Opname</a>
													</li>
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-pelanggan',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-sp/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
				//$(function(){
				//	$("#datatable").modal('show');
				//}
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Customer</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>