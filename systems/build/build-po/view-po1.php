<?php
	$tglsekarang	= date('d-m-Y');
?>	
  <script>
            //<![CDATA[
            $(window).load(function(){
            $("#ps").on("change", function(){
                var nilai  = $("#ps :selected").attr("data-kategori");
                $("#kategori").val(nilai);
            });
            });//]]>
        </script>		
			<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-po/insert-po1.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Pesanan Pembelian Barang ?');">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Pesanan Pembelian Barang</h4>
					<input type="hidden" name="person_id" class="form-control" value="<?php echo $NoUser; ?>" data-rule-email="true" autocomplete="off" readonly>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form</label>
						  <div class="controls">
							<input type="text" name="nopesanan" class="form-control" placeholder="Auto" disabled>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
						  <div class="controls">
							<input type="text" name="tanggal" class="form-control" value="<?php echo $tglsekarang; ?>" data-rule-email="true" autocomplete="off" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Permintaan Pembelian</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="ps" id="ps" class='chosen-select' required=="required">
									<option value="">--Pilih No. Permintaan Pembelian--</option>
									<?php 	
									$j="Jasa";
									$tampil=mysql_query("select * from spp
											where deleted=0 and status=1 and custom1 not like '%$j%'
											order by id_spp DESC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
										$id_spp1=$row['id_spp'];
										$ceks=mysql_query("select count(b.cs3) as jumlah , COUNT(case b.cs3 when 1  then 1 end) as jumlah_kepakai,b.id_spp from detail_spp as b where id_spp ='$id_spp1'");
										$cek=mysql_fetch_array($ceks);
										if ($cek['jumlah_kepakai']<$cek['jumlah']) {
											# code...
									?>
									<option value="<?php echo $row['id_spp']; ?>" data-kategori="<?php echo $row['custom1']; ?>"><?php echo $row['id_spp']; ?></option>
									<?php 
									}
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kategori Barang</label>
						  <div class="controls">
							<input type="text" name="kategori" class="form-control" id="kategori" placeholder="Kategori Barang" autocomplete="off" required=="required" readonly>
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Supplier</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="supplier" id="supplier" class='chosen-select' required=="required">
									<option value="">--Pilih Supplier--</option>
									<?php 	
									$tampil=mysql_query("select * from suppliers
											where deleted=0 and acc=1
											order by company_name ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['supplier_id']; ?>"><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tempat Penyerahan Barang</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="gudang" id="gudang" class='chosen-select' required=="required">
									<option value="">--Pilih Tempat--</option>
									<?php 	
									$tampil=mysql_query("select * from stock_locations
											where deleted=0 order by location_name ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['location_id']; ?>"><?php echo $row['location_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tipe Pembelian</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="tipebeli" id="select3" class='chosen-select' required=="required">
								<option value="">--Tipe Pembelian--</option>
								<option value="Tunai">Pembelian Tunai</option>
								<option value="Kredit">Pembelian Kredit</option>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>	
						</div>

						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Syarat dan Ketentuan</label>
						  <div class="controls">
							<textarea name="qs" id="qs" class="input-block-level"></textarea>
						  </div>
						</div>

					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Buat Pesanan Pembelian Barang</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
						
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="#">Pembelian</a>
						</li>
						<li>
							<a href="dash.php?hp=po1&navbar=po1&parent=pembelian">Pesanan Pembelian Barang</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Pesanan Pembelian Barang</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Pesanan Pembelian Barang
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>No. Pesanan Pembelian Barang</th>
											<th>Tanggal</th>
											<th>Supplier</th>
											<th>No. Permintaan Pembelian</th>
											<th>Kategori Barang</th>
											<th>Tipe Pembelian</th>
											<th>Status</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
									$k='Jasa';	

									$tampil=mysql_query("select a.id_spp, a.id_po, c.custom2,a.date_transaction, a.type_op, a.custom1, a.status, b.company_code, b.company_name
											from po a, suppliers b, spp c
											where a.id_spp=c.id_spp and  a.supplier_id=b.supplier_id and a.deleted=0 and a.custom1 NOT LIKE '%$k%' order by id_po DESC");
									$tt=mysql_query("select cs3 from spp where id_spp='$idsppne'");	
									$row2	= mysql_fetch_array($tt);
									$bpo=$row2['cs3'];
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_po']; ?></td>
											<td><?php echo dateBahasaIndo($row['date_transaction']); ?></td>
											<td><?php echo $row['company_name']; ?></td>
											<td><?php echo $row['id_spp']; ?></td>
											<td><?php echo $row['custom1']; ?></td>
											<td><?php echo $row['type_op']; ?></td>
											<td>
											<?php
											if($row['status']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else
											{
												echo "<span class='label label-success'>Diterima</span>";
											}
											?>
											</td>
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<li>
													<a id="edit-po" data-id="<?php echo $row['id_po'];?>" role="button" data-toggle="modal">Update Pesanan Pembelian Barang</a>
												</li>
												<?php
												if($row['status']==0)
												{
												?>
												<li>
													<a href="dash.php?hp=po2&idbaru=<?php echo $row['id_po']; ?>&navbar=po&parent=pembelian">Revisi Pesanan Pembelian Barang</a>
												</li>
												<?php
												}
												else
												{?>

												<!-- 	<li>
														<a href="dash.php?hp=po2&idbaru=<?php echo $row['id_po']; ?>&navbar=po&parent=pembelian">Update Pesanan Pembelian Barang</a>
													</li> -->
													<li>
														<a href="dash.php?hp=po3&idbaru=<?php echo $row['id_po']; ?>&navbar=po&parent=pembelian">Cetak Pesanan Pembelian Barang</a>
													</li>
													<?php
													if ($bpo==0){
														?>
													<li>
														<a id="buat-lpb" data-id="<?php echo $row['id_po'];?>" role="button" data-toggle="modal">Buat Laporan Penerimaan Barang</a>
													</li>
													<?php
													}
													
													
												}
												?>
													
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#buat-lpb',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-po/form-editlpb.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	 <script>
				$(function(){
					$(document).on('click','#edit-po',function(e){
						e.preventDefault();
						$("#view2").modal('show');
						$.post('build/build-po/form-editpo.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body3").html(html);
							}  
						);
					});
				});
	</script>
	<div class="modal hide fade" id="view2" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Update Pesanan Pembelian Barang</h4>
				</div>
				<div class="modal-body3">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Buat Laporan Penerimaan Barang</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>
 