	<?php 
		include "../../../auth/autho.php";
		
		$tglsekarang	= date('d-m-Y');
		$idpo			=$_POST['id'];
		$query = mysql_query("select b.id_po, d.company_name from po b, suppliers d where b.supplier_id=d.supplier_id and b.id_po='$idpo'") or die(mysql_error());
		$data = mysql_fetch_array($query);
	?>
	<div class="modal-body">
		<form action="build/build-po/insert-sk.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Membuat Surat Kontrak ?');">
			<input type="hidden" value="<?php echo $idpo;?>" name="idpo">
			
			<div class="control-group">
				<label for="textfield" class="control-label">Tanggal</label>
				<div class="controls">
					<input type="text" name="tanggal" id="tanggal" class="input-xlarge" value="<?php echo $tglsekarang; ?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">No Pesanan Pembelian</label>
				<div class="controls">
					<input type="text" name="nopesan" id="nopesan" class="input-xlarge" value="<?php echo $idpo ?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Supplier</label>
				<div class="controls">
					<input type="text" name="supplier" id="supplier" class="input-xlarge" value="<?php echo $data['company_name'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Produk</label>
				<div class="controls">
					<select name="produk" id="produk" class='chosen-select' required=="required">
						<option value="">--Pilih Produk--</option>
						<?php 	
						$tampil=mysql_query("select item_id, name from items
								where deleted=0 order by name ASC");
						$no=1;
						while ($row=mysql_fetch_array($tampil))
						{
						?>
						<option value="<?php echo $row['item_id']; ?>"><?php echo $row['name']; ?></option>
						<?php 
						$no++;
						} 
						?>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Perusahaan</label>
				<div class="controls">
					<select name="home" id="home" class='chosen-select' required=="required">
						<option value="">--Pilih Perusahaan--</option>
						<?php 	
						$tampil=mysql_query("select id_home, nama from home
								where deleted=0 order by nama ASC");
						$no=1;
						while ($row=mysql_fetch_array($tampil))
						{
						?>
						<option value="<?php echo $row['id_home']; ?>"><?php echo $row['nama']; ?></option>
						<?php 
						$no++;
						} 
						?>
					</select>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Buat Surat Kontrak</button>
			</div>
        </form>
	</div>