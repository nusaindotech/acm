<?php
	$tglsekarang	= date('d-m-Y');
	$bulan			= $_GET['idbaru'];
	$tahun		= $_GET['idbaru2'];
	// $jenisbeli		= $_GET['idbaru3'];

function DateToIndo($date) { // fungsi atau method untuk mengubah tanggal ke format indonesia
   // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
		$BulanIndo = array("Januari", "Februari", "Maret",
						   "April", "Mei", "Juni",
						   "Juli", "Agustus", "September",
						   "Oktober", "November", "Desember");
	
		// $tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
		$bulan = $date; // memisahkan format bulan menggunakan substring
		// $tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring
		
		$result = " " . $BulanIndo[(int)$bulan-1];
		return($result);
}
?>		
		<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-po/insert-po2.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						
					  </div>
					  </div>
					   
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
			<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=lkpsk&navbar=lkpsk&parent=penjualan">Laporan Kontrak Per SK</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th"> Laporan Kontrak Ekspoert Per SK</i>								</h3>
								
							<!-- 	<div align="right">
									<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Pesanan Pembelian</a>								</div> -->
							</div>
							<!-- <div class="box-content"> -->
								<div class="invoice-info">
								<table >
										<tr>
			                             <td style="width:100%"><h4><center><b>Laporan Kontrak  Eksport Per SK</b></center></h4></td>
			                              <td></td>
                    					</tr>
                    			
								</table>	
                    				
                                  <br />
							  </div>
							  <div class="scroll">
								<table align="center" class="table table-striped table-invoice  table-bordered" >
<thead>
										<tr>
											<th rowspan="3" style="background-color: #fff000">No. </th>
											<th rowspan="3" style="background-color: #fff000">No. SK</th>
											<th rowspan="3" style="background-color: #fff000">Tanggal Kontrak</th>
											<th rowspan="3" style="background-color: #fff000">Kode Konsumen</th>
											<th rowspan="3" style="background-color: #fff000">Nama Konsumen</th>
											<th rowspan="3" style="background-color: #fff000">Jatuh Tempo Kontrak</th>
											<th rowspan="3" style="background-color: #fff000">Kode Produk</th>
											<th rowspan="3" style="background-color: #fff000">Nama Produk</th>
											<th colspan="9" style="background-color: #0ff000">Kontrak</th>
											<th colspan="7" style="background-color: #dfd0d0">UM</th>
											<th colspan="8" >Realisasi Pengiriman</th>
											<th colspan="6" style="background-color: #dfd0d0">Saldo</th>
										</tr>
										<tr>
											
											<th colspan="2" style="background-color: #0ff000">Kuantitas</th>
										  <th rowspan="2" style="background-color: #0ff000">Harga</th>
										  <th rowspan="2" style="background-color: #0ff000">Sub Jumlah</th>
										  <th rowspan="2" style="background-color: #0ff000">PPN</th>
										  <th rowspan="2" style="background-color: #0ff000">PPh</th>
										  <th rowspan="2" style="background-color: #0ff000">Jumlah Mata Uang Asing</th>
										  <th rowspan="2" style="background-color: #0ff000">Kurs</th>
										  <th rowspan="2" style="background-color: #0ff000">JUmlah</th>
										  <th rowspan="2" style="background-color: #dfd0d0">%UM</th>
											<th rowspan="2" style="background-color: #dfd0d0">UM</th>
											<th rowspan="2" style="background-color: #dfd0d0">PPN</th>
											<th rowspan="2" style="background-color: #dfd0d0">PPh</th>
											<th rowspan="2" style="background-color: #dfd0d0">Jumlah Mata Uang Asing</th>
											<th rowspan="2" style="background-color: #dfd0d0">Kurs</th>
											<th rowspan="2" style="background-color: #dfd0d0">Jumlah</th>
											<th colspan="2">Kuantitas</th>
											<th rowspan="2">Sub Jumlah</th>
											<th rowspan="2">PPN</th>
											<th rowspan="2">PPh</th>
											<th rowspan="2">Jumlah Mata Uang Asing</th>
											<th rowspan="2">Kurs</th>
											<th rowspan="2">Jumlah</th>
											<th rowspan="2" style="background-color: #dfd0d0">Selisih Kurs</th>
											<th colspan="4" style="background-color: #dfd0d0">Saldo Kontrak</th>
											<th colspan="2" style="background-color: #dfd0d0">Saldo Piutang</th>

										</tr>
									<tr>
										<th style="background-color: #0ff000">Bag</th>
										<th style="background-color: #0ff000">Tonase</th>
										<th bgcolor="#0FF000">Bag</th>
									  	<th bgcolor="#0FF000">Tonase</th>
									  
										<th style="background-color: #dfd0d0">Bag</th>
										<th style="background-color: #dfd0d0">Tonase</th>
										<th style="background-color: #dfd0d0">Nominal</th>
										<th style="background-color: #dfd0d0">Nominal Sisa Kontrak</th>
										<th style="background-color: #dfd0d0">Jumlah</th>
									</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.no_kontrak ,g2.id_sj,g2.item_id, a.tanggal as tglsk,d.company_code,d.company_name,
 a.tgl_jt, b.qty as qty_sk,b.coly as coly_sk,b.jumlah as jumlah_sk,b.harga as harga_sk,a.ppn as ppn_sk ,a.gt as gt_sk,
 c.item_number,c.name,e.id_npum, f.id_do,g.total as qty_sj ,
e.id_npum,e.um,e.ppn as ppn_um,e.pph as pph_um, e.total_um as nilai_um,e.total  as total_um,
h.id_np ,  h.total as total_np,h.ppn as ppn_np,h.gt as gt_np from t_skj a 

INNER JOIN detail_skj b on b.no_kontrak = a.no_kontrak
INNER JOIN items c on c.item_id = b.item_id
INNER JOIN customers d on d.customer_id = a.customer_id
LEFT OUTER JOIN t_npum e ON e.id_so = a.no_kontrak
LEFT OUTER JOIN t_do f on f.id_so = a.no_kontrak
LEFT OUTER JOIN t_sj g on g.id_do = f.id_do
INNER JOIN detail_sj g2 on g2.id_sj = g.id_sj
INNER JOIN t_np h on h.id_so = a.no_kontrak

");

									$no=1;
									 
									while ($row=mysql_fetch_array($tampil))
									{
										$nk = $row['no_kontrak'];
										$qty_sk = [$nk=>$row['qty_sk']];
										$qty_sj = [$nk=>$row['qty_sk']];
										
										// print_r($qty_sk);
										// print_r(" <br>")
										$qty_sk[$nk] = $qty_sk[$nk] - $qty_sj[$nk];
										
									?>
										<tr>
											 <td bgcolor="#FFCC66">&nbsp;<?php echo $no; ?></td>
										  <td bgcolor="#FFCC66" >&nbsp;<?php echo $row['no_kontrak']; ?></td>
										  <td bgcolor="#FFCC66" >&nbsp;<?php echo $row['tglsk']; ?></td>
										  <td bgcolor="#FFCC66" >&nbsp;<?php echo $row['company_code']; ?></td>
										  <td bgcolor="#FFCC66" >&nbsp;<?php echo $row['company_name']; ?></td>
										  <td bgcolor="#FFCC66" >&nbsp;<?php echo ($row['tgl_jt']); ?></td>
										  <td bgcolor="#FFCC66" >&nbsp;<?php echo ($row['item_number']); ?></td>
										  <td bgcolor="#FFCC66" >&nbsp;<?php echo ($row['name']); ?></td>
										  <td >&nbsp;<?php echo ($row['coly_sk']); ?></td>
											<td >&nbsp;<?php echo ($row['qty_sk']); ?></td>
											<td >&nbsp;<?php echo ($row['harga_sk']); ?></td>
											<td >&nbsp;<?php echo ($row['jumlah_sk']); ?></td>
											<td >&nbsp;<?php echo ($row['ppn_sk']); ?></td>
											<td >&nbsp;<?php echo ($row['pph_sk']); ?></td>
											<td >&nbsp;<?php echo ($row['gt_sk']); ?></td>
											<td  style="background-color: #dfd0d0">&nbsp;<?php echo ($row['um']); ?></td>
											<td style="background-color: #dfd0d0">&nbsp;<?php echo ($row['nilai_um']); ?></td>
											<td style="background-color: #dfd0d0">&nbsp;<?php echo ($row['ppn_um']); ?></td>
											<td style="background-color: #dfd0d0">&nbsp;<?php echo ($row['ppn_um']); ?></td>
											<td style="background-color: #dfd0d0">&nbsp;<?php echo ($row['total_um']); ?></td>
											<td >&nbsp;<?php echo ($row['qty_sj']); ?></td>
											<td >&nbsp;<?php echo ($row['coly_sj']); ?></td>
											<td >&nbsp;<?php echo ($row['total_np']); ?></td>
											<td >&nbsp;<?php echo ($row['ppn_np']); ?></td>
											<td >&nbsp;<?php echo ($row['pph_np']); ?></td>
											<td >&nbsp;<?php echo ($row['gt_np']); ?></td>
											<td >&nbsp;-</td>
											<td >&nbsp;-</td>
											<td >&nbsp;<?php echo ($qty_sk[$nk]); ?></td>
											</tr>
									<?php 

									$no++;
									} 
									
									?>
									
                                  
									</tbody>
								</table>
								
						  </div>
						</div>
					</div>
					
				
				
				</div>
				
				<style type="text/css">
					th,td .table-invoice{
						border: 1px solid black;
					}
					
					tr .table-invoice{
						border: 2px solid black;


					}
					.table-invoice{
						overflow-x: auto; 
					}
				</style>
			
				
