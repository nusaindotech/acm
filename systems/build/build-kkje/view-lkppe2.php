<?php
	$tglsekarang	= date('d-m-Y');
	$bulan			= $_GET['idbaru'];
	$tahun		= $_GET['idbaru2'];
	// $jenisbeli		= $_GET['idbaru3'];

function DateToIndo($date) { // fungsi atau method untuk mengubah tanggal ke format indonesia
   // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
		$BulanIndo = array("Januari", "Februari", "Maret",
						   "April", "Mei", "Juni",
						   "Juli", "Agustus", "September",
						   "Oktober", "November", "Desember");
	
		// $tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
		$bulan = $date; // memisahkan format bulan menggunakan substring
		// $tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring
		
		$result = " " . $BulanIndo[(int)$bulan-1];
		return($result);
}
?>		
		<div class="modal hide fade" id="myModal" role="dialog" tabindex="-1">
              <div class="modal-dialog">
				<form action="build/build-po/insert-po2.php" method="POST" class="form-horizontal" >
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
						
					  </div>
					  </div>
					   
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
			<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=lkpp&navbar=lkpp&parent=penjualan">Laporan Kontrak Export Per Produk</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-th"> Laporan Kontrak Export Per Produk</i>								</h3>
								
							<!-- 	<div align="right">
									<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Pesanan Pembelian</a>								</div> -->
							</div>
							<!-- <div class="box-content"> -->
								<div class="invoice-info">
								<table >
										<tr>
			                             <td style="width:100%"><h4><center><b>Laporan Kontrak Export Per Produk</b></center></h4></td>
			                              <td></td>
                    					</tr>
                    			
								</table>	
                    				<table>
									 	<tr>
		                                 <th align="left">Bulan</th>
		                                 <td style="width:50%;">:&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo(DateToIndo($bulan));?></b></td>
                                    	</tr>
                                    	<tr>
	                                     <th align="left">Tahun</th>
                              		      <td>:&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo $tahun;?></b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
		                                     
                                    	</tr>
										
								</table>
                                  <br />
							  </div>
							  <div class="scroll">
								<table class="table table-striped table-invoice" style="border:1px">
									<thead>
										<tr>
											<th rowspan="3" ">No. </th>
											<th rowspan="3">Kode Produk</th>
											<th rowspan="3">Nama Produk</th>
											<th colspan="7" style="background-color: #fffddd"> <div align="center">Kontrak</div></th>
											<th colspan="9" style="background-color: #dddfff"> <div align="center">Uang Muka</div></th>
											<th colspan="7" style="background-color: #fffddd"> <div align="center">Realisasi Pengiriman</div></th>
											<th colspan="7" style="background-color: #dddfff"> <div align="center">Saldo</div></th>
										</tr>
										<tr>
											
											<th colspan="2" style="background-color: #fffddd">Kuantitas</th>
											<th rowspan="2" style="background-color: #fffddd">Harga</th>
											<th rowspan="2" style="background-color: #fffddd">Sub Jumlah</th>
											<th rowspan="2" style="background-color: #fffddd">PPN</th>
											<th rowspan="2" style="background-color: #fffddd">PPh</th>
											<th rowspan="2" style="background-color: #fffddd">Jumlah</th>
											<th rowspan="3" style="background-color: #dddfff">%UM<th>
											<th rowspan="3" style="background-color: #dddfff">UM<th>
											<th rowspan="3" style="background-color: #dddfff">PPN<th>
											<th rowspan="3" style="background-color: #dddfff">PPh<th>
											<th rowspan="3" style="background-color: #dddfff">Jumlah<th>
											<th colspan="2" style="background-color: #fffddd">Kuantitas</th>
											<th rowspan="3" style="background-color: #fffddd">Sub Jumlah</th>
											<th rowspan="3" style="background-color: #fffddd">PPN</th>
											<th rowspan="3" style="background-color: #fffddd">PPh</th>
											<th rowspan="3" style="background-color: #fffddd">Jumlah</th>
											<th colspan="3">Saldo Kontrak</th>
											<th colspan="1">Saldo Piutang</th>
										</tr>
									<tr>
										<th style="background-color: #fffddd">Bag</th>
										<th style="background-color: #fffddd">Tonase</th>
										<th style="background-color: #dddfff"></th>
										<th style="background-color: #dddfff"></th>
										<th style="background-color: #dddfff"></th>
										<th style="background-color: #dddfff"></th>
										<th style="background-color: #fffddd">Bag</th>
										<th style="background-color: #fffddd">Tonase</th>
										<th></th>
										<th>Bag</th>
										<th>Tonase</th>
										<th>Jumlah</th>
										<th>Rp</th>
									</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("SELECT lpb.id_lpb, (lpb.date_transaction) as tanggallpb,(lpb.id_po) as No_pesanan_pembelian,(po.custom1) as jenis_pembelian, (suppliers.company_code) as kode_supplier, suppliers.company_name, suppliers.ppn, (po.type_op) as tipe_pembelian,(items.item_number) as kode_barang, (items.name) as nama_barang, (detail_lpb.unit_qty) as kuantitas, (detail_lpb.unit_price) as harga_satuan,(detail_lpb.custom3) as diskon,(lpb.custom5) as dpp,(lpb.custom6) as ppnlpb,(lpb.custom7) as grand_total, (po.custom10) as umk from lpb left outer JOIN po on lpb.id_po=po.id_po left OUTER join suppliers on po.supplier_id=suppliers.supplier_id LEFT OUTER join detail_lpb on detail_lpb.id_lpb=lpb.id_lpb left OUTER join items on items.item_id=detail_lpb.item_id where month(lpb.date_transaction) ='$bulan' and year(lpb.date_transaction) ='$tahun' order by lpb.id_lpb ASC");

									$no=1;
									 
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											 <td>&nbsp;<?php echo $no; ?></td>
											<td >&nbsp;<?php echo $row['id_lpb']; ?></td>
											<td >&nbsp;<?php echo $row['tanggallpb']; ?></td>
											<td >&nbsp;<?php echo $row['No_pesanan_pembelian']; ?></td>
											<td >&nbsp;<?php echo $row['jenis_pembelian']; ?></td>
											<td >&nbsp;<?php echo ($row['kode_supplier']); ?></td>
											</tr>
									<?php 
									
									$no++;
									} 
									
									?>
									
                                  
									</tbody>
								</table>
								
							</div>
						</div>
					</div>
					
				
				
				</div>
				
			
				
