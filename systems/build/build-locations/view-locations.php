
			<div class="modal hide fade modal-besar" id="myModal" role="dialog" tabindex="-1" >
              <div class="modal-dialog">
				<form action="build/build-locations/insert-locations.php" method="POST" class="form-horizontal" >
					<div class="modal-content" >
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Gudang</h4>
					  </div>
					  <div class="modal-body overflow-visible" style="height: 250px;">
					  <div class="box-body">
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Gudang</label>
						  <div class="controls">
							<input type="text" name="namagudang" class="form-control" placeholder="Nama Gudang" required="require">
						  </div>
						</div>
						<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Kota</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="kota" id="kota" class='chosen-select' required=="required">
									<option value="">--Pilih Kota--</option>
									<?php 	
									$tampil=mysql_query("select * from city where status_deleted=0 ");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_city']; ?>" ><?php echo $row['name_city']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>
					  </div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=gudang&navbar=gudang&parent=master">Kantor & Gudang</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Tambah Kantor & Gudang</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Kantor & Gudang
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Nama Gudang</th>
											<th>Kota</th>
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.location_name, b.name_city from stock_locations a, city b where a.id_city=b.id_city and a.deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['location_name']; ?></td>
											<td><?php echo $row['name_city']; ?></td>
											<td class='hidden-350'>
											<a class="btn btn-primary" id="edit-gudang" data-id="<?php echo $row['location_id'];?>"><i class="icon-edit"></i></a>
											<a class="btn btn-danger" href="build/build-locations/delete-locations.php?idlocations=<?php echo $row['location_id']; ?>"><i class="icon-trash"></i></a>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
				
	<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-gudang',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-locations/form-edit.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Data Kota</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>