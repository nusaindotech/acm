
<?php
	$tglsekarang	= date('d-m-Y');
?>	
			<div class="modal hide fade modal" id="myModal" role="dialog" tabindex="-1" >
              <div class="modal-dialog">
				<form action="build/build-psb/insert-psb.php" method="POST" class="form-horizontal" onsubmit="return confirm('Apakah Yakin Simpan Data Pengajuan Supplier ?');">
				
					<div class="modal-content" style="height: 100px">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Buat Pengajuan Supplier</h4>
					  </div>
					  <div class="modal-body overflow-visible">
					  <div class="box-body">
					  <div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">No. Form</label>
						  <div class="controls">
							<input type="text" name="kodepsb" class="form-control" placeholder="Auto" required="require" autocomplete="off" disabled>
						  </div>
						</div>
					<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Masuk</label>
						  <div class="controls">
							<input type="text" id="input" name="tanggal" class="form-control" data-rule-email="true" autocomplete="off" value="<?php echo $tglsekarang ?>" readonly>
						  </div>
					</div>	
					<div class="control-group">
						  <label for="inputEmail3" class="col-sm-2 control-label">Nama Barang</label>
                          <div class="controls">
						    <table width="50%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>
                                <div class="input-xlarge">
								<select name="barang" id="barang" class='chosen-select' required=="required">
									<option value="">--Pilih Barang--</option>
									<?php 	
									$j='Jasa';
									$tampil=mysql_query("select * from items where category not like  'Produk Utama' order by name ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['item_id']; ?>"><?php echo $row['name']; ?></option>
									<?php 
									$no++;
									} 
									?>
								</select>
								</div>
                                </td>
                              </tr>
                            </table>
						  </div>						  
						</div>		
					</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					  </div>
				</form>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
			
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=home&navbar=dashboard">Dashboard</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="dash.php?hp=ps&navbar=ps&parent=pembelian">Pengajuan Supplier</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered">
							<br>						
						    <div align="right">
							<a href='#myModal' class="btn btn-primary" role="button" data-toggle="modal"><i class="icon-plus"></i> Buat Pengajuan Supplier Baru</a>
							</div>
				      <div class="box-title">
								<h3>
									<i class="icon-reorder"></i>
									Daftar Pengajuan Supplier Baru
								</h3>
							</div>
							<div class="box-content nopadding">
								<table class="table table-hover table-nomargin dataTable dataTable-columnfilter table-bordered">
									<thead>
										<tr class='thefilter'>
											<th>No</th>
											<th>Tanggal</th>
											<th>No. Form</th>

											<!-- <th>Kode Supplier</th>
											<th>Nama Supplier</th>
											<th>Alamat</th>
											<th>NPWP</th>
											<th>Telepon</th> -->
											<th>Nama Barang</th>
											<th>Status</th>
											<!-- <th>Syarat Pembayaran</th> -->
											<th class='hidden-350'>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php 	
																		
									$tampil=mysql_query("select a.id_psb,a.tanggal,b.name,a.status from t_psb a, items b where a.item_id=b.item_id and a.deleted=0 order by a.id_psb ASC");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
										<tr>
											<td><?php echo $no; ?></td>
											<td><?php echo $row['id_psb']; ?></td>
											<td><?php echo  dateBahasaIndo($row['tanggal']); ?></td>
											<td><?php echo $row['name']; ?></td>
											<td>
											<?php
											if($row['status']==0)
											{
												echo "<span class='label label-warning'>Belum Selesai</span>";
											}
											else if($row['status']==1)
											{
												echo "<span class='label label-blue'>Menungu ACC</span>";
											}
											else if($row['status']==2)
											{
												echo "<span class='label label-success'>Selesai</span>";
											}
											?>
											</td>
																						
											<td class='hidden-350'>
											<div class="btn-group">
												<a href="#" data-toggle="dropdown" class="btn btn-inverse dropdown-toggle"><i class="icon-cog"></i> Aksi <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-inverse">
												<li>
													<a id="edit-psb" data-id="<?php echo $row['id_psb'];?>" role="button" data-toggle="modal">Update Pengajuan Supplier</a>
												</li>
												<?php
													if($row['status']==0 )
													{
													?>
													
													<li>
														<a href="dash.php?hp=ps2&idbaru=<?php echo $row['id_psb']; ?>&navbar=ps&parent=pembelian">Revisi (AP) Pengajuan Supplier</a>
													</li>
													
													<?php
													}
													elseif($row['status']==1){
														?>
													<li>
														<a href="dash.php?hp=ps2kp&idbaru=<?php echo $row['id_psb']; ?>&navbar=ps&parent=pembelian">(KP) Persetujuan Pengajuan Supplier</a>
													</li>
													<?php
													}
													
													else
													{?>
													
														<li>
														<a href="dash.php?hp=ps3&idbaru=<?php echo $row['id_psb']; ?>&navbar=ps&parent=pembelian">Cetak Pengajuan Suplier</a>
													    </li>
													
													<?php
													}
													?>
													
												</ul>
											</div>
											</td>
										</tr>
									<?php 
									$no++;
									} 
									?>
									</tbody>
								</table>
							</div>
						</div>
				  </div>
				</div>
<script src="js/jquery.js"></script>
	<script>
				$(function(){
					$(document).on('click','#edit-psb',function(e){
						e.preventDefault();
						$("#view").modal('show');
						$.post('build/build-psb/form-editpsb.php',
							{id:$(this).attr('data-id')},
							function(html){
								$(".modal-body2").html(html);
							}  
						);
					});
				});
	</script>				
	
	
	<div class="modal hide fade" id="view" role="dialog" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times; </button>
				<h4 class="modal-title" id="myModalLabel">Update Data Pengajuan Supplier</h4>
				</div>
				<div class="modal-body2">
				</div>
				<div class="modal-footer">
				</div>
			</div>
		</div>
	</div>