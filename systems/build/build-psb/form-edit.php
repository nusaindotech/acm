	<?php 
		include "../../../auth/autho.php";
		$item_id		=$_POST['id'];
		$idbaru			=$_POST['id2'];
		$iddetail		=$_POST['id3'];
		
		$query 	= mysql_query("select b.id_detail_psb,a.item_id,a.name, b.nama_supplier, b.alamat, b.phone,b.contact_person,b.npwp,b.harga_satuan,b.diskon,b.ppn,b.total,b.ppn,b.syarat_pembayaran from detail_psb b, items a where a.item_id=b.item_id and b.id_psb='$idbaru'") or die(mysql_error());
		$data 	= mysql_fetch_array($query);
	?>
	<div class="modal-body">
		<form role="form" action="build/build-psb/update-psb1.php" method="POST" enctype="multipart/form-data" class='form-horizontal form-striped'>
			<input type="hidden" value="<?php echo $idbaru;?>" name="idbaru">
			<input type="hidden" value="<?php echo $iddetail;?>" name="iddetail">
			<input type="hidden" name="item" class="form-control" value="<?php echo $data['item_id']; ?>" data-rule-email="true" autocomplete="off" readonly>
			
			<div class="control-group">
				<label for="textfield" class="control-label">Nama Calon Supplier</label>
				<div class="controls">
					<input type="text" name="nama_supplier" id="textfield" placeholder="Spesifikasi" class="input-xlarge" value="<?php echo $data['nama_supplier'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Alamat</label>
				<div class="controls">
					<input type="text" name="alamat" id="textfield" placeholder="Alamat" class="input-xlarge" value="<?php echo $data['alamat'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Telepon</label>
				<div class="controls">
					<input type="text" name="telepon" id="textfield" placeholder="Telepon" class="input-xlarge" value="<?php echo $data['phone'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Contact Person</label>
				<div class="controls">
					<input type="text" name="cp" id="textfield" placeholder="Contact Person" class="input-xlarge" value="<?php echo $data['contact_person'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">NPWP</label>
				<div class="controls">
					<input type="text" name="npwp" id="textfield" placeholder="NPWP" class="input-xlarge" value="<?php echo $data['npwp'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Nama Barang</label>
				<div class="controls">
					<input type="text" name="barang" id="textfield" placeholder="Barang" class="input-xlarge" value="<?php echo $data['name'];?>" autocomplete="off" readonly>
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Harga Satuan</label>
				<div class="controls">
					<input type="text" name="harga" id="textfield" placeholder="Harga Satuan" class="input-xlarge" value="<?php echo $data['harga_satuan'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Diskon</label>
				<div class="controls">
					<input type="text" name="diskon" id="textfield" placeholder="Diskon" class="input-xlarge" value="<?php echo $data['diskon'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">PPN/PPh</label>
				<div class="controls">
					<input type="text" name="ppn" id="textfield" placeholder="PPN/PPh" class="input-xlarge" value="<?php echo $data['ppn'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Total</label>
				<div class="controls">
					<input type="text" name="total" id="textfield" placeholder="Total" class="input-xlarge" value="<?php echo $data['total'];?>" autocomplete="off">
				</div>
			</div>
			<div class="control-group">
				<label for="textfield" class="control-label">Syarat Pembayaran</label>
				<div class="controls">
					<input type="text" name="sp" id="textfield" placeholder="Syarat Pembayaran" class="input-xlarge" value="<?php echo $data['syarat_pembayaran'];?>" autocomplete="off">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
        </form>
	</div>