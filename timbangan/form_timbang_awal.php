<?php 
	ob_start();
	error_reporting(0);
	include "koneksi/conn.php";
	$idt = $_GET["idt"];
	$komoditi = $_GET["komoditi"];
	if($idt == "" || $idt <= 0)
	{
		$sql = mysql_query("select max(id_timbang) from ttr_scale");
		$row = mysql_fetch_array($sql);
		$hasil = $row[0]+1;
	}
	else
	{
		$hasil = $idt;
	}
	
	function dateBahasaIndo($date){
		$bulan=array(
					'01'=>'Januari',
					'02'=>'Februari',
					'03'=>'Maret',
					'04'=>'April',
					'05'=>'Mei',
					'06'=>'Juni',
					'07'=>'Juli',
					'08'=>'Agustus',
					'09'=>'September',
					'10'=>'Oktober',
					'11'=>'November',
					'12'=>'Desember',
					 );
		$pecah=explode('-',$date);

		$tgl_pecah=$pecah[2];
		$bln_pecah=$pecah[1];
		$thn_pecah=$pecah[0];
		return $tgl_pecah.' '.$bulan[$bln_pecah].' '.$thn_pecah;
		}
	
	$tglsekarang			= date('Y-m-d');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Nota Timbang | Agro Cita Mandiri</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">

	<!-- The styles -->
	<link id="bs-css" href="css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/charisma-app.css" rel="stylesheet">
	<link href="css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='css/fullcalendar.css' rel='stylesheet'>
	<link href='css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='css/chosen.css' rel='stylesheet'>
	<link href='css/uniform.default.css' rel='stylesheet'>
	<link href='css/colorbox.css' rel='stylesheet'>
	<link href='css/jquery.cleditor.css' rel='stylesheet'>
	<link href='css/jquery.noty.css' rel='stylesheet'>
	<link href='css/noty_theme_default.css' rel='stylesheet'>
	<link href='css/elfinder.min.css' rel='stylesheet'>
	<link href='css/elfinder.theme.css' rel='stylesheet'>
	<link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='css/opa-icons.css' rel='stylesheet'>
	<link href='css/uploadify.css' rel='stylesheet'>
	<link rel="shortcut icon" href="img/favicon.ico">
	<script src="js/jquery.min.js" type="text/javascript"></script>
</head>

<body>
		<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.html"> <img alt="Charisma Logo" src="img/logo20.png" /> <span>Nota Timbang|ACM</span></a>
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"> admin</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#">Profile</a></li>
						<li class="divider"></li>
						<li><a href="#">Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends --
				
				<div class="top-nav nav-collapse">
					<ul class="nav">
						<li><a href="#">Visit Site</a></li>
						<li>
							<form class="navbar-search pull-left">
								<input placeholder="Search" class="search-query span2" name="query" type="text">
							</form>
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Main</li>
						<li><a class="ajax-link" href="index.php"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
					</ul>
					<!--<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>-->
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
						
			<div id="content" class="span10">
			
			<div class="row-fluid sortable">
				<div class="box span12">
					
				<div class="box-content">
						<form action = 'add_form.php' method = 'post'>
							<table>
								<tr>
									<td width = '90'>No Nota</td>
									<td width="4">:</td>
									<td width="233">
									<input type = 'text' name = 'nonota' id = 'nonota' class='form-control1' value="auto generate" autofocus readonly>									</td>
								</tr>
								<tr>
									<td width = '90'>Tanggal</td>
									<td>:</td>
									<td><input type = 'hidden' name = 'tanggal' id = 'tanggal' class='form-control1' value='<?php echo $tglsekarang; ?>' autofocus readonly>
                                      <input type = 'text' name = 'tanggal2' id = 'tanggal2' class='form-control1' value='<?php echo dateBahasaIndo($tglsekarang); ?>' autofocus readonly></td>
								</tr>
								<tr>
									<td width = '90'>Jenis Timbang</td>
									<td>:</td>
									<td>
										 <select id="selectError2" data-rel="chosen" name="jenistimbang" id="jenistimbang" required>
											<option value="">--Pilih Jenis Timbang--</option>
									<?php 	
									$tampil=mysql_query("select * from t_jenis_timbang where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['id_jt']; ?>"><?php echo $row['nama_jenis']; ?></option>
									<?php 
									$no++;
									} 
									?>
										  </select>								
									</td>
								</tr>
								<tr>
									<td width = '90'>Supplier</td>
									<td>:</td>
									<td>
										 <select id="selectError" data-rel="chosen" name="supplier" required>
									<option value="">--Pilih Supplier--</option>
									<?php 	
									$tampil=mysql_query("select * from suppliers where deleted=0");
									$no=1;
									while ($row=mysql_fetch_array($tampil))
									{
									?>
									<option value="<?php echo $row['supplier_id']; ?>"><?php echo $row['company_name']; ?></option>
									<?php 
									$no++;
									} 
									?>
										  </select>								
									</td>
								</tr>
								<tr>
									<td width = '90'>Komoditi</td>
									<td>:</td>
									<td>
									<img src = 'img/coklat.png' width = '70px' height = '70px' align='center'> <br>
										<input type = 'text' name = 'komoditi' id = 'komoditi' class='form-control1' value="<?php echo strtoupper("$komoditi"); ?>" autofocus readonly>									</td>
								</tr>
								<tr>
									<td>No Polisi</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'nopol' id = 'nopol' class='form-control1' required>
									</td>
								</tr>
								<tr>
									<td>No Kontainer</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'kontainer' id = 'kontainer' class='form-control1'>									</td>
								</tr>
								<tr>
									<td>Seal No 1</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'seal1' id = 'seal1' class='form-control1'>									</td>
								</tr>
								<tr>
									<td>Seal No 2</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'seal2' id = 'seal2' class='form-control1'>									</td>
								</tr>
								<tr>
									<td>Jumlah Colly</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'totalcolly' id = 'jumlahcolly' class='form-control1'>									</td>
								</tr>
								<tr>
									<td>Catatan</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'catatan' id = 'catatan' class='form-control1'>									
									</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td>																				
										<button class="btn btn-primary"><i class="fa fa-save"></i> Buat Nota Timbang</button>
										<!-- <button class="btn btn-primary" onclick="add_scale();"><i class="fa fa-save"></i> Simpan</button> -->
										<button class="btn btn-danger" type = 'reset'><i class="fa fa-reset"></i> Reset</button>									</td>
								</tr>
							</table>
            <input type = 'hidden' name = 'id_timbang1' id = 'id_timbang1' class='form-control4' style = "width:50px;" value = "<?php echo $hasil;?>">
							<br/>
						</form>
					</div>	
					
				</div>
			</div><!--/row-->
			
			<!-- content starts -->
			
			<!-- content ends -->
			</div><!--/#content.span10-->
			</div><!--/fluid-row-->
			
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">�</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<footer>
			<p class="pull-left">&copy; <a href="http://usman.it" target="_blank">Agro Cita Mandiri</a> 2017</p>
		</footer>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script src="js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script src="js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script src="js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="js/jquery.elfinder.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>
	
		
</body>
</html>
