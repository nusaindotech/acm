<?php 
	ob_start();
	error_reporting(0);
	include "koneksi/conn.php";
	$idt = $_GET["idt"];
	
	$hasil = $idt;
	
	function dateBahasaIndo($date){
		$bulan=array(
					'01'=>'Januari',
					'02'=>'Februari',
					'03'=>'Maret',
					'04'=>'April',
					'05'=>'Mei',
					'06'=>'Juni',
					'07'=>'Juli',
					'08'=>'Agustus',
					'09'=>'September',
					'10'=>'Oktober',
					'11'=>'November',
					'12'=>'Desember',
					 );
		$pecah=explode('-',$date);

		$tgl_pecah=$pecah[2];
		$bln_pecah=$pecah[1];
		$thn_pecah=$pecah[0];
		return $tgl_pecah.' '.$bulan[$bln_pecah].' '.$thn_pecah;
		}
	
	$tglsekarang			= date('Y-m-d');
	
	$a			= mysql_query("SELECT a.company_name, b.id_jt, d.nama_jenis, b.tanggal, b.total_colly, b.no_polisi, b.no_kontainer, b.seal1, b.seal2, c.name, b.item_id, b.catatan
				  from suppliers a, ttr_main_scale b, items c, t_jenis_timbang d 
				  where b.id_timbang='$idt' and d.id_jt=b.id_jt and a.supplier_id=b.supplier_id and b.item_id=c.item_id");
	$row		= mysql_fetch_array($a);
	
	$totalcolly	= $row['total_colly'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Nota Timbang | Agro Cita Mandiri</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">

	<!-- The styles -->
	<link id="bs-css" href="css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/charisma-app.css" rel="stylesheet">
	<link href="css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='css/fullcalendar.css' rel='stylesheet'>
	<link href='css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='css/chosen.css' rel='stylesheet'>
	<link href='css/uniform.default.css' rel='stylesheet'>
	<link href='css/colorbox.css' rel='stylesheet'>
	<link href='css/jquery.cleditor.css' rel='stylesheet'>
	<link href='css/jquery.noty.css' rel='stylesheet'>
	<link href='css/noty_theme_default.css' rel='stylesheet'>
	<link href='css/elfinder.min.css' rel='stylesheet'>
	<link href='css/elfinder.theme.css' rel='stylesheet'>
	<link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='css/opa-icons.css' rel='stylesheet'>
	<link href='css/uploadify.css' rel='stylesheet'>
	<link rel="shortcut icon" href="img/favicon.ico">
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script language="JavaScript">
		setInterval( "SANAjax();", 10 );  // set berapa detik melakukan refresh div
		$(function() {
		SANAjax = function(){
		$('#AutodataDisplay1').load("form_tes.php").fadeIn("veryfast");  // load berarti melakukan load file
		// $('#AutodataDisplay1').load("form_tes.php?id=<?php echo "$iki";?>").fadeIn("veryfast");  // load berarti melakukan load file
		}
		});
	</script>
</head>

<body>
		<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.html"> <img alt="Charisma Logo" src="img/logo20.png" /> <span>Nota Timbang|ACM</span></a>
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"> admin</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#">Profile</a></li>
						<li class="divider"></li>
						<li><a href="#">Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends --
				
				<div class="top-nav nav-collapse">
					<ul class="nav">
						<li><a href="#">Visit Site</a></li>
						<li>
							<form class="navbar-search pull-left">
								<input placeholder="Search" class="search-query span2" name="query" type="text">
							</form>
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Main</li>
						<li><a class="ajax-link" href="index.php"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
					</ul>
					<!--<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>-->
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
						
			<div id="content" class="span10">
			
			<div class="row-fluid sortable">
				<div class="box span6">
					<div class="box-header well" data-original-title>
						<h2>Jenis Kemasan</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
					  <table>
                        <tr>
                          <td>No Nota</td>
                          <td>:</td>
                          <td><input type = 'text' value='<?php echo $idt; ?>' name = 'nonota2' id = 'nonota2' class='form-control1' autofocus readonly>
                          </td>
                        </tr>
                        <tr>
                          <td>Tanggal</td>
                          <td>:</td>
                          <td><input type = 'text' value='<?php echo dateBahasaIndo($row['tanggal']); ?>' name = 'tanggal' id = 'tanggal' class='form-control1' autofocus readonly></td>
                          </td>
                        </tr>
                        <tr>
                          <td>Jenis Timbang</td>
                          <td>:</td>
                          <td><input type = 'text' value='<?php echo $row['nama_jenis']; ?>' name = 'jenistimbang' id = 'jenistimbang' class='form-control1' autofocus readonly>
                          </td>
                        </tr>
						<tr>
                          <td>Supplier</td>
                          <td>:</td>
                          <td><input type = 'text' value='<?php echo $row['company_name']; ?>' name = 'supplier' id = 'supplier' class='form-control1' autofocus readonly>
                          </td>
                        </tr>
						<tr>
                          <td>No Polisi</td>
                          <td>:</td>
                          <td>
						  <input type = 'text' value='<?php echo $row['no_polisi']; ?>' name = 'nopol' id = 'nopol' class='form-control1' readonly>									
                          </td>
                        </tr>
                      </table>
					</div>
				</div><!--/span-->
				
				<div class="box span6">
					<div class="box-header well" data-original-title>
						<h2>Detail Nota Timbang</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
					  <table>
                        <tr>
						  <td>Komoditi</td>
						  <td>:</td>
						  <td>
							<input type = 'text' value='<?php echo $row['item_name']; ?>' name = 'item' id = 'item' class='form-control1' autofocus readonly>									
							<input type = 'hidden' value='<?php echo $row['item_id']; ?>' name = 'item_id' id = 'item_id' class='form-control1' autofocus readonly>	
							<input type = 'hidden' value='<?php echo $row['id_jt']; ?>' name = 'id_jt' id = 'id_jt' class='form-control1' autofocus readonly>									
						  </td>
                        </tr>
                        <tr>
                          <td>No.Kontainer</td>
                          <td>:</td>
                          <td><input type = 'text' value='<?php echo $row['no_kontainer']; ?>' name = 'nokon' id = 'nokon' class='form-control1' readonly>
                          </td>
                        </tr>
                        <tr>
                          <td>Seal 1</td>
                          <td>:</td>
                          <td><input type = 'text' value='<?php echo $row['seal1']; ?>' name = 'seal1' id = 'seal1' class='form-control1' readonly>
                          </td>
                        </tr>
                        <tr>
                          <td>Seal 2</td>
                          <td>:</td>
                          <td><input type = 'text' value='<?php echo $row['seal2']; ?>' name = 'seal2' id = 'seal2' class='form-control1' readonly>
                          </td>
                        </tr>
						<tr>
                          <td>Catatan</td>
                          <td>:</td>
                          <td><input type = 'text' value='<?php echo $row['catatan']; ?>' name = 'catatan' id = 'catatan' class='form-control1' readonly>
						  <input type = 'hidden' name = 'id_timbang1' id = 'id_timbang1' class='form-control4' style = "width:50px;" value = "<?php echo $hasil;?>">
                          </td>
                        </tr>
                      </table>
					</div>
				</div><!--/span-->
			</div><!--/row-->
						
			<!-- content starts -->
			<div class="row-fluid sortable">
				<div class="box span5">
					<div class="box-header well" data-original-title>
						<h2>Input Data Timbang</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
					<div style="height:300px;">
						<form action = 'add_scale.php' method = 'post'>
							<table>
							<tr>
									<td width = '90px'>Scale</td>
									<td>:</td>
									<td>
										<div id="AutodataDisplay1">
									<?php
										include "form_tes.php";
									?>
										</div>									
									</td>
								</tr>
								<tr>
									<td>Kadar Air 1</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'ka1' id = 'ka1' class='form-control4' style = "width:60px;">									</td>
								</tr>
								<tr>
									<td>Kadar Air 2</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'ka2' id = 'ka2' class='form-control4' style = "width:60px;">									</td>
								</tr>
								<tr>
									<td>Kadar Air 3</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'ka3' id = 'ka3' class='form-control4' style = "width:60px;">									</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td>																				
										<button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
										<!-- <button class="btn btn-primary" onclick="add_scale();"><i class="fa fa-save"></i> Simpan</button> -->
										<button class="btn btn-danger" type = 'reset'><i class="fa fa-reset"></i> Reset</button>									</td>
								</tr>							
							</table>
				      <input type = 'hidden' name = 'id_timbang1' id = 'id_timbang1' class='form-control4' style = "width:50px;" value = "<?php echo $hasil;?>">
							<br/>
						</form>
						</div>
					</div>
				</div><!--/span-->
				
				<div class="box span7">
					<div class="box-header well" data-original-title>
						<h2>List Data Timbang</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<div style="height:300px;overflow:auto;">
							<table class="table table-striped">
								  <thead>
									  <tr>
										  <th>No</th>
										  <th>Berat</th>
										  <th>Kadar Air 1</th>                                          
										  <th>Kadar Air 2</th>                                          
										  <th>Kadar Air 3</th>                                          
										  <th>Rata-rata KA</th>                                          
										  <th>Aksi</th>                                          
									  </tr>
								  </thead>   
								  <tbody>
									<?php
										$no = 1;
										$totalbruto = 0;
										$sql1 = mysql_query("select berat,ka1,ka2,ka3, id_scale
												from ttr_scale 
												where id_timbang = '$hasil' 
												order by id_scale desc");
										while($row1 = mysql_fetch_array($sql1))
										{
											$rataka		= ($row1['ka1'] + $row1['ka2'] + $row1['ka3'])/3;
											$totalbruto	= $totalbruto + $row1['berat'];
										?>
											
											<tr>
												<td class='center'><?php echo $no ;?></td>
												<td class='center'><?php echo $row1[0] ;?></td>
												<td class='center'><?php echo $row1[1] ;?></td>
												<td class='center'><?php echo $row1[2] ;?></td>
												<td class='center'><?php echo $row1[3] ;?></td>
												<td class='center'><?php echo number_format($rataka,2) ;?></td>
												<td class='center'><?php echo "<a href='hapus_scale.php?id_scale=$row1[id_scale]&idt=$idt'>Hapus</a>" ;?></td>
											</tr>
										<?php
											$no++;
										}
									?>
								  </tbody>
							</table>  
						</div>   
					</div>
				</div><!--/span-->
			</div><!--/row-->
			
			<div class="row-fluid sortable">
				<div class="box span6">
					<div class="box-header well" data-original-title>
						<h2>Jenis Kemasan</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form action = 'add_kemas.php' method = 'post'>
						<input type = 'hidden' name = 'id_timbang1' id = 'id_timbang1' class='form-control4' style = "width:50px;" value = "<?php echo $hasil;?>">
							<table>
								<tr>
									<td width = '90'>Jenis Kemasan</td>
									<td>:</td>
									<td>
										 <select id="selectError2" data-rel="chosen" name="jeniskemasan" required>
											<option value="">--Pilih Kemasan--</option>
											<?php 	

											// $tampil=mysql_query("select * from tms_kemasan");
											$tampil=mysql_query("select * from items where category='Kemasan' ");
											$no=1;
											while ($row=mysql_fetch_array($tampil))
											{
											?>
											<option value="<?php echo $row['item_id']; ?>"><?php echo $row['name']; ?></option>
											<?php 
											$no++;
											} 
											?>
										  </select>								
									</td>
								</tr>
								<tr>
									<td>Jumlah bag </td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'jumlahkemasan' id = 'jumlahkemasan' class='form-control1' required>
									</td>
								</tr>
								<tr>
									<td>Berat/Bag</td>
									<td>:</td>
									<td>
										<input type = 'text' name = 'beratkemasan' id = 'beratkemasan' class='form-control1' required>
									</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td>																				
										<button class="btn btn-primary"><i class="fa fa-save"></i> Tambah Kemasan</button>
										<!-- <button class="btn btn-primary" onclick="add_scale();"><i class="fa fa-save"></i> Simpan</button> -->
										<button class="btn btn-danger" type = 'reset'><i class="fa fa-reset"></i> Reset</button>									</td>
								</tr>
							</table>
						
							<table class="table table-striped">
								  <thead>
									  <tr>
										  <th>No</th>
										  <th>Kemasan</th>
										  <th>Jumlah</th>
										  <th>Berat</th>                                          
										  <th>Aksi</th>                                          
									  </tr>
								  </thead>   
								  <tbody>
									<?php
										$no = 1;
										$totaltarra = 0;
										$jumlahkali = 0;
										$sql2 = mysql_query("select a.id_kemas, a.id_timbang, a.id_kemasan, a.jumlah_kemasan, a.total_berat, b.kemasan
												from ttr_kemasan a , tms_kemasan b
												where id_timbang = '$hasil' and a.id_kemasan=b.id_kemasan 
												order by id_kemas desc");
										while($row2 = mysql_fetch_array($sql2))
										{
											$jumlahkali	= $row2['jumlah_kemasan']*$row2['total_berat'];
											$totaltarra = $totaltarra+$jumlahkali;
										?>
											
											<tr>
												<td class='center'><?php echo $no ;?></td>
												<td class='center'><?php echo $row2['kemasan'] ;?></td>
												<td class='center'><?php echo $row2['jumlah_kemasan'] ;?></td>
												<td class='center'><?php echo $row2['total_berat'] ;?></td>
												<td class='center'><?php echo "<a href='hapus_kemasan.php?id_kemas=$row2[id_kemas]&idt=$idt'>Hapus</a>" ;?></td>
											</tr>
										<?php
											$no++;
										}
									?>
								  </tbody>
							</table>  
						</form>
					</div>
				</div><!--/span-->
				
				<div class="box span6">
					<div class="box-header well" data-original-title>
						<h2>Total Data Timbang</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped">
							  <thead>
								  <tr>
									  <th>Total Colly</th>
									  <th>Total Bruto</th>
									  <th>Total Tarra</th>
									  <th>Total Netto</th>
									  <th>Rata-rata KA</th>
								  </tr>
							  </thead>   
							  <tbody>
							  <?php
								$sqlhit1 = mysql_query("select count(ka1) as totka1, sum(ka1) as jumka1 from ttr_scale where id_timbang ='$hasil' and ka1!=''");
								$rowhit1 = mysql_fetch_array($sqlhit1);
								$totka1	 = $rowhit1['totka1'];
								$jumka1	 = $rowhit1['jumka1'];
								
								$sqlhit2 = mysql_query("select count(ka2) as totka2, sum(ka2) as jumka2 from ttr_scale where id_timbang ='$hasil' and ka2!=''");
								$rowhit2 = mysql_fetch_array($sqlhit2);
								$totka2	 = $rowhit2['totka2'];
								$jumka2	 = $rowhit2['jumka2'];
								
								$sqlhit3 = mysql_query("select count(ka3) as totka3, sum(ka3) as jumka3 from ttr_scale where id_timbang ='$hasil' and ka3!=''");
								$rowhit3 = mysql_fetch_array($sqlhit2);
								$totka3	 = $rowhit3['totka32'];
								$jumka3	 = $rowhit3['jumka3'];
								
								$netto 		= $totalbruto-$totaltarra;
								
								$jumkon		= $jumka1+$jumka2+$jumka3;
								$totkon		= $totka1+$totka2+$totka3;
								$ratarataka = $jumkon/$totkon;
							  ?>
								<tr>
									<td class="center"><?php echo $totalcolly; ?></td>
									<td class="center"><?php echo number_format($totalbruto,2); ?></td>
									<td class="center"><?php echo number_format($totaltarra,2); ?></td>
									<td class="center"><?php echo number_format($netto,2);?></td>
									<td class="center"><?php echo $ratarataka ;?></td>
								</tr>                                   
							  </tbody>
						 </table>
						 <form action = 'final_nota.php' method = 'post'>
						 <input type = 'hidden' name = 'id_timbang1' id = 'id_timbang1' class='form-control4' style = "width:50px;" value = "<?php echo $hasil;?>">
						 <input type = 'hidden' name = 'totalbruto' id = 'totalbruto' class='form-control4' style = "width:50px;" value = "<?php echo $totalbruto;?>">
						 <input type = 'hidden' name = 'totaltarra' id = 'totaltarra' class='form-control4' style = "width:50px;" value = "<?php echo $totaltarra;?>">
						 <input type = 'hidden' name = 'netto' id = 'netto' class='form-control4' style = "width:50px;" value = "<?php echo $netto;?>">
						 <input type = 'hidden' name = 'ratarataka' id = 'ratarataka' class='form-control4' style = "width:50px;" value = "<?php echo $ratarataka;?>">
						 <button class="btn btn-primary"><i class="fa fa-save"></i> Selesaikan Nota Timbang</button>
						 </form>
					</div>
				</div><!--/span-->
			</div><!--/row-->    
			<!-- content ends -->
			</div><!--/#content.span10-->
			</div><!--/fluid-row-->
			
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">�</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<footer>
			<p class="pull-left">&copy; <a href="http://usman.it" target="_blank">Agro Cita Mandiri</a> 2017</p>
		</footer>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script src="js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script src="js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script src="js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="js/jquery.elfinder.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>
	
		
</body>
</html>
